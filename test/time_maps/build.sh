#!/bin/bash

if [ ! -d ./bin ] ; then
	mkdir ./bin
fi

INCLUDES="-I../../src -I../../MilePost/src -I../../MilePost/src/util -I../../MilePost/src/platform"
FLAGS="-g -DDEBUG -mmacos-version-min=10.15.4"
LIBS="-framework Carbon -framework Cocoa -framework Metal -framework QuartzCore -L../../MilePost/bin -lmilepost"

clang++ $FLAGS -o ./bin/print_curve $INCLUDES $LIBS main.cpp
