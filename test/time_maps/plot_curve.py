#!/usr/bin/env python3

import sys
import re
import os
import numpy as np
import csv
from matplotlib import pyplot as plt

#TODO parse arguments etc


if os.system('./build.sh') != 0:
	exit(1);

os.system('./bin/print_curve > curve.tmp')

if len(sys.argv) < 2 :
    print("usage: \tplot_curve.py outputFile")
    exit(0)

inFileName = "curve.tmp"
outFileName = sys.argv[1]


# read the input file
contents_lines = []

csvFile = open(inFileName)
csvReader = csv.reader(csvFile, delimiter = ';')
contents = list(csvReader)

columns = list(map(list, zip(*contents)))

# make arrays
# time from pos

wall_clock = np.asarray(columns[0], dtype=np.float)
input_time = np.asarray(columns[1], dtype=np.float)
output_pos = np.asarray(columns[2], dtype=np.float)

inferred_tempo = (output_pos[1:] - output_pos[:len(output_pos)-1])/(input_time[1:] - input_time[:len(input_time)-1])

# output out position
for f in output_pos:
	print(f)

# plot
height = np.max(input_time)

fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=[12, 8])

ax1.plot(input_time[:len(input_time)-1], inferred_tempo, color = 'red')
ax1.set_xlabel('time')
ax1.set_ylabel('tempo', color = 'red')
ax1.set_title('tempo')

ax2.plot(wall_clock, input_time, color = 'blue')
ax2.set_xlabel('wall clock')
ax2.set_ylabel('position')
ax2.set_title('input time map')

ax3.plot(wall_clock, output_pos, color = 'blue')
ax3.set_xlabel('wall clock')
ax3.set_ylabel('position')
ax3.set_title('output time map')

plt.tight_layout()
plt.savefig(outFileName + '.eps')
plt.show()
