/************************************************************//**
*
*	@file: main.cpp
*	@author: Martin Fouilleul
*	@date: 05/10/2020
*	@revision:
*
*****************************************************************/
#include<stdio.h>
#include"typedefs.h"

#include"memory.h"
#include"platform_fibers.h"
#include"platform_thread.h"
#include"platform_clock.h"
#include"sched_curves_internal.h"
#include"scheduler.cpp"
#include"sched_curves.cpp"
#include"x64_sysv_fibers.cpp"

int main(int argc, char** argv)
{
	bool fileInput = false;
	const char* fileName = 0;
	if(argc>1)
	{
		if(!strcmp(argv[1], "-i"))
		{
			if(argc <= 2)
			{
				printf("-i option needs an input file argument\n");
				return(-1);
			}
			fileInput = true;
			fileName = argv[2];
		}
	}

	float* input = 0;
	u32 inputCount = 0;

	if(fileInput)
	{
		FILE* inputFile = fopen(fileName, "r");
		fseek(inputFile, 0, SEEK_END);
		size_t size = ftell(inputFile);
		rewind(inputFile);

		input = (float*)malloc(size*sizeof(float));
		while(1)
		{
			float f;
			if(fscanf(inputFile, "%f\n", &f) < 1)
			{
				break;
			}
			input[inputCount] = f;
			inputCount++;
		}
	}
	else
	{
		inputCount = 400;
		input = (float*)malloc(inputCount*sizeof(float));
		for(int i=0; i<inputCount; i++)
		{
			input[i] = i*0.1;
		}
	}

	//TODO(martin): read curve descriptor from input.
	//              for now read built-in curve

/*
	const int eltCount = 2;
	sched_curve_descriptor_elt elements[eltCount] = {
		{.type = SCHED_CURVE_BEZIER, .startValue = 1, .endValue = 2, .length = 20, .p1x = 0.5, .p1y = 1, .p2x = 0.5, .p2y = 2},
		{.type = SCHED_CURVE_BEZIER, .startValue = 2, .endValue = 1, .length = 20, .p1x = 0.5, .p1y = 2, .p2x = 0.5, .p2y = 1}};

	sched_curve_descriptor desc = {.axes = SCHED_CURVE_TIME_TEMPO, .eltCount = eltCount, .elements = elements};
/*/
	const int eltCount = 1;
	sched_curve_descriptor_elt elements[eltCount] = {
		{.type = SCHED_CURVE_BEZIER,
		 .startValue = 0,
		 .endValue = 20,
		 .length = 20,
		 .p1x = 0.5,
		 .p1y = 5,
		 .p2x = 0.5,
		 .p2y = 15}};

	sched_curve_descriptor desc = {.axes = SCHED_CURVE_TIME_POS, .eltCount = eltCount, .elements = elements, .startPoint = 5, .transformedStartPoint = 5};
//*/
	sched_curve* curve = sched_curve_create(&desc);

	f64 time = 0;
	f64 pos = 0;

	for(int i=0; i<inputCount; i++)
	{
		f64 newPos = 0;
		f64 newTime = input[i];
		sched_curve_get_position_from_time(curve, newTime, &newPos);

		f64 timeUpdate = newTime - time;
		f64 posUpdate = newPos - pos;

		ASSERT(posUpdate >= -0.1 && timeUpdate >= -0.1);

		pos = newPos;
		time = newTime;

		printf("%.12f ; %.12f ; %.12f\n", i*0.1, time, pos);
	}

	sched_curve_destroy(curve);
	free(input);
	return(0);
}
