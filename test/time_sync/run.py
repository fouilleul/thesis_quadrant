#!/usr/bin/env python3

import os
import csv
import numpy as np
from matplotlib import pyplot as plt

os.system('../../bin/quadrant --headless --offline ./test_sync.ql > out.csv')

contents_lines = []
csvFile = open('out.csv')
csvReader = csv.reader(csvFile, delimiter = ';')
contents = list(csvReader)

leader = list(filter(lambda line: line[0] == 'L', contents))
leader_pos = np.asarray([row[1] for row in leader], dtype=np.float)
leader_time = np.asarray([row[2] for row in leader], dtype=np.float)

follower = list(filter(lambda line: line[0] == 'F', contents))
follower_pos = np.asarray([row[1] for row in follower], dtype=np.float)
follower_time = np.asarray([row[2] for row in follower], dtype=np.float)

# plot

plt.plot(leader_time, leader_pos, color='red', label='leader')
plt.plot(follower_time, follower_pos, color='green', label='follower')
plt.xlabel('wall clock')
plt.ylabel('position')
plt.legend()
plt.show()
