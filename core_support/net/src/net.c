/************************************************************//**
*
*	@file: net.cpp
*	@author: Martin Fouilleul
*	@date: 28/03/2022
*	@revision:
*
*****************************************************************/
#include<sys/socket.h>	// socket()
#include<netinet/ip.h>	// socaddr_in
#include<arpa/inet.h>	// inet_addr()
#include<ifaddrs.h>	// getifaddrs() / freeifaddrs()
#include<unistd.h>	// close()

#include"typedefs.h"

typedef struct in_addr in_addr;
typedef struct sockaddr_in sockaddr_in;
typedef struct sockaddr sockaddr;
typedef struct ip_mreq ip_mreq;

typedef u32 net_socket_type;
const net_socket_type NET_SOCK_UDP = 0,
                      NET_SOCK_TCP = 1;
typedef u64 net_socket;

net_socket net_socket_open(net_socket_type type)
{
	int sock = 0;
	if(type == NET_SOCK_UDP)
	{
		sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	}
	else
	{
		sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	}
	return(sock);
}

void net_socket_close(net_socket socket)
{
	close((int)socket);
}

u32 host_to_net_ip(u32 ip)
{
	return(htonl(ip));
}
u16 host_to_net_port(u16 port)
{
	return(htons(port));
}

u32 net_to_host_ip(u32 ip)
{
	return(ntohl(ip));
}
u16 net_to_host_port(u16 port)
{
	return(ntohs(port));
}


i32 net_socket_bind(net_socket sock, u32 ip, u16 port)
{
	sockaddr_in saddr;
	saddr.sin_addr.s_addr = host_to_net_ip(ip);
	saddr.sin_port = host_to_net_port(port);
	saddr.sin_family = AF_INET;

	return(bind(sock, (sockaddr*)&saddr, sizeof(saddr)));
}

i32 net_socket_send(net_socket sock, u32 ip, u16 port, u64 len, u8* data)
{
	sockaddr_in saddr;
	saddr.sin_addr.s_addr = host_to_net_ip(ip);
	saddr.sin_port = host_to_net_port(port);
	saddr.sin_family = AF_INET;

	return(sendto(sock, data, len, 0, (sockaddr*)&saddr, sizeof(saddr)));
}

i32 net_socket_receive(net_socket sock, u32* ip, u16* port, u64 len, u8* buffer)
{
	sockaddr_in saddr;
	socklen_t saddrSize = sizeof(saddr);

	int res = recvfrom(sock, buffer, len, 0, (sockaddr*)&saddr, &saddrSize);

	*ip = net_to_host_ip(saddr.sin_addr.s_addr);
	*port = net_to_host_port(saddr.sin_port);

	return(res);
}
