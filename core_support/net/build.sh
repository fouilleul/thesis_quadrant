#!/bin/bash

SRC="./src/net.c"
INCLUDES="-I../../src -I../../MilePost/src/util"
FLAGS="-g -undefined dynamic_lookup"
PRODUCT="libnet.dylib"

clang -shared $FLAGS $INCLUDES -o ./bin/$PRODUCT $SRC
cp bin/$PRODUCT ../../core
