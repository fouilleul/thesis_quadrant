#!/bin/bash

SRC="./src/fmt.c"
INCLUDES="-I../../src -I../../MilePost/src/util"
FLAGS="-g -undefined dynamic_lookup"
PRODUCT="libfmt.dylib"

clang -shared $FLAGS $INCLUDES -o ./bin/$PRODUCT $SRC
cp bin/$PRODUCT ../../core
