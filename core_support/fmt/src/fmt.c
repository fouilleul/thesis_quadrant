/************************************************************//**
*
*	@file: test.cpp
*	@author: Martin Fouilleul
*	@date: 24/03/2022
*	@revision:
*
*****************************************************************/
#include<stdio.h>
#include"strings.h"
#include"utf8.h"
#include"vm.h"
#include"bytecode.h"

/*NOTE(martin): formatters syntax

	*** NOTE: this is a whishlist, not all of it is implemented right now ***

	A single '%' indicates an argument. It is followed, in order, by:

	- An optional width consisting of an integer
	- An optional precision consisting of a dot followed by an integer
	- An format specifier which can be one of the following:
		For all arguments:
			*	Uses a default format

		For boolean and integer arguments:
			t	truth value, printed as "true" or "false"

		For integer arguments:
			b	base 2
			c	utf32 codepoint
			o	base 8
			d	base 10
			i	base 10
			x	base 16

		For floating-point arguments:
			e	scientific notation
			f	decimal point with no exponent

		For slices of u8:
			s	the slice data interpreted as a utf8 encoded string

		For all slices and pointers
			p	the address of the first element in hexadecimal notation

	The default format for slices is to print each elements with its default format, like:
		[element1, element2, element3, ...]

	The format for structs is to print each field with its default format, like:
		{name1: element1, name2: field2, ...}

	'%%' is printed as a single '%'.
*/

typedef enum {
	FMT_BOOL,
	FMT_CHAR_UTF8,
	FMT_INT,
	FMT_FLOAT_SCIENTIFIC,
	FMT_FLOAT_DECIMAL,
	FMT_STRING_UTF8,
	FMT_POINTER,
	FMT_ARRAY,
	FMT_STRUCT,
	FMT_ANY,

	FMT_ERROR,
} fmt_kind;

//TODO: flags (eg leading zeros, leading sign, indented layout, etc)

typedef struct fmt_format
{
	fmt_kind kind;
	u32 width;
	u32 precision;
	u8 base;

} fmt_format;

const u32 FMT_DEFAULT_PRECISION = 6;

fmt_format fmt_default_for_type(bc_type* type)
{
	fmt_format format = {0};
	format.width = 0;
	format.precision = FMT_DEFAULT_PRECISION;

	switch(type->kind)
	{
		case BC_TYPE_POINTER:
			format.kind = FMT_POINTER;
			format.width = 16;
			//TODO: leading zeros
			break;

		case BC_TYPE_PRIMITIVE:
		{
			switch(type->primitive)
			{
				case BC_TYPE_B8:
					format.kind = FMT_BOOL;
					break;
				case BC_TYPE_U8:
					format.kind = FMT_CHAR_UTF8;
					break;
				case BC_TYPE_I8:
				case BC_TYPE_U16:
				case BC_TYPE_I16:
				case BC_TYPE_U32:
				case BC_TYPE_I32:
				case BC_TYPE_U64:
				case BC_TYPE_I64:
					format.kind = FMT_INT;
					format.base = 10;
					break;
				case BC_TYPE_F32:
				case BC_TYPE_F64:
					format.kind = FMT_FLOAT_DECIMAL;
					break;
			}
		} break;
		case BC_TYPE_ARRAY:
		case BC_TYPE_SLICE:
		{
			bc_type* eltType = (bc_type*)bc_get_relptr(type->eltType);
			if(eltType->kind == BC_TYPE_PRIMITIVE && eltType->primitive == BC_TYPE_U8)
			{
				format.kind = FMT_STRING_UTF8;
			}
			else
			{
				format.kind = FMT_ARRAY;
			}
		} break;

		case BC_TYPE_STRUCT:
			format.kind = FMT_STRUCT;
			break;

		case BC_TYPE_ANY:
			format.kind = FMT_ANY;
			break;

		case BC_TYPE_FUTURE:
		case BC_TYPE_VOID:
		case BC_TYPE_NAMED:
		case BC_TYPE_PROC:
		case BC_TYPE_ERROR:
			format.kind = FMT_ERROR;
			break;
	}
	return(format);
}

mp_string fmt_arg_bool(fmt_format* format, bc_type* type, u8* data)
{
	if(type->kind == BC_TYPE_PRIMITIVE
	  && type->primitive == BC_TYPE_B8)
	{
		u8 value = *(u8*)data;
		mp_string res = value ? mp_string_lit("true") : mp_string_lit("false");
		return(res);
	}
	else
	{
		return(mp_string_lit("%(FORMAT ERROR: expected boolean)"));
	}
}

mp_string fmt_arg_int(fmt_format* format, bc_type* type, u8* data)
{
	if(type->kind == BC_TYPE_PRIMITIVE)
	{
		u64 value = 0;
		switch(type->primitive)
		{
			case BC_TYPE_U8:
				value = *(u8*)data;
				break;
			case BC_TYPE_I8:
				value = *(i8*)data;
				break;
			case BC_TYPE_U16:
				value = *(u16*)data;
				break;
			case BC_TYPE_I16:
				value = *(i16*)data;
				break;
			case BC_TYPE_U32:
				value = *(u32*)data;
				break;
			case BC_TYPE_I32:
				value = *(i32*)data;
				break;
			case BC_TYPE_U64:
				value = *(u64*)data;
				break;
			case BC_TYPE_I64:
				value = *(i64*)data;
				break;

			default:
				return(mp_string_lit("%(FORMAT ERROR: expected integer type)"));
		}

		//NOTE: estimate number of characters and allocate memory
		u64 maxChars = 0;
		u64 maxPower = 1;
		while(maxPower <= value)
		{
			maxChars++;
			maxPower *= format->base;
		}
		maxChars = maximum(maxChars, maximum(1, format->width));
		char* buffer = mem_arena_alloc_array(mem_scratch(), char, maxChars);

		//NOTE(martin): format number to buffer
		static const char digitChars[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		                                'a', 'b', 'c', 'd', 'e', 'f'};

		i64 position = maxChars - 1;
		do
		{
			u64 digit = value % format->base;
			buffer[position] = digitChars[digit];

			value /= format->base;
			position--;
		} while(value);

		while(position > 0)
		{
			buffer[position] = ' ';
			position--;
		}

		mp_string result = mp_string_from_buffer(maxChars, buffer);
		return(result);
	}
	else
	{
		return(mp_string_lit("%(FORMAT ERROR: expected integer type)"));
	}
}

mp_string fmt_arg_char(fmt_format* format, bc_type* type, u8* data)
{
	if(type->kind == BC_TYPE_PRIMITIVE)
	{
		u64 value = 0;
		switch(type->primitive)
		{
			case BC_TYPE_U8:
				value = *(u8*)data;
				break;
			case BC_TYPE_I8:
				value = *(i8*)data;
				break;
			case BC_TYPE_U16:
				value = *(u16*)data;
				break;
			case BC_TYPE_I16:
				value = *(i16*)data;
				break;
			case BC_TYPE_U32:
				value = *(u32*)data;
				break;
			case BC_TYPE_I32:
				value = *(i32*)data;
				break;
			case BC_TYPE_U64:
				value = *(u64*)data;
				break;
			case BC_TYPE_I64:
				value = *(i64*)data;
				break;

			default:
				return(mp_string_lit("%(FORMAT ERROR: expected integer type)"));
		}
		char* buffer = mem_arena_alloc_array(mem_scratch(), char, 8);
		mp_string res = utf8_encode(buffer, value);
		return(res);
	}
	else
	{
		return(mp_string_lit("%(FORMAT ERROR: expected integer type)"));
	}
}

mp_string fmt_arg_float_decimal(fmt_format* format, bc_type* type, u8* data)
{
	mp_string res = {0};
	if(type->kind == BC_TYPE_PRIMITIVE)
	{
		//NOTE(martin): we piggyback snprintf() here
		mem_arena* scratch = mem_scratch();

		char* fmtString = 0;
		u32 fmtLen = snprintf(fmtString, 0, "%%%u.%uf", format->width, format->precision);
		fmtString = mem_arena_alloc_array(scratch, char, fmtLen+1);
		fmtLen = snprintf(fmtString, fmtLen+1, "%%%u.%uf", format->width, format->precision);

		if(type->primitive == BC_TYPE_F32)
		{
			f32 value = *(f32*)data;
			res = mp_string_pushf(scratch, fmtString, value);
		}
		else if(type->primitive == BC_TYPE_F64)
		{
			f64 value = *(f64*)data;
			res = mp_string_pushf(scratch, fmtString, value);
		}
		else
		{
			res = mp_string_lit("%(FORMAT ERROR: expected floating-point type)");
		}
	}
	else
	{
		res = mp_string_lit("%(FORMAT ERROR: expected floating-point type)");
	}
	return(res);
}

mp_string fmt_arg_string(fmt_format* format, bc_type* type, u8* data)
{
	mp_string res = {0};

	if(type->kind == BC_TYPE_SLICE || type->kind == BC_TYPE_ARRAY)
	{
		bc_type* eltType = (bc_type*)bc_get_relptr(type->eltType);
		if(eltType->kind == BC_TYPE_PRIMITIVE && eltType->primitive == BC_TYPE_U8)
		{
			if(type->kind == BC_TYPE_SLICE)
			{
				u8* memory = qvm_get_memory();

				rt_slice* slice = (rt_slice*)data;
				res.len = slice->len;
				res.ptr = (char*)(memory + slice->offset);
			}
			else
			{
				res.len = type->eltCount;
				res.ptr = (char*)data;
			}
		}
		else
		{
			res = mp_string_lit("%(FORMAT ERROR: expected string)");
		}
	}
	else
	{
		res = mp_string_lit("%(FORMAT ERROR: expected string)");
	}
	return(res);
}

mp_string fmt_arg_pointer(fmt_format* format, bc_type* type, u8* data)
{
	mp_string res = {0};
	mem_arena* scratch = mem_scratch();

	if(type->kind == BC_TYPE_SLICE)
	{
		rt_slice* slice = (rt_slice*)data;
		res = mp_string_pushf(scratch, "0x%.16llx", slice->offset);
	}
	else if(type->kind == BC_TYPE_ARRAY)
	{
		u8* memory = qvm_get_memory();
		u64 offset = (u64)(data - memory);
		res = mp_string_pushf(scratch, "0x%.16llx", offset);
	}
	else if(type->kind == BC_TYPE_POINTER)
	{
		u64 offset = *(u64*)data;
		res = mp_string_pushf(scratch, "0x%.16llx", offset);
	}
	else
	{
		res = mp_string_lit("%(FORMAT ERROR: expected slice, array, or pointer type)");
	}
	return(res);
}

mp_string fmt_arg_data(fmt_format* format, bc_type* type, u8* data);

mp_string fmt_arg_array(fmt_format* format, bc_type* type, u8* data)
{
	//NOTE(martin): extract element count and array pointer
	u64 count = 0;
	u8* array = 0;

	if(type->kind == BC_TYPE_ARRAY)
	{
		count = type->eltCount;
		array = data;
	}
	else if(type->kind == BC_TYPE_SLICE)
	{
		u8* memory = qvm_get_memory();
		rt_slice* slice = (rt_slice*)data;
		count = slice->len;
		array = memory + slice->offset;
	}
	else
	{
		return(mp_string_lit("%(FORMAT ERROR: expected slice or array type)"));
	}

	//NOTE(martin): determine format of elements
	bc_type* eltType = (bc_type*)bc_get_relptr(type->eltType);
	fmt_format eltFormat = fmt_default_for_type(eltType);
	u64 stride = eltType->size;

	//NOTE(martin): format each element with delimiters
	mp_string leftMark = mp_string_lit("[");
	mp_string rightMark = mp_string_lit("]");
	mp_string sepString = mp_string_lit(", ");

	mem_arena* scratch = mem_scratch();
	mp_string_list stringList = {0};
	mp_string_list_push(scratch, &stringList, leftMark);

	u64 eltOffset = 0;
	for(u64 eltIndex = 0; eltIndex<count; eltIndex++, eltOffset += stride)
	{
		u8* elt = array + eltOffset;
		mp_string eltString = fmt_arg_data(&eltFormat, eltType, elt);

		mp_string_list_push(scratch, &stringList, eltString);
		if(eltIndex+1 < count)
		{
			mp_string_list_push(scratch, &stringList, sepString);
		}
	}
	mp_string_list_push(scratch, &stringList, rightMark);

	mp_string res = mp_string_list_join(scratch, stringList);
	return(res);
}

mp_string fmt_arg_struct(fmt_format* format, bc_type* type, u8* data)
{
	if(type->kind != BC_TYPE_STRUCT)
	{
		return(mp_string_lit("%(FORMAT ERROR: expected slice or array type)"));
	}

	mp_string fieldSep = mp_string_lit(", ");
	mp_string leftMark = mp_string_lit("{");
	mp_string rightMark = mp_string_lit("}");

	mem_arena* scratch = mem_scratch();
	mp_string_list stringList = {0};
	mp_string_list_push(scratch, &stringList, leftMark);

	bc_type_field* fieldArray = (bc_type_field*)bc_get_relptr(type->fields.ptr);
	for(u32 fieldIndex = 0; fieldIndex < type->fields.len; fieldIndex++)
	{
		bc_type_field* field = &(fieldArray[fieldIndex]);
		mp_string name = {0};
		name.len = field->name.len;
		name.ptr = (char*)bc_get_relptr(field->name.ptr);

		u8* fieldData = data + field->offset;
		bc_type* fieldType = (bc_type*)bc_get_relptr(field->type);
		fmt_format fieldFormat = fmt_default_for_type(fieldType);
		mp_string valueString = fmt_arg_data(&fieldFormat, fieldType, fieldData);

		mp_string_list_push(scratch, &stringList, name);
		mp_string_list_push(scratch, &stringList, mp_string_lit(": "));
		mp_string_list_push(scratch, &stringList, valueString);

		if(fieldIndex+1 < type->fields.len)
		{
			mp_string_list_push(scratch, &stringList, fieldSep);
		}
	}
	mp_string_list_push(scratch, &stringList, rightMark);

	mp_string res = mp_string_list_join(scratch, stringList);
	return(res);
}

mp_string fmt_arg_data(fmt_format* format, bc_type* type, u8* data)
{
	mp_string res = {0};
	switch(format->kind)
	{
		case FMT_BOOL:
			res = fmt_arg_bool(format, type, data);
			break;
		case FMT_INT:
			res = fmt_arg_int(format, type, data);
			break;
		case FMT_CHAR_UTF8:
			res = fmt_arg_char(format, type, data);
			break;
		case FMT_FLOAT_SCIENTIFIC:
			//TODO
			break;
		case FMT_FLOAT_DECIMAL:
			res = fmt_arg_float_decimal(format, type, data);
			break;
		case FMT_STRING_UTF8:
			res = fmt_arg_string(format, type, data);
			break;
		case FMT_POINTER:
			res = fmt_arg_pointer(format, type, data);
			break;
		case FMT_ARRAY:
			res = fmt_arg_array(format, type, data);
			break;
		case FMT_STRUCT:
			res = fmt_arg_struct(format, type, data);
			break;

		case FMT_ANY:
		{
			u8* memory = qvm_get_memory();
			rt_any* any = (rt_any*)data;
			bc_type* type = (bc_type*)(memory + any->typeOffset);
			u8* data = memory + any->valueOffset;

			fmt_format valFormat = fmt_default_for_type(type);
			res = fmt_arg_data(&valFormat, type, data);
		} break;

		case FMT_ERROR:
			res = mp_string_lit("%(FORMAT ERROR: unsupported argument type)");
			break;

	}
	return(res);
}

void fmt_printf(u64 fmtLen, char* fmt, u64 argc, rt_any* argv)
{
	mem_arena* scratch = mem_scratch();
	u8* memory = qvm_get_memory();

	u64 argIndex = 0;
	mp_string_list stringList = {0};
	mp_string chunk = {.len = 0, .ptr = fmt};

	for(u64 i = 0; i<fmtLen; i++)
	{
		if(fmt[i] == '%')
		{
			//NOTE(martin): flush literal chunk and prepare for parsing formatter
			mp_string_list_push(scratch, &stringList, chunk);
			mp_string argString = {0};
			i++;

			//NOTE(martin): select default format
			fmt_format argFormat = {0};
			if(argIndex < argc)
			{
				rt_any* any = &argv[argIndex];
				bc_type* type = (bc_type*)(memory + any->typeOffset);
				argFormat = fmt_default_for_type(type);
			}

			//NOTE(martin): parse width
			if(i<fmtLen && fmt[i] >= '0' && fmt[i] <= '9')
			{
				argFormat.width = 0;
				while(i<fmtLen && fmt[i] >= '0' && fmt[i] <= '9')
				{
					argFormat.width *= 10;
					argFormat.width += (fmt[i] - '0');
					i++;
				}
			}

			//NOTE(martin): parse precision
			if(i<fmtLen && fmt[i] == '.')
			{
				argFormat.precision = 0;
				i++;
				while(i<fmtLen && fmt[i] >= '0' && fmt[i] <= '9')
				{
					argFormat.precision *= 10;
					argFormat.precision += (fmt[i] - '0');
					i++;
				}
			}

			//NOTE(martin): parse format kind
			if(i<fmtLen)
			{
				switch(fmt[i])
				{
					//NOTE: use default format
					case '*':
						break;

					//NOTE: For boolean and integer arguments:
					case 't':
						argFormat.kind = FMT_BOOL;
						break;
					//NOTE: For integer arguments:
					case 'b':
						argFormat.kind = FMT_INT;
						argFormat.base = 2;
						break;
					case 'c':
						argFormat.kind = FMT_CHAR_UTF8;
						break;
					case 'o':
						argFormat.kind = FMT_INT;
						argFormat.base = 8;
						break;
					case 'd':
					case 'i':
						argFormat.kind = FMT_INT;
						argFormat.base = 10;
						break;
					case 'x':
						argFormat.kind = FMT_INT;
						argFormat.base = 16;
						break;

					//NOTE: For floating-point arguments:
					case 'e':
						argFormat.kind = FMT_FLOAT_SCIENTIFIC;
						break;
					case 'f':
						argFormat.kind = FMT_FLOAT_DECIMAL;
						break;

					//NOTE: For slices of u8:
					case 's':
						argFormat.kind = FMT_STRING_UTF8;
						break;

					//NOTE: For all slices and pointers
					case 'p':	// the address of the first element in hexadecimal notation
						argFormat.kind = FMT_POINTER;
						break;

					default:
						argString = mp_string_lit("%(FORMAT ERROR: unrecognized format tag)");
						break;
				}
			}
			else
			{
				argString = mp_string_lit("%(FORMAT ERROR: incomplete format tag)");
			}

			if(!argString.len)
			{
				if(argIndex >= argc)
				{
					argString = mp_string_lit("%(FORMAT ERROR: incomplete format tag)");
				}
				else
				{
					//NOTE(martin): format and flush argument
					rt_any* any = &argv[argIndex];
					bc_type* type = (bc_type*)(memory + any->typeOffset);
					u8* data = memory + any->valueOffset;

					argString = fmt_arg_data(&argFormat, type, data);
					argIndex++;
				}
			}
			mp_string_list_push(scratch, &stringList, argString);

			//NOTE(martin): start next literal chunk
			chunk.ptr = &(fmt[i+1]);
			chunk.len = 0;
		}
		else
		{
			chunk.len++;
		}
	}
	//NOTE(martin): flush last literal chunk
	mp_string_list_push(scratch, &stringList, chunk);

	if(argIndex < argc)
	{
		mp_string error = mp_string_lit("%(FORMAT ERROR: not enough format specifiers)");
		mp_string_list_push(scratch, &stringList, error);
	}

	mp_string_list_push(scratch, &stringList, mp_string_lit("\n"));

	//NOTE(martin): join all strings
	mp_string result = mp_string_list_join(scratch, stringList);

	qvm_puts(result);
}
