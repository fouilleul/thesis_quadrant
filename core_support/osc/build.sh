#!/bin/bash

SRC="./src/binding.c"
INCLUDES="-I../../src -I../../MilePost/src/util"
FLAGS="-g -undefined dynamic_lookup"
PRODUCT="libosc.dylib"

clang -shared $FLAGS $INCLUDES -o ./bin/$PRODUCT $SRC
cp bin/$PRODUCT ../../core
