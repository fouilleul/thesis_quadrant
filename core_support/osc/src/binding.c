/************************************************************//**
*
*	@file: binding.cpp
*	@author: Martin Fouilleul
*	@date: 01/04/2022
*	@revision:
*
*****************************************************************/
#include"osc.c"
#include"vm.h"
#include"bytecode.h"

u64 osc_msg_format(u64 buffLen, char* buffer, u64 addressLen, char* address, u64 argc, rt_any* argv)
{
	osc_element osc;
	OscBeginPacket(&osc, buffLen, buffer);
	OscBeginMessage(&osc, addressLen, address);

	u8* memory = qvm_get_memory();

	for(u64 argIndex = 0; argIndex < argc; argIndex++)
	{
		rt_any* any = &argv[argIndex];
		bc_type* type = (bc_type*)(memory + any->typeOffset);
		u8* data = memory + any->valueOffset;

		switch(type->kind)
		{
			case BC_TYPE_PRIMITIVE:
			{
				switch(type->primitive)
				{
					case BC_TYPE_B8:
						OscPushBool(&osc, *data != 0);
						break;
					case BC_TYPE_U8:
					case BC_TYPE_I8:
						OscPushChar(&osc, *(char*)data);
						break;

					case BC_TYPE_U16:
					case BC_TYPE_I16:
						OscPushInt16(&osc, *(u16*)data);
						break;

					case BC_TYPE_U32:
					case BC_TYPE_I32:
						OscPushInt32(&osc, *(u32*)data);
						break;

					case BC_TYPE_U64:
					case BC_TYPE_I64:
						//OscPushInt64(&osc, *(u64*)data);
						OscPushInt32(&osc, (u32)*(u64*)data);
						break;

					case BC_TYPE_F32:
						OscPushFloat(&osc, *(f32*)data);
						break;

					case BC_TYPE_F64:
						//OscPushDouble(&osc, *(f64*)data);
						OscPushFloat(&osc, (f32)*(f64*)data);
						break;
				}
			} break;

			case BC_TYPE_ARRAY:
			case BC_TYPE_SLICE:
			{
				bc_type* eltType = (bc_type*)bc_get_relptr(type->eltType);
				if(eltType->kind == BC_TYPE_PRIMITIVE && eltType->primitive == BC_TYPE_U8)
				{
					u64 len = 0;
					char* ptr = 0;
					if(type->kind == BC_TYPE_ARRAY)
					{
						len = type->eltCount;
						ptr = (char*)data;
					}
					else
					{
						rt_slice* slice = (rt_slice*)data;
						len = slice->len;
						ptr = (char*)(memory + slice->offset);
					}
					OscPushString(&osc, len, ptr);
				}
				else
				{
					//TODO
				}
			} break;

			case BC_TYPE_STRUCT:
			case BC_TYPE_ANY:
			case BC_TYPE_POINTER:
			case BC_TYPE_FUTURE:
			case BC_TYPE_VOID:
			case BC_TYPE_NAMED:
			case BC_TYPE_PROC:
			case BC_TYPE_ERROR:
				//TODO ERROR
				break;
		}
	}

	OscEndMessage(&osc);
	OscEndPacket(&osc);

	OscElementPrintIndented(&osc, 0);

	return(OscElementSize(&osc));
}


void osc_get_address(osc_msg* msg, rt_slice* outAddress)
{
	const char* address = OscAddressPattern(msg);
	u64 len = strlen(address);

	u8* memory = qvm_get_memory();

	outAddress->len = len;
	outAddress->offset = (u64)((u8*)address - memory);
}

void osc_get_arg_data_as_slice(osc_arg_iterator* arg, rt_slice* outSlice)
{
	u8* memory = qvm_get_memory();

	outSlice->len = OscGetArgSize(*arg->tag, arg->data);
	outSlice->offset = (u64)((u8*)arg->data - memory);
}

void osc_init_arg(osc_msg* msg, osc_arg_iterator* arg)
{
	*arg = OscArgumentsBegin(msg);
}
