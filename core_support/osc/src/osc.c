/************************************************************//**
*
*	@file: osc.c
*	@author: Martin Fouilleul
*	@date: 10/04/2019
*	@revision:
*
*****************************************************************/
#include<stdlib.h>
#include"debug_log.h"
#include"memory.h"

#define LOG_SUBSYSTEM "OSC"

#include<string.h>
#include<stdarg.h>
#include"typedefs.h"
#include"debug_log.h"
#include"macro_helpers.h"

#ifdef __cplusplus
extern "C" {
#endif	// __cplusplus


//---------------------------------------------------------------
// Endianness
//---------------------------------------------------------------

#if defined(OSC_HOST_LITTLE_ENDIAN) || defined(OSC_HOST_BIG_ENDIAN)
#elif defined(__WIN32__) || defined(WIN32) || defined(WINCE)
	#define OSC_HOST_LITTLE_ENDIAN 1
	#undef OSC_HOST_BIG_ENDIAN
#elif defined(__APPLE__)
	#if defined(__LITTLE_ENDIAN__)
		#define OSC_HOST_LITTLE_ENDIAN 1
		#undef OSC_HOST_BIG_ENDIAN
	#elif defined(__BIG_ENDIAN__)
		#define OSC_HOST_BIG_ENDIAN 1
		#undef OSC_HOST_LITTLE_ENDIAN
	#endif
#elif defined(__BYTE_ORDER__) && defined(__ORDER_LITTLE_ENDIAN__) && defined(__ORDER_BIG_ENDIAN__)
	#if (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
		#define OSC_HOST_LITTLE_ENDIAN 1
		#undef OSC_HOST_BIG_ENDIAN
	#elif (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)
		#define OSC_HOST_BIG_ENDIAN 1
		#undef OSC_HOST_LITTLE_ENDIAN
	#endif
#else
	#if defined(__LITTLE_ENDIAN__) && !defined(__BIG_ENDIAN__)
		#define OSC_HOST_LITTLE_ENDIAN 1
		#undef OSC_HOST_BIG_ENDIAN
	#elif defined(__BIG_ENDIAN__) && !defined(__LITTLE_ENDIAN__)
		#define OSC_HOST_BIG_ENDIAN 1
		#undef OSC_HOST_LITTLE_ENDIAN
	#endif
#endif

#if !defined(OSC_HOST_LITTLE_ENDIAN) && !defined(OSC_HOST_BIG_ENDIAN)
	#error Endianness not defined for your platform !
#endif


//---------------------------------------------------------------
// Osc errors
//---------------------------------------------------------------

typedef enum
{
	OSC_OK = 0,
	OSC_MISSING_ARG,
	OSC_WRONG_TYPE,
	OSC_INVALID_SIZE,
	OSC_INVALID_ADDRESS,
	OSC_INVALID_TYPE_STRING,
	OSC_UNKNOWN_TYPE,
	OSC_UNMATCHED_ARRAY,
	OSC_ARG_EXCEED_MSG,
	OSC_BAD_STRING,
	OSC_BUFFER,
	OSC_MEM,
	OSC_INVALID_MARKER,
	OSC_NOT_BUNDLE,
	OSC_NOT_MESSAGE,

	OSC_UNMATCHED_PACKET_END,
	OSC_UNMATCHED_MESSAGE_END,
	OSC_UNMATCHED_BUNDLE_END
} OscErrCode;

const char* OscGetErrorMessage(OscErrCode err);
#define OscPrintError(err) LOG_ERROR("%s\n", OscGetErrorMessage(err))

//---------------------------------------------------------------
// Osc types, tags and constants
//---------------------------------------------------------------

const int32 OSC_INT32_MAX               = 0x7FFFFFFF,
            OSC_BUNDLE_ELEMENT_SIZE_MAX = 0x7FFFFFFC,
            OSC_MIN_BUNDLE_SIZE         = 20;

typedef enum
{
	OSC_SIZEOF_INT32  = 4,
	OSC_SIZEOF_UINT32 = 4,
	OSC_SIZEOF_INT64  = 8,
	OSC_SIZEOF_UINT64 = 8,
} OscTypeSize;

typedef enum
{
	TRUE_TYPE_TAG         = 'T',
	FALSE_TYPE_TAG        = 'F',
	NIL_TYPE_TAG          = 'N',
	INFINITUM_TYPE_TAG    = 'I',
	INT16_TYPE_TAG        = 'W',
	INT32_TYPE_TAG        = 'i',
	INT64_TYPE_TAG        = 'h',
	FLOAT_TYPE_TAG        = 'f',
	DOUBLE_TYPE_TAG       = 'd',
	CHAR_TYPE_TAG         = 'c',
	STRING_TYPE_TAG       = 's',
	SYMBOL_TYPE_TAG       = 'S',
	BLOB_TYPE_TAG         = 'b',
	RGBA_COLOR_TYPE_TAG   = 'r',
	MIDI_MESSAGE_TYPE_TAG = 'm',
	TIME_TAG_TYPE_TAG     = 't',
	ARRAY_BEGIN_TYPE_TAG  = '[',
	ARRAY_END_TYPE_TAG    = ']',
	OSC_BLITZ_MARKER_TAG  = 'X',
	OSC_ELEMENT_TYPE_TAG  = 'O'
} OscTypeTag;


//NOTE(martin): ie. 'BLIT' + version number 0.0.1 in big endian
//              when we use it with OscPushInt64Variant(), the functions flips it to the correct endianness,
//	            so we only define the value here, which is independant from its representation
const int64 OSC_BLITZ_MARKER_VALUE = 0x424c495400000001;

//NOTE(martin): on the other hand, we may use some markers for (faster) direct integer comparisons :
//              hence we define big/little endian versions
#ifdef OSC_BIG_ENDIAN
const int64 OSC_BUNDLE_MARKER = 0x2362756e646c6500, // "#bundle"
            OSC_BLITZ_MARKER  = 0x424c495400000001; // 'BLIT' + version number 0.0.1
#else
const int64 OSC_BUNDLE_MARKER = 0x00656c646e756223,
            OSC_BLITZ_MARKER  = 0x0100000054494c42;
#endif

//---------------------------------------------------------------
// Osc Endianness helpers
//---------------------------------------------------------------

int32 SwapInt32(int32 val)
{
	//NOTE(martin): this generates a bswapl (at least on clang)
	//		using a union would be more explicit but it doesn't seem to produce optimized code
	val = (val & 0xff000000)>>24
	     |(val & 0x00ff0000)>>8
	     |(val & 0x0000ff00)<<8
	     |(val & 0x000000ff)<<24;

	return(val);
}

void OscToNative32(const char* dst, const char* src)
{
#ifdef OSC_HOST_LITTLE_ENDIAN

	*(int32*)dst = (*(int32*)src & 0xff000000) >> 24
		      |(*(int32*)src & 0x00ff0000) >> 8
		      |(*(int32*)src & 0x0000ff00) << 8
		      |(*(int32*)src & 0x000000ff) << 24;
#else
	*(int32*)dst = *(int32*)src);
#endif
}

void OscToNative64(const char* dst, const char* src)
{
#ifdef OSC_HOST_LITTLE_ENDIAN

	*(int64*)dst = (*(int64*)src & 0xff00000000000000) >> 56
		      |(*(int64*)src & 0x00ff000000000000) >> 40
		      |(*(int64*)src & 0x0000ff0000000000) >> 24
		      |(*(int64*)src & 0x000000ff00000000) >> 8
		      |(*(int64*)src & 0x00000000ff000000) << 8
		      |(*(int64*)src & 0x0000000000ff0000) << 24
		      |(*(int64*)src & 0x000000000000ff00) << 40
		      |(*(int64*)src & 0x00000000000000ff) << 56;
#else
	*(int64*)dst = *(int64*)src);
#endif
}

//TODO(martin): check that these are effectively a bswap...
int32 OscToInt32(const char* p)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	return((*(int32*)p & 0xff000000) >> 24
	       |(*(int32*)p & 0x00ff0000) >> 8
	       |(*(int32*)p & 0x0000ff00) << 8
	       |(*(int32*)p & 0x000000ff) << 24);
#else
	return(*(int32*)p);
#endif
}

uint32 OscToUInt32(const char* p)
{
	return((uint32)OscToInt32(p));
}

int16 OscToInt16(const char* p)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	return((int16)((*(int32*)p & 0x000000ff) << 8
	              |(*(int32*)p & 0x0000ff00) >> 8));
#else
	return((int16)((*(int32*)p) & 0x00ff));
#endif
}

int64 OscToInt64(const char *p)
{
#ifdef OSC_HOST_LITTLE_ENDIAN

	return( (*(int64*)p & 0xff00000000000000) >> 56
	       |(*(int64*)p & 0x00ff000000000000) >> 40
	       |(*(int64*)p & 0x0000ff0000000000) >> 24
	       |(*(int64*)p & 0x000000ff00000000) >> 8
	       |(*(int64*)p & 0x00000000ff000000) << 8
	       |(*(int64*)p & 0x0000000000ff0000) << 24
	       |(*(int64*)p & 0x000000000000ff00) << 40
	       |(*(int64*)p & 0x00000000000000ff) << 56);
#else
	return(*(int64*)p);
#endif
}

uint64 OscToUInt64(const char* p)
{
	return((uint64)OscToInt64(p));
}

float OscToFloat(const char* p)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	union
	{
		float f;
		int64 i;
	} u;
	u.i = (*(int32*)p & 0xff0000000) >> 24
	      |(*(int32*)p & 0x00ff0000) >> 8
	      |(*(int32*)p & 0x0000ff00) << 8
	      |(*(int32*)p & 0x000000ff) << 24;
	return(u.f);
#else
	return(*(float*)p);
#endif
}

double OscToDouble(const char* p)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	union
	{
		double d;
		int64 i;
	} u;
	u.i = (*(int64*)p & 0xff00000000000000) >> 56
	      |(*(int64*)p & 0x00ff000000000000) >> 40
	      |(*(int64*)p & 0x0000ff0000000000) >> 24
	      |(*(int64*)p & 0x000000ff00000000) >> 8
	      |(*(int64*)p & 0x00000000ff000000) << 8
	      |(*(int64*)p & 0x0000000000ff0000) << 24
	      |(*(int64*)p & 0x000000000000ff00) << 40
	      |(*(int64*)p & 0x00000000000000ff) << 56;

	return(u.d);
#else
	return *(double*)p;
#endif
}

void OscFromInt16(char* p, int16 i)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	*((int32*)p) = ((int32)i & 0xff00) >> 8
	              |((int32)i & 0x00ff) << 8;
#else
	*((int32*)p) = ((int32)i) & (0x00ff);
#endif
}

void OscFromInt32(char* p, int32 i)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	*((int32*)p) = (i & 0xff000000) >> 24
	              |(i & 0x00ff0000) >> 8
		      |(i & 0x0000ff00) << 8
		      |(i & 0x000000ff) << 24;
#else
	*((int32*)p) = i;
#endif
}

void OscFromInt64(char* p, int64 i)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	*((int64*)p) = (i & 0xff00000000000000) >> 56
	              |(i & 0x00ff000000000000) >> 40
		      |(i & 0x0000ff0000000000) >> 24
		      |(i & 0x000000ff00000000) >> 8
		      |(i & 0x00000000ff000000) << 8
		      |(i & 0x0000000000ff0000) << 24
		      |(i & 0x000000000000ff00) << 40
		      |(i & 0x00000000000000ff) << 56;
#else
	*((int64*)p) = i;
#endif
}

void OscFromFloat(char* p, float f)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	union
	{
		float f;
		int32 i;
	} u;
	u.f = f;

	*((int32*)p) = (u.i & 0xff000000) >> 24
		      |(u.i & 0x00ff0000) >> 8
	              |(u.i & 0x0000ff00) << 8
	              |(u.i & 0x000000ff) << 24;
#else
	*((float*)p) = f;
#endif
}

void OscFromDouble(char* p, double d)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	union
	{
		double d;
		int64 i;
	} u;
	u.d = d;
	*((int64*)p) = (u.i & 0xff00000000000000) >> 56
	              |(u.i & 0x00ff000000000000) >> 40
		      |(u.i & 0x0000ff0000000000) >> 24
		      |(u.i & 0x000000ff00000000) >> 8
		      |(u.i & 0x00000000ff000000) << 8
		      |(u.i & 0x0000000000ff0000) << 24
		      |(u.i & 0x000000000000ff00) << 40
		      |(u.i & 0x00000000000000ff) << 56;
#else
	*((double*)p) = d;
#endif
}

//---------------------------------------------------------------
// Size & format checks
//---------------------------------------------------------------

int OscIsMultipleOf4(int32 x)
{
    return((x & ((int32)0x03)) == 0);
}

uint32 OscRoundUp4(uint32 x)
{
    return((x + 3) & ~((uint32)0x03));
}

int OscIsValidElementSize(int32 size)
{
	return(size >= 0 && size <= OSC_BUNDLE_ELEMENT_SIZE_MAX);
}

//TODO(martin): this function can miss unaligned '\0' characters
//		which could cause inconsistent behaviour !
//		Maybe rename to OscFindStr4EndUnsafe and use it only when we know for sure that the string is valid.

const char* OscFindStr4End(const char *p, const char *end)
{
	//NOTE(martin): return the first 4 byte boundary after the first null char of str4,
	//              without scanning further than end (return 0 if no aligned null byte is found in the valid range)
	DEBUG_ASSERT(p < end);
	p += 3;

	//NOTE(martin): the loop inspect the string at each 4*n-1 index to detect a null char.
	//              the loop is unrolled with a factor 2 for performance reasons
	//		NB : This loop _can_ poke passed the end, but since we never poke passed the 16 bytes aligned block in which
	//		the last valid byte of the buffer is stored, we won't trigger a page fault.

	const char* lastBlockStart = (const char*)((int64)(end-1) & ~0x03);
	while(p < lastBlockStart)
	{
		if(*p == 0)
		{
			return(p+1);
		}
		if(p[4] == 0)
		{
			return(p < (end-4) ? p+5 : 0);
		}
		p += 8;
	}

	//NOTE(martin): continue on the last valid block with the non-unrolled loop
	while(p < end)
	{
		if(*p == 0)
		{
			return(p+1);
		}
		p += 4;
	}
	return(0);
}

uint32 OscStr4Size(const char* str)
{
	uint32 size = strlen(str) + 1;
	return(OscRoundUp4(size));
}

void OscStr4UASizes(const char* str, uint32* usize, uint32* asize)
{
	*usize = strlen(str) + 1;
	*asize = OscRoundUp4(*usize);
}

void OscStr4CopyAndPadUA(char* dst, const char* src, uint32 usize, uint32 asize)
{
	*((uint32*)(dst+asize-4)) = 0;
	memcpy(dst, src, usize-1);
}

void OscStr4Copy(char* dst, const char* src)
{
	uint32 usize, asize;
	OscStr4UASizes(src, &usize, &asize);
	OscStr4CopyAndPadUA(dst, src, usize, asize);
}

int32 OscGetArgSize(char tag, const char* arg)
{
	switch(tag)
	{
		case TRUE_TYPE_TAG:
		case FALSE_TYPE_TAG:
		case NIL_TYPE_TAG:
		case INFINITUM_TYPE_TAG:
		case ARRAY_BEGIN_TYPE_TAG:
		case ARRAY_END_TYPE_TAG:
			return(0);

		case INT16_TYPE_TAG:
		case INT32_TYPE_TAG:
		case FLOAT_TYPE_TAG:
		case CHAR_TYPE_TAG:
		case RGBA_COLOR_TYPE_TAG:
		case MIDI_MESSAGE_TYPE_TAG:
			return(4);

		case INT64_TYPE_TAG:
		case TIME_TAG_TYPE_TAG:
		case DOUBLE_TYPE_TAG:
		case OSC_BLITZ_MARKER_TAG:
			return(8);

		case STRING_TYPE_TAG:
		case SYMBOL_TYPE_TAG:
		{
			return(OscStr4Size(arg));
		}
		case BLOB_TYPE_TAG:
		case OSC_ELEMENT_TYPE_TAG:
		{
			uint32 blobSize = OscToUInt32(arg);
			return(4 + OscRoundUp4(blobSize));
		}

		default:
			LOG_ERROR("Unkown type tag\n");
			return(-1);
	}
}

//---------------------------------------------------------------
// Structs forward declarations
//---------------------------------------------------------------

typedef struct osc_element osc_element;
typedef struct osc_arg_iterator osc_arg_iterator;

//---------------------------------------------------------------
// Osc argument tagged union
//---------------------------------------------------------------

typedef struct osc_blob
{
	int32 size;
	const char* data;
} osc_blob;

/*
	WARNING: the osc_argument struct does _not_ own the memory associated with strings, blobs or nested osc elements.
	         eg., calling OscArgSetString() then freeing the string would result in a dangling pointer.
	         You be warned.
*/

typedef struct osc_argument
{
	char tag;
	union
	{
		char c;
		int16 i16;
		int32 i32;
		int64 i64;
		float f;
		double d;
		const char* s;
		osc_blob b;
		osc_element* elt;
	};
} osc_argument;

void OscArgSetArrayBegin(osc_argument* arg) { arg->tag = ARRAY_BEGIN_TYPE_TAG; }
void OscArgSetArrayEnd(osc_argument* arg) { arg->tag = ARRAY_END_TYPE_TAG; }
void OscArgSetNil(osc_argument* arg) { arg->tag = NIL_TYPE_TAG; }
void OscArgSetInfinity(osc_argument* arg) { arg->tag = INFINITUM_TYPE_TAG; }
void OscArgSetBool(osc_argument* arg, int b) { arg->tag = b ? TRUE_TYPE_TAG : FALSE_TYPE_TAG; }
void OscArgSetInt32(osc_argument* arg, int32 i) { arg->tag = 'i' ; arg->i32 = i;}
void OscArgSetInt16(osc_argument* arg, int16 i) { arg->tag = 'W' ; arg->i16 = i; }
void OscArgSetInt64(osc_argument* arg, int64 i) { arg->tag = 'h' ; arg->i64 = i; }
void OscArgSetChar(osc_argument* arg, char c)  { arg->tag = 'c' ; arg->c = c; }
void OscArgSetFloat(osc_argument* arg, float f) { arg->tag = 'f' ; arg->f = f; }
void OscArgSetDouble(osc_argument* arg, double d) { arg->tag = 'd' ; arg->d = d; }
void OscArgSetString(osc_argument* arg, const char* s) { arg->tag = 's' ; arg->s = s; }
void OscArgSetSymbol(osc_argument* arg, const char* s) { arg->tag = 'S' ; arg->s = s; }
void OscArgSetBlob(osc_argument* arg, uint32 size, const char* data)
{
	arg->tag = 'b';
	arg->b.size = size;
	arg->b.data = data;
}
void OscArgSetRgbaColor(osc_argument* arg, uint32 col) { arg->tag = 'r' ; arg->i32 = col; }
void OscArgSetMidiMessage(osc_argument* arg, uint32 m) { arg->tag = 'm' ; arg->i32 = m; }
void OscArgSetTimeTag(osc_argument* arg, uint64 t) { arg->tag = 't' ; arg->i64 = t; }
void OscArgSetInt32Variant(osc_argument* arg, char tag, int32 i) { arg->tag = tag ; arg->i32 = i; }
void OscArgSetInt64Variant(osc_argument* arg, char tag, int64 i) { arg->tag = tag ; arg->i64 = i; }
void OscArgSetStringVariant(osc_argument* arg, char tag, const char* s) { arg->tag = tag ; arg->s = s; }
void OscArgSetElement(osc_argument* arg, osc_element* nested) { arg->tag = 'O' ; arg->elt = nested; }

//---------------------------------------------------------------
// Osc message struct
//---------------------------------------------------------------

typedef struct osc_msg
{
	uint32 msgSize;
	uint32 addressPatternSize;
	uint32 typeStringSize;
	uint32 argsSize;

	const char* addressPattern;    // points to the begining of the address pattern (also the packet start)
	const char* typeString;        // points to the type string, INCLUDING the leading ','
	const char* typeStringEnd;     // points to the first '\0' of the typeString
	const char* args;              // points to the first argument
} osc_msg;

//NOTE(martin): point the message to a memory buffer
OscErrCode OscMessagePrepare(osc_msg* msg, uint32 size, char* buffer)
{
	//TODO(martin): ASSERT size
	memset(msg, 0, sizeof(osc_msg));
	msg->addressPattern = buffer;
	msg->msgSize = size;
	return(OSC_OK);
}

const char* OscMessageData(osc_msg* msg)
{
	return(msg->addressPattern);
}

uint32 OscMessageSize(osc_msg* msg)
{
	return(msg->msgSize);
}

uint32 OscArgumentsCount(osc_msg* msg)
{
	return(msg->typeStringEnd ? (msg->typeStringEnd - msg->typeString - 1) : 0);
}

const char* OscAddressPattern(osc_msg* msg)
{
	return(msg->addressPattern);
}

const char* OscTypeTags(osc_msg* msg)
{
	return(msg->typeString ? (msg->typeString+1) : 0);
}

//---------------------------------------------------------------
// Osc element
//---------------------------------------------------------------

/* NOTE(martin): osc_element allows to parse and iterate thru OSC elements, which are either messages or bundles
                 It also allows to compose OSC packets
*/

typedef struct osc_composer
{
	uint8 isBundle;
	int32 size;
	char* data;
	struct osc_composer* parent;

	union
	{
		struct
		{
			char* args;
			char* nextArg;
			char* nextTag;
			int arrayLevel;
		} msg;

		struct
		{
			char* nextSlot;
		} bundle;
	};
} osc_composer;

//NOTE(martin): composition memory overhead (per nesting level)
const int OSC_COMPOSE_OVERHEAD = sizeof(osc_composer);

typedef struct osc_element
{
	int32 size;
	const char* data;

	//NOTE(martin): iterator data
	osc_element* parent;
	int msgCache;
	osc_msg msg;

	//NOTE(martin): composition data
	int composing;
	osc_composer* composer;
} osc_element;

const char* OscElementData(osc_element* element)
{
	return(element->data);
}

int OscElementSize(osc_element* element)
{
	return(element->size);
}

uint64 OscBundleTimetag(osc_element* element)
{
	return(OscToUInt64(element->data + 8));
}

OscErrCode OscElementCopy(osc_element* dst, uint32 size, char* buffer, osc_element* src);

//---------------------------------------------------------------
// Osc composing
//---------------------------------------------------------------

OscErrCode OscBeginPacket(osc_element* elt, int32 size, char* buffer);
OscErrCode OscEndPacket(osc_element* elt);

OscErrCode OscBeginMessage(osc_element* elt, u32 len, const char* addressPattern);
OscErrCode OscEndMessage(osc_element* elt);

OscErrCode OscBeginBundleFull(osc_element* elt, uint64 timetag);
static inline OscErrCode OscBeginBundle(osc_element* elt, uint64 timetag)
{
	return(OscBeginBundleFull(elt, 0));
}

OscErrCode OscEndBundle(osc_element* elt);

OscErrCode OscArrayBegin(osc_element* elt);
OscErrCode OscArrayEnd(osc_element* elt);
OscErrCode OscPushIterator(osc_element* elt, osc_arg_iterator* it);
OscErrCode OscPushArgument(osc_element* elt, osc_argument* arg);
OscErrCode OscPushNil(osc_element* elt);
OscErrCode OscPushInfinity(osc_element* elt);
OscErrCode OscPushBool(osc_element* elt, int b);
OscErrCode OscPushInt32(osc_element* elt, int32 i);
OscErrCode OscPushInt16(osc_element* elt, int16 i);
OscErrCode OscPushInt64(osc_element* elt, int64 i);
OscErrCode OscPushChar(osc_element* elt, char c);
OscErrCode OscPushFloat(osc_element* elt, float f);
OscErrCode OscPushDouble(osc_element* elt, double d);
OscErrCode OscPushString(osc_element* elt, u32 len, const char* s);
OscErrCode OscPushSymbol(osc_element* elt, u32 len, const char* s);
OscErrCode OscPushBlob(osc_element* elt, uint32 size, const char* data);
OscErrCode OscPushRgbaColor(osc_element* elt, uint32 col);
OscErrCode OscPushMidiMessage(osc_element* elt, uint32 m);
OscErrCode OscPushTimeTag(osc_element* elt, uint64 t);

OscErrCode OscPushInt32Variant(osc_element* elt, char tag, int32 i);
OscErrCode OscPushInt64Variant(osc_element* elt, char tag, int64 i);
OscErrCode OscPushStringVariant(osc_element* elt, char tag, u32 len, const char* s);

//NOTE(martin): this is the copying version for pushing an already composed nested element
OscErrCode OscPushElement(osc_element* elt, osc_element* nested);

//---------------------------------------------------------------
// OSC Parsing functions
//---------------------------------------------------------------

OscErrCode OscParseMessage(osc_msg* msg, int32 size, const char* packet);
OscErrCode OscParsePacket(osc_element* element, int32 size, const char* packet);

//---------------------------------------------------------------------------------
// Bundle iterator functions
//---------------------------------------------------------------------------------

osc_element OscElementsBegin(osc_element* bundle);
osc_element OscElementsEnd(osc_element* bundle);
osc_element* OscElementNext(osc_element* elt);

osc_element* OscElementIteratorCreate(osc_element* bundle);
void OscElementIteratorDestroy(osc_element* iterator);

OscErrCode OscElementAsMessage(osc_element* element, osc_msg** msg);
osc_msg* OscElementAsMessageUnchecked(osc_element* element);

//NOTE(martin): comparison functions

int OscElementIsEqual(const osc_element* lhs, const osc_element* rhs)
{
	return(lhs->parent == rhs->parent
	      && lhs->data == rhs->data
	      && lhs->size == rhs->size);
}

__attribute__((used))
int OscIsAtEndOfBundle(osc_element* bundle, osc_element* elt)
{
	osc_element end = OscElementsEnd(bundle);
	return(OscElementIsEqual(&end, elt));
}

int OscElementIsBundle(const osc_element* elt)
{
	#ifdef DEBUG
		return((elt->size >= 8) && !strcmp(elt->data, "#bundle"));
	#else
		return(elt->data[0] == '#');
	#endif
}

int OscElementIsMessage(const osc_element* elt)
{
	return(!OscElementIsBundle(elt));
}

//NOTE(martin): C++ overloaded operators, for those who like it...
#ifdef __cplusplus
	extern "C++" bool operator==(const osc_element& lhs, const osc_element& rhs)
	{
		return(OscElementIsEqual(&lhs, &rhs));
	}
	extern "C++" bool operator!=(const osc_element& lhs, const osc_element& rhs)
	{
		return(!(lhs == rhs));
	}

	extern "C++" osc_element& operator++(osc_element& elt)
	{
		return(*OscElementNext(&elt));
	}

	extern "C++" osc_element operator++(osc_element& elt, int d)
	{
		osc_element old = elt;
		OscElementNext(&elt);
		return(old);
	}
#endif // __cplusplus

//---------------------------------------------------------------------------------
// Osc arguments iterator
//---------------------------------------------------------------------------------
typedef struct osc_arg_iterator
{
	const char* tag;
	const char* data;
} osc_arg_iterator;

osc_arg_iterator OscArgumentsBegin(osc_msg* msg)
{
	osc_arg_iterator arg;
	arg.tag = msg->typeString;
	if(arg.tag)
	{
		arg.tag++;
	}
	arg.data = msg->args;
	return(arg);
}

osc_arg_iterator OscArgumentsEnd(osc_msg* msg)
{
	osc_arg_iterator arg;
	arg.tag = msg->typeStringEnd;
	arg.data = msg->args + msg->argsSize;
	return(arg);
}

osc_arg_iterator* OscArgumentNext(osc_arg_iterator* arg)
{
	if(*(arg->tag))
	{
		arg->data += OscGetArgSize(*arg->tag, arg->data);
		arg->tag++;
	}
	return(arg);
}

/* NOTE(martin): osc_arg_iterator creation/destruction function
		they're useful for bindings where we prefer to only deal with pointers (eg. lisp)
*/
osc_arg_iterator* OscArgumentCreate(osc_msg* msg);
void OscArgumentDestroy(osc_arg_iterator* arg);


//NOTE(martin): get type / remaining arg count
__attribute__((used))
char OscArgumentType(const osc_arg_iterator* arg)
{
	return(*(arg->tag));
}

const char* OscArgumentRemainingTags(const osc_arg_iterator* arg)
{
	return(arg->tag);
}

uint32 OscArgumentRemainingArgCount(const osc_arg_iterator* arg)
{
	return(strlen(arg->tag));
}

//NOTE(martin): argument iterator comparisons

int OscArgumentIsEqual(const osc_arg_iterator* lhs, const osc_arg_iterator* rhs)
{
	return((lhs->tag == rhs->tag) && (lhs->data == rhs->data));
}

int OscIsAtEndOfMessage(osc_msg* msg, osc_arg_iterator* arg)
{
	return(arg->data == (msg->args + msg->argsSize));
}


//NOTE(martin): C++ overloaded operators
#ifdef __cplusplus
	extern "C++" bool operator==(const osc_arg_iterator& lhs, const osc_arg_iterator& rhs)
	{
		return(OscArgumentIsEqual(&lhs, &rhs));
	}
	extern "C++" bool operator!=(const osc_arg_iterator& lhs, const osc_arg_iterator& rhs)
	{
		return(!(lhs == rhs));
	}

	extern "C++" osc_arg_iterator& operator++(osc_arg_iterator& arg)
	{
		return(*OscArgumentNext(&arg));
	}

	extern "C++" osc_arg_iterator operator++(osc_arg_iterator& arg, int d)
	{
		osc_arg_iterator old = arg;
		OscArgumentNext(&arg);
		return(old);
	}
#endif // __cplusplus

//---------------------------------------------------------------------------------
// OSC Iterator conversion functions
//---------------------------------------------------------------------------------

OscErrCode OscAsBool(osc_arg_iterator* arg, int* b);
OscErrCode OscAsInt16(osc_arg_iterator* arg, int16* i);
OscErrCode OscAsInt32(osc_arg_iterator* arg, int32* i);
OscErrCode OscAsInt64(osc_arg_iterator* arg, int64* i);
OscErrCode OscAsFloat(osc_arg_iterator* arg, float* f);
OscErrCode OscAsDouble(osc_arg_iterator* arg, double* d);
OscErrCode OscAsChar(osc_arg_iterator* arg, char* c);
OscErrCode OscAsString(osc_arg_iterator* arg, const char** s);
OscErrCode OscAsSymbol(osc_arg_iterator* arg, const char** s);
OscErrCode OscAsBlob(osc_arg_iterator* arg, const void** data, uint32* size);
OscErrCode OscAsElement(osc_arg_iterator* arg, osc_element* element);
OscErrCode OscAsRgbaColor(osc_arg_iterator* arg, uint32* col);
OscErrCode OscAsMidiMessage(osc_arg_iterator* arg, uint32* msg);
OscErrCode OscAsTimeTag(osc_arg_iterator* arg, uint64* t);

//NOTE(martin): unchecked versions

int OscAsBoolUnchecked(const osc_arg_iterator* arg)
{
	return(*(arg->tag) == TRUE_TYPE_TAG);
}

int32 OscAsInt32Unchecked(const osc_arg_iterator* arg)
{
	return(OscToInt32(arg->data));
}

int16 OscAsInt16Unchecked(const osc_arg_iterator* arg)
{
	return(OscToInt16(arg->data));
}

int64 OscAsInt64Unchecked(const osc_arg_iterator* arg)
{
	return(OscToInt64(arg->data));
}

float OscAsFloatUnchecked(const osc_arg_iterator* arg)
{
	return(OscToFloat(arg->data));
}

double OscAsDoubleUnchecked(const osc_arg_iterator* arg)
{
	return(OscToDouble(arg->data));
}

char OscAsCharUnchecked(const osc_arg_iterator* arg)
{
    return (char)OscToInt32(arg->data);
}

const char* OscAsStringUnchecked(const osc_arg_iterator* arg)
{
	return(arg->data);
}

const char* OscAsSymbolUnchecked(const osc_arg_iterator* arg)
{
	return(OscAsStringUnchecked(arg));
}

OscErrCode OscAsBlobUnchecked(const osc_arg_iterator* arg, const void** data, uint32* size)
{
	int32 sizeResult = (int32)OscToUInt32(arg->data);
	if(!OscIsValidElementSize(sizeResult))
	{
		return(OSC_INVALID_SIZE);
	}
	*size = sizeResult;
	*data = (void*)(arg->data + OSC_SIZEOF_INT32);
	return(OSC_OK);
}

OscErrCode OscAsElementUnchecked(const osc_arg_iterator* arg, osc_element* element)
{
	int32 sizeResult = (int32)OscToUInt32(arg->data);
	if(!OscIsValidElementSize(sizeResult))
	{
		//TODO(martin): should have been checked already in OscParseMessage() ?
		return(OSC_INVALID_SIZE);
	}
	return(OscParsePacket(element, sizeResult, arg->data + OSC_SIZEOF_INT32));
}

uint32 OscAsRgbaColorUnchecked(const osc_arg_iterator* arg)
{
	return(OscToUInt32(arg->data));
}

uint32 OscAsMidiMessageUnchecked(const osc_arg_iterator* arg)
{
	return(OscToUInt32(arg->data));
}

uint64 OscAsTimeTagUnchecked(const osc_arg_iterator* arg)
{
	return(OscToUInt64(arg->data));
}

//---------------------------------------------------------------------------------
// variadic and array-based composing / parsing functions
//---------------------------------------------------------------------------------

//NOTE(martin): Push several arguments in a message
OscErrCode OscPushFormat(osc_element* elt, const char* typeTags, ...);
OscErrCode OscPushFormatVA(osc_element* elt, const char* typeTags, va_list ap);
OscErrCode OscPushA(osc_element* elt, int argc, osc_argument* argv);

//NOTE(martin): insert a message into a composing packet with a single function
OscErrCode OscFormatMessage(osc_element* elt, const char* pattern, const char* typeTags, ...);
OscErrCode OscFormatMessageVA(osc_element* elt, const char* pattern, const char* typeTags, va_list ap);
OscErrCode OscFormatMessageA(osc_element* elt, const char* pattern, int argc, osc_argument* argv);

//NOTE(martin): format a message packet with a single function
OscErrCode OscFormat(osc_element* elt, uint32 size, char* buffer, const char* pattern, const char* typeTags, ...);
OscErrCode OscFormatVA(osc_element* elt, uint32 size, char* buffer, const char* pattern, const char* typeTags, va_list ap);
OscErrCode OscFormatA(osc_element* elt, uint32 size, char* buffer, const char* pattern, int argc, osc_argument* argv);

//NOTE(martin): scan a message's arguments
OscErrCode OscScanArguments(osc_msg* elt, const char* typeTags, ...);
OscErrCode OscScanArgumentsVA(osc_msg* elt, const char* typeTags, va_list ap);

OscErrCode OscScan(osc_msg* elt, const char* address, const char* typeTags, ...);
OscErrCode OscScanVA(osc_msg* elt, const char* address, const char* typeTags, va_list ap);

//TODO(martin): scan packet ?

//---------------------------------------------------------------
// Print / Debug functions
//---------------------------------------------------------------

void OscArgumentsPrint(const char* typeTags, const char* args);
void OscMessagePrint(osc_msg* msg);

OscErrCode OscElementPrintIndented(osc_element* element, int indentLevel);
static inline OscErrCode OscElementPrint(osc_element* element)
{
	return(OscElementPrintIndented(element, 0));
}

#ifdef DEBUG
#define DEBUG_OSC_MSG_PRINT(msg) OscMessagePrint(msg)
#define DEBUG_OSC_ELT_PRINT(elt) OscElementPrint(elt)
#else
#define DEBUG_OSC_MSG_PRINT(msg)
#define DEBUG_OSC_ELT_PRINT(elt)
#endif

static const char* OscErrMsg[] = { "OK",
				   "Missing arguments",
				   "Wrong argument type",
				   "Invalid size",
				   "Invalid address pattern",
				   "Invalid type string",
				   "Unknown type",
				   "Unmatched array marker",
				   "Argument exceeds message size",
				   "Bad OSC string",
				   "OSC data exceeds buffer size",
				   "Couln't allocate memory",
				   "Invalid bundle marker",
				   "Osc element is not a bundle",
				   "Osc element is not a message" };

const char* OscGetErrorMessage(OscErrCode err)
{
	return(OscErrMsg[err]);
}

//---------------------------------------------------------------
// Osc composing functions
//---------------------------------------------------------------

/* NOTE(martin):

The composition functions allow to compose osc messages and bundles in place. The buffer in which the composition takes place, and its maximum size, are passed to OscBeginPacket().

The OscBeginXXX() functions allocate some space of the buffer to store an osc_composer struct, which keeps tracks of the offsets to the next element/arguments/tags.
The OscEndXXX() functions finalize the current message/bundle and reinstall the parent composer as the current composer.

- Example of a bundle being composed inside a bundle :

-----------------------------------------------------------------------------------------------------------
| #bundle | timetag | size1 | element1 | --- | element2 (being composed) ........ | composer2 | composer1 |
------------------------------------------------------------------------------------|-----------|----------
                                                                    ^-- next slot --+           |
                                              ^------------------ next slot --------------------+

- Example of a bundle begin composed inside a message :

-----------------------------------------------------------------------------------------------------------
| /foo | 3.14 | "Hello" | element (being composed) ........               | composer2 |O|s|f|,| composer1 |
----------------------------------------------------------------------------|-------------------|----------
                                                ^---------- next slot ------+                   |
                         ^------------------------- next argument ------------------------------+

When composing a message, the arguments are temporarily stored just after the address pattern. The type string is temporarily store at the end of the available space (just before the composer), in _reverse_ order. It allows to compose the message without knowing the arguments in advance and avoid a lot of unnecessary memory moves.
When the message is finalized, the arguments are moved to the end of the block and the type string is reversed and copied after the address pattern.

The OscFormatXXX() functions know the number of arguments and their types, so they can write the argument in a straightforward fashion, avoiding these memory moves.

*/

//NOTE(martin): internal helper functions

static void OscGetComposerAvailableSpace(osc_element* elt, int32* size, char** data)
{
	osc_composer* composer = elt->composer;
	if(!composer)
	{
		*size = elt->size;
		*data = (char*)elt->data;
	}
	else if(composer->isBundle)
	{
		*size = composer->data + composer->size - composer->bundle.nextSlot - sizeof(int32);
		*data = composer->bundle.nextSlot + 4;
	}
	else
	{
		//TODO(martin): we should check available size before writing tag
		*(composer->msg.nextTag) = 'O';
		composer->msg.nextTag--;
		*size = composer->msg.nextTag - composer->msg.nextArg - sizeof(int32);
		*data = composer->msg.nextArg + sizeof(int32);
	}
}

static void OscUpdateComposer(osc_element* elt, osc_composer* composer, int32 childSize)
{
	//NOTE(martin): this function is used to update a composer when we have finished composing a child element
	//              it writes the size of the element in the size slot and advances to the next composing slot

	if(!composer)
	{
		//NOTE(martin): There is no parent composer. The packet is a single bundle.
		DEBUG_ASSERT(childSize <= elt->size);
		elt->size = childSize;
		elt->composing = 0;
	}
	else
	{
		if(composer->isBundle)
		{
			//NOTE(martin): fill the size slot of the bundle element and advance to next slot
			OscFromInt32(composer->bundle.nextSlot, childSize);
			composer->bundle.nextSlot += (childSize + sizeof(int32));
		}
		else
		{
			//NOTE(martin): fill the size slot of the message argument and advance to the next argument
			OscFromInt32(composer->msg.nextArg, childSize);
			composer->msg.nextArg += (childSize + sizeof(int32));
		}
	}
}

//NOTE(martin): public composing API

OscErrCode OscBeginPacket(osc_element* elt, int32 size, char* buffer)
{
	DEBUG_ASSERT(elt);
	DEBUG_ASSERT(buffer);

	if(!OscIsValidElementSize(size) || size < 20)
	{
		return(OSC_INVALID_SIZE);
	}

	elt->size = size;
	elt->data = buffer;
	elt->parent = 0;
	elt->composing = 1;
	elt->composer = 0;
	elt->msgCache = 0;

	return(OSC_OK);
}
OscErrCode OscEndPacket(osc_element* elt)
{
	DEBUG_ASSERT(elt);

	if(elt->composer != 0)
	{
		return(OSC_UNMATCHED_PACKET_END);
	}
	DEBUG_ASSERT(elt->composer == 0);

	if(elt->composing)
	{
		elt->size = 0;
		elt->composing = 0;
	}
	return(OSC_OK);
}

OscErrCode OscBeginMessage(osc_element* elt, u32 addressLen, const char* addressPattern)
{
	DEBUG_ASSERT(elt);
	DEBUG_ASSERT(addressPattern);
	DEBUG_ASSERT(elt->composing); // or return compose error ?

	if(*addressPattern == '#' || *addressPattern == '\0')
	{
		return(OSC_INVALID_ADDRESS);
	}

	//NOTE(martin): compute size and start of the new composing region and reserve space for the new composer
	int32 size;
	char* data;
	OscGetComposerAvailableSpace(elt, &size, &data);
	size -= sizeof(osc_composer);

	//NOTE(martin): copy and pad address pattern

	uint32 patternUSize = addressLen + 1;
	uint32 patternASize = OscRoundUp4(patternUSize);
	if(size < patternASize)
	{
		return(OSC_BUFFER);
	}
	OscStr4CopyAndPadUA(data, addressPattern, patternUSize, patternASize);

	//NOTE(martin): init new composer at the end of the new composing region

	osc_composer* parent = elt->composer;

	elt->composer = (osc_composer*)(data + size);
	elt->composer->isBundle = 0;
	elt->composer->data = data;
	elt->composer->size = size;
	elt->composer->parent = parent;
	elt->composer->msg.args = data + patternASize;
	elt->composer->msg.nextArg = elt->composer->msg.args;
	elt->composer->msg.nextTag = data + size - 1;
	*(elt->composer->msg.nextTag) = ',';
	elt->composer->msg.nextTag--;
	elt->composer->msg.arrayLevel = 0;

	return(OSC_OK);
}

OscErrCode OscEndMessage(osc_element* elt)
{
	DEBUG_ASSERT(elt);
	DEBUG_ASSERT(elt->composing);

	osc_composer* composer = elt->composer;
	if(!composer || composer->isBundle)
	{
		return(OSC_UNMATCHED_MESSAGE_END);
	}
	DEBUG_ASSERT(elt->composer->msg.nextTag >= elt->composer->msg.nextArg);

	if(composer->msg.arrayLevel)
	{
		return(OSC_UNMATCHED_ARRAY);
	}

	//NOTE(martin): compute the typeString size and make an padded & aligned copy of the composed type string
	//              typeStringChars is the number of char in type string including leading ',' and trailing '\0'
	//		typeStringSize is the aligned size
	uint32 typeStringChars = (char*)composer->data + composer->size - composer->msg.nextTag;
	uint32 typeStringSize = OscRoundUp4(typeStringChars);
	char* tmp = (char*)mem_arena_alloc(mem_scratch(), typeStringSize);
	if(!tmp)
	{
		return(OSC_MEM);
	}

	uint32 pad = typeStringSize - typeStringChars;
	*(uint32*)tmp = 0;
	memcpy(tmp + pad + 1, composer->msg.nextTag+1, typeStringChars - 1);

	//NOTE(martin): compute the arguments size and shift the arguments right to make room for the type string

	uint32 argsSize = composer->msg.nextArg - composer->msg.args;
	memmove((void*)(composer->msg.args + typeStringSize), (void*)composer->msg.args, argsSize);

	//NOTE(martin): copy the type string in reverse at its proper location (we do the copy using byteswaps)

	char* typeString = composer->msg.args;
	uint32* chunk = (uint32*)(typeString + typeStringSize - 4);
	for(int i=0; i < typeStringSize; i+=4)
	{
		 *(chunk--) = SwapInt32(*(uint32*)(tmp+i));
	}

	//NOTE(martin): compute the actual size of the packet

	uint32 msgSize = typeString + typeStringSize + argsSize - composer->data;
	DEBUG_ASSERT(OscIsMultipleOf4(msgSize));

	//NOTE(martin): update the parent composer and reinstall it as the current composer
	OscUpdateComposer(elt, composer->parent, msgSize);
	elt->composer = composer->parent;
	return(OSC_OK);
}

OscErrCode OscBeginBundleFull(osc_element* elt, uint64 timetag)
{
	DEBUG_ASSERT(elt);
	DEBUG_ASSERT(elt->composing);

	//NOTE(martin): compute size and start of the new composing region and reserve space for the new composer
	int32 size;
	char* data;
	OscGetComposerAvailableSpace(elt, &size, &data);
	size -= sizeof(osc_composer);
	if(size < OSC_MIN_BUNDLE_SIZE)
	{
		return(OSC_BUFFER);
	}

	//NOTE(martin): set bundle identifier and timetag

	*((int64*)data) = OSC_BUNDLE_MARKER;
	OscFromInt64(data + 8, timetag);

	//NOTE(martin): init new composer at the end of the new composing region

	osc_composer* parent = elt->composer;

	elt->composer = (osc_composer*)(data + size);
	elt->composer->isBundle = 1;
	elt->composer->data = data;
	elt->composer->size = size;
	elt->composer->parent = parent;
	elt->composer->bundle.nextSlot = elt->composer->data + 16;

	return(OSC_OK);
}

OscErrCode OscEndBundle(osc_element* elt)
{
	DEBUG_ASSERT(elt->composing);

	if(!elt->composer || !elt->composer->isBundle)
	{
		return(OSC_UNMATCHED_BUNDLE_END);
	}
	DEBUG_ASSERT(elt->composer->bundle.nextSlot <= (elt->composer->data + elt->composer->size));

	osc_composer* composer = elt->composer;
	int32 bundleSize = composer->bundle.nextSlot - composer->data;

	DEBUG_ASSERT(OscIsValidElementSize(bundleSize));

	//NOTE(martin): update the parent composer and reinstall it as the current composer
	OscUpdateComposer(elt, composer->parent, bundleSize);
	elt->composer = composer->parent;
	return(OSC_OK);
}

//---------------------------------------------------------------------------------
// OSC arguments composing functions
//---------------------------------------------------------------------------------

static inline OscErrCode OscCheckNextArgSpace(osc_element* elt, uint32 size)
{
	//NOTE(martin): check that the next argument won't overwrite the type string
	//		that is also a good place to assert that we are indeed composing a message
	//TODO(martin): maybe we should return an error or always assert rather than debug_assert ?
	DEBUG_ASSERT(elt->composing);

	osc_composer* composer = elt->composer;
	DEBUG_ASSERT(composer);
	DEBUG_ASSERT(!composer->isBundle);
	DEBUG_ASSERT(composer->msg.nextArg && composer->msg.nextTag);

	return((composer->msg.nextArg + size <= composer->msg.nextTag) ? OSC_OK : OSC_BUFFER);
}

OscErrCode OscArrayBegin(osc_element* elt)
{
	OscErrCode err;
	if((err = OscCheckNextArgSpace(elt, 0)) != OSC_OK)
	{
		return(err);
	}
	osc_composer* composer = elt->composer;
	*(composer->msg.nextTag) = '[';
	composer->msg.arrayLevel++;
	composer->msg.nextTag--;
	return(OSC_OK);
}
OscErrCode OscArrayEnd(osc_element* elt)
{
	OscErrCode err;
	if((err = OscCheckNextArgSpace(elt, 0)) != OSC_OK)
	{
		return(err);
	}
	osc_composer* composer = elt->composer;
	if(!composer->msg.arrayLevel)
	{
		return(OSC_UNMATCHED_ARRAY);
	}
	*(composer->msg.nextTag) = ']';
	composer->msg.arrayLevel--;
	composer->msg.nextTag--;
	return(OSC_OK);
}

OscErrCode OscPushIterator(osc_element* elt, osc_arg_iterator* arg)
{
	int32 size = OscGetArgSize(*(arg->tag), arg->data);
	OscErrCode err;
	if((err = OscCheckNextArgSpace(elt, size)) != OSC_OK)
	{
		return(err);
	}
	osc_composer* composer = elt->composer;
	*(composer->msg.nextTag) = *(arg->tag);
	composer->msg.nextTag--;
	memcpy(composer->msg.nextArg, arg->data, size);
	composer->msg.nextArg += size;
	return(OSC_OK);
}

OscErrCode OscPushArgument(osc_element* elt, osc_argument* arg)
{
	switch(arg->tag)
	{
		case ARRAY_BEGIN_TYPE_TAG:
			return(OscArrayBegin(elt));
		case ARRAY_END_TYPE_TAG:
			return(OscArrayEnd(elt));
		case TRUE_TYPE_TAG:
			return(OscPushBool(elt, 1));
		case FALSE_TYPE_TAG:
			return(OscPushBool(elt, 0));
		case NIL_TYPE_TAG:
			return(OscPushNil(elt));
		case INFINITUM_TYPE_TAG:
			return(OscPushInfinity(elt));
		case CHAR_TYPE_TAG:
			return(OscPushChar(elt, arg->c));
		case INT16_TYPE_TAG:
			return(OscPushInt16(elt, arg->i16));
		case INT32_TYPE_TAG:
			return(OscPushInt32(elt, arg->i32));
		case INT64_TYPE_TAG:
			return(OscPushInt64(elt, arg->i64));
		case FLOAT_TYPE_TAG:
			return(OscPushFloat(elt, arg->f));
		case DOUBLE_TYPE_TAG:
			return(OscPushDouble(elt, arg->d));
		case RGBA_COLOR_TYPE_TAG:
			return(OscPushRgbaColor(elt, arg->i32));
		case MIDI_MESSAGE_TYPE_TAG:
			return(OscPushMidiMessage(elt, arg->i32));
		case TIME_TAG_TYPE_TAG:
			return(OscPushTimeTag(elt, arg->i64));
		case STRING_TYPE_TAG:
			return(OscPushString(elt, strlen(arg->s), arg->s));
		case SYMBOL_TYPE_TAG:
			return(OscPushSymbol(elt, strlen(arg->s), arg->s));
		case BLOB_TYPE_TAG:
			return(OscPushBlob(elt, arg->b.size, arg->b.data));
		case OSC_BLITZ_MARKER_TAG:
			return(OscPushInt64Variant(elt, 'X', OSC_BLITZ_MARKER_VALUE));
		case OSC_ELEMENT_TYPE_TAG:
			return(OscPushElement(elt, arg->elt));
		default:
			return(OSC_UNKNOWN_TYPE);
	}
}

OscErrCode OscPushNil(osc_element* elt)
{
	OscErrCode err;
	if((err = OscCheckNextArgSpace(elt, 0)) != OSC_OK)
	{
		return(err);
	}
	osc_composer* composer = elt->composer;
	*(composer->msg.nextTag) = 'N';
	composer->msg.nextTag--;
	return(OSC_OK);
}
OscErrCode OscPushInfinity(osc_element* elt)
{
	OscErrCode err;
	if((err = OscCheckNextArgSpace(elt, 0)) != OSC_OK)
	{
		return(err);
	}
	osc_composer* composer = elt->composer;
	*(composer->msg.nextTag) = 'I';
	composer->msg.nextTag--;
	return(OSC_OK);
}

OscErrCode OscPushBool(osc_element* elt, int b)
{
	OscErrCode err;
	if((err = OscCheckNextArgSpace(elt, 0)) != OSC_OK)
	{
		return(err);
	}
	osc_composer* composer = elt->composer;
	*(composer->msg.nextTag) = b ? 'T' : 'F';
	composer->msg.nextTag--;
	return(OSC_OK);
}

OscErrCode OscPushInt32Variant(osc_element* elt, char tag, int32 i)
{
	OscErrCode err;
	if((err = OscCheckNextArgSpace(elt, sizeof(i))) != OSC_OK)
	{
		return(err);
	}
	osc_composer* composer = elt->composer;

	*(composer->msg.nextTag) = tag;
	composer->msg.nextTag--;
	OscFromInt32(composer->msg.nextArg, i);
	composer->msg.nextArg += sizeof(i);
	return(OSC_OK);
}

OscErrCode OscPushInt32(osc_element* elt, int32 i)
{
	return(OscPushInt32Variant(elt, 'i', i));
}

OscErrCode OscPushInt16(osc_element* elt, int16 i)
{
	//WARNING(martin): that short int type is specific to our implementation

	OscErrCode err;
	if((err = OscCheckNextArgSpace(elt, sizeof(int32))) != OSC_OK)
	{
		return(err);
	}
	osc_composer* composer = elt->composer;

	*(composer->msg.nextTag) = 'W';
	composer->msg.nextTag--;
	OscFromInt16(composer->msg.nextArg, i);
	composer->msg.nextArg += sizeof(int32);
	return(OSC_OK);
}

OscErrCode OscPushInt64Variant(osc_element* elt, char tag, int64 i)
{
	OscErrCode err;
	if((err = OscCheckNextArgSpace(elt, sizeof(i))) != OSC_OK)
	{
		return(err);
	}
	osc_composer* composer = elt->composer;

	*(composer->msg.nextTag) = tag;
	composer->msg.nextTag--;
	OscFromInt64(composer->msg.nextArg, i);
	composer->msg.nextArg += sizeof(i);
	return(OSC_OK);
}

OscErrCode OscPushInt64(osc_element* elt, int64 i)
{
	return(OscPushInt64Variant(elt, 'h', i));
}

OscErrCode OscPushChar(osc_element* elt, char c)
{
	return(OscPushInt32Variant(elt, 'c', (int32)c));
}

OscErrCode OscPushFloat(osc_element* elt, float f)
{
	OscErrCode err;
	if((err = OscCheckNextArgSpace(elt, sizeof(f))) != OSC_OK)
	{
		return(err);
	}
	osc_composer* composer = elt->composer;

	*(composer->msg.nextTag) = 'f';
	composer->msg.nextTag--;
	OscFromFloat(composer->msg.nextArg, f);
	composer->msg.nextArg += sizeof(f);
	return(OSC_OK);
}

OscErrCode OscPushDouble(osc_element* elt, double d)
{
	OscErrCode err;
	if((err = OscCheckNextArgSpace(elt, sizeof(d))) != OSC_OK)
	{
		return(err);
	}
	osc_composer* composer = elt->composer;

	*(composer->msg.nextTag) = 'd';
	composer->msg.nextTag--;
	OscFromDouble(composer->msg.nextArg, d);
	composer->msg.nextArg += sizeof(d);
	return(OSC_OK);
}

OscErrCode OscPushStringVariant(osc_element* elt, char tag, u32 len, const char* s)
{
	uint32 usize = len+1;
	uint32 asize = OscRoundUp4(usize);

	OscErrCode err;
	if((err = OscCheckNextArgSpace(elt, asize)) != OSC_OK)
	{
		return(err);
	}
	osc_composer* composer = elt->composer;

	*(composer->msg.nextTag) = tag;
	composer->msg.nextTag--;

	OscStr4CopyAndPadUA(composer->msg.nextArg, s, usize, asize);

	composer->msg.nextArg += asize;
	return(OSC_OK);
}

OscErrCode OscPushString(osc_element* elt, u32 len, const char* s)
{
	//TODO(martin): make sure it's properly static inlined
	return(OscPushStringVariant(elt, 's', len, s));
}

OscErrCode OscPushSymbol(osc_element* elt, u32 len, const char* s)
{
	return(OscPushStringVariant(elt, 'S', len, s));
}

OscErrCode OscPushBlob(osc_element* elt, uint32 blobSize, const char* data)
{
	uint32 alignedBlobSize = OscRoundUp4(blobSize);
	if(!OscIsValidElementSize(alignedBlobSize))
	{
		return(OSC_INVALID_SIZE);
	}

	uint32 size = sizeof(blobSize) + alignedBlobSize;
	OscErrCode err;
	if((err = OscCheckNextArgSpace(elt, size)) != OSC_OK)
	{
		return(err);
	}
	osc_composer* composer = elt->composer;

	*(composer->msg.nextTag) = 'b';
	composer->msg.nextTag--;
	OscFromInt32(composer->msg.nextArg, alignedBlobSize);
	composer->msg.nextArg += sizeof(uint32);
	memcpy(composer->msg.nextArg, data, blobSize);
	composer->msg.nextArg += blobSize;
	uint32 padding = alignedBlobSize - blobSize;
	memset(composer->msg.nextArg, 0, padding);
	composer->msg.nextArg += padding;
	return(OSC_OK);
}

OscErrCode OscPushRgbaColor(osc_element* elt, uint32 col)
{
	return(OscPushInt32Variant(elt, 'r', (int32)col));
}

OscErrCode OscPushMidiMessage(osc_element* elt, uint32 m)
{
	return(OscPushInt32Variant(elt, 'm', (int64)m));
}

OscErrCode OscPushTimeTag(osc_element* elt, uint64 t)
{
	return(OscPushInt64Variant(elt, 't', (int64)t));
}

OscErrCode OscPushElement(osc_element* elt, osc_element* nested)
{
	int32 eltSize = OscElementSize(nested);
	if(!OscIsValidElementSize(eltSize) || !(OscIsMultipleOf4(eltSize)))
	{
		return(OSC_INVALID_SIZE);
	}

	uint32 size = sizeof(eltSize) + eltSize;
	OscErrCode err;
	if((err = OscCheckNextArgSpace(elt, size)) != OSC_OK)
	{
		return(err);
	}
	osc_composer* composer = elt->composer;

	*(composer->msg.nextTag) = OSC_ELEMENT_TYPE_TAG;
	composer->msg.nextTag--;
	OscFromInt32(composer->msg.nextArg, eltSize);
	composer->msg.nextArg += sizeof(uint32);
	memcpy(composer->msg.nextArg, OscElementData(nested), eltSize);
	composer->msg.nextArg += eltSize;

	return(OSC_OK);
}

//---------------------------------------------------------------------------------
// message parsing function
//---------------------------------------------------------------------------------

OscErrCode OscParseMessage(osc_msg* msg, int32 size, const char* packet)
{
	if(!OscIsValidElementSize(size) || !OscIsMultipleOf4(size))
	{
		return(OSC_INVALID_SIZE);
	}

	const char* end = packet + size;

	msg->msgSize = size;
	msg->addressPattern = packet;

	//TODO(martin): Should we rather use OscStr4Size(), then typeString = packet + addressPatternSize ?
	msg->typeString = OscFindStr4End(packet, end);

	if(!msg->typeString)
	{
		return(OSC_INVALID_ADDRESS);
	}
	if(msg->typeString == end)
	{
		//NOTE(martin): no type string, no arguments
		msg->typeString = 0;
		msg->typeStringEnd = 0;
		msg->typeStringSize = 0;
		msg->args = 0;
		msg->argsSize = 0;
		return(OSC_OK);
	}
	if(msg->typeString[0] != ',')
	{
		return(OSC_INVALID_TYPE_STRING);
	}
	msg->args = OscFindStr4End(msg->typeString, end);
	msg->typeStringSize = msg->args - msg->typeString;

	if(!msg->args)
	{
		return(OSC_INVALID_TYPE_STRING);
	}
	if(msg->args == end)
	{
		//NOTE(martin): zero length type string (except for the leading ',') ie no arguments
		msg->typeString = 0;
		msg->typeStringEnd = 0;
		msg->typeStringSize = 0;
		msg->args = 0;
		msg->argsSize = 0;
		return(OSC_OK);
	}

	//NOTE(martin): we have set everything except msg->typeStringEnd and arg size,
	//              which we will set now as part of the validation process :

	const char* arg = msg->args;
	const char* tag = msg->typeString;
	tag++;
	int arrayLevel = 0;

	while(*tag != '\0')
	{
		switch(*tag)
		{
			case TRUE_TYPE_TAG:
			case FALSE_TYPE_TAG:
			case NIL_TYPE_TAG:
			case INFINITUM_TYPE_TAG:
				break;

			case ARRAY_BEGIN_TYPE_TAG:
				++arrayLevel;
				break;

			case ARRAY_END_TYPE_TAG:
				--arrayLevel;
				break;

			case INT32_TYPE_TAG:
			case INT16_TYPE_TAG:
			case FLOAT_TYPE_TAG:
			case CHAR_TYPE_TAG:
			case RGBA_COLOR_TYPE_TAG:
			case MIDI_MESSAGE_TYPE_TAG:

				if(arg == end)
				{
				    return(OSC_ARG_EXCEED_MSG);
				}
				arg += 4;
				break;

			case INT64_TYPE_TAG:
			case TIME_TAG_TYPE_TAG:
			case DOUBLE_TYPE_TAG:
			case OSC_BLITZ_MARKER_TAG:

				if(arg == end)
				{
					return(OSC_ARG_EXCEED_MSG);
				}
				arg += 8;
				break;

			case STRING_TYPE_TAG:
			case SYMBOL_TYPE_TAG:

				if(arg == end)
				{
					return(OSC_ARG_EXCEED_MSG);
				}
				arg = OscFindStr4End(arg, end);
				if(arg == 0)
				{
					return(OSC_BAD_STRING);
				}
				break;

			case BLOB_TYPE_TAG:
			case OSC_ELEMENT_TYPE_TAG:
			{
				if(arg + 4 > end)
				{
					return(OSC_ARG_EXCEED_MSG);
				}
				uint32 blobSize = OscToUInt32(arg);
				//TODO(martin): check element size ?
				arg = arg + 4 + OscRoundUp4(blobSize);
				if(arg > end)
				{
					return(OSC_ARG_EXCEED_MSG);
				}
			} break;

			default:
			{
				return(OSC_UNKNOWN_TYPE);
			}
		}
		if(arg > end)
		{
			return(OSC_ARG_EXCEED_MSG);
		}
		tag++;
	}

	if(arrayLevel !=  0)
	{
		return(OSC_UNMATCHED_ARRAY);
	}

	msg->argsSize = arg - msg->args;
	msg->typeStringEnd = tag;
	int32 argCount = (msg->typeStringEnd - msg->typeString) - 1; //NOTE(martin): account for the ','
	DEBUG_ASSERT(argCount >= 0);
	DEBUG_ASSERT(argCount <= OSC_INT32_MAX);

	return(OSC_OK);
}

OscErrCode OscParsePacket(osc_element* bundle, int32 size, const char* buffer)
{
	bundle->data = (char*)buffer;
	bundle->size = size;
	bundle->composing = 0;
	bundle->composer = 0;
	bundle->msgCache = 0;
	bundle->parent = 0;

	return(OSC_OK);
}

//---------------------------------------------------------------------------------
// bundle iterator functions
//---------------------------------------------------------------------------------

osc_element OscElementsBegin(osc_element* bundle)
{
	if(OscElementIsBundle(bundle) && bundle->size > 16)
	{
		osc_element it;
		int size = OscToInt32(bundle->data + 16);
		OscParsePacket(&it, size, bundle->data + 20);
		it.parent = bundle;
		return(it);
	}
	else
	{
		osc_element it = {0};
		it.parent = bundle;
		it.data = bundle->data+bundle->size;
		it.size = 0;
		return(it);
	}
}
osc_element OscElementsEnd(osc_element* bundle)
{
	osc_element it = {0};
	it.parent = bundle;
	it.data = bundle->data + bundle->size;
	it.size = 0;
	return(it);
}

osc_element* OscElementNext(osc_element* elt)
{
	DEBUG_ASSERT(elt->parent);
	const char* p = elt->data + elt->size;
	const char* parentEnd = elt->parent->data + elt->parent->size;
	if(p >= parentEnd)
	{
		elt->data = parentEnd;
		elt->size = 0;
		return(elt);
	}
	else
	{
		int size = OscToInt32(p);
		if(p+size >= parentEnd)
		{
			//NOTE(martin): that's probably an error
			elt->data = parentEnd;
			elt->size = 0;
			return(elt);
		}
		p += 4;
		elt->data = p;
		elt->size = size;
		return(elt);
	}
}

osc_element* OscElementIteratorCreate(osc_element* bundle)
{
	osc_element* it = malloc_type(osc_element);
	*it = OscElementsBegin(bundle);
	return(it);
}
void OscElementIteratorDestroy(osc_element* iterator)
{
	free(iterator);
}

OscErrCode OscElementAsMessage(osc_element* element, osc_msg** msg)
{
	if(element->msgCache)
	{
		DEBUG_ASSERT(OscElementIsMessage(element));
		*msg = &element->msg;
		return(OSC_OK);
	}
	else
	{
		if(OscElementIsBundle(element))
		{
			return(OSC_NOT_MESSAGE);
		}
		OscErrCode err = OscParseMessage(&element->msg, element->size, element->data);
		if(err == OSC_OK)
		{
			*msg = &element->msg;
		}
		return(err);
	}
}

//TODO(martin): that is not really a faster version, we only forward to the checked version...
osc_msg* OscElementAsMessageUnchecked(osc_element* element)
{
	osc_msg* msg = 0;
	return(OscElementAsMessage(element, &msg) == OSC_OK ? msg : 0);
}



//---------------------------------------------------------------------------------
// argument iterator functions
//---------------------------------------------------------------------------------

osc_arg_iterator* OscArgumentCreate(osc_msg* msg)
{
	osc_arg_iterator* arg = malloc_type(osc_arg_iterator);
	*arg = OscArgumentsBegin(msg);
	return(arg);
}

void OscArgumentDestroy(osc_arg_iterator* arg)
{
	free(arg);
}

OscErrCode OscAsBool(osc_arg_iterator* arg, int* b)
{
	if(!arg->tag || *(arg->tag) == 0)
	{
		return(OSC_MISSING_ARG);
	}
	else if(*(arg->tag) == TRUE_TYPE_TAG)
	{
		*b = 1;
		return(OSC_OK);
	}
	else if(*(arg->tag) == FALSE_TYPE_TAG)
	{
		*b = 0;
		return(OSC_OK);
	}
	else
	{
		return(OSC_WRONG_TYPE);
	}
}

OscErrCode OscAsInt16(osc_arg_iterator* arg, int16* i)
{
	if(!arg->tag || *(arg->tag) == 0)
	{
		return(OSC_MISSING_ARG);
	}
	else if(*(arg->tag) == INT16_TYPE_TAG)
	{
		*i = OscAsInt16Unchecked(arg);
		return(OSC_OK);
	}
	else
	{
		return(OSC_WRONG_TYPE);
	}
}


OscErrCode OscAsInt32(osc_arg_iterator* arg, int32* i)
{
	if(!arg->tag || *(arg->tag) == 0)
	{
		return(OSC_MISSING_ARG);
	}
	else if(*(arg->tag) == INT32_TYPE_TAG)
	{
		*i = OscAsInt32Unchecked(arg);
		return(OSC_OK);
	}
	else
	{
		return(OSC_WRONG_TYPE);
	}
}

OscErrCode OscAsInt64(osc_arg_iterator* arg, int64* i)
{
	if(!arg->tag || *(arg->tag) == 0)
	{
		return(OSC_MISSING_ARG);
	}
	else if(*(arg->tag) == INT64_TYPE_TAG)
	{
		*i = OscAsInt64Unchecked(arg);
		return(OSC_OK);
	}
	else
	{
		return(OSC_WRONG_TYPE);
	}
}

OscErrCode OscAsFloat(osc_arg_iterator* arg, float* f)
{
	if(!arg->tag || *(arg->tag) == 0)
	{
		return(OSC_MISSING_ARG);
	}
	else if(*(arg->tag) == FLOAT_TYPE_TAG)
	{
		*f = OscAsFloatUnchecked(arg);
		return(OSC_OK);
	}
	else
	{
		return(OSC_WRONG_TYPE);
	}
}

OscErrCode OscAsDouble(osc_arg_iterator* arg, double* d)
{
	if(!arg->tag || *(arg->tag) == 0)
	{
		return(OSC_MISSING_ARG);
	}
	else if(*(arg->tag) == DOUBLE_TYPE_TAG )
	{
		*d = OscAsDoubleUnchecked(arg);
		return(OSC_OK);
	}
	else
	{
		return(OSC_WRONG_TYPE);
	}
}

OscErrCode OscAsChar(osc_arg_iterator* arg, char* c)
{
	if(!arg->tag || *(arg->tag) == 0)
	{
		return(OSC_MISSING_ARG);
	}
	else if(*(arg->tag) == CHAR_TYPE_TAG )
	{
		*c = OscAsCharUnchecked(arg);
		return(OSC_OK);
	}
	else
	{
		return(OSC_WRONG_TYPE);
	}
}

OscErrCode OscAsString(osc_arg_iterator* arg, const char** s)
{
	if(!arg->tag || *(arg->tag) == 0)
	{
		return(OSC_MISSING_ARG);
	}
	else if(*(arg->tag) == STRING_TYPE_TAG )
	{
		*s = arg->data;
		return(OSC_OK);
	}
	else
	{
		return(OSC_WRONG_TYPE);
	}
}

OscErrCode OscAsSymbol(osc_arg_iterator* arg, const char** s)
{
	return(OscAsString(arg, s));
}

OscErrCode OscAsBlob(osc_arg_iterator* arg, const void** data, uint32* size)
{
	if(!arg->tag || *(arg->tag) == 0)
	{
		return(OSC_MISSING_ARG);
	}
	else if(*(arg->tag) == BLOB_TYPE_TAG)
	{
		return(OscAsBlobUnchecked(arg, data, size));
	}
	else
	{
		return(OSC_WRONG_TYPE);
	}
}

OscErrCode OscAsElement(osc_arg_iterator* arg, osc_element* element)
{
	if(!arg->tag || *(arg->tag) == 0)
	{
		return(OSC_MISSING_ARG);
	}
	else if(*(arg->tag) == OSC_ELEMENT_TYPE_TAG)
	{
		return(OscAsElementUnchecked(arg, element));
	}
	else
	{
		return(OSC_WRONG_TYPE);
	}
}

OscErrCode OscAsRgbaColor(osc_arg_iterator* arg, uint32* col)
{
	if(!arg->tag || *(arg->tag) == 0)
	{
		return(OSC_MISSING_ARG);
	}
	else if(*(arg->tag) == RGBA_COLOR_TYPE_TAG)
	{
		*col = OscAsRgbaColorUnchecked(arg);
		return(OSC_OK);
	}
	else
	{
		return(OSC_WRONG_TYPE);
	}
}


OscErrCode OscAsMidiMessage(osc_arg_iterator* arg, uint32* msg)
{
	if(!arg->tag || *(arg->tag) == 0)
	{
		return(OSC_MISSING_ARG);
	}
	else if(*(arg->tag) == MIDI_MESSAGE_TYPE_TAG )
	{
		*msg = OscAsMidiMessageUnchecked(arg);
		return(OSC_OK);
	}
	else
	{
		return(OSC_WRONG_TYPE);
	}
}

OscErrCode OscAsTimeTag(osc_arg_iterator* arg, uint64* t)
{
	if(!arg->tag || *(arg->tag) == 0)
	{
		return(OSC_MISSING_ARG);
	}
	else if(*(arg->tag) == TIME_TAG_TYPE_TAG )
	{
		*t = OscAsTimeTagUnchecked(arg);
		return(OSC_OK);
	}
	else
	{
		return(OSC_WRONG_TYPE);
	}
}

//---------------------------------------------------------------------------------
// variadic and array-based composing / parsing functions
//---------------------------------------------------------------------------------

OscErrCode OscPushFormatVA(osc_element* elt, const char* typeTags, va_list ap)
{
	OscErrCode err = OSC_OK;
	if(!typeTags)
	{
		return(OSC_OK);
	}
	while(*typeTags != 0)
	{
		switch(*typeTags)
		{
			case TRUE_TYPE_TAG:
				err = OscPushBool(elt, 1);
				break;
			case FALSE_TYPE_TAG:
				err = OscPushBool(elt, 0);
				break;
			case NIL_TYPE_TAG:
				err = OscPushNil(elt);
				break;
			case INFINITUM_TYPE_TAG:
				err = OscPushInfinity(elt);
				break;
			case ARRAY_BEGIN_TYPE_TAG:
				err = OscArrayBegin(elt);
				break;
			case ARRAY_END_TYPE_TAG:
				err = OscArrayEnd(elt);
				break;
			case INT16_TYPE_TAG:
				err = OscPushInt16(elt, va_arg(ap, int32));
				break;
			case INT32_TYPE_TAG:
				err = OscPushInt32(elt, va_arg(ap, int32));
				break;
			case FLOAT_TYPE_TAG:
				err = OscPushFloat(elt, va_arg(ap, double));
				break;
			case CHAR_TYPE_TAG:
				err = OscPushChar(elt, va_arg(ap, int32));
				break;
			case RGBA_COLOR_TYPE_TAG:
				err = OscPushRgbaColor(elt, va_arg(ap, int32));
				break;
			case MIDI_MESSAGE_TYPE_TAG:
				err = OscPushMidiMessage(elt, va_arg(ap, int32));
				break;
			case INT64_TYPE_TAG:
				err = OscPushInt64(elt, va_arg(ap, int64));
				break;
			case TIME_TAG_TYPE_TAG:
				err = OscPushTimeTag(elt, va_arg(ap, uint64));
				break;
			case DOUBLE_TYPE_TAG:
				err = OscPushDouble(elt, va_arg(ap, double));
				break;
			case STRING_TYPE_TAG:
			{
				char* arg = va_arg(ap, char*);
				err = OscPushString(elt, strlen(arg), arg);
			} break;
			case SYMBOL_TYPE_TAG:
			{
				char* arg = va_arg(ap, char*);
				err = OscPushSymbol(elt, strlen(arg), arg);
			} break;
			case BLOB_TYPE_TAG:
				err = OscPushBlob(elt, va_arg(ap, uint32), va_arg(ap, char*));

				break;
			case OSC_BLITZ_MARKER_TAG:
				err = OscPushInt64Variant(elt, 'X', OSC_BLITZ_MARKER_VALUE);
				break;

			case OSC_ELEMENT_TYPE_TAG:
				err = OscPushElement(elt, va_arg(ap, osc_element*));
				break;
			default:
				LOG_ERROR("Unkown type tag '%c'\n", *typeTags);
				err = OSC_UNKNOWN_TYPE;
				break;
		}
		if(err != OSC_OK)
		{
			return(err);
		}
		typeTags++;
	}
	return(err);
}

OscErrCode OscPushFormat(osc_element* elt, const char* typeTags, ...)
{
	va_list ap;
	va_start(ap, typeTags);
	OscErrCode err = OscPushFormatVA(elt, typeTags, ap);
	va_end(ap);
	return(err);
}

OscErrCode OscPushA(osc_element* elt, int argc, osc_argument* argv)
{
	for(int i=0; i<argc; i++)
	{
		OscErrCode err = OscPushArgument(elt, &(argv[i]));
		if(err != OSC_OK)
		{
			return(err);
		}
	}
	return(OSC_OK);
}

//NOTE(martin): insert a message into a packet with a single function

OscErrCode OscFormatMessageVA(osc_element* elt, const char* addressPattern, const char* typeTags, va_list ap)
{
	/* NOTE(martin):
		Here we use OscFormatVA() to directly compose the message inside our parent composer. It leverages the
		fact that OscFormatVA() can be optimized by having the a priori knowledge of the type string.
	*/

	DEBUG_ASSERT(elt);
	DEBUG_ASSERT(elt->composing); // or return compose error ?

	//NOTE(martin): compute size and start of the new composing region

	int32 size;
	char* data;
	OscGetComposerAvailableSpace(elt, &size, &data);

	//NOTE(martin): compose the nested message as if we were composing a fresh packet
	osc_element msg;
	OscErrCode err = OscFormatVA(&msg, size, data, addressPattern, typeTags, ap);
	if(err != OSC_OK)
	{
		return(err);
	}

	//NOTE(martin): update parent composer, if there is one
	int32 childSize = OscElementSize(&msg);
	OscUpdateComposer(elt, elt->composer, childSize);
	return(OSC_OK);
}

OscErrCode OscFormatMessage(osc_element* elt, const char* pattern, const char* typeTags, ...)
{
	va_list ap;
	va_start(ap, typeTags);
	OscErrCode err = OscFormatMessageVA(elt, pattern, typeTags, ap);
	va_end(ap);
	return(err);
}

OscErrCode OscFormatMessageA(osc_element* elt, const char* pattern, int argc, osc_argument* argv)
{
	//TODO(martin): use the types directly to avoid much copying...
	OscErrCode err = OSC_OK;
	if((err = OscBeginMessage(elt, strlen(pattern), pattern)) != OSC_OK)
	{
		return(err);
	}
	if((err = OscPushA(elt, argc, argv)) != OSC_OK)
	{
		return(err);
	}
	return(OscEndMessage(elt));
}

//NOTE(martin): format a message packet with a single function

OscErrCode OscFormatVA(osc_element* elt, uint32 size, char* buffer, const char* pattern, const char* typeTags, va_list ap)
{
	/* NOTE(martin):
		Here we duplicate some code instead of reusing OscPushFormatVA(), because it allows to leverage
		our a priori knowledge of the type tags and compose the message directly. In doing so we avoid the
		memmove of the arguments and the reverse copy of the type string which occurs in OscEndMessage()
		which provide a significant speed gain.
	*/
	DEBUG_ASSERT(elt);
	DEBUG_ASSERT(pattern);

	if(*pattern == '#' || *pattern == '\0')
	{
		return(OSC_INVALID_ADDRESS);
	}

	//NOTE(martin): compute pattern and type string sizes
	uint32 patternUSize, patternASize;
	OscStr4UASizes(pattern, &patternUSize, &patternASize);

	int32 typeTagsSize = typeTags ? strlen(typeTags) : 0;
	int32 typeStringSize = typeTags ? OscRoundUp4(typeTagsSize+2) : 0;
	int32 argIndex = patternASize + typeStringSize;

	//NOTE(martin): we check that pattern+typeString can fit. Arguments are checked separately
	if(argIndex > size)
	{
		return(OSC_BUFFER);
	}

	//NOTE(martin): init osc_element struct
	memset((void*)elt, 0, sizeof(osc_element));
	elt->data = buffer;

	//NOTE(martin): copy pattern and typeString
	//OscStr4Copy(buffer, pattern);
	OscStr4CopyAndPadUA(buffer, pattern, patternUSize, patternASize);

	if(!typeTags)
	{
		elt->size = patternASize;
		return(OSC_OK);
	}
	//NOTE(martin): here we have to copy one byte further, with and aligned size reduced by one,
	//		since we insert the first ',' character manually
	OscStr4CopyAndPadUA(buffer+patternASize+1, typeTags, typeTagsSize+1, typeStringSize-1);

	//NOTE(martin): The order is important here, because OscStrCopyAndPadUA() overwrites the last four bytes of its dst buffer
	//		If the type string is four bytes length, the ',' would be overwritten !
	buffer[patternASize] = ',';

	//NOTE(martin): loop thru the arguments and push them side by side

	char* nextArg = buffer + argIndex;
	while(*typeTags != '\0')
	{
		int32 remSpace = buffer + size - nextArg;
		switch(*typeTags)
		{
			case ARRAY_BEGIN_TYPE_TAG:
			case ARRAY_END_TYPE_TAG:
			case TRUE_TYPE_TAG:
			case FALSE_TYPE_TAG:
			case NIL_TYPE_TAG:
			case INFINITUM_TYPE_TAG:
				break;
			case CHAR_TYPE_TAG:
				if(remSpace < 4) { return(OSC_BUFFER); }
				OscFromInt32(nextArg, (int32)va_arg(ap, int)); //NOTE(martin): argument is promoted to int
				nextArg += 4;
				break;
			case INT16_TYPE_TAG:
				if(remSpace < 4) { return(OSC_BUFFER); }
				OscFromInt16(nextArg, (int16)va_arg(ap, int)); //NOTE(martin): argument is promoted to int
				nextArg += 4;
				break;
			case INT32_TYPE_TAG:
			case RGBA_COLOR_TYPE_TAG:
			case MIDI_MESSAGE_TYPE_TAG:
				if(remSpace < 4) { return(OSC_BUFFER); }
				OscFromInt32(nextArg, va_arg(ap, int32));
				nextArg += 4;
				break;
			case INT64_TYPE_TAG:
			case TIME_TAG_TYPE_TAG:
				if(remSpace < 8) { return(OSC_BUFFER); }
				OscFromInt64(nextArg, va_arg(ap, int64));
				nextArg += 8;
				break;
			case FLOAT_TYPE_TAG:
				if(remSpace < 4) { return(OSC_BUFFER); }
				OscFromFloat(nextArg, (float)va_arg(ap, double)); //NOTE(martin): argument is promoted to double
				nextArg += 4;
				break;
			case DOUBLE_TYPE_TAG:
				if(remSpace < 8) { return(OSC_BUFFER); }
				OscFromDouble(nextArg, va_arg(ap, double));
				nextArg += 8;
				break;
			case STRING_TYPE_TAG:
			case SYMBOL_TYPE_TAG:
			{
				const char* s = va_arg(ap, char*);
				uint32 usize, asize;
				OscStr4UASizes(s, &usize, &asize);

				if(remSpace < asize) { return(OSC_BUFFER); }

				OscStr4CopyAndPadUA(nextArg, s, usize, asize);
				nextArg += asize;
			} break;

			case BLOB_TYPE_TAG:
			{
				int32 blobSize = va_arg(ap, int32);
				char* data = va_arg(ap, char*);
				if(!OscIsValidElementSize(blobSize))
				{
					return(OSC_INVALID_SIZE);
				}
				int32 alignedBlobSize = OscRoundUp4(blobSize);

				uint32 totalSize = sizeof(blobSize) + alignedBlobSize;
				if(remSpace < totalSize)
				{
					return(OSC_BUFFER);
				}
				OscFromInt32(nextArg, alignedBlobSize);
				nextArg += sizeof(uint32);
				*(uint32*)(nextArg + alignedBlobSize - 4) = 0;
				memcpy(nextArg, data, blobSize);
				nextArg += alignedBlobSize;
			} break;

			case OSC_BLITZ_MARKER_TAG:
				if(remSpace < 8) { return(OSC_BUFFER); }
				*(uint64*)nextArg = OSC_BLITZ_MARKER;
				nextArg += 8;
				break;
			case OSC_ELEMENT_TYPE_TAG:
			{
				osc_element* nested = va_arg(ap, osc_element*);
				int32 eltSize = OscElementSize(nested);
				if(!OscIsValidElementSize(eltSize) || !(OscIsMultipleOf4(eltSize)))
				{
					return(OSC_INVALID_SIZE);
				}

				uint32 totalSize = sizeof(eltSize) + eltSize;
				if(remSpace < totalSize)
				{
					return(OSC_BUFFER);
				}
				OscFromInt32(nextArg, eltSize);
				nextArg += sizeof(uint32);
				memcpy(nextArg, OscElementData(nested), eltSize);
				nextArg += eltSize;
			} break;

			default:
				return(OSC_UNKNOWN_TYPE);
		}
		typeTags++;
	}
	elt->size = nextArg - buffer;
	return(OSC_OK);
}

OscErrCode OscFormat(osc_element* elt, uint32 size, char* buffer, const char* pattern, const char* typeTags, ...)
{
	va_list ap;
	va_start(ap, typeTags);
	OscErrCode err = OscFormatVA(elt, size, buffer, pattern, typeTags, ap);
	va_end(ap);
	return(err);
}

OscErrCode OscFormatA(osc_element* elt, uint32 size, char* buffer, const char* pattern, int argc, osc_argument* argv)
{
	OscErrCode err = OSC_OK;
	if((err = OscBeginPacket(elt, size, buffer)))
	{
		return(err);
	}
	if((err = OscFormatMessageA(elt, pattern, argc, argv)) != OSC_OK)
	{
		return(err);
	}
	return(OscEndPacket(elt));
}

OscErrCode OscScanArgumentsVA(osc_msg* msg, const char* typeTags, va_list ap)
{
	DEBUG_ASSERT(msg);
	DEBUG_ASSERT(typeTags);

	const char* msgTags = OscTypeTags(msg);
	if(!msgTags)
	{
		return(OSC_INVALID_TYPE_STRING);
	}

	const char* arg = msg->args;
	while(*typeTags)
	{
		//TODO(martin): is it the behaviour we need ?
		//		ie. shouldn't we have an "unspecified bool" tag that matches T and F?
		if(*msgTags != *typeTags)
		{
			return(OSC_INVALID_TYPE_STRING);
		}
		switch(*typeTags)
		{
			case TRUE_TYPE_TAG:
				*va_arg(ap, int*) = 1;
				break;
			case FALSE_TYPE_TAG:
				*va_arg(ap, int*) = 0;
				break;
			case NIL_TYPE_TAG:
			case INFINITUM_TYPE_TAG:
			case ARRAY_BEGIN_TYPE_TAG:
			case ARRAY_END_TYPE_TAG:
				break;

			case INT16_TYPE_TAG:
				*va_arg(ap, int16*) = OscToInt16(arg);
				arg += 4;
				break;

			case INT32_TYPE_TAG:
			case FLOAT_TYPE_TAG:
			case CHAR_TYPE_TAG:
			case RGBA_COLOR_TYPE_TAG:
			case MIDI_MESSAGE_TYPE_TAG:
				OscToNative32((char*)va_arg(ap, int32*), arg);
				arg += 4;
				break;

			case INT64_TYPE_TAG:
			case TIME_TAG_TYPE_TAG:
			case DOUBLE_TYPE_TAG:
			case OSC_BLITZ_MARKER_TAG:
				OscToNative64((char*)va_arg(ap, int64*), arg);
				arg += 8;
				break;

			case STRING_TYPE_TAG:
			case SYMBOL_TYPE_TAG:
				*va_arg(ap, const char**) = arg;
				arg = OscFindStr4End(arg, arg + msg->argsSize);
				break;

			//TODO(martin): is it the behaviour we need for boolean tags ?


			case BLOB_TYPE_TAG:
			{
				uint32* blobSize = va_arg(ap, uint32*);
				const void** blob = va_arg(ap, const void**);

				*blobSize = OscToInt32(arg);
				arg += 4;
				*blob = arg;
				arg += *blobSize;
			} break;

			case OSC_ELEMENT_TYPE_TAG:
			{
				osc_element* nested = va_arg(ap, osc_element*);

				int32 size = OscToInt32(arg);
				arg += 4;
				OscErrCode err = OscParsePacket(nested, size, arg);
				if(err != OSC_OK)
				{
					return(err);
				}
				arg += size;
			} break;

			default:
				LOG_ERROR("Unkown type tag '%c'\n", *typeTags);
				return(OSC_UNKNOWN_TYPE);
		}
		typeTags++;
		msgTags++;
	}
	return(OSC_OK);
}

OscErrCode OscScanArguments(osc_msg* msg, const char* typeTags, ...)
{
	va_list ap;
	va_start(ap, typeTags);
	OscErrCode err = OscScanArgumentsVA(msg, typeTags, ap);
	va_end(ap);
	return(err);
}

OscErrCode OscScanVA(osc_msg* msg, const char* address, const char* typeTags, va_list ap)
{
	if(strcmp(OscAddressPattern(msg), address))
	{
		return(OSC_INVALID_ADDRESS);
	}
	return(OscScanArgumentsVA(msg, typeTags, ap));
}

OscErrCode OscScan(osc_msg* msg, const char* address, const char* typeTags, ...)
{
	if(!typeTags)
	{
		if(strcmp(OscAddressPattern(msg), address))
		{
			return(OSC_INVALID_ADDRESS);
		}
		else
		{
			return(OSC_OK);
		}
	}
	else
	{
		va_list ap;
		va_start(ap, typeTags);
		OscErrCode err = OscScanVA(msg, address, typeTags, ap);
		va_end(ap);
		return(err);
	}
}

//---------------------------------------------------------------------------------
// debug/print functions
//---------------------------------------------------------------------------------

void OscArgumentsPrint(const char* typeTags, const char* args)
{
	osc_arg_iterator arg;
	arg.tag = typeTags;
	arg.data = args;
	while(*arg.tag != '\0')
	{
		if(arg.data != args)
		{
			printf(", ");
		}

		switch(*arg.tag)
		{
			case TRUE_TYPE_TAG:         printf("TRUE"); break;
			case FALSE_TYPE_TAG:        printf("FALSE"); break;
			case NIL_TYPE_TAG:          printf("NIL"); break;
			case INFINITUM_TYPE_TAG:    printf("INFINITY"); break;
			case ARRAY_BEGIN_TYPE_TAG:  printf("["); break;
			case ARRAY_END_TYPE_TAG:    printf("]"); break;
			case INT16_TYPE_TAG:        printf("int16 %i", OscAsInt16Unchecked(&arg)); break;
			case INT32_TYPE_TAG:        printf("int32 %i", OscAsInt32Unchecked(&arg)); break;
			case FLOAT_TYPE_TAG:        printf("float %f", OscAsFloatUnchecked(&arg)); break;
			case CHAR_TYPE_TAG:         printf("char %c", OscAsCharUnchecked(&arg)); break;
			case RGBA_COLOR_TYPE_TAG:   printf("rgba %i", OscAsInt32Unchecked(&arg)); break;
			case MIDI_MESSAGE_TYPE_TAG: printf("midi %i", OscAsInt32Unchecked(&arg)); break;
			case INT64_TYPE_TAG:        printf("int64 %lli", OscAsInt64Unchecked(&arg)); break;
			case TIME_TAG_TYPE_TAG:     printf("time %llu", (uint64)OscAsInt64Unchecked(&arg)); break;
			case DOUBLE_TYPE_TAG:       printf("double %f", OscAsDoubleUnchecked(&arg)); break;
			case STRING_TYPE_TAG:       printf("\"%s\"", OscAsStringUnchecked(&arg)); break;
			case SYMBOL_TYPE_TAG:       printf("\"%s\"", OscAsStringUnchecked(&arg)); break;

			case BLOB_TYPE_TAG:
			{
				uint32 blobSize;
				const void* blob;
				OscAsBlobUnchecked(&arg, &blob, &blobSize);
				printf("blob (size: %i)", blobSize);
			} break;

			case OSC_BLITZ_MARKER_TAG:
			{
				const char* version = (arg.data + 4);
				const char* mark = arg.data;

				printf("Blitz mark %c%c%c%c, version %i.%i.%i\n",
				       mark[0], mark[1], mark[2], mark[3],
				       version[1], version[2], version[3]);
			} break;
			case OSC_ELEMENT_TYPE_TAG:
			{
				osc_element nested;
				if(OscAsElementUnchecked(&arg, &nested) != 0)
				{
					LOG_ERROR("(can't parse nested bundle element)");
				}
				else
				{
					OscElementPrint(&nested);
				}

			} break;

			default: printf("UNKNOWN");
		}
		OscArgumentNext(&arg);
	}
	printf("\n");
}

void OscMessagePrint(osc_msg* msg)
{
	printf("%s", msg->addressPattern);
	if(msg->typeString)
	{
		printf(", %s : ", msg->typeString+1);
		OscArgumentsPrint(msg->typeString+1, msg->args);
	}
	else
	{
		printf("\n");
	}
}

#define PrintIndent(level)			\
	{					\
		for(int i=0; i<level; i++)	\
		{				\
			printf("  ");		\
		}				\
	}

OscErrCode OscElementPrintIndented(osc_element* element, int indentLevel)
{
	if(OscElementIsBundle(element))
	{
		PrintIndent(indentLevel);
		printf("#bundle (timetag %lli): {\n", OscToInt64(element->data+8));

		for(osc_element it = OscElementsBegin(element);
		    !OscIsAtEndOfBundle(element, &it);
		    OscElementNext(&it))
		{
			//printf("element size : %i\n", it.size);

			OscErrCode err = OscElementPrintIndented(&it, indentLevel + 1);
			if(err != OSC_OK)
			{
				return(err);
			}
		}

		PrintIndent(indentLevel);
		printf("}\n");
	}
	else
	{
		osc_msg* msg;
		OscErrCode err = OscElementAsMessage(element, &msg);
		if(err != OSC_OK)
		{
			OscPrintError(err);
			return(err);
		}
		PrintIndent(indentLevel);
		OscMessagePrint(msg);
	}
	return(OSC_OK);
}


OscErrCode OscElementCopy(osc_element* dst, uint32 size, char* buffer, osc_element* src)
{
	uint32 srcSize = OscElementSize(src);
	if(srcSize > size)
	{
		return(OSC_BUFFER);
	}

	memcpy(buffer, OscElementData(src), srcSize);
	memset(dst, 0, sizeof(osc_element));
	dst->size = srcSize;
	dst->data = buffer;

	return(OSC_OK);
}

#undef LOG_SUBSYSTEM // "OSC"

#ifdef __cplusplus
} // extern "C"
#endif //__cplusplus
