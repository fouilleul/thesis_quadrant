#/bin/bash

for file in ./saves/*.ql ; do
	echo "updating $file"
	./bin/test ql-update "$file" "$file"
done
