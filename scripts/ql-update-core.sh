#/bin/bash

for file in ./core/*.ql ; do
	echo "updating $file"
	./bin/test ql-update "$file" "$file"
done
