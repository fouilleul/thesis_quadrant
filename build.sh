#!/bin/bash


#--------------------------------------------------------------
# set target
#--------------------------------------------------------------

target="$1"
if [ -z $target ] ; then
	target='qed'
fi

target=$(echo $target | tr '[:upper:]' '[:lower:]')

#--------------------------------------------------------------
# Set paths
#--------------------------------------------------------------

BinDir=./bin
ResDir=./resources
SrcDir=./src
MilePostDir=./MilePost
MilePostSrcDir=./$MilePostDir/src

#--------------------------------------------------------------
# Build
#--------------------------------------------------------------

if [ $target = milepost ] ; then
	pushd $MilePostDir
	./build.sh lib "$2"
	popd
	cp $MilePostDir/resources/shader.metallib $ResDir/
else

	if [ ! -e $BinDir ] ; then
		mkdir $BinDir
	fi

	MilePostIncludeDirs=($(find $MilePostSrcDir -type d))
	MilePostIncludes=${MilePostIncludeDirs[@]/#/-I}

	INCLUDES="$MilePostIncludes -Idlmalloc -I$SrcDir "
	LIBS="-framework Carbon -framework Cocoa -framework Metal -framework QuartzCore -L./MilePost/bin -lmilepost -lffi"
	SRC="$SrcDir/main.cpp"
	FLAGS="-mmacos-version-min=10.15.4 -g -O0 -DDEBUG -maes"

	clang++ -o $BinDir/quadrant $FLAGS $INCLUDES $LIBS $SRC

fi
