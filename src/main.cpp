#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<ctype.h>

#define LOG_DEFAULT_LEVEL LOG_LEVEL_DEBUG
#define LOG_COMPILE_DEBUG

#include"milepost.h"

#include"x64_sysv_fibers.cpp"
#include"sched_curves.cpp"
#include"scheduler.cpp"
#include"ringbuffer.cpp"
#include"hashmap.cpp"

#include"workspace.cpp"
#include"cells.cpp"
#include"tempo_editor.cpp"
#include"editor.cpp"
#include"parser.cpp"
#include"checker.cpp"
#include"generator.cpp"
#include"vm.cpp"
#include"debug.cpp"
#include"save.cpp"

#define ONLY_MSPACES 1
#include"dlmalloc.c"

#define LOG_SUBSYSTEM "Main"

//------------------------------------------------------------------------------------
// App struct
//------------------------------------------------------------------------------------

typedef enum { Q_CLI_RUN = 0, Q_CLI_UPDATE } q_cli_command;

typedef struct q_cli_options
{
	q_cli_command command;
	bool headless;
	bool offline;
	mp_string in;
	mp_string out;

	bool error;
} q_cli_options;

typedef struct source_map
{
	q_hashmap offsetToLoc;
	q_hashmap moduleIdToPath;
	q_hashmap idToOffset;
} source_map;

typedef struct q_app
{
	q_cli_options options;

	// GUI
	mp_window window;
	mp_graphics_surface surface;
	mp_graphics_context graphics;
	mp_graphics_font font;
	f32 fontSize;
	f32 fontScale;
	mp_graphics_font_extents fontExtents;
	mp_gui_style style;
	mp_gui_io input;
	mp_gui_context* gui;


	i32 windowWidth;
	i32 windowHeight;
	f32 sourceHeightRatio;

	//TODO: Could recompute from ratio
	i32 sourceViewWidth;
	i32 sourceViewHeight;

	// Editor/Compiler
	q_workspace workspace;
	q_editor editor;

	//NOTE: VM
	_Atomic(bool) vmRunning;
	platform_thread* vmThread;
	vm_info vm;

	q_ringbuffer commandBuffer;
	q_ringbuffer feedbackBuffer;
	q_ringbuffer outputBuffer;

	//NOTE: console buffer
	char* consoleBuffer;
	u32 consoleBufferSize;
	u32 consoleBufferCap;

	source_map sourceMap;
	q_hashmap targetMap;

} q_app;

//------------------------------------------------------------------------------------------
// Utility printing proc
//------------------------------------------------------------------------------------------
//TODO move elsewhere?
vec2 q_print_string(mp_gui_context* gui, mp_string string, vec2 pos, mp_graphics_color color)
{
	//NOTE(martin): print a string using current font, advance x,y to next position.
	//WARN(martin): (x, y) coordinates are the top-left of the line (ie y is not the text baseline)

	mp_graphics_context graphics = mp_gui_get_graphics(gui);
	mp_gui_style* style = mp_gui_style_top(gui);

	mp_graphics_font_extents fontExtents = {};
	mp_graphics_font_get_extents(graphics, style->font, &fontExtents);
	f32 fontScale = mp_graphics_font_get_scale_for_em_pixels(graphics, style->font, style->fontSize);

	f32 baseLine = pos.y + fontScale * fontExtents.ascent;

	mp_graphics_set_font(graphics, style->font);
	mp_graphics_set_font_size(graphics, style->fontSize);
	mp_graphics_set_color(graphics, color);

	mp_graphics_move_to(graphics, pos.x, baseLine);
	mp_graphics_text_outlines(graphics, string);
	mp_graphics_fill(graphics);

	mp_graphics_get_current_position(graphics, &pos.x, &pos.y);
	pos.y -= fontScale * fontExtents.ascent;

	return(pos);
}

//------------------------------------------------------------------------------------------
// GUI panels layout
//------------------------------------------------------------------------------------------
//NOTE(martin): source fixed (for now) colors
const mp_graphics_color Q_SOURCE_BG_COLOR            = {0.01, 0.01, 0.01, 1},
                        Q_CUE_ENTRY_BG_COLOR          = {0.3, 0.3, 0.3, 1},
                        Q_CUE_ENTRY_SEL_COLOR         = {0.32549, 0.345098, 0.5, 1.0},
                        Q_GROUP_OUTLINE_COLOR         = {1, 0, 0, 1},
                        Q_GROUP_COLLAPSE_BUTTON_COLOR = {1, 1, 1, 1},
                        Q_CUE_STATUS_VALID_COLOR      = {0, 0.7, 0, 1},
                        Q_CUE_STATUS_ACTIVE_COLOR     = {0, 1, 0, 1},
                        Q_CUE_ENTRY_TEXT_COLOR        = {1, 1, 1, 1},
                        Q_COLOR_CLEAR_BG              = {0, 0, 0, 1},
                        Q_COLOR_VIEW_BG               = {0.1, 0.1, 0.1, 1};

//NOTE(martin): default gui dimensions
const f32 Q_WINDOW_DEFAULT_WIDTH = 2400,
          Q_WINDOW_DEFAULT_HEIGHT = 1350,
          Q_WINDOW_MARGIN = 10,
          Q_STATUS_HEIGHT = 70,
          Q_SOURCE_TO_CONSOLE_DEFAULT_RATIO = 0.8;

void q_compute_view_rects(q_app* app, mp_aligned_rect* statusRect, mp_aligned_rect* sourceRect, mp_aligned_rect* consoleRect)
{
	f32 viewSpaceX = Q_WINDOW_MARGIN;
	f32 viewSpaceY = Q_WINDOW_MARGIN;
	f32 viewSpaceW = app->windowWidth - 2*Q_WINDOW_MARGIN;
	f32 viewSpaceH = app->windowHeight - Q_STATUS_HEIGHT - 3*Q_WINDOW_MARGIN;

	f32 sourceH = viewSpaceH * app->sourceHeightRatio - 0.5*Q_WINDOW_MARGIN;
	f32 consoleH = viewSpaceH - sourceH - Q_WINDOW_MARGIN;

	*sourceRect = (mp_aligned_rect){ viewSpaceX, viewSpaceY, viewSpaceW, sourceH};
	*consoleRect = (mp_aligned_rect){ viewSpaceX, viewSpaceY + sourceH + Q_WINDOW_MARGIN, viewSpaceW, consoleH};

	*statusRect = (mp_aligned_rect){viewSpaceX, viewSpaceY + viewSpaceH + Q_WINDOW_MARGIN, viewSpaceW, Q_STATUS_HEIGHT};

	app->sourceViewWidth = viewSpaceW;
	app->sourceViewHeight = sourceH;
}

void q_recompute_view_ratios(q_app* app, mp_aligned_rect sourceRect, f32 deltaW, f32 deltaH)
{
	//NOTE(martin): deltaW and deltaH are the increase in width and height of source view.
	//NOTE(martin): first compute view space width and height, which are the dimensions of the
	//              space occupied by resizeable views (ie excluding the toolbar).
	f32 viewSpaceW = app->windowWidth - 2*Q_WINDOW_MARGIN;
	f32 viewSpaceH = app->windowHeight - Q_STATUS_HEIGHT - 3*Q_WINDOW_MARGIN;

	//NOTE(martin): computes the new dimensions of views
	//             (splitting the inner margin between the two views)
	f32 sourceH = sourceRect.h + deltaH + 0.5*Q_WINDOW_MARGIN;
	f32 consoleH = viewSpaceH - sourceH;

	//TODO(martin): adjust view to honor minimum heights

	//NOTE(martin): recompute ratios
	app->sourceHeightRatio = sourceH/ viewSpaceH;
}

//------------------------------------------------------------------------------------------
// Status bar GUI
//------------------------------------------------------------------------------------------
mp_string status_shorten_module_path(mp_string path, u32 maxLen)
{
	if(path.len > maxLen)
	{
		path = mp_string_slice(path, path.len - maxLen, path.len);
		mp_string_list list = {};
		mp_string_list_push(mem_scratch(), &list, mp_string_lit("..."));
		mp_string_list_push(mem_scratch(), &list, path);
		path = mp_string_list_join(mem_scratch(), list);
	}
	return(path);
}

typedef struct module_select_result
{
	f32 xAdvance;
	bool clicked;
	qed_tab* tab;
	q_module* module;
} module_select_result;

module_select_result status_module_selector(mp_gui_context* gui, const char* widgetID, q_editor* editor, vec2 startPos, q_module* module)
{
	module_select_result result = {};

	vec2 pos = startPos;

	//NOTE(martin): display selected module name
	mp_string label = mp_string_lit("none");
	if(module)
	{
		label = status_shorten_module_path(module->path, 24);
	}
	mp_graphics_color color = {1, 1, 1, 1};
	pos = q_print_string(gui, label, pos, color);

	result.xAdvance = pos.x - startPos.x;

	//NOTE(martin): compute popup button dimensions
	mp_graphics_context graphics = mp_gui_get_graphics(gui);
	mp_gui_style* style = mp_gui_style_top(gui);
	mp_graphics_font_extents fontExtents = {};
	mp_graphics_font_get_extents(graphics, style->font, &fontExtents);
	f32 fontScale = mp_graphics_font_get_scale_for_em_pixels(graphics, style->font, style->fontSize);
	f32 lineHeight = (fontExtents.ascent + fontExtents.descent + fontExtents.leading) * fontScale;

	mp_aligned_rect buttonBox = { startPos.x, startPos.y, pos.x - startPos.x, lineHeight};

	//NOTE(martin): open popup panel when button is clicked
	if(  mp_gui_is_mouse_over(gui, buttonBox)
	  && mp_gui_is_mouse_button_pressed(gui, MP_MOUSE_LEFT))
	{
		mp_gui_open_popup(gui, widgetID);
	}

	//NOTE(martin): collect modules which are open in the editor and compute panel dimensions
	//TODO: should probably do this inside panel block and set panel rect there
	u32 optionCount = editor->tabCount;
	qed_tab** tabs = mem_arena_alloc_array(mem_scratch(), qed_tab*, optionCount);
	mp_string* labels = mem_arena_alloc_array(mem_scratch(), mp_string, optionCount);
	u32 optionIndex = 0;
	u32 selectedOption = 0;

	f32 maxWidth = 0;
	f32 height = 0;

	for_each_in_list(&editor->tabs, tab, qed_tab, listElt)
	{
		tabs[optionIndex] = tab;
		labels[optionIndex] = status_shorten_module_path(tab->module->path, 36);

		mp_aligned_rect bbox = mp_graphics_text_bounding_box(graphics, labels[optionIndex]);
		maxWidth = maximum(maxWidth, bbox.w);
		height += bbox.h;

		if(module == tab->module)
		{
			selectedOption = optionIndex;
		}
		optionIndex++;
	}
	maxWidth += 20;
	height += 20;

	mp_aligned_rect panelRect = {buttonBox.x,
	                             buttonBox.y - height,
	                             maxWidth,
	                             height};

	//NOTE(martin): popup panel
	if(mp_gui_begin_popup(gui, widgetID, panelRect))
	{
		vec2 pos = {10, 10};
		for(int i=0; i<optionCount; i++)
		{
			mp_aligned_rect optionBox = {0, pos.y, maxWidth, lineHeight};
			if(mp_gui_is_mouse_over(gui, optionBox))
			{
				mp_graphics_set_color_rgba(graphics, 0, 0, 1, 1);
				mp_graphics_rectangle_fill(graphics, optionBox.x, optionBox.y, optionBox.w, optionBox.h);

				if(mp_gui_is_mouse_button_simple_clicked(gui, MP_MOUSE_LEFT))
				{
					mp_gui_view_close(gui);

					result.clicked = true;
					result.tab = tabs[i];
					result.module = tabs[i]->module;
				}
			}
			pos = q_print_string(gui, labels[i], pos, color);
			pos.x = 10;
			pos.y += lineHeight;
		}
	}
	mp_gui_end_popup(gui);

	return(result);
}

void status_gui(mp_gui_context* gui, q_app* app)
{
	vec2 pos = {10, 15};
	mp_graphics_color color = {1, 1, 1, 1};

	mp_gui_style style = app->style;
	style.fontSize = 32;
	mp_gui_style_push(gui, &style);
	{
		pos = q_print_string(gui, mp_string_lit("Edited module: "), pos, color);

		module_select_result editSelect = status_module_selector(gui, "edit_selector", &app->editor, pos, qed_current_module(&app->editor));
		if(editSelect.clicked)
		{
			qed_tab_select(&app->editor, editSelect.tab);
		}
		pos.x += editSelect.xAdvance + 100;

		pos = q_print_string(gui, mp_string_lit("Main module: "), pos, color);

		module_select_result mainSelect = status_module_selector(gui, "main_selector", &app->editor, pos, app->workspace.mainModule);
		if(mainSelect.clicked)
		{
			workspace_set_main_module(&app->workspace, mainSelect.module);
			//TODO: q_update_build(app);
		}

	} mp_gui_style_pop(gui);
}

//------------------------------------------------------------------------------------------
// Console GUI
//------------------------------------------------------------------------------------------
void q_console_gui(mp_gui_context* gui, q_app* app)
{
	//NOTE(martin): compute font width / line height
	mp_graphics_context graphics = mp_gui_get_graphics(gui);

	mp_gui_style* style = mp_gui_style_top(gui);
	mp_graphics_font_extents fontExtents;
	mp_graphics_font_get_extents(graphics, style->font, &fontExtents);
	f32 fontScale = mp_graphics_font_get_scale_for_em_pixels(graphics, style->font, style->fontSize);

	f32 spaceWidth = fontExtents.width * fontScale;
	f32 lineHeight = (fontExtents.ascent + fontExtents.descent + fontExtents.leading) * fontScale;

	//NOTE: get characters from the output pipe and copy to our console buffer
	u32 bytesToRead = q_ringbuffer_read_available(&app->outputBuffer);

	if(app->consoleBufferSize + bytesToRead >= app->consoleBufferCap)
	{
		app->consoleBufferSize -= bytesToRead;
		memcpy(app->consoleBuffer, app->consoleBuffer + bytesToRead, app->consoleBufferSize);
	}
	q_ringbuffer_read(&app->outputBuffer, bytesToRead, (u8*)app->consoleBuffer + app->consoleBufferSize);
	app->consoleBufferSize += bytesToRead;

	//NOTE: count lines according to width and newline chars
	mem_arena* scratch = mem_scratch();
	i32 lineCount = 0;
	u64* lineBuffer = (u64*)(scratch->ptr + scratch->offset);

	f32 margin = 10;
	f32 maxLineWidth = floor((app->sourceViewWidth - 2*margin)/ spaceWidth);

	f32 lineWidth = 0;
	u64 bufferOffset = 0;
	u64 lineStart = 0;

	//TODO: directly store console buffer as a string
	mp_string consoleBufferString = mp_string_from_buffer(app->consoleBufferSize, app->consoleBuffer);

	while(bufferOffset < app->consoleBufferSize)
	{
		u64 offset = bufferOffset;
		utf8_dec decode = utf8_decode_at(consoleBufferString, bufferOffset);
		utf32 codePoint = decode.codepoint;
		bufferOffset += decode.size;

		if(lineWidth == 0)
		{
			u64* startSlot = mem_arena_alloc_type(scratch, u64);
			*startSlot = offset;
			lineStart = offset;
			lineCount++;
		}

		if( lineWidth > maxLineWidth
		  ||codePoint == '\n')
		{
			u64* lenSlot = mem_arena_alloc_type(scratch, u64);
			*lenSlot = bufferOffset - lineStart;
			lineWidth = 0;
		}
		else
		{
			lineWidth++;
		}
	}
	if(lineWidth != 0)
	{
		u64* lenSlot = mem_arena_alloc_type(scratch, u64);
		*lenSlot = bufferOffset - lineStart;
	}

	//NOTE: adjust contents dimensions and scroll
	mp_aligned_rect frame = mp_gui_view_get_frame(app->gui);
	f32 contentsWidth = ClampLowBound(maxLineWidth, frame.w);
	f32 contentsHeight = ClampLowBound(lineCount*lineHeight, frame.h);
	mp_gui_view_set_contents_dimensions(gui, contentsWidth, contentsHeight);

	if(bytesToRead)
	{
		f32 scrollX = 0;
		f32 scrollY = contentsHeight - frame.h;
		mp_gui_view_set_scroll(gui, scrollX, scrollY);
	}

	//NOTE: print visible lines
	i32 visibleLineCount = (u32)floor((contentsHeight - 2*margin)/ lineHeight);
	i32 startLineIndex = ClampLowBound(lineCount - visibleLineCount, 0);
	mp_graphics_color color = {1, 1, 1, 1};

	vec2 pos = {margin, margin};

	for(int lineIndex=startLineIndex; lineIndex<lineCount; lineIndex++)
	{
		u64 lineStart = lineBuffer[2*lineIndex];
		u64 lineLen = lineBuffer[2*lineIndex + 1];

		mp_string string = {.ptr = app->consoleBuffer + lineStart, .len = lineLen};

		pos = q_print_string(app->gui, string, pos, color);
		pos.y += lineHeight;
		pos.x = margin;
	}
}

void q_clear_console(q_app* app)
{
	app->consoleBufferSize = 0;
}

//------------------------------------------------------------------------------------
// Load/Save
//------------------------------------------------------------------------------------
void q_save_with_dialog(q_app* app)
{
	const char* filters[1] = {"ql"};
	mp_string path = mp_save_dialog(mem_scratch(), "Save file", "./", 1, filters);
	if(path.len)
	{
		q_module* module = qed_current_module(&app->editor);
		module_save(&app->workspace, module, path);
	}
}

void q_save_quick(q_app* app)
{
	q_module* module = qed_current_module(&app->editor);
	if(!module->transient)
	{
		module_save(&app->workspace, module, module->path);
	}
	else
	{
		q_save_with_dialog(app);
	}
}

void q_load_in_new_tab(q_app* app, mp_string path)
{
	q_module* module = module_load(&app->workspace, path);
	if(module)
	{
		qed_tab* oldTab = qed_current_tab(&app->editor);

		if(!qed_tab_open(&app->editor, module))
		{
			module_recycle(&app->workspace, module);
		}
		else if(  oldTab->module->transient
			      && !oldTab->module->dirty)
		{
			qed_tab_close(&app->editor, oldTab);
		}

		if(!app->workspace.mainModule || app->workspace.mainModule->transient)
		{
			workspace_set_main_module(&app->workspace, module);
		}
	}
	workspace_rebuild(&app->workspace);
}

void q_load_with_dialog(q_app* app)
{
	const char* filters[1] = {"ql"};
	mp_string path = mp_open_dialog(mem_scratch(), "Open file", "./", 1, filters, false);
	if(path.len)
	{
		q_load_in_new_tab(app, path);
	}
}
//------------------------------------------------------------------------------------
// Code offset to cell ID map
//------------------------------------------------------------------------------------

//TODO: which arena to use? we need it to persist even when we try (and fail) rebuilding the program
//      in fact, we want it to be attached to the running program, so that even if we edit/rebuild during
//      execution, the source map is still mostly correct for the running program
//			>> simplest solution for now is maybe to build the map each time we launch the vm?
//			>> so we do it in yet another separate arena.

void source_map_cleanup(source_map* map)
{
	q_hashmap_cleanup(&map->moduleIdToPath);
	q_hashmap_cleanup(&map->offsetToLoc);
	q_hashmap_cleanup(&map->idToOffset);
}

void source_map_build(source_map* map, q_program* program)
{
	source_map_cleanup(map);

	char* locSection = program->contents + program->locOffset;
	for(int i=0; i<program->locCount; i++)
	{
		qc_loc_entry* entry = (qc_loc_entry*)(locSection + i*sizeof(qc_loc_entry));

		u64 offsetHash = hash_aesdec_u64(entry->offset);

		u64 idHash = hash_aesdec_u64_x2(entry->moduleID, entry->id);

		q_hashmap_insert(&map->offsetToLoc, offsetHash, (uintptr_t)entry);
		q_hashmap_insert(&map->idToOffset, idHash, entry->offset);
	}

	char* moduleTable = program->contents + program->modulesOffset;
	char* ptr = moduleTable;
	for(int i=0; i<program->modulesCount; i++)
	{
		u64 indexHash = hash_aesdec_u64(i);
		q_hashmap_insert(&map->moduleIdToPath, indexHash, (uintptr_t)ptr);
		u64 len = *(u64*)ptr;
		ptr += sizeof(u64) + len;
	}
}

qc_loc_entry* source_map_offset_to_loc(source_map* map, u64 offset)
{
	u64 hash = hash_aesdec_u64(offset);
	u64 val = 0;
	if(q_hashmap_find(&map->offsetToLoc, hash, &val))
	{
		return((qc_loc_entry*)val);
	}
	else
	{
		return(0);
	}
}

q_cell* source_map_offset_to_cell(q_app* app, u64 offset)
{
	q_cell* cell = 0;
	qc_loc_entry* loc = source_map_offset_to_loc(&app->sourceMap, offset);
	if(loc)
	{
		u64 indexHash = hash_aesdec_u64(loc->moduleID);
		u64 val = 0;
		if(q_hashmap_find(&app->sourceMap.moduleIdToPath, indexHash, &val))
		{
			char* ptr = (char*)val;
			u64 len = *(u64*)ptr;
			ptr += sizeof(u64);

			mp_string path = {.len = len, .ptr = ptr};
			q_module* module = workspace_find_module(&app->workspace, path);
			if(module)
			{
				cell = cell_find(&module->tree, loc->id);
			}
		}
	}
	return(cell);
}

bool source_map_loc_to_offset(source_map* map, u64 moduleIndex, u64 cellID, u64* outOffset)
{
	u64 hash = hash_aesdec_u64_x2(moduleIndex, cellID);
	return(q_hashmap_find(&map->idToOffset, hash, outOffset));
}

//------------------------------------------------------------------------------------
// Target map
//------------------------------------------------------------------------------------

void q_build_target_map(q_hashmap* map, q_program* program)
{
	q_hashmap_cleanup(map);
	char* targetBlockSection = program->contents + program->targetBlocksOffset;
	for(u64 i = 0; i<program->targetBlocksCount; i++)
	{
		qc_target_block* block = (qc_target_block*)(targetBlockSection + i*sizeof(qc_target_block));

		u64 offsetHash = hash_aesdec_u64(block->targetOffset);
		q_hashmap_insert(map, offsetHash, (uintptr_t)block);
	}
}

bool target_map_find(q_hashmap* map, u64 targetOffset, qc_target_block** block)
{
	u64 hash = hash_aesdec_u64(targetOffset);
	u64 res = 0;
	bool found = q_hashmap_find(map, hash, &res);
	*block = found ? (qc_target_block*)res : 0;
	return(found);
}


//------------------------------------------------------------------------------------
// Running
//------------------------------------------------------------------------------------

void* q_vm_main(void* userPointer)
{
	q_app* app = (q_app*)userPointer;

	if(app->options.offline)
	{
		sched_init_offline();
	}
	else
	{
		sched_init();
	}

	//NOTE: run the global initialization procedure at offset 0
	vm_eval(&app->vm, 0);

	//NOTE: jump to start procedure
	ir_symbol* startSym = 0;
	for_each_in_list(&app->workspace.mainModule->procedures, procSym, ir_symbol, auxElt)
	{
		if(!mp_string_cmp(procSym->name, mp_string_lit("start")))
		{
			startSym = procSym;
			break;
		}
	}

	if(startSym)
	{
		vm_eval(&app->vm, startSym->offset);
	}
	else
	{
		mp_string string = app->workspace.mainModule->path;
		LOG_ERROR("no start symbol in module %.*s\n", (int)string.len, string.ptr);
	}

	sched_end();

	vm_cleanup(&app->vm);

	app->vmRunning = false;
	return(0);
}

void q_vm_run(q_app* app)
{
	if(!app->vmRunning)
	{
		if(app->vmThread)
		{
			void* res;
			ThreadJoin(app->vmThread, &res);
			app->vmThread = 0;
		}
		if(app->workspace.programArena.offset)
		{
			source_map_build(&app->sourceMap, &app->workspace.program);
			q_build_target_map(&app->targetMap, &app->workspace.program);

			vm_options options = {
				.offline = app->options.offline,
				.noFeedback = app->options.headless,
				.printStdout = app->options.headless };

			vm_init(&app->vm, &app->workspace.program, &app->commandBuffer, &app->feedbackBuffer, &app->outputBuffer, options);
			app->vmRunning = true;
			app->vmThread = ThreadCreateWithName(q_vm_main, app, "vm thread");
		}
	}
}

//------------------------------------------------------------------------------------------
// Handling VM messages
//------------------------------------------------------------------------------------------

void qed_set_call_sites_progress(q_app* app, u64 count, u64* offsets, f32 progress, f32 timeout)
{
	for(int i=0; i<count; i++)
	{
		q_cell* cell = source_map_offset_to_cell(app, offsets[i]);
		if(cell)
		{
			cell->progressTimeout = timeout;
			cell->progress = progress;
		}
	}
}

void qed_set_call_sites_waiting_timeout(q_app* app, u64 count, u64* offsets, f32 timeout)
{
	for(int i=0; i<count; i++)
	{
		q_cell* cell = source_map_offset_to_cell(app, offsets[i]);
		if(cell)
		{
			cell->waitingTimeout = timeout;
			if(!timeout)
			{
				cell->waitingProgress = 0;
			}
		}
	}
}

void qed_set_call_sites_standby_timeout(q_app* app, u64 count, u64* offsets, f32 timeout)
{
	for(int i=0; i<count; i++)
	{
		q_cell* cell = source_map_offset_to_cell(app, offsets[i]);
		if(cell)
		{
			cell->standbyTimeout = timeout;
			if(!timeout)
			{
				cell->standbyProgress = 0;
			}
		}
	}
}

void q_handle_messages(q_app* app)
{
	q_msg* msg = q_ringbuffer_poll_msg(&app->feedbackBuffer, mem_scratch());

	while(msg->kind != Q_MSG_NONE)
	{
		switch(msg->kind)
		{
			case Q_MSG_RESUMED:
			{
				q_cell* cell = source_map_offset_to_cell(app, msg->point.offset);
				if(cell)
				{
					cell->progressTimeout = 0;
					cell->progress = 0;
					cell->waitingTimeout = 0;
					cell->waitingProgress = 0;
					cell->standbyTimeout = 0;
					cell->standbyProgress = 0;
				}
				u32 callStackDepth = msg->payloadSize/sizeof(u64);
				u64* callStack = (u64*)msg->payload;
				qed_set_call_sites_progress(app, callStackDepth, callStack, 0, 0);
				qed_set_call_sites_waiting_timeout(app, callStackDepth, callStack, 0);
				qed_set_call_sites_standby_timeout(app, callStackDepth, callStack, 0);
			} break;

			case Q_MSG_PROGRESS:
			{
				q_cell* cell = source_map_offset_to_cell(app, msg->progress.point.offset);
				LOG_MESSAGE("received progress message for bytecode offset %llu\n", msg->progress.point.offset);
				if(cell)
				{
					cell->progressTimeout = 10;
					cell->progress = 1 - msg->progress.localRemaining/msg->progress.initialDelay;
				}
				else
				{
					LOG_ERROR("can't find cell for progress message (bytecode offset = %llu)\n", msg->progress.point.offset);
				}

				u32 callStackDepth = msg->payloadSize/sizeof(u64);
				u64* callStack = (u64*)msg->payload;
				f32 progress = 1 - msg->progress.localRemaining/msg->progress.initialDelay;
				f32 timeout = 10;
				qed_set_call_sites_progress(app, callStackDepth, callStack, progress, timeout);
			} break;

			case Q_MSG_WAITING:
			{
				q_cell* cell = source_map_offset_to_cell(app, msg->point.offset);
				if(cell)
				{
					cell->waitingTimeout = 10;
				}

				u32 callStackDepth = msg->payloadSize/sizeof(u64);
				u64* callStack = (u64*)msg->payload;
				qed_set_call_sites_waiting_timeout(app, callStackDepth, callStack, 10);

			} break;

			case Q_MSG_STANDBY:
			{
				q_cell* cell = source_map_offset_to_cell(app, msg->point.offset);
				if(cell)
				{
					cell->standbyTimeout = 10;
				}

				u32 callStackDepth = msg->payloadSize/sizeof(u64);
				u64* callStack = (u64*)msg->payload;
				qed_set_call_sites_standby_timeout(app, callStackDepth, callStack, 10);

			} break;

			case Q_MSG_JMP_TARGET:
			{
				qc_target_block* block = 0;
				if(target_map_find(&app->targetMap, msg->point.offset, &block))
				{
					//NOTE(martin): find module
					q_module* module = 0;
					u64 indexHash = hash_aesdec_u64(block->moduleID);
					u64 val = 0;
					if(q_hashmap_find(&app->sourceMap.moduleIdToPath, indexHash, &val))
					{
						char* ptr = (char*)val;
						u64 len = *(u64*)ptr;
						ptr += sizeof(u64);

						mp_string path = {.len = len, .ptr = ptr};
						module = workspace_find_module(&app->workspace, path);
					}
					if(!module)
					{
						break;
					}

					//NOTE(martin): find cells in module
					qed_point start = {};
					qed_point end = {};
					start.parent = cell_find(&module->tree, block->start.parentID);
					start.leftFrom = cell_find(&module->tree, block->start.leftFromID);
					end.parent = cell_find(&module->tree, block->end.parentID);
					end.leftFrom = cell_find(&module->tree, block->end.leftFromID);

					if( start.parent == 0
					  ||end.parent == 0
					  ||(start.leftFrom == 0 && block->start.leftFromID != 0)
					  ||(end.leftFrom == 0 && block->end.leftFromID != 0))
					{
						//NOTE(martin): one of the cells was not found, (eg. the cell tree was modified),
						//              skip the lighting.
						break;
					}

					qed_cell_span range = qed_cell_span_from_points(start, end);
					ASSERT(range.start && range.end); //NOTE(martin): blocks should not be empty

					mem_arena* scratch = mem_scratch();
					q_cell** flashCells = (q_cell**)(scratch->ptr + scratch->offset);
					u64 flashCellCount = 0;

					qed_point itLeft = start;
					while(qed_point_right_cell(itLeft) != range.start)
					{
						q_cell* parent = itLeft.parent;

						q_ast* parentNode = cell_ast_lookup(parent);
						if(parentNode
						  && ( parentNode->kind == AST_IF
						     ||parentNode->kind == AST_WHILE
						     ||parentNode->kind == AST_DO
						     ||parentNode->kind == AST_FLOW))
						{
							for(q_cell* cell = itLeft.leftFrom;
							    cell;
							    cell = ListNextEntry(&parent->children, cell, q_cell, listElt))
							{
								q_cell** slot = mem_arena_alloc_type(scratch, q_cell*);
								*slot = cell;
								flashCellCount++;
							}
							itLeft = (qed_point){ parent->parent,
							                           ListNextEntry(&parent->parent->children, parent, q_cell, listElt),
							                           0 };
						}
						else
						{
							itLeft = (qed_point){ parent->parent, parent, 0 };
						}

						if(parent == range.start)
						{
							break;
						}
					}

					//TODO: review this, this is quite unsafe
					qed_point itRight = end;
					while(qed_point_left_cell(itRight) != range.end)
					{
						q_cell* parent = itRight.parent;

						q_ast* parentNode = cell_ast_lookup(parent);
						if(parentNode
						  && ( parentNode->kind == AST_IF
						     ||parentNode->kind == AST_WHILE
						     ||parentNode->kind == AST_DO
						     ||parentNode->kind == AST_FLOW))
						{
							for(q_cell* cell = qed_point_left_cell(itRight);
							    cell;
							    cell = ListPrevEntry(&parent->children, cell, q_cell, listElt))
							{
								q_cell** slot = mem_arena_alloc_type(mem_scratch(), q_cell*);
								*slot = cell;
								flashCellCount++;
							}
							itRight = (qed_point){ parent->parent, parent, 0 };
						}
						else
						{
							itRight = (qed_point){ parent->parent,
							                       ListNextEntry(&parent->parent->children, parent, q_cell, listElt),
							                       0 };
						}

						if(parent == range.end)
						{
							break;
						}
					}

					for(q_cell* cell = itLeft.leftFrom;
					    (cell && cell != itRight.leftFrom);
					    cell = ListNextEntry(&cell->parent->children, cell, q_cell, listElt))
					{
						q_ast* node = cell_ast_lookup(cell);
						if(  node
						  && node->kind != AST_PROC_DECL
						  && node->kind != AST_TYPE_DECL)
						{
							q_cell** slot = mem_arena_alloc_type(mem_scratch(), q_cell*);
							*slot = cell;
							flashCellCount++;
						}
					}

					for(int i=0; i<flashCellCount; i++)
					{
						q_cell* cell = flashCells[i];
						cell_flash(cell);
					}
				}
			} break;

			default:
				break;
		}
		msg = q_ringbuffer_poll_msg(&app->feedbackBuffer, mem_scratch());
	}
}

//------------------------------------------------------------------------------------------
// Main GUI
//------------------------------------------------------------------------------------------
void q_increase_font_size(q_app* app)
{
	app->style.fontSize = ClampHighBound(app->style.fontSize + 2, 60);
}

void q_decrease_font_size(q_app* app)
{
	app->style.fontSize = ClampLowBound(app->style.fontSize - 2, 10);
}

void q_debug_print_ast(q_app* app)
{
	q_module* module = qed_current_module(&app->editor);
	debug_print_ast(module->ast);
}

void q_debug_print_ir(q_app* app)
{
	q_module* module = qed_current_module(&app->editor);
	debug_print_ir(module);
}

void q_debug_print_bytecode(q_app* app)
{
	q_module* module = qed_current_module(&app->editor);
	debug_print_module_bytecode(&app->workspace, module);
}

void q_debug_print_all_bytecode(q_app* app)
{
	debug_print_program_bytecode(&app->workspace);
}

void q_trigger_command(q_app* app)
{
	q_editor* editor = &app->editor;
	qed_tab* tab = qed_current_tab(editor);
	q_module* module = qed_current_module(editor);

	//NOTE(martin): send trigger message if we're inside a standby cue
	q_cell* cell = tab->cursor.leftFrom;
	if(!cell)
	{
		cell = tab->cursor.parent;
	}
	while(cell != 0)
	{
		q_ast* node = cell_ast_lookup(cell);
		if(node && node->kind == AST_STANDBY)
		{
			break;
		}
		cell = cell->parent;
	}

	if(cell)
	{
		//NOTE(martin): find offset from cell id and module index, and send trigger message
		if(module->moduleIndex >= 0)
		{
			u64 offset = 0;
			if(source_map_loc_to_offset(&app->sourceMap, module->moduleIndex, cell->id, &offset))
			{
				q_msg msg = {.kind = Q_MSG_TRIGGER, .point.offset = offset, .payloadSize = 0};
				q_ringbuffer_push_msg(&app->commandBuffer, &msg);
			}
		}
	}
}


typedef void (*q_app_action)(q_app* app);

typedef struct q_app_command
{
	mp_key_code key;
	mp_key_mods mods;
	q_app_action action;

} q_app_command;

const q_app_command Q_APP_COMMANDS[] = {
	{
		.key = MP_KEY_SLASH,
		.mods = MP_KEYMOD_CMD | MP_KEYMOD_SHIFT,
		.action = q_increase_font_size,
	},
	{
		.key = MP_KEY_EQUAL,
		.mods = MP_KEYMOD_CMD | MP_KEYMOD_SHIFT,
		.action = q_decrease_font_size,
	},

	{
		.key = MP_KEY_O,
		.mods = MP_KEYMOD_CMD,
		.action = q_load_with_dialog,
	},

	{
		.key = MP_KEY_S,
		.mods = MP_KEYMOD_CMD,
		.action = q_save_quick,
	},

	{
		.key = MP_KEY_S,
		.mods = MP_KEYMOD_CMD | MP_KEYMOD_SHIFT,
		.action = q_save_with_dialog,
	},

	{
		.key = MP_KEY_R,
		.mods = MP_KEYMOD_CMD,
		.action = q_vm_run,
	},

	{
		.key = MP_KEY_BACKSPACE,
		.mods = MP_KEYMOD_CMD,
		.action = q_clear_console,
	},

	{
		.key = MP_KEY_P,
		.mods = MP_KEYMOD_CMD,
		.action = q_debug_print_ast,
	},

	{
		.key = MP_KEY_P,
		.mods = MP_KEYMOD_CMD | MP_KEYMOD_SHIFT,
		.action = q_debug_print_ir,
	},

	{
		.key = MP_KEY_P,
		.mods = MP_KEYMOD_CMD | MP_KEYMOD_ALT | MP_KEYMOD_SHIFT,
		.action = q_debug_print_bytecode,
	},

	{
		.key = MP_KEY_P,
		.mods = MP_KEYMOD_CMD | MP_KEYMOD_ALT | MP_KEYMOD_CTRL | MP_KEYMOD_SHIFT,
		.action = q_debug_print_all_bytecode,
	},


	//NOTE(martin): VM commands
	{
		.key = MP_KEY_SPACE,
		.mods = MP_KEYMOD_CTRL,
		.action = q_trigger_command,
	},
};

const u32 Q_APP_COMMAND_COUNT = sizeof(Q_APP_COMMANDS) / sizeof(q_app_command);

void q_gui(q_app* app)
{
	q_handle_messages(app);

	mp_gui_context* gui = app->gui;
	mp_graphics_context graphics = mp_gui_get_graphics(gui);

	mp_graphics_set_clear_color(graphics, Q_SOURCE_BG_COLOR);
	mp_graphics_set_font(graphics, app->style.font);
	mp_graphics_set_font_size(graphics, app->style.fontSize);

	mp_gui_style_push(gui, &app->style);

	mp_gui_begin_frame(gui);
	{
		//--------------------------------------------------------------------
		//NOTE(martin): general keyboard shortcuts
		//--------------------------------------------------------------------
		if(!mp_gui_get_focus_id(gui))
		{
			mp_key_mods mods = mp_gui_key_mods(gui);

			for(int i=0; i<Q_APP_COMMAND_COUNT; i++)
			{
				const q_app_command* command = &(Q_APP_COMMANDS[i]);
				if(  mp_gui_is_key_pressed(gui, command->key)
				  && command->mods == mods)
				{
					command->action(app);
					break;
				}
			}
		}

		//TODO: fix lag between the two views dimensions when redimensionning
		mp_aligned_rect statusRect, sourceRect, consoleRect;
		q_compute_view_rects(app, &statusRect, &sourceRect, &consoleRect);

		//--------------------------------------------------------------------
		//NOTE(martin): source view
		//--------------------------------------------------------------------
		mp_gui_next_view_frame(gui, sourceRect);
		mp_gui_view_flags sourceFlags = MP_GUI_VIEW_FLAG_RESIZEABLE_BOTTOM
					                   | MP_GUI_VIEW_FLAG_SCROLL;
		mp_gui_begin_view(gui, "source_view", sourceRect, sourceFlags);
		{
			if(mp_gui_view_frame_changed(gui))
			{
				mp_aligned_rect frame = mp_gui_view_get_frame(gui);
				f32 deltaW = frame.w - sourceRect.w;
				f32 deltaH = frame.h - sourceRect.h;
				q_recompute_view_ratios(app, sourceRect, deltaW, deltaH);

				//WARN: temporary fix to view lag (just defer resizing to next frame):
				mp_gui_view_set_frame(gui, sourceRect);
			}

			qed_gui(gui, &app->editor);

		} mp_gui_end_view(gui);

		//--------------------------------------------------------------------
		//NOTE(martin): console view
		//--------------------------------------------------------------------
		mp_gui_next_view_frame(gui, consoleRect);
		mp_gui_view_flags consoleFlags = MP_GUI_VIEW_FLAG_RESIZEABLE_TOP
		                               | MP_GUI_VIEW_FLAG_SCROLL;

		mp_gui_begin_view(gui, "console_view", consoleRect, consoleFlags);
		{
			if(mp_gui_view_frame_changed(gui))
			{
				mp_aligned_rect frame = mp_gui_view_get_frame(gui);
				f32 deltaH = frame.h - consoleRect.h;
				q_recompute_view_ratios(app, sourceRect, 0, -deltaH);

				//WARN: temporary fix to view lag (just defer resizing to next frame):
				mp_gui_view_set_frame(gui, consoleRect);
			}

			q_console_gui(gui, app);

		} mp_gui_end_view(gui);

		//--------------------------------------------------------------------
		//NOTE(martin): status bar
		//--------------------------------------------------------------------
		mp_gui_next_view_frame(gui, statusRect);
		mp_gui_view_flags statusFlags = 0;
		mp_gui_begin_view(gui, "status_bar", statusRect, statusFlags);
		{
			status_gui(gui, app);
		} mp_gui_end_view(gui);


	} mp_gui_end_frame(gui);

	mp_gui_style_pop(gui);
}

//------------------------------------------------------------------------------------------
// Init
//------------------------------------------------------------------------------------------
int q_app_gui_init(q_app* app)
{
	app->windowWidth = Q_WINDOW_DEFAULT_WIDTH;
	app->windowHeight = Q_WINDOW_DEFAULT_HEIGHT;
	app->sourceHeightRatio = Q_SOURCE_TO_CONSOLE_DEFAULT_RATIO;

	//NOTE(martin): create window and graphics context
	mp_aligned_rect windowRect = {.x = 100, .y = 100, .w = 0.5*Q_WINDOW_DEFAULT_WIDTH, .h = 0.5*Q_WINDOW_DEFAULT_HEIGHT};
	app->window = mp_window_create(windowRect, "Quadrant", 0);
	app->surface = mp_window_get_surface(app->window);
	app->graphics = mp_graphics_context_create();

	//NOTE(martin): create font
	char* fontPath = 0;
	mp_app_get_resource_path("../resources/Andale Mono.ttf", &fontPath);
	FILE* fontFile = fopen(fontPath, "r");
	free(fontPath);
	if(!fontFile)
	{
		LOG_ERROR("Could not load font file '%s'\n", fontPath);
		return(-1);
	}
	unsigned char* fontData = 0;
	fseek(fontFile, 0, SEEK_END);
	u32 fontDataSize = ftell(fontFile);
	rewind(fontFile);
	fontData = (unsigned char*)malloc(fontDataSize);
	fread(fontData, 1, fontDataSize, fontFile);
	fclose(fontFile);

	unicode_range ranges[5] = {UNICODE_RANGE_BASIC_LATIN,
	                           UNICODE_RANGE_C1_CONTROLS_AND_LATIN_1_SUPPLEMENT,
	                           UNICODE_RANGE_LATIN_EXTENDED_A,
	                           UNICODE_RANGE_LATIN_EXTENDED_B,
	                           UNICODE_RANGE_SPECIALS};

	app->font = mp_graphics_font_create_from_memory(app->graphics, fontDataSize, fontData, 5, ranges);
	free(fontData);

	//NOTE(martin): create gui context and populate gui style
	app->gui = mp_gui_context_create(Q_WINDOW_DEFAULT_WIDTH, Q_WINDOW_DEFAULT_HEIGHT, app->graphics, app->font);
	app->input.displayWidth = Q_WINDOW_DEFAULT_WIDTH;
	app->input.displayHeight = Q_WINDOW_DEFAULT_HEIGHT;

	app->style = (mp_gui_style){
		.font = app->font,
		.fontSize = 34,
		.borderWidth = 0,
		.borderWidthHover = 0,
		.borderWidthActive = 4,
		.viewBorderWidth = 2,
		.frameRoundness = 0,
		.elementRoundess = 0,
		.scrollbarRoundness = 1,
		.margin = 10,
		.titleHeight = 80,

		.colors = {
			[MP_GUI_STYLE_COLOR_BG] = Q_CUE_ENTRY_BG_COLOR,
			[MP_GUI_STYLE_COLOR_BG_HOVER] = Q_CUE_ENTRY_BG_COLOR,
			[MP_GUI_STYLE_COLOR_BG_ACTIVE] = Q_CUE_ENTRY_BG_COLOR,

			[MP_GUI_STYLE_COLOR_BORDER] = {1, 1, 1, 1},
			[MP_GUI_STYLE_COLOR_BORDER_HOVER] = {1, 1, 1, 1},
			[MP_GUI_STYLE_COLOR_BORDER_ACTIVE] = {1, 1, 1, 1},

			[MP_GUI_STYLE_COLOR_ELT] = Q_CUE_ENTRY_BG_COLOR,
			[MP_GUI_STYLE_COLOR_ELT_HOVER] = Q_CUE_ENTRY_BG_COLOR,
			[MP_GUI_STYLE_COLOR_ELT_ACTIVE] = {0.1, 0.1, 0.1, 1},

			[MP_GUI_STYLE_COLOR_TEXT] = {1, 1, 1, 1},
			[MP_GUI_STYLE_COLOR_TEXT_HOVER] = {1, 1, 1, 1},
			[MP_GUI_STYLE_COLOR_TEXT_ACTIVE] = {1, 1, 1, 1},
			[MP_GUI_STYLE_COLOR_TEXT_SELECTION_BG] = {0, 0, 1, 1},
			[MP_GUI_STYLE_COLOR_TEXT_SELECTION_FG] = {1, 1, 1, 1},

			[MP_GUI_STYLE_COLOR_VIEW_BG] = Q_COLOR_VIEW_BG,
			[MP_GUI_STYLE_COLOR_VIEW_TITLE_BG] = {0.1, 0.1, 0.1, 1},
			[MP_GUI_STYLE_COLOR_VIEW_TITLE_TEXT] = {1, 1, 1, 1},
			[MP_GUI_STYLE_COLOR_VIEW_BORDER] = {0.3, 0.3, 0.3, 1},

			[MP_GUI_STYLE_COLOR_SCROLLBAR] = {0.02, 0.02, 0.02, 1},
			[MP_GUI_STYLE_COLOR_SCROLLBAR_ACTIVE] = {0.05, 0.05, 0.05, 1},
		}
	};

	return(0);
}

//------------------------------------------------------------------------------------------
// Main entry points / event loop etc.
//------------------------------------------------------------------------------------------

void q_event_callback(mp_event* event, void* userData)
{
	q_app* app = (q_app*)userData;

	mp_gui_process_event(mp_gui_context_get_io(app->gui), event);
	switch(event->type)
	{
		case MP_EVENT_WINDOW_CLOSE:
		{
			mp_do_quit();
		} break;

		case MP_EVENT_WINDOW_RESIZE:
		{
			app->windowWidth = event->frame.rect.w * 2;
			app->windowHeight = event->frame.rect.h * 2;
		} break;

		case MP_EVENT_FRAME:
		{
			mp_graphics_set_clear_color(app->graphics, Q_COLOR_CLEAR_BG);
			mp_graphics_clear(app->graphics);

			q_gui(app);

			mp_graphics_context_flush(app->graphics, app->surface);
			mp_graphics_surface_present(app->surface);

			mem_scratch_clear();
		} break;

		default:
			break;
	}
}

q_cli_options q_parse_cli_options(int argc, char** argv)
{
	q_cli_options options = {};

	int optIndex = 1;
	int fileIndex = 0;
	while(optIndex < argc)
	{
		const char* arg = argv[optIndex];

		if(!strcmp(arg, "--ql-update"))
		{
			options.command = Q_CLI_UPDATE;
			if(  optIndex + 2 >= argc
			  || argv[optIndex+1][0] == '-'
			  || argv[optIndex+2][0] == '-')
			{
				fprintf(stderr, "error: command --ql-update takes an input and an output file as arguments\n");
				options.error = true;
			}
			else
			{
				options.in = mp_string_from_cstring(argv[optIndex+1]);
				options.out = mp_string_from_cstring(argv[optIndex+2]);
			}
			argv+=2;
			break;
		}
		else if(!strcmp(arg, "--headless"))
		{
			options.headless = true;
		}
		else if(!strcmp(arg, "--offline"))
		{
			options.offline = true;
		}
		else
		{
			options.in = mp_string_from_cstring((char*)arg);
		}
		optIndex++;
	}

	if(optIndex < argc)
	{
		fprintf(stderr, "warning: ignored extraneous arguments, starting from argument '%s'\n", argv[optIndex]);
	}

	if(!options.in.len)
	{
		if(options.headless)
		{
			fprintf(stderr, "error: option --headless requires a startup file to be specified\n");
			options.error = true;
		}
		else
		{
			if(options.offline)
			{
				fprintf(stderr, "warning: option --offline implies --headless\n");
				options.headless = true;
			}
		}
	}

	return(options);
}

int main(int argc, char** argv)
{
	LogLevel(LOG_LEVEL_MESSAGE);
	LogFilter("Application", LOG_LEVEL_WARNING);
	LogFilter("Scheduler", LOG_LEVEL_WARNING);

	q_cli_options options = q_parse_cli_options(argc, argv);
	if(options.error)
	{
		return(-1);
	}

	if(options.command == Q_CLI_UPDATE)
	{
		//TODO: return(q_update_save_file(options.in, options.out));
		return(0);
	}

	mp_init();

	q_app app;
	memset(&app, 0, sizeof(q_app));
	app.options = options;

	workspace_init(&app.workspace);

	if(!options.headless)
	{
		if(q_app_gui_init(&app))
		{
			return(-1);
		}
		q_ringbuffer_init(&app.commandBuffer, 11);
		q_ringbuffer_init(&app.feedbackBuffer, 11);
		q_ringbuffer_init(&app.outputBuffer, 11);

		app.consoleBuffer = malloc_array(char, 1024);
		app.consoleBufferCap = 1024;
		app.consoleBufferSize = 0;

		qed_init(&app.editor, &app.workspace, &app.commandBuffer);
	}

	if(options.in.len)
	{
		//TODO q_load_in_new_tab(&app, options.in);
	}

	if(!options.headless)
	{
		//NOTE(martin): run
		mp_app_set_process_event_callback(q_event_callback, &app);
		mp_set_target_fps(60);
		mp_window_center(app.window);
		mp_window_bring_to_front_and_focus(app.window);
		mp_event_loop();

		mp_graphics_font_destroy(app.graphics, app.font);
		mp_graphics_context_destroy(app.graphics);
		mp_window_destroy(app.window);
	}
	else
	{
		q_vm_run(&app);
		void* res;
		ThreadJoin(app.vmThread, &res);
	}

	//NOTE(martin): cleanup
	mp_terminate();

	return(0);
}

#undef LOG_SUBSYSTEM
