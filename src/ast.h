/************************************************************//**
*
*	@file: ast.h
*	@author: Martin Fouilleul
*	@date: 13/05/2022
*	@revision:
*
*****************************************************************/
#ifndef __AST_H_
#define __AST_H_

#include"typedefs.h"

#define AST_KINDS(X) \
X(AST_ROOT, "root") \
X(AST_ERROR_INVALID, "invalid") \
X(AST_ERROR_MISSING, "missing") \
X(AST_OPT_EMPTY, "empty option") \
X(AST_IMPORT, "import directive") \
X(AST_NIL, "nil") \
X(AST_INT, "int literal") \
X(AST_FLOAT, "float literal") \
X(AST_CHAR, "character literal") \
X(AST_STRING, "string literal") \
X(AST_ARRAY, "array literal") \
X(AST_ID, "identifier") \
X(AST_ADD, "add") \
X(AST_SUB, "sub") \
X(AST_MUL, "mul") \
X(AST_DIV, "div") \
X(AST_MOD, "mod") \
X(AST_NEG, "neg") \
X(AST_EQ, "equal") \
X(AST_NEQ, "not equal") \
X(AST_LT, "less") \
X(AST_GT, "greater") \
X(AST_LE, "less or equal") \
X(AST_GE, "greater or equal") \
X(AST_LAND, "logical and") \
X(AST_LOR, "logical or") \
X(AST_LNOT, "logical not") \
X(AST_REF, "reference") \
X(AST_DEREF, "dereference") \
X(AST_AT, "array indexing") \
X(AST_LEN, "array length") \
X(AST_SLICE, "slice operator") \
X(AST_FIELD, "field access") \
X(AST_SIZEOF, "field access") \
X(AST_SET, "affectation") \
X(AST_CAST, "cast") \
X(AST_CALL, "call") \
X(AST_ARGS, "args") \
X(AST_FOREIGN_BLOCK, "foreign block") \
X(AST_FOREIGN_DEF, "foreign procedure declaration") \
X(AST_VAR_DECL, "variable declaration") \
X(AST_TYPE_DECL, "type declaration") \
X(AST_TYPE_ARRAY, "array type specification") \
X(AST_TYPE_SLICE, "slice type specification") \
X(AST_TYPE_POINTER, "pointer type specification") \
X(AST_TYPE_FUTURE, "future type specification") \
X(AST_TYPE_STRUCT, "struct type specification") \
X(AST_FIELD_SPEC, "struct field specification") \
X(AST_PROC_DECL, "procedure declaration") \
X(AST_PROC_SIG, "procedure signature") \
X(AST_PARAM_LIST, "procedure parameter list") \
X(AST_PARAM, "procedure parameter") \
X(AST_TYPEID, "typeid parameter") \
X(AST_POLY_ID, "polymorphic parameter") \
X(AST_VARG, "variable argument parameter") \
X(AST_MULTI, "multi") \
X(AST_BODY, "body") \
X(AST_IF, "if statement") \
X(AST_WHILE, "while loop") \
X(AST_FOR, "for loop") \
X(AST_DO, "do statement") \
X(AST_RETURN, "return statement") \
X(AST_BACKGROUND, "background block") \
X(AST_FLOW, "flow block") \
X(AST_STANDBY, "standby") \
X(AST_WAIT, "wait") \
X(AST_PAUSE, "pause") \
X(AST_TIMEOUT, "timeout") \
X(AST_FDUP, "future duplicate") \
X(AST_FDROP, "future drop") \
X(AST_PUT, "put") \
X(AST_ATTR_RECURSIVE, "attribute recursive") \
X(AST_ATTR_AS, "attribute as") \
X(AST_TEMPO_EDITOR, "tempo editor") \


typedef enum {
	#define AST_KIND_ENUM(kind, string) kind,
	AST_KINDS(AST_KIND_ENUM)
	AST_KIND_COUNT,
} ast_kind;

const char* AST_KIND_STRINGS[AST_KIND_COUNT] =
{
	#define AST_KIND_STRING(kind, string) string,
	AST_KINDS(AST_KIND_STRING)
};

typedef enum {
	PARSE_RULE_UNDEF,
	PARSE_RULE_EXPRESSION,
	PARSE_RULE_INITIALIZER,
	PARSE_RULE_STATEMENT,
	PARSE_RULE_TOP_LEVEL,
	PARSE_RULE_TYPESPEC,
} parse_rule;


typedef struct ir_node ir_node;

typedef struct q_ast
{
	q_ast* parent;
	list_elt listElt;

	list_info children;
	u32 childCount;

	u64 generation;
	q_range range;
	ast_kind kind;

	parse_rule parseRule;

	ir_node* ir;
	u64 irBuildGen;
} q_ast;

typedef struct ast_range
{
	q_ast* start;
	q_ast* end;
} ast_range;

#define ast_first_child(parent) ListFirstEntry(&((parent)->children), q_ast, listElt)
#define ast_last_child(parent) ListLastEntry(&((parent)->children), q_ast, listElt)
#define ast_next_sibling(ast) ListNextEntry(&((ast)->parent->children), (ast), q_ast, listElt)
#define ast_prev_sibling(ast) ListPrevEntry(&((ast)->parent->children), (ast), q_ast, listElt)

typedef struct q_module q_module;
void ast_ir_cache(q_module* module, q_ast* ast, ir_node* ir);
ir_node* ast_ir_lookup(q_module* module, q_ast* ast);

typedef enum { Q_ERROR_MISSING,
               Q_ERROR_INVALID,
               Q_ERROR_CHECKING,
               Q_ERROR_BUILD, } q_error_kind;

typedef struct q_error
{
	list_elt moduleElt;
	list_elt underCursorElt;

	q_error_kind kind;
	q_ast* ast;
	u64 astGen;
	mp_string msg;
	mp_string cellHint;

} q_error;


#endif //__AST_H_
