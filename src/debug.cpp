/************************************************************//**
*
*	@file: debug.cpp
*	@author: Martin Fouilleul
*	@date: 16/08/2021
*	@revision:
*
*****************************************************************/
#include<stdio.h>

#include"ast.h"

#define LOG_SUBSYSTEM "Debug"

void debug_print_indent(u32 indent)
{
	for(int i=0; i<indent; i++)
	{
		printf("  ");
	}
}

void debug_print_ast_node(q_ast* node, u32 indent)
{
	debug_print_indent(indent);

	const char* kindString = AST_KIND_STRINGS[node->kind];
	printf("%s", kindString);

	q_cell* cell = node->range.start.leftFrom;
	switch(node->kind)
	{
		case AST_INT:
		case AST_CHAR:
			printf(": %llu", cell->valueU64);
			break;

		case AST_FLOAT:
			printf(": %f", cell->valueF64);
			break;

		case AST_STRING:
		case AST_ID:
		{
			mp_string string = cell->text.string;
			printf(": '%.*s'", (int)string.len, string.ptr);
		} break;

		default:
			break;
	}
	printf("\n");

	for_each_in_list(&node->children, child, q_ast, listElt)
	{
		debug_print_ast_node(child, indent+1);
	}
}

void debug_print_ast(q_ast* root)
{
	if(root)
	{
		printf("--------------------- AST ----------------------- \n\n");
		for_each_in_list(&root->children, child, q_ast, listElt)
		{
			debug_print_ast_node(child, 0);
		}
		printf("------------------------------------------------- \n\n");
	}
	else
	{
		LOG_ERROR("No AST\n");
	}
}

void debug_print_type(ir_type* type)
{
	switch(type->kind)
	{
		case TYPE_ERROR:
			printf("<type error>");
			break;
      	case TYPE_VOID:
			printf("void");
			break;
      	case TYPE_PRIMITIVE:
      	{
			switch(type->primitive)
			{
				case TYPE_B8:
					printf("b8");
					break;
				case TYPE_U8:
					printf("u8");
					break;
				case TYPE_I8:
					printf("i8");
					break;
				case TYPE_U16:
					printf("u16");
					break;
				case TYPE_I16:
					printf("i16");
					break;
				case TYPE_U32:
					printf("u32");
					break;
				case TYPE_I32:
					printf("i32");
					break;
				case TYPE_U64:
					printf("u64");
					break;
				case TYPE_I64:
					printf("i64");
					break;
				case TYPE_F32:
					printf("f32");
					break;
				case TYPE_F64:
					printf("f64");
					break;
			}
		} break;

      	case TYPE_NAMED:
			printf("(type name '%.*s': ", (int)type->symbol->name.len, type->symbol->name.ptr);
			debug_print_type(type->symbol->type);
			printf(")");
			break;
      	case TYPE_POINTER:
			printf("(ptr \n");
			debug_print_type(type->eltType);
			printf(")");
			break;
      	case TYPE_FUTURE:
			printf("(future ");
			debug_print_type(type->eltType);
			printf(")");
			break;
      	case TYPE_ARRAY:
			printf("(array %llu ", type->eltCount);
			debug_print_type(type->eltType);
			printf(")");
			break;
      	case TYPE_SLICE:
			printf("(slice");
			debug_print_type(type->eltType);
			printf(")");
			break;
      	case TYPE_STRUCT:
			printf("(struct ");
			for_each_in_list(&type->params, param, ir_param, listElt)
			{
				printf("%.*s", (int)param->name.len, param->name.ptr);
				debug_print_type(param->type);
			}
			printf(")");
			break;
      	case TYPE_PROC:
			printf("procedure type");
			break;
      	case TYPE_ANY:
			printf("any");
			break;
      	case TYPE_POLY:
			printf("poly name '%.*s'", (int)type->name.len, type->name.ptr);
			break;
      	case TYPE_POLY_TYPEID:
			printf("typeid");
			break;
	}
}

void debug_print_ir_node(ir_node* ir, u32 indent)
{
	debug_print_indent(indent);

	const char* kindString = IR_KIND_STRINGS[ir->kind];
	printf("%s of type ", kindString);
	debug_print_type(ir->type);

	if(ir->kind == IR_INT)
	{
		printf(" (%llu)", ir->valueU64);
	}
	else if(ir->kind == IR_FLOAT)
	{
		printf(" (%f)", ir->valueF64);
	}

	printf("\n");

	for_each_in_list(&ir->children, child, ir_node, listElt)
	{
		debug_print_ir_node(child, indent+1);
	}
}

void debug_print_ir_proc(ir_symbol* proc)
{
	printf("proc %.*s:\n", (int)proc->name.len, proc->name.ptr);
	if(proc->type->polymorphic)
	{
		printf("\t(polymorphic template)\n");
	}
	else if(proc->proc.foreign)
	{
		printf("\t(foreign proc)\n");
	}
	else
	{
		debug_print_ir_node(proc->proc.body, 1);
	}
	printf("\n");
}

void debug_print_ir(q_module* module)
{
	printf("--------------------- IR ----------------------- \n\n");
	for_each_in_list(&module->procedures, proc, ir_symbol, auxElt)
	{
		debug_print_ir_proc(proc);
	}
	printf("------------------------------------------------- \n\n");
}

void debug_print_module_bytecode(q_workspace* workspace, q_module* module)
{
	printf("----------------------------\n");
	printf("Module %.*s:\n", (int)module->path.len, module->path.ptr);
	printf("----------------------------\n");

	q_program* program = &workspace->program;
	u8* bytecode = (u8*)program->contents + program->codeOffset + module->codeOffset;
	u64 size = module->codeLen;

	u32 ip = 0;
	while(ip < size)
	{
		u8 opcode = bytecode[ip];

		if(opcode == OP_ENTER)
		{
			//TODO: print name of procedure here
			for_each_in_list(&module->procedures, procSym, ir_symbol, auxElt)
			{
				if(procSym->offset == ip + module->codeOffset)
				{
					printf("\nprocedure %.*s:\n", (int)procSym->name.len, procSym->name.ptr);
					break;
				}
			}
		}

		printf("  %2llu: %s ", ip + module->codeOffset, OP_STRINGS[opcode]);
		ip++;

		for(int i=0; i<OP_OPERAND_COUNT[opcode] && ip<size; i++)
		{
			u64 val = 0;
			u32 opdSize = OP_OPERAND_SIZE[opcode][i];
			switch(opdSize)
			{
				case 1:
					printf("%hhu ", bytecode[ip]);
					break;
				case 2:
					printf("%hi ", *(i16*)&bytecode[ip]);
					break;
				case 4:
					printf("(%i, %f) ", *(i32*)&bytecode[ip], *(f32*)&bytecode[ip]);
					break;
				case 8:
				{
					if(opcode == OP_CALL)
					{
						u64 callAddress = *(u64*)&bytecode[ip];
						mp_string name = {0};
						for_each_in_list(&workspace->modules, searchMod, q_module, listElt)
						{
							if(callAddress >= searchMod->codeOffset && callAddress < (searchMod->codeOffset + searchMod->codeLen))
							{
								for_each_in_list(&searchMod->procedures, procSym, ir_symbol, auxElt)
								{
									if(procSym->offset == callAddress)
									{
										name = procSym->name;
										break;
									}
								}
							}
						}
						printf("%llu ", callAddress);
						if(name.len)
						{
							printf("(procedure %.*s) ", (int)name.len, name.ptr);
						}
					}
					else
					{
						printf("(%lli, %f) ", *(i64*)&bytecode[ip], *(f64*)&bytecode[ip]);
					}
				} break;
			}
			ip += opdSize;
		}

		if(opcode == OP_CALL)
		{
			u64 callAddress = *(u64*)&bytecode[ip - 8];


		}

		printf("\n");
	}
}


void debug_print_program_bytecode(q_workspace* workspace)
{
	printf("----------------------------\n");
	printf("Program bytecode:\n");
	printf("----------------------------\n");

	q_program* program = &workspace->program;
	u8* bytecode = (u8*)program->contents + program->codeOffset;
	u64 size = program->codeSize;

	u32 ip = 0;
	while(ip < size)
	{
		u8 opcode = bytecode[ip];
/*
		if(opcode == OP_ENTER)
		{
			//TODO: print name of procedure here
			for_each_in_list(&module->procedures, procSym, ir_symbol, auxElt)
			{
				if(procSym->offset == ip + module->codeOffset)
				{
					printf("\nprocedure %.*s:\n", (int)procSym->name.len, procSym->name.ptr);
					break;
				}
			}
		}
*/
//		printf("  %2llu: %s ", ip + module->codeOffset, OP_STRINGS[opcode]);
		printf("  %2u: %s ", ip, OP_STRINGS[opcode]);
		ip++;

		for(int i=0; i<OP_OPERAND_COUNT[opcode] && ip<size; i++)
		{
			u64 val = 0;
			u32 opdSize = OP_OPERAND_SIZE[opcode][i];
			switch(opdSize)
			{
				case 1:
					printf("%hhu ", bytecode[ip]);
					break;
				case 2:
					printf("%hi ", *(i16*)&bytecode[ip]);
					break;
				case 4:
					printf("(%i, %f) ", *(i32*)&bytecode[ip], *(f32*)&bytecode[ip]);
					break;
				case 8:
				{
					if(opcode == OP_CALL)
					{
						u64 callAddress = *(u64*)&bytecode[ip];
/*						mp_string name = {0};
						for_each_in_list(&workspace->modules, searchMod, q_module, listElt)
						{
							if(callAddress >= searchMod->codeOffset && callAddress < (searchMod->codeOffset + searchMod->codeLen))
							{
								for_each_in_list(&searchMod->procedures, procSym, ir_symbol, auxElt)
								{
									if(procSym->offset == callAddress)
									{
										name = procSym->name;
										break;
									}
								}
							}
						}
*/						printf("%llu ", callAddress);
/*						if(name.len)
						{
							printf("(procedure %.*s) ", (int)name.len, name.ptr);
						}
*/					}
					else
					{
						printf("(%lli, %f) ", *(i64*)&bytecode[ip], *(f64*)&bytecode[ip]);
					}
				} break;
			}
			ip += opdSize;
		}

		if(opcode == OP_CALL)
		{
			u64 callAddress = *(u64*)&bytecode[ip - 8];


		}

		printf("\n");
	}
}


#undef LOG_SUBSYSTEM
