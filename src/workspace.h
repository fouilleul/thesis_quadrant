/************************************************************//**
*
*	@file: workspace.h
*	@author: Martin Fouilleul
*	@date: 10/05/2022
*	@revision:
*
*****************************************************************/
#ifndef __WORKSPACE_H_
#define __WORKSPACE_H_

#include"typedefs.h"
#include"memory.h"
#include"lists.h"
#include"hashmap.h"
#include"cells.h"
#include"ast.h"
#include"ir.h"

typedef enum { Q_MODULE_INIT, Q_MODULE_BUILDING, Q_MODULE_BUILT } q_module_status;

typedef struct q_module
{
	list_elt listElt;
	list_elt buildListElt;

	bool transient;
	mp_string path;
	u64 pathHash;

	bool dirty;

	cell_tree tree;

	q_module_status status;
	u64 buildCounter;
	u64 moduleIndex;

	//TODO: regroup in ast_tree?
	//NOTE: we don't use a normal pool here because we want to persist generation index in-between allocations
	q_ast* ast;
	mem_arena astArena;
	list_info astFreeList;
	list_info parsingErrorFreeList;

	list_info parsingErrors;
	list_info checkingErrors;

	mem_arena symbolArena;
	ir_scope* globalScope;
	u64 globalSize;
	u64 rodataSize;
	list_info procedures;
	list_info types;
	list_info rodata;
	list_info polyProcInstances;
	list_info foreignBlocks;
	list_info editorWidgets;

	u64 codeOffset;
	u64 codeLen;

} q_module;

////////////////////////////////////////////////////////////////
//TODO: put somewhere else? (visible by both workspace and vm?)

typedef struct q_program
{
	list_info foreignDeps;
	u64 foreignSymbolsCount;

	u64 bssSize;

	u64 rodataOffset;
	u64 rodataSize;

	u64 widgetsOffset;
	u64 widgetsSize;

	u64 codeOffset;
	u64 codeSize;

	u64 locOffset;
	u64 locSize;
	u64 locCount;

	u64 modulesOffset;
	u64 modulesCount;
	u64 modulesSize;

	u64 targetBlocksOffset;
	u64 targetBlocksCount;

	char* contents;

} q_program;

////////////////////////////////////////////////////////////////

typedef struct q_search_path
{
	list_elt listElt;
	mp_string string;
} q_search_path;

typedef struct q_workspace
{
	list_info searchPaths;

	mem_pool modulePool;
	list_info modules;
	u64 nextTransientModuleID;

	q_hashmap moduleMap;

	q_module* mainModule;

	list_info modifiedCells;

	u64 buildCounter;

	mem_arena programArena;
	q_program program;

} q_workspace;


void workspace_init(q_workspace* workspace);

q_module* module_alloc_transient(q_workspace* workspace);
void module_recycle(q_workspace* workspace, q_module* module);
q_module* module_load(q_workspace* workspace, mp_string path);
void module_save(q_workspace* workspace, q_module* module, mp_string path);

q_module* workspace_main_module(q_workspace* workspace);
void workspace_set_main_module(q_workspace* workspace, q_module* module);

void workspace_begin_modify(q_workspace* workspace);
void workspace_mark_modified(q_workspace* workspace, q_cell* cell);
void workspace_rebuild(q_workspace* workspace);

q_error* q_error_alloc(q_module* module);

#endif //__WORKSPACE_H_
