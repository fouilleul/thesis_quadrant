/************************************************************//**
*
*	@file: hashmap.h
*	@author: Martin Fouilleul
*	@date: 25/02/2022
*	@revision:
*
*****************************************************************/
#ifndef __HASHMAP_H_
#define __HASHMAP_H_

#include<immintrin.h>
#include"typedefs.h"

//------------------------------------------------------------------------------------
// Hash to u64 hashmap
//------------------------------------------------------------------------------------

//TODO: abstract this and put it in the platform layer
static inline u64 hash_aesdec_u64(u64 x)
{
	u8 seed[16] = {
    	0xaa, 0x9b, 0xbd, 0xb8,
    	0xa1, 0x98, 0xac, 0x3f,
    	0x1f, 0x94, 0x07, 0xb3,
    	0x8c, 0x27, 0x93, 0x69 };

	__m128i hash = _mm_set_epi64x(0L, x);
	__m128i key = _mm_loadu_si128((__m128i*)seed);
	hash = _mm_aesdec_si128(hash, key);
	hash = _mm_aesdec_si128(hash, key);
	u64 result = _mm_extract_epi64(hash, 0);

	return(result);
}

static inline u64 hash_aesdec_u64_x2(u64 x, u64 y)
{
	u8 seed[16] = {
    	0xaa, 0x9b, 0xbd, 0xb8,
    	0xa1, 0x98, 0xac, 0x3f,
    	0x1f, 0x94, 0x07, 0xb3,
    	0x8c, 0x27, 0x93, 0x69 };

	__m128i hash = _mm_set_epi64x(x, y);
	__m128i key = _mm_loadu_si128((__m128i*)seed);
	hash = _mm_aesdec_si128(hash, key);
	hash = _mm_aesdec_si128(hash, key);
	u64 result = _mm_extract_epi64(hash, 0);

	return(result);
}

u64 q_hash_string(mp_string string);

typedef struct q_hashmap_slot
{
	u64 key;
	u64 val;
} q_hashmap_slot;

typedef struct q_hashmap
{
	u64 mask;
	u64 filled;

	q_hashmap_slot* slots;

} q_hashmap;

const u64 Q_HASHMAP_FILLED = 1ULL<<63,
          Q_HASHMAP_DEFAULT_SIZE = 1024;

void q_hashmap_clear(q_hashmap* map);
void q_hashmap_cleanup(q_hashmap* map);
void q_hashmap_insert(q_hashmap* map, u64 key, u64 val);
void q_hashmap_remove_index(q_hashmap* map, u64 index);
void q_hashmap_remove(q_hashmap* map, u64 key);
bool q_hashmap_find(q_hashmap* map, u64 key, u64* outValue);

#endif //__HASHMAP_H_
