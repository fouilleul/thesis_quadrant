/************************************************************//**
*
*	@file: editor.cpp
*	@author: Martin Fouilleul
*	@date: 10/05/2022
*	@revision:
*
*****************************************************************/
#include"typedefs.h"
#include"cells.h"
#include"tempo_editor.h"
#include"tokens.h"
#include"completions.h"
#include"sched_curves.h"
#include"bytecode.h"


#define LOG_SUBSYSTEM "Editor"

typedef struct qed_tab
{
	list_elt listElt;

	q_module* module;
	qed_point cursor;
	qed_point mark;

	// GUI
	f32 contentsWidth;
	f32 contentsHeight;
	f32 scrollX;
	f32 scrollY;

} qed_tab;

const u32 QED_MAX_TABS = 128;

typedef struct q_completion_item
{
	list_elt listElt;
	q_completion completion;
} q_completion_item;

typedef struct q_completion_panel
{
	bool active;
	mp_aligned_rect frame;

	cell_kind savedKind;
	mp_string savedText;

	q_cell* completionCell;
	q_cell* textCell;

	u32 count;
	i32 index;
	list_info items;
	q_completion* completions;
	mem_arena arena;

} q_completion_panel;

typedef struct q_editor
{
	mp_gui_context* gui;
	q_workspace* workspace;

	// tabs
	qed_tab tabsStore[QED_MAX_TABS];
	list_info tabsFreeList;
	list_info tabs;
	u32 tabCount;
	qed_tab* currentTab;
	qed_tab* prevCurrentTab;

	// cursor
	bool scrollToCursorOnNextFrame;
	vec2 animatedCursor;
	f32 cursorBlinkTimer;
	bool cursorBlinkState;

	// layout
	float lineHeight;
	float spaceWidth;

	// completion panel
	q_completion_panel completionPanel;

	// VM commands
	q_ringbuffer* commandBuffer;

} q_editor;

//------------------------------------------------------------------------------------------
// qed_point helpers
//------------------------------------------------------------------------------------------
bool qed_point_same_cell(qed_point a, qed_point b)
{
	return(  a.parent == b.parent
	      && a.leftFrom == b.leftFrom);
}

bool qed_point_equal(qed_point a, qed_point b)
{
	return(  a.parent == b.parent
	      && a.leftFrom == b.leftFrom
	      && a.textIndex == b.textIndex);
}

q_cell* qed_point_left_cell(qed_point point)
{
	return(point.leftFrom ?
	       ListPrevEntry(&point.parent->children, point.leftFrom, q_cell, listElt) :
	       ListLastEntry(&point.parent->children, q_cell, listElt));
}

q_cell* qed_point_right_cell(qed_point point)
{
	return(point.leftFrom ? point.leftFrom : 0);
}

qed_point qed_prev_point(qed_point point)
{
	if(cell_has_text(point.parent)
	  && point.textIndex > 0)
	{
		point.textIndex = utf8_prev_offset(point.parent->text.string, point.textIndex);
	}
	else
	{
		q_cell* leftSibling = qed_point_left_cell(point);
		if(leftSibling)
		{
			if( cell_has_children(leftSibling)
			  ||cell_has_text(leftSibling))
			{
				//NOTE(martin): new point is at the end of the left sibling's children list
				point.leftFrom = 0;
				point.parent = leftSibling;
				if(point.parent->kind == CELL_HOLE)
				{
					point.textIndex = 0;
				}
				else
				{
					point.textIndex = point.parent->text.string.len;
				}
			}
			else
			{
				//NOTE(martin): new point is before left sibling
				point.leftFrom = leftSibling;
			}
		}
		else if(point.parent->parent)
		{
			//NOTE(martin): new point is before the parent
			point.leftFrom = point.parent;
			point.parent = point.parent->parent;
			point.textIndex = 0;
		}
	}
	return(point);
}

qed_point qed_next_point(qed_point point)
{
	if(cell_has_text(point.parent)
	  && point.textIndex < point.parent->text.string.len
	  && point.parent->kind != CELL_HOLE)
	{
		point.textIndex = utf8_next_offset(point.parent->text.string, point.textIndex);
	}
	else if(point.leftFrom)
	{
		if( cell_has_children(point.leftFrom)
		  ||cell_has_text(point.leftFrom))
		{
			//NOTE(martin): new point is at the begining of right sibling
			point.parent = point.leftFrom;
			point.leftFrom = cell_first_child(point.leftFrom);
			point.textIndex = 0;
		}
		else
		{
			//NOTE(martin): next point is after right sibling
			point.leftFrom = cell_next_sibling(point.leftFrom);
		}
	}
	else if(point.parent->parent)
	{
		//NOTE(martin): new point is after parent
		point.leftFrom = cell_next_sibling(point.parent);
		point.parent = point.parent->parent;
		point.textIndex = 0;
	}
	return(point);
}

void qed_cell_build_ancestor_array(mem_arena* arena, q_cell* child, u32* outCount, q_cell*** outCellPointerArray)
{
	//NOTE(martin): build an array including child and all its ancestors up to the root
	q_cell** ancestors = mem_arena_alloc_type(arena, q_cell*);
	ancestors[0] = child;
	u32 ancestorCount = 1;

	for(q_cell* cell = child->parent;
	    cell;
	    cell = cell->parent)
	{
		q_cell** slot = mem_arena_alloc_type(arena, q_cell*);
		//WARN(martin): this assumes arena allocate contiguously, which is true for x64 style arenas,
		//              but not chunked arenas, so assert if we change the arena in the future
		DEBUG_ASSERT(slot == (ancestors+ancestorCount));

		*slot = cell;
		ancestorCount++;
	}
	*outCount = ancestorCount;
	*outCellPointerArray = ancestors;
}

qed_cell_span qed_cell_span_from_points(qed_point point, qed_point mark)
{
	/*NOTE:
		We want to select a span of sibling cells [S, E] where 1 edit point live directly to the left of S or in the subtree of S,
		and the other edit point lives directly right to E or in the subtree of E.

		- We build two lists from point and mark's parents up to the root
		- go downard in lockstep and detect when they diverge
		- this gives us a common ancestor (which can be one or both of the parents), so we have three cases:
			1) point and mark have the same parent (of course this can be detected early):
				-> S is the the right of the first edit point (in siblings order), E is to the left of the second edit point.
			2) One edit point (say P0) is in the common ancestor, the other (P1) is in the subtree of one of the common ancestor's children
				-> if M0 is to the left of the subtree in which P1 lies, S is to the right of M0 and E is the root of P1's subtree.
				   otherwise S is the root of P1's subtree and E is to the left of P0
			3) P0 and P1 are in two distinct subtrees T0 and T1 of the common ancestor
				-> S is the root of the first subtree, E is the root of the second subtree.
	*/

	if(point.parent == mark.parent)
	{
		//NOTE(martin): detect case 1) early
		for_each_in_list(&point.parent->children, child, q_cell, listElt)
		{
			if(child == point.leftFrom)
			{
				return((qed_cell_span){point.leftFrom, qed_point_left_cell(mark)});
			}
			else if(child == mark.leftFrom)
			{
				return((qed_cell_span){mark.leftFrom, qed_point_left_cell(point)});
			}
		}
		return((qed_cell_span){});
	}

	//NOTE(martin): find common ancestor of point.parent and mark.parent
	q_cell** pointAncestors = 0;
	u32 pointAncestorCount = 0;

	q_cell** markAncestors = 0;
	u32 markAncestorCount = 0;

	mem_arena* scratch = mem_scratch();
	qed_cell_build_ancestor_array(scratch, point.parent, &pointAncestorCount, &pointAncestors);
	qed_cell_build_ancestor_array(scratch, mark.parent, &markAncestorCount, &markAncestors);

	u32 minAncestorCount = minimum(pointAncestorCount, markAncestorCount);
	q_cell** pointIterator = pointAncestors + (pointAncestorCount-1);
	q_cell** markIterator = markAncestors + (markAncestorCount-1);

	q_cell* commonAncestor = *pointIterator;

	for(u32 i=0; i < minAncestorCount; i++)
	{
		if(*pointIterator != *markIterator)
		{
			break;
		}
		commonAncestor = *pointIterator;

		if(i < pointAncestorCount - 1)
		{
			pointIterator--;
		}
		if(i < markAncestorCount - 1)
		{
			markIterator--;
		}
	}
	//NOTE(martin): here, commonAncestor is set to the last cell before the lineage diverge. pointIterator and markIterator
	//              point to the subtrees in which point and mark live. If point (resp. mark) is between children of the common ancestor,
	//              pointIterator (resp. markIterator) points to the common ancestor.

	if(point.parent == commonAncestor || mark.parent == commonAncestor)
	{
		//NOTE(martin): case 2)
		qed_point p0 = (point.parent == commonAncestor) ? point : mark;
		q_cell* subTree = (point.parent == commonAncestor) ? *markIterator : *pointIterator;

		for_each_in_list(&commonAncestor->children, child, q_cell, listElt)
		{
			if(child == p0.leftFrom)
			{
				return((qed_cell_span){p0.leftFrom, subTree});
			}
			else if(child == subTree)
			{
				return((qed_cell_span){subTree, qed_point_left_cell(p0)});
			}
		}
		DEBUG_ASSERT(0, "unreachable");
		return((qed_cell_span){});
	}
	else
	{
		//NOTE(martin): case 3)
		for_each_in_list(&commonAncestor->children, child, q_cell, listElt)
		{
			if(child == *pointIterator)
			{
				return((qed_cell_span){*pointIterator, *markIterator});
			}
			else if(child == *markIterator)
			{
				return((qed_cell_span){*markIterator, *pointIterator});
			}
		}
		DEBUG_ASSERT(0, "unreachable");
		return((qed_cell_span){});
	}
}
//------------------------------------------------------------------------------------
// cell box helpers
//------------------------------------------------------------------------------------
mp_aligned_rect qed_combined_box(mp_aligned_rect a, mp_aligned_rect b)
{
	f32 x0 = minimum(a.x, b.x);
	f32 y0 = minimum(a.y, b.y);
	f32 x1 = maximum(a.x + a.w, b.x + b.w);
	f32 y1 = maximum(a.y + a.h, b.y + b.h);
	return((mp_aligned_rect){x0, y0, x1 - x0, y1 - y0});
}

mp_aligned_rect qed_cell_frame_box(q_cell* cell)
{
	mp_aligned_rect r = cell->box;
	r.x += cell->frameOffset;
	r.w -= cell->frameOffset;
	return(r);
}

f32 qed_cell_left_decorator_width(q_editor* editor, q_cell* cell)
{
	//NOTE(martin): return the width of the left decorator of a cell, which can be '(', '[', '"', '@(', or none.
	//WARN(martin): here we assume that all decorator characters are single width, which may not be true for every font.
	f32 width = 0;
	switch(cell->kind)
	{
		case CELL_LIST:
		case CELL_ARRAY:
		case CELL_STRING:
		case CELL_CHAR:
		case CELL_UI:
			width = editor->spaceWidth;
			break;
		case CELL_ATTRIBUTE:
		case CELL_COMMENT:
			width = 2*editor->spaceWidth;
			break;
		default:
			break;
	}
	return(width);
}

f32 qed_cell_right_decorator_width(q_editor* editor, q_cell* cell)
{
	//NOTE(martin): return the width of the right decorator of a cell, which can be ')', ']', '"' or none.
	//WARN(martin): here we assume that all decorator characters are single width, which may not be true for every font.
	f32 width = 0;

	switch(cell->kind)
	{
		case CELL_LIST:
		case CELL_ARRAY:
		case CELL_STRING:
		case CELL_CHAR:
		case CELL_ATTRIBUTE:
			width = editor->spaceWidth;
			break;

		case CELL_COMMENT:
			width = 2*editor->spaceWidth;
			break;
		default:
			break;
	}
	return(width);
}

mp_aligned_rect qed_cell_contents_box(q_editor* editor, q_cell* cell)
{
	mp_aligned_rect r = qed_cell_frame_box(cell);

	f32 leftDecoratorW = qed_cell_left_decorator_width(editor, cell);
	f32 rightDecoratorW = qed_cell_right_decorator_width(editor, cell);
	r.x += leftDecoratorW;
	r.w -= (leftDecoratorW + rightDecoratorW);

	if(cell->box.w > cell->lastLineWidth)
	{
		r.w = cell->box.x + cell->box.w - r.x;
	}
	return(r);
}

f32 qed_display_offset_for_text_index(q_editor* editor, mp_string text, u32 textIndex)
{
	mp_graphics_context graphics = mp_gui_get_graphics(editor->gui);
	mp_string leftText = mp_string_slice(text, 0, textIndex);
	mp_aligned_rect leftTextBox = mp_graphics_text_bounding_box(graphics, leftText);
	return(leftTextBox.w);
}

//------------------------------------------------------------------------------------
// Display <-> edit point correspondence
//------------------------------------------------------------------------------------
vec2 qed_point_to_display_pos(q_editor* editor, qed_point point)
{
	f32 lineHeight = editor->lineHeight;

	vec2 cursorPos = {};

	if(point.leftFrom)
	{
		cursorPos.x = point.leftFrom->box.x;
		cursorPos.y = point.leftFrom->box.y;
	}
	else if(cell_has_text(point.parent))
	{
		mp_string string = point.parent->text.string;
		mp_aligned_rect box = qed_cell_contents_box(editor, point.parent);
		cursorPos.x = box.x + qed_display_offset_for_text_index(editor, string, point.textIndex);
		cursorPos.y = box.y;
	}
	else if(!ListEmpty(&point.parent->children))
	{
		q_cell* leftSibling = ListLastEntry(&point.parent->children, q_cell, listElt);
		cursorPos.x = leftSibling->box.x + leftSibling->lastLineWidth;
		cursorPos.y = leftSibling->box.y + leftSibling->box.h - lineHeight;
	}
	else
	{
		mp_aligned_rect box = qed_cell_contents_box(editor, point.parent);
		cursorPos.x = box.x;
		cursorPos.y = box.y;
	}
	return(cursorPos);
}

//------------------------------------------------------------------------------------------
// Tabs management
//------------------------------------------------------------------------------------------
qed_tab* qed_current_tab(q_editor* editor)
{
	return(editor->currentTab);
}

q_module* qed_current_module(q_editor* editor)
{
	return(editor->currentTab->module);
}

cell_tree* qed_current_tree(q_editor* editor)
{
	return(&editor->currentTab->module->tree);
}

qed_tab* qed_tab_next(q_editor* editor, qed_tab* tab)
{
	qed_tab* next = ListNextEntry(&editor->tabs, tab, qed_tab, listElt);
	if(!next)
	{
		next = ListFirstEntry(&editor->tabs, qed_tab, listElt);
	}
	return(next);
}

void qed_tab_select(q_editor* editor, qed_tab* tab)
{
	editor->currentTab = tab;
}

void qed_tab_select_next(q_editor* editor)
{
	qed_tab* tab = qed_tab_next(editor, editor->currentTab);
	qed_tab_select(editor, tab);
}

qed_tab* qed_tab_open(q_editor* editor, q_module* module)
{
	qed_tab* tab = ListPopEntry(&editor->tabsFreeList, qed_tab, listElt);
	memset(tab, 0, sizeof(qed_tab));

	if(tab)
	{
		tab->module = module;
		tab->cursor.parent = module->tree.rootCell;
		tab->cursor.leftFrom = cell_first_child(tab->cursor.parent);
		tab->cursor.textIndex = 0;
		tab->mark = tab->cursor;

		if(editor->currentTab)
		{
			ListInsert(&editor->tabs, &editor->currentTab->listElt, &tab->listElt);
		}
		else
		{
			ListPush(&editor->tabs, &tab->listElt);
		}
		editor->tabCount++;
		qed_tab_select(editor, tab);

		parse_module(tab->module);
	}
	else
	{
		//TODO: emit error in message box
		LOG_ERROR("Can't open new tab: too much tabs in use\n");
	}
	return(tab);
}

void qed_tab_open_transient(q_editor* editor)
{
	q_module* module = module_alloc_transient(editor->workspace);
	if(!qed_tab_open(editor, module))
	{
		module_recycle(editor->workspace, module);
	}
}

void qed_tab_close(q_editor* editor, qed_tab* tab)
{
	qed_tab* nextTab = (tab == editor->currentTab) ?
	                   ListPrevEntry(&editor->tabs, editor->currentTab, qed_tab, listElt) :
	                   editor->currentTab;
	if(!nextTab)
	{
		nextTab = ListNextEntry(&editor->tabs, editor->currentTab, qed_tab, listElt);
	}

	if(workspace_main_module(editor->workspace) == tab->module
	  && tab->module->transient)
	{
		module_recycle(editor->workspace, tab->module);
	}

	ListRemove(&editor->tabs, &tab->listElt);
	tab->module = 0;
	ListPush(&editor->tabsFreeList, &tab->listElt);

	editor->tabCount--;
	qed_tab_select(editor, nextTab);
	if(!editor->currentTab)
	{
		qed_tab_open_transient(editor);
	}
}

void qed_tab_close_current(q_editor* editor)
{
	qed_tab_close(editor, editor->currentTab);
}

//------------------------------------------------------------------------------------------
// Editor Init
//------------------------------------------------------------------------------------------
void qed_init(q_editor* editor, q_workspace* workspace, q_ringbuffer* commandBuffer)
{
	editor->workspace = workspace;
	editor->commandBuffer = commandBuffer;

	mem_arena_init(&editor->completionPanel.arena);

	for(int i=0; i<QED_MAX_TABS; i++)
	{
		ListPush(&editor->tabsFreeList, &editor->tabsStore[i].listElt);
	}
	qed_tab_open_transient(editor);
	workspace_set_main_module(editor->workspace, qed_current_module(editor));
}

//------------------------------------------------------------------------------------
// UI display constants
//------------------------------------------------------------------------------------
const f32 QED_MARGIN_X    = 10,
          QED_MARGIN_Y    = 10,
          QED_LINE_GUTTER = 5,
          QED_CELL_MIN_UI_WIDTH = 200,
          QED_CELL_MIN_UI_HEIGHT = 100;


const mp_graphics_color QED_COLOR_TEXT = {1, 1, 1, 1},
                        QED_COLOR_KEYWORD = {0.797, 0.398, 0.359, 1},
                        QED_COLOR_COMMENT = {0.94, 0.59, 0.21, 1},
                        QED_COLOR_STRING = {0, 0.9, 0, 1},
                        QED_COLOR_UNKNOWN = {1, 0, 1, 1},
                        QED_COLOR_HOLE = {0, 1, 1, 1},
                        QED_COLOR_UI = {0, 0.5, 0.8, 1},
                        QED_COLOR_COMPLETION_HELP = {0.4, 0.7, 0.7, 1};

//------------------------------------------------------------------------------------------
// Cell layout
//------------------------------------------------------------------------------------------
/*NOTE(martin):

	We separate layout, which compute the starting position of each cell relative to its parent's contents area,
	and printing, which both draws the cells and compute absolute coordinates that can then be used to draw boxes,
	markers, and determine cursor positions. This allows layout of a cell to depend on the dimensions of its children.

	Layout is controlled by layout options, which are determined by the kind of the AST node corresponding to a cell.

	List cells are laid out horizontally, unless one of these conditions is met:
		- the options specify a vertical orientation.
		- the cumulated width of the children, excluding progress wheels and UI panels (which we refer to as the "dry" width),
		  is more than CELL_LAYOUT_MAX_WIDTH.
		- the number of children is more than CELL_LAYOUT_MAX_CHILD_COUNT.

	Vertical layout is done as follows:
		- options.inlineCount children are laid out horizontally at the beginning of the cell
		- Then, we lay out options.alignedGroupCount groups of options.alignedGroupSize children vertically,
		  aligned to the end of the inline section.
		- Then we lay out groups of options.indentedGroupSize children vertically, aligned 2 spaces right from
		  the left side of the cell.

	Example:

		(for (var i i32 0)
		     (< i 10)
		     (set i (+ i 1))
		  (put "Hello " i))

		Here we have one inline child (the "for" token), three aligned groups of one cell each, and one indented group of one cell

	While we lay out cells, we keep track of the width and height of each cell, but we also track the dry width, which is the width
	the cell would have if itself and its children didn't contain any progress wheels or UI panels.
	This is done so that we can decide to break cells into a multiline layout based on the dry width. This avoids situations where
	the execution of a pause, or the opening of a UI panel would flip the layout to vertical, which would be confusing (especially
	for quick pauses that would flip the layout back and forth without a user interaction).

	//TODO: define the different areas of a cell: content, frame, bounding box
	        explain how attributes are laid out

*/

typedef enum { CELL_LAYOUT_HORIZONTAL,
               CELL_LAYOUT_VERTICAL } cell_layout_orientation;

const f32 CELL_LAYOUT_MAX_WIDTH = 2000;
const u32 CELL_LAYOUT_MAX_CHILD_COUNT = 8;

typedef struct cell_layout_options
{
	cell_layout_orientation preferredOrientation;
	i32 inlineCount;
	i32 alignedGroupCount;
	i32 alignedGroupSize;
	i32 indentedGroupSize;

	bool endGap;

} cell_layout_options;

cell_layout_options qed_cell_get_layout_options(q_editor* editor, q_cell* cell)
{
	cell_layout_options layout = {.preferredOrientation = CELL_LAYOUT_HORIZONTAL,
	                              .inlineCount = 1,
	                              .alignedGroupCount = 0,
	                              .indentedGroupSize = 1 };
	//TODO non-default
	q_ast* ast = cell_ast_lookup(cell);
	if(ast)
	{
		switch(ast->kind)
		{
			case AST_ROOT:
				layout.preferredOrientation = CELL_LAYOUT_VERTICAL;
				layout.inlineCount = 0;
				layout.alignedGroupCount = -1;
				layout.alignedGroupSize = 1;
				layout.indentedGroupSize = 0;
				break;

        	case AST_PROC_DECL:
				layout.endGap = true;
				layout.preferredOrientation = CELL_LAYOUT_VERTICAL;
				layout.inlineCount = 3;
				break;

        	case AST_PROC_SIG:
				layout.preferredOrientation = CELL_LAYOUT_HORIZONTAL;
				layout.inlineCount = 0;
				layout.alignedGroupCount = -1;
				layout.alignedGroupSize = 2;
				break;

			case AST_VAR_DECL:
				layout.inlineCount = 3;
				break;

			case AST_TYPE_DECL:
				layout.inlineCount = 2;
				layout.endGap = true;
				break;

        	case AST_TYPE_STRUCT:
				layout.preferredOrientation = CELL_LAYOUT_VERTICAL;
				layout.inlineCount = 1;
				layout.indentedGroupSize = 2;
				break;

			case AST_IF:
				layout.preferredOrientation = CELL_LAYOUT_HORIZONTAL;
				layout.inlineCount = 1;
				layout.alignedGroupCount = 3;
				layout.alignedGroupSize = 1;
				break;

			case AST_FOR:
				layout.preferredOrientation = CELL_LAYOUT_VERTICAL;
				layout.inlineCount = 1;
				layout.alignedGroupCount = 3;
				layout.alignedGroupSize = 1;
				break;

			case AST_WHILE:
				layout.preferredOrientation = CELL_LAYOUT_VERTICAL;
				layout.inlineCount = 1;
				layout.alignedGroupCount = 1;
				layout.alignedGroupSize = 1;
				break;

			case AST_DO:
				layout.preferredOrientation = CELL_LAYOUT_VERTICAL;
				break;

			case AST_FLOW:
				layout.preferredOrientation = CELL_LAYOUT_VERTICAL;
				break;

			case AST_FOREIGN_BLOCK:
				layout.preferredOrientation = CELL_LAYOUT_VERTICAL;
				layout.inlineCount = 2;
				layout.endGap = true;
				break;

			case AST_ARRAY:
				layout.inlineCount = 0;
				layout.alignedGroupCount = -1;
				layout.alignedGroupSize = 8;
				layout.endGap = true;
				break;

			default:
				break;
		}
	}
	return(layout);
}

typedef struct layout_result
{
	f32 width;         //NOTE(martin): total width of token
	f32 height;        //NOTE(martin): total height of token
	f32 lastLineWidth; //NOTE(martin): width of last line of contents

	f32 dryWidth;          //NOTE(martin): width of token excluding wheels and UI panels.
	f32 dryLastLineWidth;  //NOTE(martin): width of last line of contents, excluding wheels and UI panels.

	bool vertical;  //NOTE(martin): true if the cell has been vertically laid out
	bool endGap;

} layout_result;

layout_result qed_update_layout(mp_gui_context* gui, q_editor* editor, q_cell* cell, vec2 pos);

layout_result qed_update_layout_list(mp_gui_context* gui, q_editor* editor, q_cell* cell)
{
	layout_result result = {};

	//NOTE(martin): first compute dimensions of children and layout horizontally
	result.height = editor->lineHeight;

	layout_result* childResults = mem_arena_alloc_array(mem_scratch(), layout_result, cell->childCount);
	vec2 childPos = {0, 0};
	u32 childIndex = 0;

	for_each_in_list(&cell->children, child, q_cell, listElt)
	{
		childResults[childIndex] = qed_update_layout(gui, editor, child, childPos);
		childPos.x += childResults[childIndex].width;

		result.width += childResults[childIndex].width;
		result.dryWidth += childResults[childIndex].dryWidth;

		if(child != ListLastEntry(&cell->children, q_cell, listElt))
		{
			result.dryWidth += editor->spaceWidth;
			result.width += editor->spaceWidth;
			childPos.x += editor->spaceWidth;
		}
		result.height = maximum(result.height, childResults[childIndex].height);
		result.vertical = result.vertical || childResults[childIndex].vertical;

		childIndex++;
	}
	result.lastLineWidth = result.width;
	result.dryLastLineWidth = result.dryWidth;


	//NOTE(martin): if preferred layout is vertical, or one of the children was vertical,
	//              or dry width or number of children exceeds some threshold, we relayout vertically
	cell_layout_options options = qed_cell_get_layout_options(editor, cell);
	result.endGap = options.endGap;

	result.vertical = result.vertical
		            ||(options.preferredOrientation == CELL_LAYOUT_VERTICAL)
		            ||(cell->childCount > CELL_LAYOUT_MAX_CHILD_COUNT)
		            ||(result.dryWidth > CELL_LAYOUT_MAX_WIDTH);

	if(result.vertical)
	{
		result.width = 0;
		result.dryWidth = 0;
		result.height = editor->lineHeight;

		typedef enum { LAYOUT_INLINE, LAYOUT_ALIGNED, LAYOUT_INDENTED } layout_status;
		layout_status status = LAYOUT_INLINE;

		i32 groupSize = 0;
		i32 groupCount = 0;
		i32 maxGroupSize = options.inlineCount;

		f32 align = 0;
		f32 dryAlign = 0;
		f32 maxWidth = 0;
		f32 dryMaxWidth = 0;
		f32 lineHeight = editor->lineHeight;

		f32 dryChildX = 0;
		childPos = (vec2){0, 0};

		childIndex = 0;

		for_each_in_list(&cell->children, child, q_cell, listElt)
		{
			//------------------------------------------------------------
			//NOTE(martin): count groups and switch between layout modes
			//------------------------------------------------------------
			if(childIndex)
			{
				//NOTE(martin): attributes are horizontally aligned with their prev siblings, and don't count
				//              towards inline/aligned counts.
				if(child->kind != TOKEN_ATTRIBUTE)
				{
					groupSize++;
					if(status == LAYOUT_INLINE)
					{
						align = childPos.x + editor->spaceWidth;
						dryAlign = dryChildX + editor->spaceWidth;
					}
				}
			}

			bool endOfLine = false;
			if(groupSize == maxGroupSize)
			{
				groupSize = 0;
				groupCount++;

				//NOTE(martin): end of line is generated at the end of an aligned or indented group
				endOfLine = (status == LAYOUT_ALIGNED || status == LAYOUT_INDENTED);

				if(status == LAYOUT_INLINE)
				{
					//NOTE(martin): at the end of the inline group, we switch to aligned layout
					groupCount = 0;
					maxGroupSize = options.alignedGroupSize;
					status = LAYOUT_ALIGNED;
				}

				if(status == LAYOUT_ALIGNED && groupCount == options.alignedGroupCount)
				{
					//NOTE(martin): at the end of the aligned section, we switch to indented layout
					groupCount = 0;
					maxGroupSize = options.indentedGroupSize;
					align = 2*editor->spaceWidth;
					dryAlign = align;

					//NOTE(martin): we can fallback here just after the end of the inline group, so
					//              we need to force end of line in this case.
					endOfLine = true;
				}
			}

			if(endOfLine)
			{
				//NOTE(martin): end of line
				maxWidth = maximum(maxWidth, childPos.x);
				childPos.x = align;
				childPos.y += lineHeight + QED_LINE_GUTTER;
				lineHeight = editor->lineHeight;

				if(  childIndex
				  && childResults[childIndex-1].vertical
				  && childResults[childIndex-1].endGap)
				{
					childPos.y += lineHeight + QED_LINE_GUTTER;
				}

				dryMaxWidth = maximum(dryMaxWidth, dryChildX);
				dryChildX = dryAlign;
			}
			else if(childIndex)
			{
				//NOTE(martin): advance on the same line
				childPos.x += editor->spaceWidth;
				dryChildX += editor->spaceWidth;
			}

			//------------------------------------------------------------------
			//NOTE(martin): set children relative coordinates and adjust widths
			//------------------------------------------------------------------
			child->targetPos = childPos;

			childPos.x += childResults[childIndex].width;
			dryChildX += childResults[childIndex].dryWidth;
			lineHeight = maximum(lineHeight, childResults[childIndex].height);

			maxWidth = maximum(maxWidth, childPos.x);
			dryMaxWidth = maximum(dryMaxWidth, dryChildX);

			childIndex++;
		}
		result.dryWidth = dryMaxWidth;
		result.width = maxWidth;
		result.height = childPos.y + lineHeight;
		result.lastLineWidth = childPos.x;
		result.dryLastLineWidth = dryChildX;
	}

	//NOTE(martin): add width of parentheses / attribute marker
	f32 leftDecoratorW = qed_cell_left_decorator_width(editor, cell);
	f32 rightDecoratorW = qed_cell_right_decorator_width(editor, cell);

	if(result.lastLineWidth >= result.width)
	{
		result.width += rightDecoratorW;
	}
	if(result.dryLastLineWidth >= result.dryWidth)
	{
		result.dryWidth += rightDecoratorW;
	}

	result.width += leftDecoratorW;
	result.dryWidth += leftDecoratorW;

	return(result);
}

layout_result qed_update_layout(mp_gui_context* gui, q_editor* editor, q_cell* cell, vec2 pos)
{
	layout_result result = {};

	cell->targetPos = pos;

	if(cell_has_text(cell))
	{
		if(!cell->text.string.len)
		{
			result.dryWidth = editor->spaceWidth;
		}
		else
		{
			mp_graphics_context graphics = mp_gui_get_graphics(gui);
			mp_aligned_rect textBox = mp_graphics_text_bounding_box(graphics, cell->text.string);
			result.dryWidth = textBox.w;
		}

		result.dryWidth += qed_cell_left_decorator_width(editor, cell);
		result.dryWidth += qed_cell_right_decorator_width(editor, cell);

		result.width = result.dryWidth;
		result.height = editor->lineHeight;

		if(cell->kind == CELL_UI)
		{
			qed_tab* tab =  qed_current_tab(editor);
			if(cell_has_ancestor(tab->cursor.parent, cell))
			{
				result.width = maximum(result.width, cell->ui.width);
				result.height = maximum(result.height, cell->ui.height + editor->lineHeight + QED_LINE_GUTTER);
			}
		}
	}
	else if(cell_has_children(cell))
	{
		result = qed_update_layout_list(gui, editor, cell);
	}

	//NOTE: If cell has active wheel, advance everything from the wheel size. Putting this at the bottom here
	//      simplifies computation above, since we don't have to worry about the presence/absence of a progress wheel.
	if(cell->progressTimeout || cell->waitingTimeout || cell->standbyTimeout)
	{
		result.width += editor->lineHeight;
	}
	return(result);
}

//------------------------------------------------------------------------------------------
// Cells Display
//------------------------------------------------------------------------------------------
//TODO: move somewhere else?
void qed_rebuild(q_editor* editor);
void qed_update_tempo_curve(q_editor* editor, q_module* module, q_cell* cell);
vec2 q_print_string(mp_gui_context* gui, mp_string string, vec2 pos, mp_graphics_color color);

void qed_display_cell(mp_gui_context* gui, q_editor* editor, q_cell* cell, vec2 origin)
{
	const f32 animationRatio = 0.8;

	vec2 absTargetPos = {origin.x + cell->targetPos.x,
	                     origin.y + cell->targetPos.y};

	if(cell->flags & CELL_INITIALIZED)
	{
		cell->box.x += animationRatio * (absTargetPos.x - cell->box.x);
		cell->box.y += animationRatio * (absTargetPos.y - cell->box.y);
	}
	else
	{
		cell->box.x = absTargetPos.x;
		cell->box.y = absTargetPos.y;
		cell->flags |= CELL_INITIALIZED;
	}
	vec2 pos = { cell->box.x, cell->box.y };

	mp_graphics_context graphics = mp_gui_get_graphics(gui);

	if(cell->flashValue > 0)
	{
		mp_graphics_set_color_rgba(graphics, 0, 0.5, 0, cell->flashValue / CELL_FLASH_LENGTH);
		//TODO: remove frame box
		mp_aligned_rect box = qed_cell_frame_box(cell);
		mp_graphics_rectangle_fill(graphics, box.x, box.y, box.w, box.h);
		cell->flashValue -= 1./60; //TODO: depends on frame rate
	}

	f32 targetFrameOffset = 0;

	if(cell->progressTimeout > 0)
	{
		f32 progressWheelAngle = 2*M_PI*cell->progress;

		if(progressWheelAngle > 0)
		{
			mp_graphics_arc(graphics,
		                	pos.x + editor->lineHeight/2.,
		                	pos.y + editor->lineHeight/2.,
		                	0.3*editor->lineHeight,
		                	progressWheelAngle,
		                	M_PI/2);
			mp_graphics_set_color_rgba(graphics, 0, 1, 0, 1);
			mp_graphics_set_width(graphics, 5);
			mp_graphics_stroke(graphics);

			targetFrameOffset += editor->lineHeight;
		}
		cell->progressTimeout--;
	}
	else
	{
		cell->progress = 0;
	}

	if(cell->waitingTimeout)
	{
		f32 waitWheelAngle = 2*M_PI*cell->waitingProgress;

		mp_graphics_arc(graphics,
		                pos.x + editor->lineHeight/2.,
		                pos.y + editor->lineHeight/2.,
		                0.3*editor->lineHeight,
		                M_PI/2,
		                M_PI/2 + waitWheelAngle);

		mp_graphics_set_color_rgba(graphics, 0, 1, 0, 1);
		mp_graphics_set_width(graphics, 5);
		mp_graphics_stroke(graphics);

		mp_graphics_arc(graphics,
		                pos.x + editor->lineHeight/2.,
		                pos.y + editor->lineHeight/2.,
		                0.3*editor->lineHeight,
		                M_PI/2,
		                3*M_PI/2 + waitWheelAngle);

		mp_graphics_set_color_rgba(graphics, 0, 1, 0, 1);
		mp_graphics_set_width(graphics, 5);
		mp_graphics_stroke(graphics);

		targetFrameOffset += editor->lineHeight;

		cell->waitingProgress += 1./60;
		cell->waitingTimeout--;
	}
	else
	{
		cell->waitingProgress = 0;
	}

	if(cell->standbyTimeout)
	{
		f32 alpha = 0.5*sin(2*M_PI*cell->standbyProgress) + 0.5;

		f32 size = 0.8*editor->lineHeight;
		f32 margin = (editor->lineHeight - size)/2;

		mp_graphics_set_color_rgba(graphics, 0, 1, 0, alpha);
		mp_graphics_rectangle_fill(graphics, pos.x + margin, pos.y + margin, size/3, size);
		mp_graphics_rectangle_fill(graphics, pos.x + margin+ size*2./3, pos.y + margin, size/3, size);

		targetFrameOffset += editor->lineHeight;

		cell->standbyProgress += 1./60;
		cell->standbyTimeout--;
	}
	else
	{
		cell->standbyProgress = 0;
	}
	absTargetPos.x += targetFrameOffset;
	cell->frameOffset += animationRatio * (targetFrameOffset - cell->frameOffset);
	pos.x = cell->box.x + cell->frameOffset;

	if(cell_has_text(cell))
	{
		mp_graphics_color textColor = QED_COLOR_TEXT;
		mp_graphics_color borderColor = {0.8, 0.8, 0.8, 1};

		switch(cell->kind)
		{
			case CELL_COMMENT:
				textColor = QED_COLOR_COMMENT;
				break;
			case CELL_STRING:
			case CELL_CHAR:
				textColor = QED_COLOR_STRING;
				break;

			case CELL_WORD:
				if(token_is_keyword(cell->valueU64))
				{
					textColor = QED_COLOR_KEYWORD;
				}
				else if(cell->valueU64 == TOKEN_UNKNOWN)
				{
					textColor = QED_COLOR_UNKNOWN;
				}
				break;

			case CELL_HOLE:
				textColor = QED_COLOR_HOLE;
				break;

			case CELL_UI:
				textColor = QED_COLOR_UI;
				borderColor = QED_COLOR_UI;
				break;

			default:
				break;
		}

		//NOTE: quotes
		mp_string leftQuote = {};
		mp_string rightQuote = {};

		switch(cell->kind)
		{
			case CELL_CHAR:
				leftQuote = mp_string_lit("\'");
				rightQuote = mp_string_lit("\'");
				break;

			case CELL_STRING:
				leftQuote = mp_string_lit("\"");
				rightQuote = mp_string_lit("\"");
				break;

			case CELL_COMMENT:
				leftQuote = mp_string_lit("/*");
				rightQuote = mp_string_lit("*/");
				break;

			case CELL_UI:
				leftQuote = mp_string_lit("#");
				break;

			default:
				break;
		}

		if(leftQuote.len)
		{
			pos = q_print_string(gui, leftQuote, pos, textColor);
		}

		//TODO draw border around unrecognized tokens?

		if(cell->text.string.len)
		{
			pos = q_print_string(gui, cell->text.string, pos, textColor);

			if(cell->kind == CELL_HOLE)
			{
				mp_graphics_set_color(graphics, borderColor);
				mp_graphics_set_width(graphics, 1);
				mp_graphics_rectangle_stroke(graphics, cell->box.x, cell->box.y, pos.x - cell->box.x, editor->lineHeight);
			}
		}
		else
		{
			mp_graphics_set_color(graphics, borderColor);
			mp_graphics_set_width(graphics, 1);
			mp_graphics_rectangle_stroke(graphics, pos.x, pos.y, editor->spaceWidth, editor->lineHeight);
			pos.x += editor->spaceWidth;
		}

		if(rightQuote.len)
		{
			pos = q_print_string(gui, rightQuote, pos, textColor);
		}
	}

	//NOTE: display UI cells' panel
	qed_tab* tab = qed_current_tab(editor);
	if(cell->kind == CELL_UI && cell_has_ancestor(tab->cursor.parent, cell))
	{
		mp_aligned_rect panelRect = { cell->box.x,
		                              cell->box.y + editor->lineHeight + QED_LINE_GUTTER,
		                              maximum(QED_CELL_MIN_UI_WIDTH, cell->ui.width),
		                              maximum(QED_CELL_MIN_UI_HEIGHT, cell->ui.height)};

		mp_gui_view_flags flags = MP_GUI_VIEW_FLAG_VSCROLL
		                        | MP_GUI_VIEW_FLAG_RESIZEABLE_RIGHT
		                        | MP_GUI_VIEW_FLAG_RESIZEABLE_BOTTOM;

		mp_gui_begin_view(gui, "cell_ui_panel", panelRect, flags);
		{
			panelRect = mp_gui_view_get_frame(gui);
			panelRect.x = cell->box.x;
			panelRect.y = cell->box.y + editor->lineHeight + QED_LINE_GUTTER;
			panelRect.w = maximum(QED_CELL_MIN_UI_WIDTH, panelRect.w);
			panelRect.h = maximum(QED_CELL_MIN_UI_HEIGHT, panelRect.h);
			cell->ui.width = panelRect.w;
			cell->ui.height = panelRect.h;
			mp_gui_view_set_frame(gui, panelRect);

			switch(cell->ui.kind)
			{
				case CELL_UI_NONE:
					break;
				case CELL_UI_TEMPO_CURVE:
				{
					if(qed_tempo_editor_gui(gui, cell))
					{
						printf("curve editor updated!\n");
						qed_update_tempo_curve(editor, qed_current_module(editor), cell);
						qed_rebuild(editor);
					}
				} break;
			}
		} mp_gui_end_view(gui);

		pos.x = cell->box.x + panelRect.w;
		pos.y += panelRect.h + QED_LINE_GUTTER;
	}

	//NOTE: render children
	f32 maxWidth = 0;
	if(cell_has_children(cell))
	{
		mp_graphics_color sepColor = {1, 1, 1, 1};
		mp_string leftSep = {};
		mp_string rightSep = {};

		switch(cell->kind)
		{
			case CELL_LIST:
				leftSep = mp_string_lit("(");
				rightSep = mp_string_lit(")");
				break;

			case CELL_ATTRIBUTE:
				leftSep = mp_string_lit("@(");
				rightSep = mp_string_lit(")");
				break;

			case CELL_ARRAY:
				leftSep = mp_string_lit("[");
				rightSep = mp_string_lit("]");
				break;

			default:
				break;
		}
		pos = q_print_string(gui, leftSep, pos, sepColor);

		vec2 childrenOrigin = {absTargetPos.x + pos.x - (cell->box.x + cell->frameOffset),
	                       	   absTargetPos.y + pos.y - cell->box.y};

		for_each_in_list(&cell->children, child, q_cell, listElt)
		{
			qed_display_cell(gui, editor, child, childrenOrigin);
			pos.x = child->box.x + child->lastLineWidth;
			pos.y = child->box.y + child->box.h - editor->lineHeight;

			maxWidth = maximum(maxWidth, child->box.x + child->box.w - cell->box.x);
		}

		pos = q_print_string(gui, rightSep, pos, sepColor);
	}

	maxWidth = maximum(maxWidth, pos.x - cell->box.x);

	cell->box.w = maxWidth;
	cell->box.h = pos.y + editor->lineHeight - cell->box.y;
	cell->lastLineWidth = pos.x - cell->box.x;

}

//------------------------------------------------------------------------------------------
// Cells span / Cursor Drawing
//------------------------------------------------------------------------------------------

const f32 QED_CURSOR_BLINK_DURATION = 1.;

void qed_reset_cursor_blink(q_editor* editor)
{
	editor->cursorBlinkState = true;
	editor->cursorBlinkTimer = QED_CURSOR_BLINK_DURATION;
}

void qed_draw_cursor(mp_gui_context* gui, q_editor* editor)
{
	const f32 QED_CURSOR_ANIMATION_RATE = 0.7;

	qed_tab* tab = qed_current_tab(editor);

	mp_graphics_context graphics = mp_gui_get_graphics(gui);
	f32 lineHeight = editor->lineHeight;
	vec2 cursorPos = qed_point_to_display_pos(editor, tab->cursor);

	editor->animatedCursor.x += QED_CURSOR_ANIMATION_RATE * (cursorPos.x - editor->animatedCursor.x);
	editor->animatedCursor.y += QED_CURSOR_ANIMATION_RATE * (cursorPos.y - editor->animatedCursor.y);

	if(editor->cursorBlinkTimer <= 0)
	{
		editor->cursorBlinkState = !editor->cursorBlinkState;
		editor->cursorBlinkTimer = QED_CURSOR_BLINK_DURATION;
	}
	else
	{
		editor->cursorBlinkTimer -= 1./30.; //WARN: hardcoded for 30fps...
	}

	if(editor->cursorBlinkState)
	{
		mp_graphics_set_color(graphics, (mp_graphics_color){0.95, 0.71, 0.25, 1});
		mp_graphics_rectangle_fill(graphics, editor->animatedCursor.x-2, editor->animatedCursor.y, 4, lineHeight);
	}
}

void qed_draw_sibling_marker(mp_gui_context* gui, q_editor* editor, q_cell* cell, mp_graphics_color color)
{
	mp_graphics_context graphics = mp_gui_get_graphics(gui);
	f32 lineHeight = editor->lineHeight;

	mp_graphics_set_color(graphics, color);
	mp_graphics_set_width(graphics, 4);
	mp_graphics_move_to(graphics, cell->box.x, cell->box.y + cell->box.h - 2);
	mp_graphics_line_to(graphics, cell->box.x + cell->lastLineWidth, cell->box.y + cell->box.h - 2);
	mp_graphics_stroke(graphics);
}

void qed_draw_edit_range(mp_gui_context* gui, q_editor* editor)
{
	qed_tab* tab = qed_current_tab(editor);

	mp_graphics_context graphics = mp_gui_get_graphics(gui);

	f32 spaceWidth = editor->spaceWidth;
	f32 lineHeight = editor->lineHeight;

	qed_point cursor = tab->cursor;
	qed_point mark = tab->mark;

	if(!qed_point_same_cell(cursor, mark))
	{
		//NOTE(martin): multiple nodes are selected, highlight the selection (in blue)
		qed_cell_span span = qed_cell_span_from_points(cursor, mark);
		q_cell* parent = span.start->parent;
		q_cell* stop = cell_next_sibling(span.end);

		mp_aligned_rect startBox = qed_cell_frame_box(span.start);
		mp_aligned_rect box = startBox;
		mp_aligned_rect endBox = startBox;

		for(q_cell* cell = span.start; cell != stop; cell = cell_next_sibling(cell))
		{
			endBox = qed_cell_frame_box(cell);
			box = qed_combined_box(box, endBox);
		}

		mp_aligned_rect firstLine = {startBox.x,
		                             startBox.y,
		                             maximum(0, box.x + box.w - startBox.x),
		                             lineHeight};

		mp_aligned_rect innerSpan = {box.x,
		                             box.y + lineHeight,
		                             box.w,
		                             maximum(0, box.h - 2*lineHeight)};

		mp_aligned_rect lastLine = {box.x,
		                            box.y + box.h - lineHeight,
		                            maximum(0, endBox.x + endBox.w - box.x),
		                            maximum(0, endBox.y + endBox.h - (box.y + box.h - lineHeight))};

		mp_graphics_set_color_rgba(graphics, 0.2, 0.2, 1, 1);

		mp_graphics_rectangle_fill(graphics, firstLine.x, firstLine.y, firstLine.w, firstLine.h);
		mp_graphics_rectangle_fill(graphics, innerSpan.x, innerSpan.y, innerSpan.w, innerSpan.h);
		mp_graphics_rectangle_fill(graphics, lastLine.x, lastLine.y, lastLine.w, lastLine.h);

		qed_draw_cursor(gui, editor);
	}
	else
	{
		//NOTE(martin): shade the parent cell of cursor/mark
		mp_aligned_rect parentBox = qed_cell_contents_box(editor, cursor.parent);
		mp_graphics_set_color_rgba(graphics, 0.2, 0.2, 0.2, 1);
		mp_graphics_rectangle_fill(graphics, parentBox.x, parentBox.y, parentBox.w, parentBox.h);

		if(!cursor.leftFrom
		  && (cell_has_text(cursor.parent))
		  && cursor.textIndex != mark.textIndex)
		{
			//NOTE(martin): some text is selected. Highlight the selection (in blue).
			mp_aligned_rect box = qed_cell_contents_box(editor, cursor.parent);

			u64 start = minimum(cursor.textIndex, mark.textIndex);
			u64 end = maximum(cursor.textIndex, mark.textIndex);

			mp_string string = cursor.parent->text.string;
			mp_aligned_rect leftBox = mp_graphics_text_bounding_box(graphics, mp_string_slice(string, 0, start));
			mp_aligned_rect selBox = mp_graphics_text_bounding_box(graphics, mp_string_slice(string, start, end));

			selBox.x += box.x + leftBox.w;
			selBox.y = box.y;

			mp_graphics_set_color_rgba(graphics, 0.2, 0.2, 1, 1);
			mp_graphics_rectangle_fill(graphics, selBox.x, selBox.y, selBox.w, selBox.h);

			editor->animatedCursor = qed_point_to_display_pos(editor, tab->cursor);
		}
		else
		{
			qed_draw_cursor(gui, editor);
		}
	}
	//NOTE(martin): underline cells to the left/right of cursor
	q_cell* leftSibling = qed_point_left_cell(cursor);
	q_cell* rightSibling = qed_point_right_cell(cursor);

	if(leftSibling)
	{
		qed_draw_sibling_marker(gui, editor, leftSibling, (mp_graphics_color){0.2, 0.5, 1, 1});
	}
	if(rightSibling)
	{
		qed_draw_sibling_marker(gui, editor, rightSibling, (mp_graphics_color){0, 1, 0, 1});
	}
}

//------------------------------------------------------------------------------------
// Re-building
//------------------------------------------------------------------------------------

void qed_remove_trailing_holes(q_editor* editor, q_cell* cell)
{
	cell_tree* tree = qed_current_tree(editor);
	qed_tab* tab = qed_current_tab(editor);

	q_cell* child = cell_last_child(cell);
	while(child && child->kind == CELL_HOLE)
	{
		q_cell* prev = cell_prev_sibling(child);
		if(tab->cursor.parent == child)
		{
			break;
		}
		else if(tab->cursor.leftFrom == child)
		{
			tab->cursor.leftFrom = 0;
			prev = 0;
		}
		cell_recycle(tree, child);
		child = prev;
	}
	tab->mark = tab->cursor;
}

void qed_mark_modified(q_editor* editor, q_cell* cell)
{
	workspace_mark_modified(editor->workspace, qed_current_module(editor), cell->parent);

	//NOTE: remove trailing holes in edited cells
	qed_remove_trailing_holes(editor, cell);
}

void qed_create_holes_for_errors(q_module* module, list_info* errors)
{
	for_each_in_list(errors, error, q_error, moduleElt)
	{
		q_ast* ast = error->ast;
		if(error->kind == Q_ERROR_MISSING && qed_point_equal(ast->range.start, ast->range.end))
		{
			q_cell* hole = cell_alloc(&module->tree, CELL_HOLE);
			cell_ast_cache(hole, ast);
			cell_text_replace(hole, error->cellHint);

			//NOTE fixup cursor
			if(ast->range.start.leftFrom)
			{
				cell_insert_before(ast->range.start.leftFrom, hole);
			}
			else
			{
				cell_push(ast->range.start.parent, hole);
			}

			//NOTE fixup nodes
			q_ast* leftNode = ast_prev_sibling(ast);
			if(!leftNode)
			{
				q_ast* parent = ast->parent;
				while(!leftNode && parent && qed_point_equal(parent->range.start, ast->range.start))
				{
					parent->range.start.leftFrom = hole;
					leftNode = ast_prev_sibling(parent);
					parent = parent->parent;
				}
			}
			while(leftNode && qed_point_equal(leftNode->range.end, ast->range.start))
			{
				leftNode->range.end.leftFrom = hole;
				leftNode = ast_last_child(leftNode);
			}

			ast->range.start.leftFrom = hole;
		}
	}
}

void qed_rebuild(q_editor* editor)
{
	//NOTE: rebuild
	workspace_rebuild(editor->workspace);

	//NOTE: parse current module if it's not in build context
	q_module* editedModule = qed_current_module(editor);
	if(editedModule->buildCounter != editor->workspace->buildCounter)
	{
		parse_module(editedModule);
	}

	//NOTE: create holes for missing tokens
	for_each_in_list(&editor->tabs, tab, qed_tab, listElt)
	{
		q_module* module = tab->module;
		qed_create_holes_for_errors(module, &module->parsingErrors);
		qed_create_holes_for_errors(module, &module->checkingErrors);
	}
}

void qed_update_tempo_curve(q_editor* editor, q_module* module, q_cell* cell)
{
	////////////////////////////////////////////////////////////
	//TODO check that module is included in current build
	////////////////////////////////////////////////////////////

	DEBUG_ASSERT(cell->kind == CELL_UI && cell->ui.kind == CELL_UI_TEMPO_CURVE);
	qed_curve_editor* curveEditor = (qed_curve_editor*)cell->ui.data.ptr;

	u64 totalSize = sizeof(q_msg)
	              + sizeof(bc_widget)
	              + sizeof(bc_tempo_curve_desc)
	              + sizeof(sched_curve_descriptor_elt) * curveEditor->eltCount;

	q_msg* msg = (q_msg*)mem_arena_alloc(mem_scratch(), totalSize);
	msg->kind = Q_MSG_UPDATE_TEMPO_CURVE;
	msg->payloadSize = totalSize - sizeof(q_msg);

	bc_widget* widget = (bc_widget*)msg->payload;
	widget->moduleIndex = module->moduleIndex;
	widget->cellID = cell->id;
	widget->size = sizeof(bc_tempo_curve_desc)
	              + sizeof(sched_curve_descriptor_elt) * curveEditor->eltCount;

	bc_tempo_curve_desc* curveDesc = (bc_tempo_curve_desc*)(((char*)widget) + sizeof(bc_widget));
	curveDesc->axes = curveEditor->axes;
	curveDesc->eltCount = curveEditor->eltCount;

	sched_curve_descriptor_elt* elements = (sched_curve_descriptor_elt*)(((char*)curveDesc) + sizeof(bc_tempo_curve_desc));

	u32 i = 0;
	for_each_in_list(&curveEditor->elements, editorElt, qed_curve_editor_elt, listElt)
	{
		elements[i].type = editorElt->type;
		elements[i].length = (editorElt->p[3].x - editorElt->p[0].x);
		elements[i].startValue = editorElt->p[0].y;
		elements[i].endValue = editorElt->p[3].y;

		//NOTE(martin): descriptor control points X's are normalized to (0, 1)
		f32 eltLengthScale = 1/(editorElt->p[3].x - editorElt->p[0].x);

		//NOTE(martin): control points are normalized to 0,1 between startValue/endValue end 0/length
		elements[i].p1x = (editorElt->p[1].x - editorElt->p[0].x) * eltLengthScale;
		elements[i].p1y = editorElt->p[1].y;
		elements[i].p2x = (editorElt->p[2].x - editorElt->p[0].x) * eltLengthScale;
		elements[i].p2y = editorElt->p[2].y;

		i++;
	}

	q_ringbuffer_push_msg(editor->commandBuffer, msg);
}

//------------------------------------------------------------------------------------
// Re-Lexing
//------------------------------------------------------------------------------------

#include"lexer.cpp"
//TODO: do we really need to set cursor in here?
//      do we need to get cursor graphics position?
//      warning: we set cell->text.string.len to 0, but we need to keep cell->text.string.ptr around!!

void qed_relex_cell(q_editor* editor, q_cell* cell, mp_string string)
{
	qed_tab* tab = qed_current_tab(editor);
	cell_tree* tree = qed_current_tree(editor);

	qed_mark_modified(editor, cell->parent);

	cell_kind srcKind = cell->kind;
	qed_point cursorPoint = {.parent = cell, .leftFrom = 0, .textIndex = 0};
	vec2 cursorPos = qed_point_to_display_pos(editor, cursorPoint);
	qed_point nextPoint = tab->cursor;
	u32 byteOffset = 0;

	do
	{
		lex_result lex = lex_next(string, byteOffset, srcKind);
		byteOffset += lex.string.len;

		cell_text_replace(cell, lex.string);
		cell->kind = lex.kind;
		cell->valueU64 = lex.valueU64;
		cell->valueF64 = lex.valueF64;

		if(cell->kind == CELL_UI)
		{
			if(cell->ui.kind != lex.valueU64)
			{
				//NOTE: ui kind has changed, so we delete the previous ui data
				switch(cell->ui.kind)
				{
					case CELL_UI_NONE:
						break;
					case CELL_UI_TEMPO_CURVE:
						qed_tempo_editor_recycle(tree, cell);
						break;
				}
			}

			cell->ui.kind = (cell_ui_kind)lex.valueU64;

			switch(cell->ui.kind)
			{
				case CELL_UI_NONE:
					break;
				case CELL_UI_TEMPO_CURVE:
					cell->ui.data = qed_tempo_editor_alloc(tree);
					break;
			}

			//NOTE: change srcKind so that subsequent tokens are treated as unquoted "words"
			srcKind = CELL_WORD;
		}

		if(byteOffset < string.len)
		{
			q_cell* prevCell = cell;
			cell = cell_alloc(tree, CELL_WORD);
			cell_insert(prevCell, cell);

			//NOTE: set default position and replace cursor if needed
			cell->box.x = cursorPos.x + qed_display_offset_for_text_index(editor, string, byteOffset);
			cell->box.y = cursorPos.y;

			if(tab->cursor.textIndex >= byteOffset)
			{
				nextPoint.parent = cell;
				nextPoint.textIndex = tab->cursor.textIndex - byteOffset;
			}
		}
	} while(byteOffset < string.len);

	tab->cursor = nextPoint;
	tab->mark = tab->cursor;
}

//------------------------------------------------------------------------------------
// Text edition
//------------------------------------------------------------------------------------

void qed_completion_update(q_editor* editor);

void qed_replace_text_selection_with_utf8(q_editor* editor, u64 count, char* input)
{
	qed_tab* tab = qed_current_tab(editor);
	q_cell* cell = tab->cursor.parent;

	u32 selStart = minimum(tab->cursor.textIndex, tab->mark.textIndex);
	u32 selEnd = maximum(tab->cursor.textIndex, tab->mark.textIndex);

	if(cell->kind == CELL_HOLE)
	{
		selStart = 0;
		selEnd = cell->text.string.len;
	}

	u64 newLen = cell->text.string.len + count - (selEnd - selStart);


	mem_arena* scratch = mem_scratch();
	mp_string string = cell->text.string;

	mp_string_list insertList = {};
	mp_string_list_push(scratch, &insertList, mp_string_slice(string, 0, selStart));
	mp_string_list_push(scratch, &insertList, mp_string_from_buffer(count, input));
	mp_string_list_push(scratch, &insertList, mp_string_slice(string, selEnd, string.len));

	string = mp_string_list_join(scratch, insertList);

	tab->cursor.textIndex = selStart + count;
	qed_relex_cell(editor, cell, string);
	qed_completion_update(editor);
}

//------------------------------------------------------------------------------------------
// Moves
//------------------------------------------------------------------------------------------
typedef enum { QED_PREV, QED_NEXT } qed_direction;

void qed_move_one(q_editor* editor, qed_direction direction)
{
	qed_tab* tab = qed_current_tab(editor);
	tab->cursor = (direction == QED_PREV) ?
	             qed_prev_point(tab->cursor) :
	             qed_next_point(tab->cursor);
}

void qed_move_vertical(q_editor* editor, qed_direction direction)
{
	//NOTE(martin): intercept vertical moves if completion panel is focused
	if(editor->completionPanel.active)
	{
		return;
	}

	qed_tab* tab = qed_current_tab(editor);
	cell_tree* tree = qed_current_tree(editor);
	//NOTE(martin): we need to get the x coordinate of the current cursor, then move to next/prev until we find a box
	qed_point point = tab->cursor;
	vec2 oldCursorPos = qed_point_to_display_pos(editor, point);
	vec2 cursorPos = oldCursorPos;
	f32 lineY = cursorPos.y;
	u32 lineCount = 0;

	//TODO: use boxes broad phase first, don't call prev/next point each time?
	while(1)
	{
		qed_point oldPoint = point;

		point = (direction == QED_PREV) ?
		        qed_prev_point(point) :
		        qed_next_point(point) ;

		if(qed_point_equal(oldPoint, point))
		{
			tab->cursor = point;
			break;
		}
		cursorPos = qed_point_to_display_pos(editor, point);

		if( (direction == QED_PREV && cursorPos.y < lineY)
		  ||(direction == QED_NEXT && cursorPos.y > lineY))
		{
			lineY = cursorPos.y;
			lineCount++;
		}

		if(lineCount > 1)
		{
			tab->cursor = oldPoint;
			break;
		}

		bool condition = (direction == QED_PREV)?
		                 (cursorPos.y < oldCursorPos.y && cursorPos.x <= oldCursorPos.x) :
		                 (cursorPos.y > oldCursorPos.y && cursorPos.x >= oldCursorPos.x) ;

		if(condition)
		{
			tab->cursor = point;
			break;
		}
	}
}

//------------------------------------------------------------------------------------------
// Cell Creation / Insertion
//------------------------------------------------------------------------------------------
void qed_insert_at_cursor(q_editor* editor, q_cell* cell)
{
	qed_tab* tab = qed_current_tab(editor);
	cell_tree* tree = qed_current_tree(editor);

	vec2 start = qed_point_to_display_pos(editor, tab->cursor);
	cell->box.x = start.x;
	cell->box.y = start.y;

	q_cell* cursorParent = tab->cursor.parent;

	if(cell_has_children(cursorParent))
	{
		if(tab->cursor.leftFrom)
		{
			cell_insert_before(tab->cursor.leftFrom, cell);
		}
		else
		{
			cell_push(cursorParent, cell);
		}
	}
	else if(cell_has_text(cursorParent))
	{
		if(cursorParent->kind == CELL_HOLE)
		{
			cell_insert(cursorParent, cell);
			cell_recycle(tree, cursorParent);
		}
		else if(tab->cursor.textIndex == 0)
		{
			cell_insert_before(cursorParent, cell);
		}
		else
		{
			cell_insert(cursorParent, cell);
		}
	}
	else
	{
		cell_insert(cursorParent, cell);
	}

	tab->cursor = (qed_point){cell, 0, 0};
	tab->mark = tab->cursor;

	qed_mark_modified(editor, cell->parent);
}

void qed_insert_cell(q_editor* editor, cell_kind kind)
{
	q_cell* cell = cell_alloc(qed_current_tree(editor), kind);
	qed_insert_at_cursor(editor, cell);
	if(cell_has_text(cell))
	{
		mp_string empty = {0};
		qed_relex_cell(editor, cell, empty);
	}
}

void qed_insert_placeholder(q_editor* editor)
{
	qed_tab* tab = qed_current_tab(editor);

	q_cell* nextCell = 0;
	if(cell_has_text(tab->cursor.parent))
	{
		nextCell = cell_next_sibling(tab->cursor.parent);
	}
	else if(tab->cursor.leftFrom)
	{
		nextCell = tab->cursor.leftFrom;
	}

	if(nextCell && nextCell->kind == CELL_HOLE)
	{
		tab->cursor = (qed_point){.parent = nextCell, .leftFrom = 0, .textIndex = 0};
		tab->mark = tab->cursor;
	}
	else
	{
		qed_insert_cell(editor, CELL_HOLE);
	}
}

void qed_insert_char_literal(q_editor* editor)
{
	qed_insert_cell(editor, CELL_CHAR);
}

void qed_insert_string_literal(q_editor* editor)
{
	qed_insert_cell(editor, CELL_STRING);
}

void qed_insert_comment(q_editor* editor)
{
	qed_insert_cell(editor, CELL_COMMENT);
}

void qed_insert_list(q_editor* editor)
{
	qed_insert_cell(editor, CELL_LIST);
}
void qed_insert_attr(q_editor* editor)
{
	qed_insert_cell(editor, CELL_ATTRIBUTE);
}
void qed_insert_array(q_editor* editor)
{
	qed_insert_cell(editor, CELL_ARRAY);
}

void qed_insert_ui(q_editor* editor)
{
	qed_insert_cell(editor, CELL_UI);
}

void qed_parenthesize_span(q_editor* editor)
{
	qed_tab* tab = qed_current_tab(editor);

	qed_cell_span span = {tab->cursor.leftFrom, tab->cursor.leftFrom};

	if(!qed_point_same_cell(tab->cursor, tab->mark))
	{
		//NOTE(martin): multiple nodes are selected, highlight the selection (in blue)
		span = qed_cell_span_from_points(tab->cursor, tab->mark);
	}

	if(span.start)
	{
		q_cell* listCell = cell_alloc(&tab->module->tree, CELL_LIST);

		tab->cursor = (q_point){span.start->parent, span.start, 0};
		qed_insert_at_cursor(editor, listCell);

		q_cell* child = span.start;
		q_cell* end = cell_next_sibling(span.end);

		while(child != end)
		{
			q_cell* next = cell_next_sibling(child);
			child->parent->childCount--;
			ListRemove(&child->parent->children, &child->listElt);
			cell_push(listCell, child);
			listCell->childCount++;

			child = next;
		}

		tab->cursor.parent = listCell->parent;
		tab->cursor.leftFrom = listCell;
	}
	else
	{
		qed_insert_list(editor);
	}
	tab->mark = tab->cursor;
}

//TODO: unparenthesize

//------------------------------------------------------------------------------------
// Deletion
//------------------------------------------------------------------------------------

void qed_delete(q_editor* editor)
{
	qed_tab* tab = qed_current_tab(editor);

	if(!qed_point_same_cell(tab->cursor, tab->mark))
	{
		qed_cell_span span = qed_cell_span_from_points(tab->cursor, tab->mark);
		q_cell* parent = span.start->parent;
		q_cell* stop = cell_next_sibling(span.end);

		q_cell* cell = span.start;
		while(cell != stop)
		{
			q_cell* nextCell = cell_next_sibling(cell);
			cell_recycle(qed_current_tree(editor), cell);
			cell = nextCell;
		}
		tab->cursor = (qed_point){parent, stop, 0};
		tab->mark = tab->cursor;

		qed_mark_modified(editor, parent);
	}
	else if(!tab->cursor.leftFrom && cell_has_text(tab->cursor.parent))
	{
		qed_replace_text_selection_with_utf8(editor, 0, 0);
	}
}

void qed_copy_selection(qed_tab* tab)
{
	mem_arena* scratch = mem_scratch();

	if(  tab->cursor.parent != tab->mark.parent
	  || tab->cursor.leftFrom != tab->mark.leftFrom)
	{
		//NOTE(martin): cell selection
		qed_cell_span span = qed_cell_span_from_points(tab->cursor, tab->mark);
		mp_string data = serialize_cell_span(scratch, span);

		mp_clipboard_clear();
		mp_clipboard_set_data_for_tag("qed cells", data);

		//TODO: maybe also have a text version?
	}
	else if(!tab->cursor.leftFrom && cell_has_text(tab->cursor.parent))
	{
		//NOTE(martin): text selection
		u64 startIndex = minimum(tab->cursor.textIndex, tab->mark.textIndex);
		u64 endIndex = maximum(tab->cursor.textIndex, tab->mark.textIndex);

		mp_string text = tab->cursor.parent->text.string;
		mp_string selection = mp_string_slice(text, startIndex, endIndex);

		mp_clipboard_clear();
		mp_clipboard_set_string(selection);
	}
}

void qed_copy(q_editor* editor)
{
	qed_tab* tab = qed_current_tab(editor);
	qed_copy_selection(tab);
	tab->mark = tab->cursor;
}

void qed_cut(q_editor* editor)
{
	qed_tab* tab = qed_current_tab(editor);
	qed_copy_selection(tab);
	qed_delete(editor);
}


void qed_paste(q_editor* editor)
{
	qed_tab* tab = qed_current_tab(editor);
	mem_arena* scratch = mem_scratch();

	if(mp_clipboard_has_tag("qed cells"))
	{
		mp_string clip = mp_clipboard_get_data_for_tag(scratch, "qed cells");
		list_info cells = deserialize_cell_span(qed_current_tree(editor), clip);

		qed_delete(editor);

		//TODO: select point where to insert
		if(cell_has_text(tab->cursor.parent))
		{
			qed_point point = {.parent = tab->cursor.parent->parent,
			                        .leftFrom = tab->cursor.parent,
			                        .textIndex = 0};
			tab->cursor = point;
		}

		for_each_in_list_safe(&cells, cell, q_cell, listElt)
		{
			ListRemove(&cells, &cell->listElt);
			qed_insert_at_cursor(editor, cell);
			tab->cursor = (q_point){.parent = cell->parent,
			                        .leftFrom = cell_next_sibling(cell),
			                        .textIndex = 0};
		}
	}
	else
	{
		mp_string clip = mp_clipboard_get_string(scratch);
		qed_replace_text_selection_with_utf8(editor, clip.len, clip.ptr);
	}
	tab->mark = tab->cursor;
}

void qed_constrain_scroll_to_cursor(mp_gui_context* gui, q_editor* editor)
{
	qed_tab* tab = qed_current_tab(editor);

	//NOTE(martin): constrain scroll to show cursor
	vec2 cursorPos = qed_point_to_display_pos(editor, tab->cursor);

	f32 scrollX = 0;
	f32 scrollY = 0;
	mp_gui_view_get_scroll(gui, &scrollX, &scrollY);

	mp_aligned_rect viewRect = mp_gui_view_get_frame(gui);

	scrollX = Clamp(scrollX, cursorPos.x - viewRect.w + QED_MARGIN_X, cursorPos.x - QED_MARGIN_X);
	scrollY = Clamp(scrollY, cursorPos.y - viewRect.h + editor->lineHeight + QED_MARGIN_X, cursorPos.y - QED_MARGIN_Y);
	mp_gui_view_set_scroll(gui, scrollX, scrollY);
}

void qed_set_cursor_to_mouse_pos(mp_gui_context* gui, q_editor* editor)
{
	qed_tab* tab = qed_current_tab(editor);
	cell_tree* tree = qed_current_tree(editor);

	f32 x = mp_gui_mouse_x(gui);
	f32 y = mp_gui_mouse_y(gui);

	qed_point point = {.parent = tree->rootCell,
	                   .leftFrom = cell_first_child(tree->rootCell),
	                   .textIndex = 0};

	vec2 pointPos = qed_point_to_display_pos(editor, point);

	f32 lineHeight = editor->lineHeight;

	//TODO(martin): This is doing a full search, maybe we should de-pessimize this by walking the tree and looking at cell's boxes
	while(point.parent != tree->rootCell || point.leftFrom != 0)
	{
		qed_point nextPoint = qed_next_point(point);
		vec2 nextPointPos = qed_point_to_display_pos(editor, nextPoint);

		if(  nextPointPos.y > y
		  || (nextPointPos.y + lineHeight >= y && nextPointPos.x > x))
		{
			break;
		}

		point = nextPoint;
		pointPos = nextPointPos;
	}
	tab->cursor = point;
}

//------------------------------------------------------------------------------------------
// Editor Commands
//------------------------------------------------------------------------------------------

void qed_select_next_build_module(q_editor* editor)
{
	q_module* currentBuildModule = workspace_main_module(editor->workspace);
	qed_tab* currentBuildTab = 0;
	for_each_in_list(&editor->tabs, tab, qed_tab, listElt)
	{
		if(tab->module == currentBuildModule)
		{
			currentBuildTab = tab;
			break;
		}
	}
	qed_tab* nextTab = currentBuildTab ?
	                   qed_tab_next(editor, currentBuildTab) :
	                   ListFirstEntry(&editor->tabs, qed_tab, listElt);

	workspace_set_main_module(editor->workspace, nextTab->module);
}


#include"completions.h"

/*
void qed_print_completion_list(q_editor* editor)
{
	//TODO...

	qed_tab* tab = qed_current_tab(editor);

	if(cell_has_text(tab->cursor.parent))
	{
		q_cell* cell = tab->cursor.parent;
		build_completion_list(editor, cell);
	}

	printf("--------------- completion list ---------------\n");
		for(int i=0; i<editor->completionPanel.count; i++)
		{
			q_completion* item = &editor->completionPanel.completions[i];
			printf("%.*s\n", (int)item->text.len, item->text.ptr);
		}
	printf("-----------------------------------------------\n");
}
*/

//TODO: move somewhere else?
bool ir_type_pattern_match(ir_type_pattern* expected, ir_type_pattern* test)
{
	bool result = false;
	switch(expected->kind)
	{
		case TYPE_PATTERN_ANYTHING:
			result = true;
			break;

		case TYPE_PATTERN_ARRAY_OR_SLICE:
		{
			if(test->kind == TYPE_PATTERN_EXACT)
			{
				ir_type* baseType = ir_type_unwrap(test->exactType);
				if(  baseType->kind == TYPE_ARRAY
				  || baseType->kind == TYPE_SLICE)
				{
					result = true;
				}
			}
			else if(  test->kind == TYPE_PATTERN_ARRAY_OR_SLICE
			       || test->kind == TYPE_PATTERN_STRING)
			{
				result = true;
			}
		} break;

		case TYPE_PATTERN_EXACT:
		{
			ir_type* expectedBase = ir_type_unwrap(expected->exactType);
			switch(test->kind)
			{
				case TYPE_PATTERN_ANYTHING:
					result = true;
					break;

				case TYPE_PATTERN_ARRAY_OR_SLICE:
					if(  expectedBase->kind == TYPE_ARRAY
					  || expectedBase->kind == TYPE_SLICE)
					{
						result = true;
					}
					break;

				case TYPE_PATTERN_STRING:
					if(  expectedBase->kind == TYPE_ARRAY
					  || expectedBase->kind == TYPE_SLICE)
					{
						ir_type* expectedEltBase = ir_type_unwrap(expectedBase->eltType);
						if(  expectedEltBase->kind == TYPE_PRIMITIVE
						  && expectedEltBase->primitive == TYPE_U8)
						{
							result = true;
						}
					}
					break;

				case TYPE_PATTERN_NUMERIC:
					if(ir_type_is_numeric(expectedBase))
					{
						result = true;
					}
					break;

				case TYPE_PATTERN_EXACT:
					result = ir_type_is_equivalent(expectedBase, test->exactType);
					break;

				default:
					break;
			}
		} break;

		default:
			break;
	}
	return(result);
}


void qed_completion_update(q_editor* editor)
{
	qed_tab* tab = qed_current_tab(editor);
	q_module* module = tab->module;
	q_completion_panel* panel = &editor->completionPanel;

	if(!editor->completionPanel.active)
	{
		return;
	}

	if(!cell_has_text(tab->cursor.parent))
	{
		qed_insert_placeholder(editor);
		qed_rebuild(editor);
	}

	// update completion based on textCell text
	DEBUG_ASSERT(tab->cursor.parent && cell_has_text(tab->cursor.parent));

	q_cell* textCell = tab->cursor.parent;
	q_cell* completionCell = textCell;
	q_ast* ast = cell_ast_lookup(textCell);
	cell_kind kind = textCell->kind;

	if(!ast
	  && textCell->parent
	  && textCell == cell_first_child(textCell->parent))
	{
		completionCell = textCell->parent;
		ast = cell_ast_lookup(textCell->parent);
		kind = textCell->parent->kind;
	}

	mem_arena_clear(&panel->arena);
	panel->count = 0;
	panel->index = -1;
	panel->completions = 0;
	ListInit(&panel->items);

	if(ast)
	{
		parse_rule rule = ast->parseRule;
		if(rule != PARSE_RULE_UNDEF)
		{
			ir_node* ir = ast_ir_lookup(module, ast);
			ir_type_pattern expectedTypePattern = {TYPE_PATTERN_ANYTHING};
			if(ir)
			{
				expectedTypePattern = ir->expectedTypePattern;
			}

			q_completion_array* completions = &Q_HEAD_COMPLETIONS[rule];
			for(int i=0; i<completions->count; i++)
			{
				q_completion* completion = &completions->data[i];

				if(completion->tokenClass == TOKEN_CLASS_CALL)
				{
					//NOTE: gather procedure names that can be called here
					for_each_in_list(&module->procedures, sym, ir_symbol, auxElt)
					{
						if(sym->name.ptr[0] == '$')
						{
							continue;
						}

						if(kind == CELL_HOLE
						  || (  kind == completion->cellKind
						     && textCell->text.string.len <= sym->name.len
						     && !strncmp(textCell->text.string.ptr, sym->name.ptr, textCell->text.string.len)))
						{
							ir_type_pattern pattern = {TYPE_PATTERN_EXACT, sym->type->retType};
							if(ir_type_pattern_match(&expectedTypePattern, &pattern))
							{
								q_completion_item* item = mem_arena_alloc_type(&panel->arena, q_completion_item);
								item->completion = *completion;
								item->completion.text = mp_string_push_copy(&panel->arena, sym->name);
								ListAppend(&panel->items, &item->listElt);
								panel->count++;
							}
						}
					}

				}
				else if(completion->tokenClass == TOKEN_CLASS_VAR)
				{
					//NOTE: gather variable names that are reachable from completion point
					ir_scope* scope = 0;
					q_ast* astIterator = ast;
					while(astIterator)
					{
						ir_node* ir = ast_ir_lookup(module, astIterator);
						if(ir && ir->scope)
						{
							scope = ir->scope;
							break;
						}
						astIterator = astIterator->parent;
					}

					if(!scope)
					{
						scope = module->globalScope;
					}

					while(scope)
					{
						//TODO: mask shadowed variables
						//TODO: special case: filter out variable if we're inside it's own declaration

						for_each_in_list(&scope->variables, symbol, ir_symbol, auxElt)
						{
							if(symbol->anonymous)
							{
								continue;
							}

							mp_string name = symbol->name;
							if(kind == CELL_HOLE
							  || (  kind == CELL_WORD
							     && textCell->text.string.len <= name.len
							     && !strncmp(textCell->text.string.ptr, name.ptr, textCell->text.string.len)))
							{
								ir_type_pattern pattern = {TYPE_PATTERN_EXACT, symbol->type};
								if(ir_type_pattern_match(&expectedTypePattern, &pattern))
								{
									q_completion_item* item = mem_arena_alloc_type(&panel->arena, q_completion_item);
									item->completion = *completion;
									item->completion.text = mp_string_push_copy(&panel->arena, name);
									ListAppend(&panel->items, &item->listElt);
									panel->count++;
								}
							}
						}
						scope = scope->parent;
					}

				}
				else if( kind == CELL_HOLE
				  ||(  kind == completion->cellKind
				    && textCell->text.string.len <= completion->text.len
					&& !strncmp(textCell->text.string.ptr, completion->text.ptr, textCell->text.string.len)))
				{
					if(ir_type_pattern_match(&expectedTypePattern, &completion->typePattern))
					{
						q_completion_item* item = mem_arena_alloc_type(&panel->arena, q_completion_item);
						item->completion = *completion;
						ListAppend(&panel->items, &item->listElt);
						panel->count++;
					}
				}
			}
		}
	}

	panel->completions = mem_arena_alloc_array(&panel->arena, q_completion, panel->count);
	int index = 0;
	for_each_in_list(&panel->items, item, q_completion_item, listElt)
	{
		panel->completions[index] = item->completion;
		index++;
	}

	panel->textCell = textCell;
	panel->completionCell = completionCell;
	panel->savedKind = kind;
	if(textCell->kind == CELL_HOLE)
	{
		panel->savedText = (mp_string){0, 0};
	}
	else
	{
		panel->savedText = mp_string_push_copy(&panel->arena, textCell->text.string);
	}
}

void qed_completion_activate(q_editor* editor)
{
	//NOTE: initialize completion panel for cell under cursor
	qed_tab* tab = qed_current_tab(editor);
	q_completion_panel* panel = &editor->completionPanel;

	if(!panel->active)
	{
		panel->active = true;
		qed_completion_update(editor);
		mp_gui_open_popup(editor->gui, "completion_panel");
	}
}

void qed_completion_cancel(q_editor* editor)
{
	//NOTE: cancel completion
	// (this does _not_ close the panel)

	qed_tab* tab = qed_current_tab(editor);
	cell_tree* tree = &tab->module->tree;
	q_completion_panel* panel = &editor->completionPanel;

	if(panel->savedKind != panel->completionCell->kind)
	{
		q_cell* newCell = cell_alloc(tree, panel->savedKind);
		cell_insert_before(panel->completionCell, newCell);
		cell_recycle(tree, panel->completionCell);

		panel->textCell = newCell;
		panel->completionCell = panel->textCell;
		tab->cursor = (q_point){panel->textCell, 0, 0};
		tab->mark = tab->cursor;
	}

	qed_relex_cell(editor, panel->textCell, panel->savedText);

	tab->cursor.textIndex = panel->textCell->text.string.len;
	tab->mark = tab->cursor;
	qed_rebuild(editor);
}

void qed_completion_select(q_editor* editor)
{
	//NOTE: select one of the options in completion panel
	qed_tab* tab = qed_current_tab(editor);
	cell_tree* tree = &tab->module->tree;
	q_completion_panel* panel = &editor->completionPanel;

	if(!panel->count)
	{
		return;
	}

	q_cell* textCell = panel->textCell;
	q_cell* completionCell = panel->completionCell;

	if(panel->index<0)
	{
		qed_completion_cancel(editor);
	}
	else
	{
		if(completionCell->kind != panel->completions[panel->index].cellKind)
		{
			//NOTE: transition from a compound cell to an atom cell or vice versa
			q_cell* newCell = cell_alloc(tree, panel->completions[panel->index].cellKind);
			cell_insert_before(completionCell, newCell);
			cell_recycle(tree, completionCell);

			completionCell = newCell;
			textCell = newCell;

			if(  panel->completions[panel->index].cellKind == CELL_LIST
			  || panel->completions[panel->index].cellKind == CELL_ARRAY
			  || panel->completions[panel->index].cellKind == CELL_ATTRIBUTE)
			{
				q_cell* head = cell_alloc(tree, CELL_HOLE);
				cell_push(newCell, head);
				textCell = head;
			}

			tab->cursor = (q_point){textCell, 0, 0};
			tab->mark = tab->cursor;
		}
		qed_relex_cell(editor, textCell, panel->completions[panel->index].text);

		panel->completionCell = completionCell;
		panel->textCell = textCell;

		tab->cursor.textIndex = textCell->text.string.len;
		tab->mark = tab->cursor;
		qed_rebuild(editor);
	}
}

void qed_completion_gui(mp_gui_context* gui, q_editor* editor, qed_tab* tab)
{
	mp_graphics_context graphics = mp_gui_get_graphics(gui);
	q_completion_panel* panel = &editor->completionPanel;

	q_cell* textCell = tab->cursor.parent;
	if(panel->completionCell)
	{
		panel->frame.x = panel->completionCell->box.x,
		panel->frame.y = panel->completionCell->box.y + textCell->box.h;
	}

	if(mp_gui_begin_popup(gui, "completion_panel", panel->frame))
	{
		if(  textCell
			 && textCell == panel->textCell
			 && cell_has_text(textCell))
		{
			vec2 pos = {10, 10};
			f32 maxWidth = 0;

			if(!panel->count)
			{
				pos = q_print_string(gui, mp_string_lit("no completion found"), pos, (mp_graphics_color){0, 1, 1, 1});
				maxWidth = pos.x;
				pos.x = 10;
				pos.y += editor->lineHeight+10;
			}
			else
			{
				f32 maxTextWidth = 0;
				f32 maxHelpWidth = 0;
				for(int i=0; i<panel->count; i++)
				{
					q_completion* item = &panel->completions[i];

					mp_aligned_rect textBox = mp_graphics_text_bounding_box(graphics, item->text);
					if(  item->cellKind == CELL_LIST
					  || item->cellKind == CELL_ARRAY)
					{
						textBox.w += 5*editor->spaceWidth;
						if(item->text.len)
						{
							textBox.w += editor->spaceWidth;
						}
					}

					mp_aligned_rect helpBox = mp_graphics_text_bounding_box(graphics, item->help);

					if(textBox.w > maxTextWidth)
					{
						maxTextWidth = textBox.w;
					}
					if(helpBox.w > maxHelpWidth)
					{
						maxHelpWidth = helpBox.w;
					}
				}
				maxWidth = maxTextWidth + maxHelpWidth + 60;

				for(int i=0; i<panel->count; i++)
				{
					if(i == panel->index)
					{
						mp_graphics_set_color_rgba(graphics, 0, 0, 1, 1);
						mp_graphics_rectangle_fill(graphics, pos.x, pos.y, panel->frame.w, editor->lineHeight);
					}
					q_completion* item = &panel->completions[i];

					mp_graphics_color textColor = QED_COLOR_TEXT;
					mp_string leftSep = mp_string_lit("");
					mp_string rightSep = mp_string_lit("");
					bool elision = false;

					switch(item->cellKind)
					{
						case CELL_LIST:
							elision = true;
							leftSep = mp_string_lit("(");
							rightSep = mp_string_lit(")");
							break;

						case CELL_ARRAY:
							elision = true;
							leftSep = mp_string_lit("[");
							rightSep = mp_string_lit("]");
							break;

						default:
							break;
					}

					switch(item->tokenClass)
					{
						case TOKEN_CLASS_KEYWORD:
							textColor = QED_COLOR_KEYWORD;
							break;

						default:
							break;
					}
					pos = q_print_string(gui, leftSep, pos, QED_COLOR_TEXT);
					pos = q_print_string(gui, item->text, pos, textColor);
					if(elision && item->text.len)
					{
						pos = q_print_string(gui, mp_string_lit(" "), pos, QED_COLOR_TEXT);
					}
					if(elision)
					{
						pos = q_print_string(gui, mp_string_lit("..."), pos, QED_COLOR_TEXT);
					}
					pos = q_print_string(gui, rightSep, pos, QED_COLOR_TEXT);

					pos.x = maxTextWidth + 40;

					pos = q_print_string(gui, item->help, pos, QED_COLOR_COMPLETION_HELP);
					pos.x = 10;
					pos.y += editor->lineHeight + 10;
				}
			}
			panel->frame.w = maxWidth;
			panel->frame.h = pos.y;
			mp_gui_view_set_frame(gui, panel->frame);

			////////////////////////////////////////////////////////////////////////////////
			//TODO  intercept those keys from editor when panel is active
			////////////////////////////////////////////////////////////////////////////////
			if (mp_gui_is_key_pressed(gui, MP_KEY_UP)
				|| mp_gui_is_key_pressed(gui, MP_KEY_DOWN))
			{
				panel->index += mp_gui_is_key_pressed(gui, MP_KEY_UP) ? -1 : +1;
				panel->index = Clamp(panel->index, -1, (i32)panel->count-1);

				qed_completion_select(editor);
			}
			if(mp_gui_is_key_pressed(gui, MP_KEY_TAB))
			{
				q_cell* next = cell_next_sibling(textCell);
				if(next)
				{
					if(cell_has_text(next))
					{
						tab->cursor = (q_point){next, 0, 0};
						panel->textCell = next;
						panel->completionCell = next;
						qed_completion_update(editor);

						//////////////////////////////////////////////////
						//TODO: close completion in other cases...
						//////////////////////////////////////////////////
					}
					else
					{
						tab->cursor = (q_point){next, cell_first_child(next), 0};
					}
				}
				else
				{
					tab->cursor = (q_point){textCell->parent, 0, 0};
				}
				tab->mark = tab->cursor;
			}
			else if(mp_gui_is_key_pressed(gui, MP_KEY_ESCAPE))
			{
				qed_completion_cancel(editor);
				mp_gui_view_close(gui);
				panel->active = false;
				panel->completionCell = 0;
			}
			else if(mp_gui_is_key_pressed(gui, MP_KEY_ENTER))
			{
				mp_gui_view_close(gui);
				panel->active = false;
				panel->completionCell = 0;
			}
		}
		else
		{
			mp_gui_view_close(gui);
			panel->active = false;
			panel->completionCell = 0;
		}

	} mp_gui_end_popup(gui);
}


typedef void(*qed_move)(q_editor* editor, qed_direction direction);
typedef void(*qed_action)(q_editor* editor);

typedef struct qed_command
{
	mp_key_code key;
	utf32 codePoint;
	mp_key_mods mods;

	qed_move move;
	qed_direction direction;
	bool setMark;

	qed_action action;
	bool focusCursor;
	bool rebuild;
	bool updateCompletion;

} qed_command;

const qed_command QED_COMMANDS[] = {
	//NOTE(martin): move
	{
		.key = MP_KEY_LEFT,
		.move = qed_move_one,
		.direction = QED_PREV,
		.setMark = true,
	},
	{
		.key = MP_KEY_RIGHT,
		.move = qed_move_one,
		.direction = QED_NEXT,
		.setMark = true,
	},
	{
		.key = MP_KEY_UP,
		.move = qed_move_vertical,
		.direction = QED_PREV,
		.setMark = true,
	},
	{
		.key = MP_KEY_DOWN,
		.move = qed_move_vertical,
		.direction = QED_NEXT,
		.setMark = true,
	},
	//NOTE(martin): move select
	{
		.key = MP_KEY_LEFT,
		.mods = MP_KEYMOD_SHIFT,
		.move = qed_move_one,
		.direction = QED_PREV,
	},
	{
		.key = MP_KEY_RIGHT,
		.mods = MP_KEYMOD_SHIFT,
		.move = qed_move_one,
		.direction = QED_NEXT,
	},
	{
		.key = MP_KEY_UP,
		.mods = MP_KEYMOD_SHIFT,
		.move = qed_move_vertical,
		.direction = QED_PREV,
	},
	{
		.key = MP_KEY_DOWN,
		.mods = MP_KEYMOD_SHIFT,
		.move = qed_move_vertical,
		.direction = QED_NEXT,
	},
	//NOTE(martin): cells insertion
	{
		.key = MP_KEY_PERIOD,
		.mods = MP_KEYMOD_CMD,
		.action = qed_insert_comment,
		.rebuild = true,
		.focusCursor = true,
	},
	{
		.codePoint = '(',
		.action = qed_insert_list,
		.rebuild = true,
		.updateCompletion = true,
		.focusCursor = true,
	},
	{
		.codePoint = '[',
		.action = qed_insert_array,
		.rebuild = true,
		.updateCompletion = true,
		.focusCursor = true,
	},
	{
		.codePoint = ' ',
		.action = qed_insert_placeholder,
		.rebuild = true,
		.updateCompletion = true,
		.focusCursor = true,
	},
	{
		.codePoint = '\"',
		.action = qed_insert_string_literal,
		.rebuild = true,
		.updateCompletion = true,
		.focusCursor = true,
	},
	{
		.codePoint = '\'',
		.action = qed_insert_char_literal,
		.rebuild = true,
		.updateCompletion = true,
		.focusCursor = true,
	},
	{
		.codePoint = '@',
		.action = qed_insert_attr,
		.rebuild = true,
		.updateCompletion = true,
		.focusCursor = true,
	},
	{
		.codePoint = '#',
		.action = qed_insert_ui,
		.rebuild = true,
		.updateCompletion = true,
		.focusCursor = true,
	},

	{
		.key = MP_KEY_5,
		.mods = MP_KEYMOD_CMD,
		.action = qed_parenthesize_span,
		.rebuild = true,
		.updateCompletion = true,
		.focusCursor = true,
	},

	//NOTE(martin): deletion
	{
		.key = MP_KEY_BACKSPACE,
		.move = qed_move_one,
		.direction = QED_PREV,
		.action = qed_delete,
		.rebuild = true,
		.updateCompletion = true,
		.focusCursor = true,
	},

	//NOTE(martin): copy/paste
	{
		.key = MP_KEY_C,
		.mods = MP_KEYMOD_CMD,
		.action = qed_copy,
	},
	{
		.key = MP_KEY_X,
		.mods = MP_KEYMOD_CMD,
		.action = qed_cut,
		.focusCursor = true,
	},
	{
		.key = MP_KEY_V,
		.mods = MP_KEYMOD_CMD,
		.action = qed_paste,
		.rebuild = true,
		.focusCursor = true,
	},

	//NOTE(martin): tree selection commands
	{
		.key = MP_KEY_N,
		.mods = MP_KEYMOD_CMD,
		.action = qed_tab_open_transient,
	},
	{
		.key = MP_KEY_N,
		.mods = MP_KEYMOD_CTRL,
		.action = qed_tab_select_next,
	},
	{
		.key = MP_KEY_K,
		.mods = MP_KEYMOD_CMD,
		.action = qed_tab_close_current,
	},
	{
		.key = MP_KEY_N,
		.mods = MP_KEYMOD_CTRL | MP_KEYMOD_SHIFT,
		.action = qed_select_next_build_module,
	},

	//NOTE(martin): print completion list
	{
		.key = MP_KEY_TAB,
		.action = qed_completion_activate,
	},
};

const u32 QED_COMMAND_COUNT = sizeof(QED_COMMANDS) / sizeof(qed_command);

void qed_run_command(mp_gui_context* gui, q_editor* editor, const qed_command* command)
{
	qed_tab* tab = qed_current_tab(editor);

	if(command->move)
	{
		//NOTE(martin): we special case delete here, so that we don't move if we're deleting a selection
		if(command->action == qed_delete)
		{
			if(qed_point_equal(tab->cursor, tab->mark))
			{
				if(!cell_has_text(tab->cursor.parent))
				{
					//NOTE(martin): select cells first before deleting them
					command->move(editor, command->direction);
					return;
				}
			}
			else
			{
				qed_delete(editor);
				goto rebuild;
				return;
			}
		}

		command->move(editor, command->direction);
		if(command->setMark)
		{
			tab->mark = tab->cursor;
		}
	}

	if(command->action)
	{
		command->action(editor);
	}

	if(command->move || command->focusCursor)
	{
		editor->scrollToCursorOnNextFrame = true; //NOTE(martin): requests to constrain scroll to cursor on next frame
		qed_reset_cursor_blink(editor);
	}

	rebuild:
	if(command->rebuild)
	{
		qed_rebuild(editor);
	}

	if(command->updateCompletion)
	{
		qed_completion_update(editor);
	}
}

//------------------------------------------------------------------------------------
// Error drawing
//------------------------------------------------------------------------------------
void qed_draw_squiggly_red_line(mp_graphics_context graphics, f32 xStart, f32 xStop, f32 xStep, f32 y)
{
	mp_graphics_set_color_rgba(graphics, 1, 0, 0, 1);
	mp_graphics_set_width(graphics, 2);
	mp_graphics_move_to(graphics, xStart, y);

	f32 x = xStart;
	f32 yStep = -xStep;
	while(x < xStop)
	{
		mp_graphics_line_to(graphics, x+xStep, y+yStep);
		x += xStep;
		y += yStep;
		yStep = -yStep;
	}
	mp_graphics_stroke(graphics);
}

void qed_draw_errors_from_list(mp_gui_context* gui, q_editor* editor, list_info* errors, list_info* outUnderCursor)
{
	mp_graphics_context graphics = mp_gui_get_graphics(gui);
	qed_tab* tab = qed_current_tab(editor);

	for_each_in_list(errors, error, q_error, moduleElt)
	{
		qed_point point = error->ast->range.start;
		q_cell* cell = point.leftFrom;
		if(cell)
		{
			qed_draw_squiggly_red_line(graphics,
			                         		cell->box.x,
			                         		cell->box.x + cell->box.w,
			                         		editor->spaceWidth/4,
			                         		cell->box.y + cell->box.h + QED_LINE_GUTTER);
		}
		else
		{
			vec2 errPos = qed_point_to_display_pos(editor, point);

			qed_draw_squiggly_red_line(graphics,
			                         		errPos.x,
			                         		errPos.x + editor->spaceWidth,
			                         		editor->spaceWidth/4,
			                         		errPos.y + editor->lineHeight + QED_LINE_GUTTER);
		}

		if(qed_point_equal(tab->cursor, error->ast->range.start))
		{
			ListAppend(outUnderCursor, &error->underCursorElt);
		}
	}
}

void qed_draw_errors(mp_gui_context* gui, q_editor* editor)
{
	q_module* module = qed_current_module(editor);
	qed_tab* tab = qed_current_tab(editor);

	list_info errorsUnderCursor;
	ListInit(&errorsUnderCursor);

	qed_draw_errors_from_list(gui, editor, &module->parsingErrors, &errorsUnderCursor);
	qed_draw_errors_from_list(gui, editor, &module->checkingErrors, &errorsUnderCursor);

	if(!ListEmpty(&errorsUnderCursor))
	{
		vec2 cursorPos = qed_point_to_display_pos(editor, tab->cursor);

		f32 scrollX = 0;
		f32 scrollY = 0;
		mp_gui_view_get_scroll(gui, &scrollX, &scrollY);

		mp_aligned_rect editorFrame = mp_gui_view_get_frame(gui);

		mp_aligned_rect panelRect = { cursorPos.x, cursorPos.y + editor->lineHeight + QED_LINE_GUTTER, 10, 10};

		mp_gui_begin_view(gui, "error_panel", panelRect, MP_GUI_VIEW_FLAG_VSCROLL);
		{
			//NOTE(martin): populate panel with errors under cursor
			vec2 start = {10, 10};
			vec2 pos = {start.x, start.y};

			mp_aligned_rect box = {0, 0, 0, 0};
			for_each_in_list(&errorsUnderCursor, error, q_error, underCursorElt)
			{
				pos = q_print_string(gui, error->msg, pos, (mp_graphics_color){1, 0, 0, 1});
				box.w = maximum(box.w, pos.x - start.x);
				box.h = maximum(box.h, pos.y - box.h + editor->lineHeight);
				pos.x = 10;
				pos.y += editor->lineHeight;
			}

			//NOTE(martin): compute/constrain panel dimensions
			f32 totalWidth = box.w + 30;
			f32 totalHeight = pos.y + 10;
			panelRect.w = totalWidth;
			panelRect.h = ClampHighBound(totalHeight, 300);

			/////////////////////////////////////////////////////////////////////////////
			//TODO: review this. shouldn't it be > ?
			/////////////////////////////////////////////////////////////////////////////
			//NOTE(martin): compute panel position to avoid croping
			if(panelRect.x + panelRect.w + 20 > editorFrame.w + scrollX)
			{
				panelRect.x = ClampLowBound(editorFrame.w - panelRect.w - 20 + scrollX, 0);
			}
			if(panelRect.y + panelRect.h + 20 > editorFrame.h + scrollY)
			{
				panelRect.y = ClampLowBound(editorFrame.h - panelRect.h - 20 + scrollY, 0);
			}

			//NOTE(martin): position/resize panel
			mp_gui_view_set_frame(gui, panelRect);
			mp_gui_view_set_contents_dimensions(gui, totalWidth, totalHeight);

		} mp_gui_end_view(gui);
	}
}

//------------------------------------------------------------------------------------------
// Editor GUI
//------------------------------------------------------------------------------------------


void qed_gui(mp_gui_context* gui, q_editor* editor)
{
	workspace_begin_modify(editor->workspace);
	editor->gui = gui;

	qed_tab* tab = qed_current_tab(editor);

	//NOTE: if we changed tabs, reset source view contents dimensions and scroll values
	if(tab != editor->prevCurrentTab)
	{
		mp_aligned_rect frame = mp_gui_view_get_frame(gui);
		tab->contentsWidth = maximum(tab->contentsWidth, frame.w);
		tab->contentsHeight = maximum(tab->contentsHeight, frame.h);
		mp_gui_view_set_contents_dimensions(gui, tab->contentsWidth, tab->contentsHeight);
		mp_gui_view_set_scroll(gui, tab->scrollX, tab->scrollY);
		editor->prevCurrentTab = tab;
	}

	//NOTE(martin): recompute font width / line height
	{
		mp_graphics_context graphics = mp_gui_get_graphics(gui);

		mp_gui_style* style = mp_gui_style_top(gui);
		mp_graphics_font_extents fontExtents;
		mp_graphics_font_get_extents(graphics, style->font, &fontExtents);
		f32 fontScale = mp_graphics_font_get_scale_for_em_pixels(graphics, style->font, style->fontSize);

		editor->spaceWidth = fontExtents.width * fontScale;
		editor->lineHeight = (fontExtents.ascent + fontExtents.descent + fontExtents.leading) * fontScale;
	}

	//NOTE(martin): setup graphics streams which allows us to draw foreground/background in any order
	mp_graphics_context graphics = mp_gui_get_graphics(gui);
	mp_graphics_stream foregroundStream = mp_graphics_stream_create(graphics);
	mp_graphics_stream mainStream = mp_graphics_stream_swap(graphics, foregroundStream);

	//NOTE: layout and display cells
	cell_tree* tree = &tab->module->tree;
	vec2 pos = { QED_MARGIN_X, QED_MARGIN_Y };

	qed_update_layout(gui, editor, tree->rootCell, pos);
	qed_display_cell(gui, editor, tree->rootCell, pos);

	//NOTE(martin): update contents dimensions and scroll.
	{
		mp_aligned_rect frame = mp_gui_view_get_frame(gui);

		mp_aligned_rect rootBox = tree->rootCell->box;
		tab->contentsWidth = maximum(rootBox.x + rootBox.w, frame.w);
		tab->contentsHeight = maximum(rootBox.y + rootBox.h + 2*(editor->lineHeight + QED_LINE_GUTTER) + QED_MARGIN_Y, frame.h);
		mp_gui_view_set_contents_dimensions(gui, tab->contentsWidth, tab->contentsHeight);

		if(editor->scrollToCursorOnNextFrame)
		{
			qed_constrain_scroll_to_cursor(gui, editor);
			editor->scrollToCursorOnNextFrame = false;
		}

		mp_gui_view_get_scroll(gui, &tab->scrollX, &tab->scrollY);
		tab->scrollX = Clamp(tab->scrollX, 0, tab->contentsWidth - frame.w);
		tab->scrollY = Clamp(tab->scrollY, 0, tab->contentsHeight - frame.h);
	}

	//NOTE(martin): draw the background: edit span and focus underline
	mp_graphics_stream_swap(graphics, mainStream);
	qed_draw_edit_range(gui, editor);
	mp_graphics_stream_append(graphics, foregroundStream);

	qed_draw_errors(gui, editor);


	//NOTE(martin): completion panel UI
	qed_completion_gui(gui, editor, tab);

	//NOTE: global input
	if(!mp_gui_get_focus_id(gui))
	{
		mp_key_mods mods = mp_gui_key_mods(gui);

		//NOTE(martin): handle mouse clicks in source
		mp_aligned_rect cuelistRect = {0};
		mp_gui_view_get_contents_dimensions(gui, &cuelistRect.w, &cuelistRect.h);

		if(mp_gui_is_mouse_button_pressed(gui, MP_MOUSE_LEFT))
		{
			if(mp_gui_is_mouse_over(gui, cuelistRect))
			{
				qed_set_cursor_to_mouse_pos(gui, editor);
				qed_reset_cursor_blink(editor);
				if(!(mods & MP_KEYMOD_SHIFT))
				{
					tab->mark = tab->cursor;
				}
			}
		}

		//NOTE(martin): handle keyboard shortcuts
		bool runCommand = false;
		for(int i = 0; i<QED_COMMAND_COUNT; i++)
		{
			const qed_command* command = &(QED_COMMANDS[i]);

			if(  mp_gui_is_key_pressed(gui, command->key)
		  	  && command->mods == mods)
			{
				qed_run_command(gui, editor, command);
				runCommand = true;
				break;
			}
		}

		if(!runCommand)
		{
			//NOTE(martin): handle character input
			mp_gui_input_text* textInput = &mp_gui_context_get_io(gui)->text;

			for(int textIndex = 0; textIndex < textInput->count; textIndex++)
			{
				utf32 codePoint = textInput->codePoints[textIndex];

				//NOTE: character-based commands
				if(  tab->cursor.parent->kind != CELL_STRING
				  && tab->cursor.parent->kind != CELL_CHAR
				  && tab->cursor.parent->kind != CELL_COMMENT)
				{
					bool found = false;
					for(int i=0; i<QED_COMMAND_COUNT; i++)
					{
						const qed_command* command = &(QED_COMMANDS[i]);

						if(command->codePoint == codePoint)
						{
							qed_run_command(gui, editor, command);
							found = true;
							break;
						}
					}
					if(found)
					{
						continue;
					}
				}

				//TODO: allow replacing cell span with text

				//NOTE: text insertion
				if(!cell_has_text(tab->cursor.parent))
				{
					qed_insert_placeholder(editor);
				}

				if(tab->cursor.parent == tab->mark.parent)
				{
					char backing[4];
					mp_string utf8Input = utf8_encode(backing, codePoint);
					qed_replace_text_selection_with_utf8(editor, utf8Input.len, utf8Input.ptr);
				}

				qed_rebuild(editor);
			}

			if(textInput->count)
			{
				qed_reset_cursor_blink(editor);
			}
		}
	}
}


#undef LOG_SUBSYSTEM
