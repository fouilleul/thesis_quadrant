/************************************************************//**
*
*	@file: save.cpp
*	@author: Martin Fouilleul
*	@date: 16/02/2022
*	@revision:
*
*****************************************************************/

#include"memory.h"
#include"cells.h"
#include"tempo_editor.h"

#define LOG_SUBSYSTEM "Saves"

const u32 Q_SAVE_MAGIC_NUMBER = 0x3a444551, //NOTE(martin): "QED:" little endian
          Q_SAVE_VERSION = 2;

typedef struct qsr_save_header
{
	u32 magic;
	u32 version;
	u64 nextID;

} qsr_save_header;

typedef struct qsr_string
{
	i64 offset;
	u64 len;
} qsr_string;

const u16 SR_CELL_FLAG_CHILDREN     = 1,
          SR_CELL_FLAG_PAREN        = 1<<1,
          SR_CELL_FLAG_ATTR         = 1<<2,
          SR_CELL_FLAG_ARRAY        = 1<<3,
          SR_CELL_FLAG_TEXT         = 1<<4,
          SR_CELL_FLAG_DOUBLE_QUOTE = 1<<5,
          SR_CELL_FLAG_SINGLE_QUOTE = 1<<6,
          SR_CELL_FLAG_COMMENT      = 1<<7,
          SR_CELL_FLAG_UI           = 1<<8;

typedef struct qsr_cell_v2
{
	u64 id;
	u16 flags;
	qsr_string text;
	qsr_string children;
	qsr_string ui;
} qsr_cell_v2;

typedef struct qsr_ui
{
	u32 kind;
	f32 width;
	f32 height;
	qsr_string data;
} qsr_ui;

typedef struct qsr_curve_elt
{
	sched_curve_type type;
	qed_curve_point p[4];

	bool startDerivability;
	bool startContinuity;
	bool endDerivability;
	bool endContinuity;
} qsr_curve_elt;

typedef struct qsr_curve
{
	f32 cameraX;
	f32 cameraW;
	f32 cameraH;
	sched_curve_axes axes;

	qsr_string elements;

} qsr_curve;

#define qsr_set_relptr(var, addr) (var = (i64)((char*)(addr) - (char*)(&(var))))
#define qsr_get_relptr(var) (((char*)&(var)) + (var))

void serialize_curve_in_slot(mem_arena* arena, q_cell* cell, qsr_curve* sr)
{
	qed_curve_editor* editor = (qed_curve_editor*)cell->ui.data.ptr;

	sr->cameraX = editor->cameraX;
	sr->cameraW = editor->cameraW;
	sr->cameraH = editor->cameraH;
	sr->axes = editor->axes;

	qsr_curve_elt* eltArray = mem_arena_alloc_array(arena, qsr_curve_elt, editor->eltCount);
	qsr_set_relptr(sr->elements.offset, eltArray);
	sr->elements.len = editor->eltCount*sizeof(qsr_curve_elt);

	u64 eltIndex = 0;
	for_each_in_list(&editor->elements, elt, qed_curve_editor_elt, listElt)
	{
		qsr_curve_elt* srElt = &eltArray[eltIndex];

		srElt->type = elt->type;
		memcpy(srElt->p, elt->p, 4*sizeof(qed_curve_point));

		srElt->startDerivability = elt->startDerivability;
		srElt->startContinuity = elt->startContinuity;
		srElt->endDerivability = elt->endDerivability;
		srElt->endContinuity = elt->endContinuity;

		eltIndex++;
	}
}

void serialize_ui_in_slot(mem_arena* arena, q_cell* cell, qsr_ui* ui)
{
	ui->kind = cell->ui.kind;
	ui->width = cell->ui.width;
	ui->height = cell->ui.height;
	ui->data = (qsr_string){0};

	switch(cell->ui.kind)
	{
		case CELL_UI_NONE:
			break;
		case CELL_UI_TEMPO_CURVE:
		{
			qsr_curve* srCurve = mem_arena_alloc_type(arena, qsr_curve);
			serialize_curve_in_slot(arena, cell, srCurve);

			qsr_set_relptr(ui->data.offset, srCurve);
			ui->data.len = sizeof(qsr_curve);
		} break;
	}
}

void serialize_cell_in_slot(mem_arena* arena, q_cell* cell, qsr_cell_v2* sr)
{
	sr->id = cell->id;

	sr->flags = 0;
	if(cell_has_children(cell))
	{
		sr->flags |= SR_CELL_FLAG_CHILDREN;

		switch(cell->kind)
		{
			case CELL_LIST:
				sr->flags |= SR_CELL_FLAG_PAREN;
				break;
			case CELL_ATTRIBUTE:
				sr->flags |= SR_CELL_FLAG_ATTR;
				break;
			case CELL_ARRAY:
				sr->flags |= SR_CELL_FLAG_ARRAY;
				break;
			case CELL_ROOT:
				break;
			default:
				LOG_ERROR("Unknown list kind");
				break;
		}
	}
	if(cell_has_text(cell))
	{
		sr->flags |= SR_CELL_FLAG_TEXT;

		switch(cell->kind)
		{
			case CELL_STRING:
				sr->flags |= SR_CELL_FLAG_DOUBLE_QUOTE;
				break;
			case CELL_CHAR:
				sr->flags |= SR_CELL_FLAG_SINGLE_QUOTE;
				break;
			case CELL_COMMENT:
				sr->flags |= SR_CELL_FLAG_COMMENT;
				break;
			case CELL_UI:
				sr->flags |= SR_CELL_FLAG_UI;
				break;
			default:
				break;
		}
	}

	if(cell_has_text(cell))
	{
		mp_string string = mp_string_push_copy(arena, cell->text.string);
		qsr_set_relptr(sr->text.offset, string.ptr);
		sr->text.len = string.len;
	}

	if(cell->kind == CELL_UI)
	{
		qsr_ui* ui = mem_arena_alloc_type(arena, qsr_ui);
		serialize_ui_in_slot(arena, cell, ui);

		qsr_set_relptr(sr->ui.offset, ui);
		sr->ui.len = sizeof(qsr_ui);
	}

	if(cell_has_children(cell))
	{
		u64 childCount = 0;
		for_each_in_list(&cell->children, child, q_cell, listElt)
		{
			childCount++;
		}
		qsr_cell_v2* childrenSlots = mem_arena_alloc_array(arena, qsr_cell_v2, childCount);
		memset(childrenSlots, 0, sizeof(qsr_cell_v2)*childCount);

		qsr_set_relptr(sr->children.offset, childrenSlots);
		sr->children.len = childCount * sizeof(qsr_cell_v2);

		u64 childIndex = 0;
		for_each_in_list(&cell->children, child, q_cell, listElt)
		{
			serialize_cell_in_slot(arena, child, &childrenSlots[childIndex]);
			childIndex++;
		}
	}
}

mp_string serialize_cell_tree(mem_arena* arena, u64 nextID, q_cell* root)
{
	u64 startOffset = arena->offset;
	char* startPointer = (char*)mem_arena_alloc(arena, sizeof(qsr_save_header));

	qsr_save_header header = {.magic = Q_SAVE_MAGIC_NUMBER,
	                          .version = Q_SAVE_VERSION,
	                          .nextID = nextID};

	memcpy(startPointer, &header, sizeof(qsr_save_header));

	qsr_cell_v2* sr = mem_arena_alloc_type(arena, qsr_cell_v2);
	memset(sr, 0, sizeof(qsr_cell_v2));

	serialize_cell_in_slot(arena, root, sr);

	mp_string result = {.len = arena->offset - startOffset, .ptr = startPointer};
	return(result);
}

mp_string deserialize_curve(mp_string data)
{
	mp_string result = {0};

	if(data.len < sizeof(qsr_curve))
	{
		LOG_ERROR("curve overflows serialized buffer\n");
		return(result);
	}

	qed_curve_editor* editor = malloc_type(qed_curve_editor);
	memset(editor, 0, sizeof(qed_curve_editor));

	qsr_curve* sr = (qsr_curve*)data.ptr;

	editor->cameraX = sr->cameraX;
	editor->cameraW = sr->cameraW;
	editor->cameraH = sr->cameraH;
	editor->axes = sr->axes;

	qsr_curve_elt* eltArray = (qsr_curve_elt*)qsr_get_relptr(sr->elements.offset);
	if( ((char*)eltArray < data.ptr + sizeof(qsr_curve))
	  ||((char*)eltArray + sr->elements.len > data.ptr + data.len))
	{
		LOG_ERROR("curve elements overflow serialized buffer\n");
		return(result);
	}

	editor->eltCount = sr->elements.len / sizeof(qsr_curve_elt);
	for(u64 eltIndex = 0; eltIndex < editor->eltCount; eltIndex++)
	{
		qsr_curve_elt* srElt = &eltArray[eltIndex];

		qed_curve_editor_elt* elt = malloc_type(qed_curve_editor_elt);
		memset(elt, 0, sizeof(qed_curve_editor_elt));

		elt->type = srElt->type;
		memcpy(elt->p, srElt->p, 4*sizeof(qed_curve_point));

		elt->startDerivability = srElt->startDerivability;
		elt->startContinuity = srElt->startContinuity;
		elt->endDerivability = srElt->endDerivability;
		elt->endContinuity = srElt->endContinuity;

		ListAppend(&editor->elements, &elt->listElt);
	}
	result = mp_string_from_buffer(sizeof(qed_curve_editor), (char*)editor);
	return(result);
}

q_cell* cell_alloc_with_id(cell_tree* tree, cell_kind kind, u64 id);

q_cell* deserialize_cell_recursive_v2(cell_tree* tree, mp_string data, q_cell* parent, bool keepID)
{
	if(data.len < sizeof(qsr_cell_v2))
	{
		LOG_ERROR("cell body overflows serialized buffer\n");
		return(0);
	}

	qsr_cell_v2* sr = (qsr_cell_v2*)data.ptr;
	q_cell* cell = 0;


	//TODO: create cell from text if sr->kind has text. Check that we get the same kind, and that no text is left
	//      otherwise, check there's no text and create cell from kind

	//NOTE(martin): deserialize text
	if(sr->flags & SR_CELL_FLAG_TEXT)
	{
		mp_string text = {.ptr = qsr_get_relptr(sr->text.offset),
	                  	  .len = sr->text.len};
		if(  (text.ptr < data.ptr + sizeof(qsr_cell_v2))
	  	|| (text.ptr + text.len > data.ptr + data.len))
		{
			LOG_ERROR("cell text overflows serialized buffer\n");
			return(0);
		}

		if( (sr->flags & SR_CELL_FLAG_SINGLE_QUOTE)
		  ||(sr->flags & SR_CELL_FLAG_DOUBLE_QUOTE)
		  ||(sr->flags & SR_CELL_FLAG_COMMENT))
		{
			cell_kind kind = (sr->flags & SR_CELL_FLAG_SINGLE_QUOTE) ?
							  CELL_CHAR :
							  (sr->flags & SR_CELL_FLAG_DOUBLE_QUOTE)?
							  CELL_STRING:
							  CELL_COMMENT;
			lex_result lex = lex_next(text, 0, kind);
			if(keepID)
			{
				cell = cell_alloc_with_id(tree, kind, sr->id);
			}
			else
			{
				cell = cell_alloc(tree, kind);
			}
			cell_text_replace(cell, text);
			cell->valueU64 = lex.valueU64;
			cell->valueF64 = lex.valueF64;
		}
		else if(sr->flags & SR_CELL_FLAG_UI)
		{
			lex_result lex = lex_next(text, 0, CELL_UI);
			if(keepID)
			{
				cell = cell_alloc_with_id(tree, CELL_UI, sr->id);
			}
			else
			{
				cell = cell_alloc(tree, CELL_UI);
			}
			cell_text_replace(cell, text);
			cell->valueU64 = lex.valueU64;
			cell->valueF64 = lex.valueF64;
		}
		else
		{
			lex_result lex = lex_next(text, 0, CELL_WORD);
			if(keepID)
			{
				cell = cell_alloc_with_id(tree, lex.kind, sr->id);
			}
			else
			{
				cell = cell_alloc(tree, lex.kind);
			}
			cell_text_replace(cell, lex.string);
			cell->valueU64 = lex.valueU64;
			cell->valueF64 = lex.valueF64;
			//TODO: check that there's no text left
		}
	}
	else if(sr->flags & SR_CELL_FLAG_CHILDREN)
	{
		cell_kind kind = CELL_ROOT;
		if(sr->flags & SR_CELL_FLAG_PAREN)
		{
			kind = CELL_LIST;
		}
		else if(sr->flags & SR_CELL_FLAG_ATTR)
		{
			kind = CELL_ATTRIBUTE;
		}
		else if(sr->flags & SR_CELL_FLAG_ARRAY)
		{
			kind = CELL_ARRAY;
		}

		if(sr->text.len)
		{
			LOG_ERROR("saved cell has text but is of incompatible kind, skip text\n");
		}
		if(keepID)
		{
			cell = cell_alloc_with_id(tree, kind, sr->id);
		}
		else
		{
			cell = cell_alloc(tree, kind);
		}
	}

	if(parent)
	{
		cell_push(parent, cell);
	}

	//TODO: deserialize UI
	if(sr->ui.len)
	{
		char* uiPtr = qsr_get_relptr(sr->ui.offset);
		if( (uiPtr < data.ptr + sizeof(qsr_cell_v2))
		  ||(uiPtr + sr->ui.len > data.ptr + data.len))
		{
			LOG_ERROR("UI overflows serialized buffer\n");
			return(0);
		}
		if(sr->ui.len != sizeof(qsr_ui))
		{
			LOG_ERROR("wrong size for serialized UI\n");
			return(0);
		}
		qsr_ui* ui = (qsr_ui*)uiPtr;

		cell->ui.kind = (cell_ui_kind)ui->kind;
		cell->ui.width = ui->width;
		cell->ui.height = ui->height;

		switch(cell->ui.kind)
		{
			case CELL_UI_NONE:
				break;
			case CELL_UI_TEMPO_CURVE:
			{
				char* curvePtr = qsr_get_relptr(ui->data.offset);
				if( (curvePtr < data.ptr + sizeof(qsr_cell_v2))
		  		  ||(curvePtr + ui->data.len > data.ptr + data.len))
				{
					LOG_ERROR("curve overflow serialized buffer\n");
					return(0);
				}
				mp_string curveData = {.len = data.ptr + data.len - curvePtr, .ptr = curvePtr, };
				cell->ui.data = deserialize_curve(curveData);
			} break;
		}
	}
	/*
	if(sr->curve.len)
	{
		char* curvePtr = qsr_get_relptr(sr->curve.offset);
		if( (curvePtr < data.ptr + sizeof(qsr_cell_v2))
		  ||(curvePtr + sr->curve.len > data.ptr + data.len))
		{
			LOG_ERROR("curve overflow serialized buffer\n");
			return(0);
		}
		mp_string curveData = {.len = data.ptr + data.len - curvePtr, .ptr = curvePtr, };

		cell->curveEditor = mem_pool_alloc_type(&tree->curvePool, qed_curve_editor);
		memset(cell->curveEditor, 0, sizeof(qed_curve_editor));
		deserialize_curve(tree, cell->curveEditor, curveData);
	}
	*/

	//NOTE(martin): deserialize children
	if(cell_has_children(cell))
	{
		char* childrenData = qsr_get_relptr(sr->children.offset);
		if( (childrenData < data.ptr + sizeof(qsr_cell_v2))
		  ||(childrenData + sr->children.len > data.ptr + data.len))
		{
			LOG_ERROR("cell children overflow serialized buffer\n");
			return(0);
		}
		u64 childCount = sr->children.len / sizeof(qsr_cell_v2);
		for(u64 childIndex = 0; childIndex < childCount; childIndex++)
		{
			mp_string childData = {};
			childData.ptr = childrenData + childIndex*sizeof(qsr_cell_v2);
			childData.len = data.ptr + data.len - childData.ptr;

			deserialize_cell_recursive_v2(tree, childData, cell, keepID);
		}
	}
	else if(sr->children.len)
	{
		LOG_ERROR("saved cell has children but is of incompatible kind, skip children\n");
	}

	return(cell);
}


int deserialize_cell_tree(cell_tree* tree, mp_string data)
{
	if(data.len < sizeof(qsr_save_header))
	{
		LOG_ERROR("invalid save data\n");
		return(-1);
	}
	qsr_save_header* header = (qsr_save_header*)data.ptr;

	if(header->magic != Q_SAVE_MAGIC_NUMBER)
	{
		LOG_ERROR("invalid save data (does not contain magic identifier)\n");
		return(-1);
	}
	if(header->version != Q_SAVE_VERSION)
	{
		LOG_ERROR("unsupported version %i\n", header->version);
		return(-1);
	}

	//TODO: clear cell arena here?
	mem_pool_clear(&tree->cellPool);
//	mem_pool_clear(&tree->curvePool);
//	mem_pool_clear(&tree->curveEltPool);

	q_hashmap_clear(&tree->cellMap);

	data.ptr += sizeof(qsr_save_header);
	data.len -= sizeof(qsr_save_header);

	q_cell* rootCell = 0;
	if(header->version == 2)
	{
		LOG_DEBUG("deserialize v2\n");
		rootCell = deserialize_cell_recursive_v2(tree, data, 0, true);
	}

	tree->nextCellID = header->nextID;

	if(!rootCell)
	{
		return(-1);
	}
	else
	{
		tree->rootCell = rootCell;
		return(0);
	}
}


typedef struct qed_clipboard_header
{
	u32 magic;
	u32 version;
	u32 count;

} qed_clipboard_header;

mp_string serialize_cell_span(mem_arena* arena, qed_cell_span span)
{
	q_cell* parent = span.start->parent;
	u32 count = 0;

	for(q_cell* cell = span.start;
	    cell;
	    cell = ListNextEntry(&parent->children, cell, q_cell, listElt))
	{
		count++;
		if(cell == span.end)
		{
			break;
		}
	}

	u64 startOffset = arena->offset;
	char* startPointer = (char*)mem_arena_alloc(arena, sizeof(qed_clipboard_header));

	qed_clipboard_header header = {.magic = Q_SAVE_MAGIC_NUMBER,
	                               .version = Q_SAVE_VERSION,
	                               .count = count};

	memcpy(startPointer, &header, sizeof(qed_clipboard_header));

	qsr_cell_v2* slotArray = mem_arena_alloc_array(arena, qsr_cell_v2, count);
	memset(slotArray, 0, count * sizeof(qsr_cell_v2));

	u32 slotIndex = 0;
	for(q_cell* cell = span.start;
	    cell;
	    cell = ListNextEntry(&parent->children, cell, q_cell, listElt))
	{
		serialize_cell_in_slot(arena, cell, &slotArray[slotIndex]);
		slotIndex++;
		if(cell == span.end)
		{
			break;
		}
	}

	mp_string result = {.len = arena->offset - startOffset, .ptr = startPointer};
	return(result);
}

list_info deserialize_cell_span(cell_tree* tree, mp_string data)
{
	list_info result = {};
	if(data.len < sizeof(qed_clipboard_header))
	{
		LOG_ERROR("invalid clipboard data\n");
		return(result);
	}
	qed_clipboard_header* header = (qed_clipboard_header*)data.ptr;

	if(header->magic != Q_SAVE_MAGIC_NUMBER)
	{
		LOG_ERROR("invalid clipboard data (does not contain magic identifier)\n");
		return(result);
	}
	if(header->version != Q_SAVE_VERSION)
	{
		LOG_ERROR("unsupported version %i\n", header->version);
		return(result);
	}

	if(data.len < sizeof(qed_clipboard_header) + header->count*sizeof(qsr_cell_v2))
	{
		LOG_ERROR("cells overflow clipboard data\n");
		return(result);
	}

	qsr_cell_v2* slotArray = (qsr_cell_v2*)(data.ptr + sizeof(qed_clipboard_header));
	for(int slotIndex=0; slotIndex<header->count; slotIndex++)
	{
		mp_string cellData = {};
		cellData.ptr = (char*)(&slotArray[slotIndex]);
		cellData.len = data.ptr + data.len - cellData.ptr;
		q_cell* cell = 0;

		if(header->version == 2)
		{
			cell = deserialize_cell_recursive_v2(tree, cellData, 0, false);
		}
		if(!cell)
		{
			return(result);
		}
		ListAppend(&result, &cell->listElt);
	}
	return(result);
}

#undef LOG_SUBSYSTEM
