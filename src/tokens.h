/************************************************************//**
*
*	@file: tokens.h
*	@author: Martin Fouilleul
*	@date: 13/05/2022
*	@revision:
*
*****************************************************************/
#ifndef __TOKENS_H_
#define __TOKENS_H_

#include"macro_helpers.h"

#define Q_TOKEN_OPERATORS(X) \
	X(OP_ADD, "+") \
	X(OP_SUB, "-") \
	X(OP_MUL, "*") \
	X(OP_DIV, "/") \
	X(OP_MOD, "%") \
	X(OP_EQ, "=") \
	X(OP_NEQ, "!=") \
	X(OP_LT, "<") \
	X(OP_GT, ">") \
	X(OP_LE, "<=") \
	X(OP_GE, ">=") \
	X(OP_ARROW, "->")

#define Q_TOKEN_KEYWORDS(X) \
	X(KW_LAND, "and") \
	X(KW_LOR, "or") \
	X(KW_LNOT, "not") \
	X(KW_VAR, "var") \
	X(KW_DEF, "def") \
	X(KW_SET, "set") \
	X(KW_IF, "if") \
	X(KW_WHILE, "while") \
	X(KW_FOR, "for") \
	X(KW_DO, "do") \
	X(KW_RETURN, "return") \
	X(KW_ARRAY, "array") \
	X(KW_AT, "at") \
	X(KW_SLICE, "slice") \
	X(KW_LEN, "len") \
	X(KW_CAST, "cast") \
	X(KW_TYPE, "type") \
	X(KW_TYPEID, "typeid") \
	X(KW_POINTER, "ptr") \
	X(KW_REF, "ref") \
	X(KW_DEREF, "deref") \
	X(KW_STRUCT, "struct") \
	X(KW_FIELD, "field") \
	X(KW_VARG, "varg") \
	X(KW_FUTURE, "future") \
	X(KW_FLOW, "flow") \
	X(KW_PAUSE, "pause") \
	X(KW_WAIT, "wait") \
	X(KW_TIMEOUT, "timeout") \
	X(KW_STANDBY, "standby") \
	X(KW_FDUP, "fdup") \
	X(KW_FDROP, "fdrop") \
	X(KW_BACKGROUND, "background") \
	X(KW_FOREIGN, "foreign") \
	X(KW_IMPORT, "import") \
	X(KW_NIL, "nil") \
	X(KW_SIZEOF, "sizeof") \
	X(KW_PUT, "put")


//WARN(martin): First part of the token enum up to "unknown" overlaps with cell_kind, so this has to be kept in sync
#define Q_TOKENS(X) \
	X(ROOT, "<root>") \
	X(LIST, "<list>") \
	X(ATTRIBUTE, "<attribute>") \
	X(ARRAY, "<array>") \
	X(HOLE, "<hole>") \
	X(COMMENT, "<comment>") \
	X(INT, "<int literal>") \
	X(FLOAT, "<float literal>") \
	X(STRING, "<string literal>") \
	X(CHAR, "<char literal>") \
	X(UI, "<ui identifier>") \
	X(UNKNOWN, "<unknown>") \
	X(IDENTIFIER, "<id>") \
	X(POLY, "<polymorphic id>") \
	\
	Q_TOKEN_OPERATORS(X) \
	\
	X(RANGE_KW_START, "<keywords range start>") \
	Q_TOKEN_KEYWORDS(X) \
	X(RANGE_KW_END, "<keywords range end>") \
	\
	X(END, "<end of cell>") \

typedef u32 q_token;

enum {
	#define X(tok, str) _cat2_(TOKEN_, tok),
		Q_TOKENS(X)
	#undef X
};


typedef enum {
	TOKEN_CLASS_NUMBER,
	TOKEN_CLASS_STRING,
	TOKEN_CLASS_OPERATOR,
	TOKEN_CLASS_KEYWORD,
	TOKEN_CLASS_IDENTIFIER,
	TOKEN_CLASS_ARRAY,
	TOKEN_CLASS_LIST,

	TOKEN_CLASS_CALL,
	TOKEN_CLASS_VAR,
} token_class;

#define token_is_keyword(tok) ((tok) > TOKEN_RANGE_KW_START && (tok) < TOKEN_RANGE_KW_END)

//TODO: move that in parser?
typedef struct q_cell q_cell;
q_token cell_token(q_cell* cell);

#endif //__TOKENS_H_
