/************************************************************//**
*
*	@file: ir.h
*	@author: Martin Fouilleul
*	@date: 29/06/2022
*	@revision:
*
*****************************************************************/
#ifndef __IR_H_
#define __IR_H_

#include"ast.h"

typedef struct ir_symbol ir_symbol;
typedef struct ir_param ir_param;
typedef struct ir_node ir_node;

//WARN(martin): this must stay in sync with bc_type_kind
typedef enum { TYPE_ERROR,
               TYPE_VOID,
               TYPE_PRIMITIVE,
               TYPE_NAMED,
               TYPE_POINTER,
               TYPE_FUTURE,
               TYPE_ARRAY,
               TYPE_SLICE,
               TYPE_STRUCT,
               TYPE_PROC,
               TYPE_ANY,
               TYPE_POLY,
               TYPE_POLY_TYPEID } type_kind;

typedef enum { TYPE_B8,
               TYPE_U8,
               TYPE_I8,
               TYPE_U16,
               TYPE_I16,
               TYPE_U32,
               TYPE_I32,
               TYPE_U64,
               TYPE_I64,
               TYPE_F32,
               TYPE_F64 } type_primitive;

typedef struct ir_type
{
	type_kind kind;
	u32 size;
	u8 align;

	type_primitive primitive;
	list_info params;
	ir_param* retParam;
	ir_symbol* symbol;

	union
	{
		ir_type* retType;
		ir_type* eltType;
	};
	u64 eltCount;

	//...
	mp_string name;
	bool polymorphic;

	u64 offset; //NOTE: offset in rodata section, set/used by generator
} ir_type;

typedef struct ir_param
{
	list_elt listElt;

	mp_string name;
	ir_type* type;
	u32 offset;
	bool variadic;
	bool polyTypeid;

} ir_param;

typedef struct ir_poly_binding
{
	list_elt listElt;
	mp_string name;
	ir_type* type;
} ir_poly_binding;

typedef struct ir_typeref
{
	list_elt listElt;
	ir_type* type;
} ir_typeref;

typedef enum {
	SYM_TYPE,
	SYM_VAR,
	SYM_PROC,
	SYM_RODATA,
	SYM_FOREIGN_BLOCK,
	SYM_MODULE,
	SYM_EDITOR_WIDGET } symbol_kind;

typedef enum {
	SYM_UNDEF,
	SYM_PENDING,
	SYM_COMPLETE,
} symbol_status;

typedef struct ir_symbol_proc
{
	bool foreign;
	u64 foreignIndex;
	list_elt foreignElt;

	bool inferReturnType;
	ir_node* body;
	u32 frameSize;
	bool capturedFrame;
	list_info captures;

} ir_symbol_proc;

typedef struct ir_symbol_foreign_block
{
	list_info procDefs;
} ir_symbol_foreign_block;

typedef struct ir_symbol_capture
{
	u32 level;
	u32 offset;
	list_elt listElt;
	ir_symbol* symbol;
} ir_symbol_variable;

typedef struct ir_symbol_widget
{
	ir_symbol* global;
	q_cell* cell;
} ir_symbol_widget;

typedef struct q_module q_module;

typedef struct ir_symbol
{
	list_elt bucketElt;
	list_elt auxElt;

	u64 hash;
	mp_string name;

	q_ast* ast;

	symbol_status status;
	symbol_kind kind;
	ir_type* type;
	bool global; //could store scope...
	bool anonymous;
	u64 offset;

	union
	{
		ir_symbol_capture capture;
		ir_symbol_proc proc;
		ir_symbol_foreign_block foreignBlock;
		q_module* module;
		mp_string data;
		ir_symbol_widget widget;
	};

} ir_symbol;


typedef enum { SCOPE_NORMAL, SCOPE_PROC, SCOPE_TASK } scope_kind;

const u32 IR_SCOPE_BUCKET_COUNT_EXP = 8,
          IR_SCOPE_BUCKET_COUNT     = 1<<IR_SCOPE_BUCKET_COUNT_EXP,
          IR_SCOPE_INDEX_MASK       = IR_SCOPE_BUCKET_COUNT-1;

typedef struct ir_scope
{
	list_elt listElt;
	list_info children;
	ir_scope* parent;
	scope_kind kind;

	list_info variables;
	list_info buckets[IR_SCOPE_BUCKET_COUNT];

	//...
} ir_scope;


////////////////TODO: maybe move somewhere

typedef enum
{
	TYPE_PATTERN_ANYTHING,
	TYPE_PATTERN_NONE,
	TYPE_PATTERN_ARRAY_OR_SLICE,
	TYPE_PATTERN_STRING,
	TYPE_PATTERN_NUMERIC,
	TYPE_PATTERN_POINTER,
	TYPE_PATTERN_FUTURE,
	TYPE_PATTERN_EXACT,

} ir_type_pattern_kind;

typedef struct ir_type_pattern
{
	ir_type_pattern_kind kind;
	ir_type* exactType;

} ir_type_pattern;

////////////////

#define IR_KINDS(X) \
	X(IR_ERROR, "error") \
	X(IR_NOP, "nop") \
	X(IR_POINTER_NIL, "nil pointer") \
	X(IR_INT, "int literal") \
	X(IR_FLOAT, "float literal") \
	X(IR_RODATA, "rodata") \
	X(IR_VAR, "variable") \
	X(IR_AT, "at") \
	X(IR_LEN, "len") \
	X(IR_FIELD, "field") \
	X(IR_REF, "ref") \
	X(IR_DEREF, "deref") \
	X(IR_NEG, "neg") \
	X(IR_ADD, "add") \
	X(IR_SUB, "sub") \
	X(IR_MUL, "mul") \
	X(IR_DIV, "div") \
	X(IR_MOD, "mod") \
	X(IR_EQ, "eq") \
	X(IR_NEQ, "neq") \
	X(IR_LT, "lt") \
	X(IR_GT, "gt") \
	X(IR_LE, "le") \
	X(IR_GE, "ge") \
	X(IR_LNOT, "logical not") \
	X(IR_LAND, "logical and") \
	X(IR_LOR, "logical or") \
	X(IR_CAST, "cast") \
	X(IR_LOAD, "load") \
	X(IR_STORE, "store") \
	X(IR_TSTORE, "T store") \
	X(IR_SLICE, "slice") \
	X(IR_BOX, "box") \
	X(IR_BODY, "body") \
	X(IR_SEQ, "ir sequence") \
	X(IR_ARRAY_INIT, "array initializer") \
	X(IR_SLICE_INIT, "slice initializer") \
	X(IR_CALL, "call") \
	X(IR_RETURN, "return") \
	X(IR_IF, "if statement") \
	X(IR_FOR, "for loop") \
	X(IR_WHILE, "while loop") \
	X(IR_PUT, "put") \
	X(IR_TEMPO_CURVE, "tempo curve") \
	X(IR_FLOW, "flow block") \
	X(IR_BACKGROUND, "background block") \
	X(IR_STANDBY, "standby") \
	X(IR_PAUSE, "pause") \
	X(IR_WAIT, "wait") \
	X(IR_WAIT_RECURSIVE, "recursive wait") \
	X(IR_TIMEOUT, "timeout") \
	X(IR_TIMEOUT_RECURSIVE, "recursive timeout") \
	X(IR_FDUP, "duplicate future") \
	X(IR_FDROP, "drop future") \


typedef enum {
	#define IR_KIND_ENUM(kind, str) kind,
	IR_KINDS(IR_KIND_ENUM)
	IR_KIND_COUNT
} ir_kind;

const char* IR_KIND_STRINGS[IR_KIND_COUNT] =
{
	#define IR_KIND_STRING(kind, string) string,
	IR_KINDS(IR_KIND_STRING)
};

typedef enum { IR_MODE_INVALID,
               IR_CONSTANT,
               IR_VALUE,
               IR_ADDRESS } ir_mode;

typedef struct ir_node
{
	list_elt listElt;
	ir_node* parent;
	list_info children;
	u32 childCount;

	q_ast* ast;

	ir_kind kind;
	ir_mode mode;
	ir_type* type;
	ir_symbol* symbol;
	ir_param* param;
	ir_scope* scope;
	bool implicitlyConvertible;
	u64 valueU64;
	f64 valueF64;

	ir_type_pattern expectedTypePattern;

} ir_node;

#define ir_first_child(ir) (ListFirstEntry(&(ir)->children, ir_node, listElt))
#define ir_last_child(ir) (ListLastEntry(&(ir)->children, ir_node, listElt))
#define ir_next_sibling(ir) (ListNextEntry(&(ir)->parent->children, (ir), ir_node, listElt))

ir_type* ir_type_unwrap(ir_type* type);
bool ir_type_is_numeric(ir_type* type);
bool ir_type_is_equivalent(ir_type* t1, ir_type* t2);

#endif //__IR_H_
