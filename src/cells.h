/************************************************************//**
*
*	@file: cells.h
*	@author: Martin Fouilleul
*	@date: 11/05/2022
*	@revision:
*
*****************************************************************/
#ifndef __CELLS_H_
#define __CELLS_H_

#include"typedefs.h"
#include"strings.h"
#include"lists.h"
#include"memory.h"

typedef struct cell_text
{
	mp_string string;
	u32 cap;
} cell_text;


typedef enum
{
	CELL_UI_NONE,
	CELL_UI_TEMPO_CURVE,
} cell_ui_kind;

typedef struct cell_ui
{
	cell_ui_kind kind;
	f32 width;
	f32 height;
	mp_string data;

} cell_ui;

typedef u32 cell_flags;
enum
{
	CELL_INITIALIZED = 1<<0,
	CELL_DIRTY       = 1<<1,
	//...
};

//WARN: this should be kept in sync with cell_has_xxx() procs.
typedef enum
{
	CELL_ROOT,
	CELL_LIST,
	CELL_ATTRIBUTE,
	CELL_ARRAY,
	CELL_HOLE,
	CELL_COMMENT,
	CELL_INT,
	CELL_FLOAT,
	CELL_STRING,
	CELL_CHAR,
	CELL_UI,
	CELL_WORD,
} cell_kind;

typedef struct q_ast q_ast;

typedef struct q_cell
{
	q_cell* parent;
	list_elt listElt;
	list_info children;
	u32 childCount;

	u64 id;
	cell_flags flags;
	cell_kind kind;
	cell_text text;

	u64 valueU64;
	f64 valueF64;

	cell_ui ui;

	q_ast* ast; // cached ast
	u64 astGen; // cache generation index

	//NOTE: layout
	vec2 targetPos; //NOTE: target position relative to the parent
	mp_aligned_rect box;
	f32 frameOffset;
	f32 lastLineWidth;

	f32 flashValue;
	f32 progressTimeout;
	f32 progress;
	f32 waitingTimeout;
	f32 waitingProgress;
	f32 standbyTimeout;
	f32 standbyProgress;

} q_cell;

const f32 CELL_FLASH_LENGTH = 1.;

typedef struct q_point
{
	q_cell* parent;
	q_cell* leftFrom;
	u32 textIndex;

} qed_point;

typedef struct q_range
{
	q_point start;
	q_point end;
} q_range;

typedef struct qed_cell_span
{
	q_cell* start;
	q_cell* end;
} qed_cell_span;

typedef struct cell_tree
{
	u64 nextCellID;

	mem_pool cellPool;
	q_cell* rootCell;

	q_hashmap cellMap;

} cell_tree;

#define cell_first_child(parent) ListFirstEntry(&((parent)->children), q_cell, listElt)
#define cell_last_child(parent) ListLastEntry(&((parent)->children), q_cell, listElt)
#define cell_next_sibling(cell) ListNextEntry(&((cell)->parent->children), (cell), q_cell, listElt)
#define cell_prev_sibling(cell) ListPrevEntry(&((cell)->parent->children), (cell), q_cell, listElt)

bool cell_has_children(q_cell* cell);
bool cell_has_text(q_cell* cell);
bool cell_can_edit(q_cell* cell);
bool cell_has_ancestor(q_cell* cell, q_cell* ancestor);

void cell_tree_init(cell_tree* tree);
q_cell* cell_alloc(cell_tree* tree, cell_kind kind);
q_cell* cell_alloc_with_id(cell_tree* tree, cell_kind kind, u64 id);
void cell_recycle(cell_tree* tree, q_cell* cell);

void cell_insert(q_cell* after, q_cell* cell);
void cell_insert_before(q_cell* before, q_cell* cell);
void cell_push(q_cell* parent, q_cell* cell);

void cell_text_replace(q_cell* cell, mp_string string);

void cell_ast_cache(q_cell* cell, q_ast* ast);
q_ast* cell_ast_lookup(q_cell* cell);


#endif //__CELLS_H_
