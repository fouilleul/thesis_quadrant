/************************************************************//**
*
*	@file: checker.h
*	@author: Martin Fouilleul
*	@date: 23/06/2022
*	@revision:
*
*****************************************************************/
#ifndef __CHECKER_H_
#define __CHECKER_H_

#include"workspace.h"

typedef struct checker_context
{
	q_workspace* workspace;
	q_module* module;
	mem_arena* arena;

	ir_symbol* proc;
	list_info* polyBindings;

	ir_scope* scopeStackTop;

} checker_context;

void checker_context_init(checker_context* context, q_workspace* workspace, q_module* module);
void check_module(checker_context* context);
ir_symbol* ir_symbol_alloc(checker_context* context);
ir_scope* checker_scope_push(checker_context* context, scope_kind kind);
ir_symbol* checker_insert_symbol(checker_context* context, ir_symbol* symbol);
ir_symbol* checker_find_symbol_in_module(q_module* module, mp_string name);
mp_string ast_copy_string(mem_arena* arena, q_ast* ast);

#endif //__CHECKER_H_
