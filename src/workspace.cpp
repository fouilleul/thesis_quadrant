/************************************************************//**
*
*	@file: workspace.cpp
*	@author: Martin Fouilleul
*	@date: 10/05/2022
*	@revision:
*
*****************************************************************/
#include"workspace.h"
#include"parser.h"
#include"checker.h"
#include"save.h"

#define LOG_SUBSYSTEM "Workspace"

//TODO: adjust per-platform
const u32 QED_PATH_MAX = 512;

void workspace_add_search_path(q_workspace* workspace, mp_string path)
{
	q_search_path* stringElt = (q_search_path*)malloc(sizeof(q_search_path) + path.len);
	stringElt->string.len = path.len;
	stringElt->string.ptr = ((char*)stringElt) + sizeof(q_search_path);
	memcpy(stringElt->string.ptr, path.ptr, path.len);

	ListAppend(&workspace->searchPaths, &stringElt->listElt);
}

void workspace_remove_search_path(q_workspace* workspace, q_search_path* path)
{
	ListRemove(&workspace->searchPaths, &path->listElt);
	free(path);
}

void workspace_init(q_workspace* workspace)
{
	mem_pool_init(&workspace->modulePool, sizeof(q_module));
	mem_arena_init(&workspace->programArena);

	//NOTE: add default search paths
	mem_arena* scratch = mem_scratch();
	mp_string executablePath = mp_app_get_executable_path(scratch);
	mp_string executableDir = mp_path_directory(executablePath);

	mp_string_list list = {};
	mp_string_list_push(scratch, &list, executableDir);
	mp_string_list_push(scratch, &list, mp_string_lit("../core"));
	mp_string corePath = mp_string_list_join(scratch, list);
	char* corePathString = mp_string_to_cstring(scratch, corePath);

	char* buffer = mem_arena_alloc_array(scratch, char, QED_PATH_MAX);
	char* res = realpath(corePathString, buffer);

	if(res)
	{
		workspace_add_search_path(workspace, mp_string_from_cstring(res));
	}
	else
	{
		LOG_ERROR("Couldn't find core library path: %s\n", buffer);
	}
}

void module_set_path(q_workspace* workspace, q_module* module, mp_string path)
{
	if(module->path.len)
	{
		q_hashmap_remove(&workspace->moduleMap, module->pathHash);
		free(module->path.ptr);
		module->path.len = 0;
	}
	module->path.len = path.len;
	module->path.ptr = malloc_array(char, path.len);
	memcpy(module->path.ptr, path.ptr, path.len);

	module->pathHash = q_hash_string(path);
	q_hashmap_insert(&workspace->moduleMap, module->pathHash, (uintptr_t)module);
}

q_module* module_alloc(q_workspace* workspace)
{
	q_module* module = mem_pool_alloc_type(&workspace->modulePool, q_module);
	memset(module, 0, sizeof(q_module));

	cell_tree_init(&module->tree);

	mem_arena_init(&module->astArena);

	mem_arena_init(&module->symbolArena);

	ListPush(&workspace->modules, &module->listElt);
	return(module);
}

void module_recycle(q_workspace* workspace, q_module* module)
{
	if(workspace->mainModule == module)
	{
		workspace->mainModule = 0;
	}
	q_hashmap_remove(&workspace->moduleMap, module->pathHash);
	ListRemove(&workspace->modules, &module->listElt);


	mem_arena_release(&module->astArena);
	mem_arena_release(&module->symbolArena);

	mem_pool_recycle(&workspace->modulePool, module);
}

q_module* module_alloc_transient(q_workspace* workspace)
{
	q_module* module = module_alloc(workspace);
	module->transient = true;

	u64 transientID = workspace->nextTransientModuleID;
	workspace->nextTransientModuleID++;

	mp_string name = mp_string_pushf(mem_scratch(), "*transient module %llu*", transientID);
	module_set_path(workspace, module, name);

	return(module);
}

q_module* workspace_main_module(q_workspace* workspace)
{
	return(workspace->mainModule);
}

void workspace_set_main_module(q_workspace* workspace, q_module* module)
{
	q_module* oldModule = workspace->mainModule;
	workspace->mainModule = module;
	if(module != oldModule)
	{
		workspace_rebuild(workspace);
	}
}

void workspace_begin_modify(q_workspace* workspace)
{
	ListInit(&workspace->modifiedCells);
}

void workspace_mark_modified(q_workspace* workspace, q_module* module, q_cell* cell)
{
	module->dirty = true;
}

void build_error_va(q_module* module, q_ast* ast, const char* fmt, va_list ap)
{
	q_error* error = mem_arena_alloc_type(&module->symbolArena, q_error);
	memset(error, 0, sizeof(q_error));

	//WARN: build error string is allocated on the symbol arena
	error->msg = mp_string_pushfv(&module->symbolArena, fmt, ap);
	error->kind = Q_ERROR_BUILD;
	error->ast = ast;
	error->astGen = ast->generation;
	ListAppend(&module->checkingErrors, &error->moduleElt);
}

void build_error(q_module* module, q_ast* ast, const char* fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	build_error_va(module, ast, fmt, ap);
	va_end(ap);
}

q_module* workspace_find_module(q_workspace* workspace, mp_string path)
{
	q_module* module = 0;
	u64 key = q_hash_string(path);
	u64 value = 0;
	if(q_hashmap_find(&workspace->moduleMap, key, &value))
	{
		module = (q_module*)value;
	}
	return(module);
}

mp_string q_workspace_search_path(q_workspace* workspace, mp_string basename)
{
	mp_string result = {};
	mem_arena* scratch = mem_scratch();

	mp_string buildModulePath = workspace->mainModule->path;
	mp_string buildDir = mp_path_directory(buildModulePath);

	mp_string_list stringList = {};
	mp_string_list_push(scratch, &stringList, buildDir);
	mp_string_list_push(scratch, &stringList, basename);

	mp_string importPath = mp_string_list_join(mem_scratch(), stringList);

	char* importPathCString = mp_string_to_cstring(mem_scratch(), importPath);
	if(!access(importPathCString, R_OK))
	{
		result = importPath;
	}
	else
	{
		//NOTE: if the file doesn't exist in the build directory, we search in the global search paths
		for_each_in_list(&workspace->searchPaths, searchPath, q_search_path, listElt)
		{
			mp_string_list stringList = {};
			mp_string_list_push(scratch, &stringList, searchPath->string);
			mp_string_list_push(scratch, &stringList, mp_string_lit("/"));
			mp_string_list_push(scratch, &stringList, basename);

			importPath = mp_string_list_join(mem_scratch(), stringList);
			char* importPathCString = mp_string_to_cstring(mem_scratch(), importPath);
			if(!access(importPathCString, R_OK))
			{
				result = importPath;
				break;
			}
		}
	}

	return(result);
}

void parse_and_check_recursive(q_workspace* workspace, list_info* buildModules, q_module* module)
{
	module->buildCounter = workspace->buildCounter;
	module->status = Q_MODULE_BUILDING;

	//NOTE: clear checking error
	mem_arena_clear(&module->symbolArena);
	ListInit(&module->checkingErrors);

	//NOTE: parse
	parse_module(module);

	//NOTE: prune parsing errors
	for_each_in_list_safe(&module->parsingErrors, error, q_error, moduleElt)
	{
		if(error->ast->generation != error->astGen)
		{
			//WARN: parsing error strings are allocated on the heap
			free(error->msg.ptr);
			free(error->cellHint.ptr);

			ListRemove(&module->parsingErrors, &error->moduleElt);
			ListPush(&module->parsingErrorFreeList, &error->moduleElt);
		}
	}

	//NOTE: initialize checker context
	mem_arena_clear(&module->symbolArena);
	ListInit(&module->procedures);
	ListInit(&module->types);
	ListInit(&module->rodata);
	ListInit(&module->polyProcInstances);
	ListInit(&module->foreignBlocks);
	ListInit(&module->editorWidgets);

	checker_context checkerContext;
	checker_context_init(&checkerContext, workspace, module);
	module->globalScope = checker_scope_push(&checkerContext, SCOPE_NORMAL);


	//NOTE: imports
	for_each_in_list(&module->ast->children, child, q_ast, listElt)
	{
		if(child->kind == AST_IMPORT)
		{
			q_ast* nameNode = ast_first_child(child);
			q_ast* aliasNode = ast_next_sibling(nameNode);

			if(nameNode->kind != AST_ID)
			{
				continue;
			}

			q_cell* nameCell = nameNode->range.start.leftFrom;
			mp_string name = nameCell->text.string;

			mem_arena* scratch = mem_scratch();
			mp_string_list stringList = {};
			mp_string_list_push(scratch, &stringList, name);
			mp_string_list_push(scratch, &stringList, mp_string_lit(".ql"));
			mp_string baseName = mp_string_list_join(scratch, stringList);


			mp_string importPath = q_workspace_search_path(workspace, baseName);

			if(importPath.len)
			{
				q_module* importedModule = 0;

				for_each_in_list(buildModules, checkModule, q_module, buildListElt)
				{
					if(!mp_string_cmp(importPath, checkModule->path))
					{
						importedModule = checkModule;
						break;
					}
				}

				if(!importedModule)
				{
					importedModule = module_load(workspace, importPath);
				}

				if(!importedModule)
				{
					build_error(module, nameNode, "Couldn't load module %.*s\n", (int)importPath.len, importPath.ptr);
				}
				else if(  importedModule->buildCounter == workspace->buildCounter
				       && importedModule->status == Q_MODULE_BUILDING)
				{
					build_error(module, nameNode, "Cyclic import of module %.*s\n", (int)importPath.len, importPath.ptr);
				}
				else
				{
					if(importedModule->buildCounter != workspace->buildCounter)
					{
						parse_and_check_recursive(workspace, buildModules, importedModule);
					}

					//NOTE: check import name
					mp_string importName = nameCell->text.string;

					if(aliasNode->kind == AST_ATTR_AS)
					{
						importName = ast_copy_string(mem_scratch(), ast_first_child(aliasNode));
					}

					//NOTE: we need check context here...

					//NOTE: create module symbol
					ir_symbol* symbol = ir_symbol_alloc(&checkerContext);

					symbol->kind = SYM_MODULE;
					symbol->name = mp_string_push_copy(checkerContext.arena, importName);
					symbol->module = importedModule;
					checker_insert_symbol(&checkerContext, symbol);
				}
			}
			else
			{
				build_error(module, nameNode, "Couldn't find module %.*s\n", (int)name.len, name.ptr);
			}
		}
	}

	//NOTE: check
	check_module(&checkerContext);
	ListAppend(buildModules, &module->buildListElt);

	DEBUG_ASSERT(module->path.ptr);

	module->status = Q_MODULE_BUILT;
}


void gen_program(mem_arena* arena, q_program* program, list_info* modules);

void workspace_rebuild(q_workspace* workspace)
{
	workspace->buildCounter++;

	list_info buildModules = {0};
	parse_and_check_recursive(workspace, &buildModules, workspace->mainModule);

	//NOTE(martin): check errors
	bool hasErrors = false;
	for_each_in_list(&buildModules, module, q_module, buildListElt)
	{
		if(  !ListEmpty(&module->parsingErrors)
		  || !ListEmpty(&module->checkingErrors))
		{
			hasErrors = true;
		}
	}

	//NOTE(martin): if there's no error, generate bytecode program
	if(!hasErrors)
	{
		mem_arena_clear(&workspace->programArena);
		gen_program(&workspace->programArena, &workspace->program, &buildModules);
	}
}


q_module* module_load(q_workspace* workspace, mp_string path)
{
	//NOTE: check that module is not already loaded in workspace
	q_module* module = workspace_find_module(workspace, path);
	if(module)
	{
		return(module);
	}

	module = module_alloc(workspace);
	cell_tree* tree = &module->tree;

	module->transient = false;
	module_set_path(workspace, module, path);

	const char* fileName = mp_string_to_cstring(mem_scratch(), path);
	FILE* file = fopen(fileName, "r");
	if(!file)
	{
		LOG_ERROR("Couldn't open file '%s'\n", fileName);
		module_recycle(workspace, module);
		return(0);
	}

	fseek(file, 0, SEEK_END);
	u64 size = (u64)ftello(file);
	rewind(file);

	char* buffer = mem_arena_alloc_array(mem_scratch(), char, size);
	fread(buffer, 1, size, file);
	fclose(file);

	mp_string data = {.ptr = buffer, .len = size};
	if(deserialize_cell_tree(tree, data) != 0)
	{
		module_recycle(workspace, module);
		module = 0;
	}
	return(module);
}

void module_save(q_workspace* workspace, q_module* module, mp_string path)
{
	const char* fileName = mp_string_to_cstring(mem_scratch(), path);
	FILE* file = fopen(fileName, "w");
	if(!file)
	{
		LOG_ERROR("Couldn't open file '%s'\n", fileName);
		return;
	}

	cell_tree* tree = &module->tree;

	char* buffer = 0;
	u64 len = 0;
	mp_string data = serialize_cell_tree(mem_scratch(), tree->nextCellID, tree->rootCell);

	fwrite(data.ptr, 1, data.len, file);
	fclose(file);

	module->transient = false;
	module_set_path(workspace, module, mp_string_from_cstring((char*)fileName));
}


#undef LOG_SUBSYSTEM
