/************************************************************//**
*
*	@file: parser.h
*	@author: Martin Fouilleul
*	@date: 13/05/2022
*	@revision:
*
*****************************************************************/
#ifndef __PARSER_H_
#define __PARSER_H_

#include"cells.h"
#include"workspace.h"

void parse_module(q_module* module);
void parse_top_level(q_module* module, q_cell* cell);

#endif //__PARSER_H_
