/************************************************************//**
*
*	@file: vm.cpp
*	@author: Martin Fouilleul
*	@date: 12/08/2021
*	@revision:
*
*****************************************************************/
#include<ffi/ffi.h>
#include<dlfcn.h>

#define ONLY_MSPACES 1
#include"dlmalloc.h"

#include"bytecode.h"
#include"scheduler.h"
#include"ringbuffer.h"

#include"vm.h"

#define LOG_SUBSYSTEM "VM"

//------------------------------------------------------------------------------------
// Structs and constants
//------------------------------------------------------------------------------------

typedef enum { VM_TASK_RUNNING,
               VM_TASK_PAUSED,
               VM_TASK_WAITING,
               VM_TASK_STANDBY,
               VM_TASK_BACKGROUND,
               VM_TASK_DONE } vm_task_status;

typedef struct vm_info vm_info;

typedef struct vm_task
{
	vm_info* vm;
	u32 generation;
	list_elt listElt;

	vm_task_status status;
	u32 openHandles;
	u32 activeCaptures;
	sched_task schedHandle;

	bool ownReturnPtr;
	bool capturedFrame; // the task is a call to a captured frame function
	u32 callSite;       // this is the call site of the captured frame function

	vm_task* parent;
	list_elt parentListElt;
	list_info children;

	u8* opStack;
	u8* ctlStack;

	u64 osp;
	u64 csp;
	u64 bp;
	u64 ip;
	i8  sr;
	u64 retValue;

	u64 backgroundIP;

} vm_task;

/////// review these
const u64 VM_TASK_STACK_SIZE = 1<<20,
          VM_OPSTACK_SIZE    = 256,
          VM_CALLSTACK_SIZE  = VM_TASK_STACK_SIZE - VM_OPSTACK_SIZE,
          VM_MEM_SIZE        = (4ULL)<<30,
          VM_STACK_ZONE_SIZE = 1<<30,
          VM_MAX_TASK_COUNT  = 1<<10;

typedef struct vm_ffi_info
{
	ffi_cif cif;
	void* funPtr;
	u64 argCount;
	u64* argOffsets;
} vm_ffi_info;

typedef struct vm_options
{
	bool offline;
	bool noFeedback;
	bool printStdout;
} vm_options;

typedef struct vm_info
{
	u8* bytecode;
	u64 codeSize;

	u8* memory;
	u64 allocOffset;
	u8* freeList;

	u32 nextTaskSlot;
	list_info tasks;
	list_info taskFreeList;
	vm_task taskPool[VM_MAX_TASK_COUNT];

	u64 heapBaseOffset;
	mspace mspaceHandle;
	i64 totalAllocations;

	mem_arena foreign;
	vm_ffi_info** ffiTable;

	q_ringbuffer* commandBuffer;
	q_ringbuffer* feedbackBuffer;
	q_ringbuffer* outputBuffer;

	q_hashmap widgetMap;

	//Temporary debug stuff
	vm_options options;
	vm_task* currentTask;

} vm_info;

vm_info* __vmInfo = 0;

//------------------------------------------------------------------------------------
// VM init
//------------------------------------------------------------------------------------

ffi_type* ffi_type_from_ir_type(mem_arena* arena, ir_type* type)
{
	switch(type->kind)
	{
		case TYPE_VOID:
			return(&ffi_type_void);
		//TODO: maybe we should only have stripped types at this point
		case TYPE_NAMED:
			return(ffi_type_from_ir_type(arena, type->symbol->type));

		case TYPE_PRIMITIVE:
		{
			switch(type->primitive)
			{
				case TYPE_B8:
				case TYPE_U8:
					return(&ffi_type_uint8);
				case TYPE_I8:
					return(&ffi_type_sint8);
				case TYPE_U16:
					return(&ffi_type_uint16);
				case TYPE_I16:
					return(&ffi_type_sint16);
				case TYPE_U32:
					return(&ffi_type_uint32);
				case TYPE_I32:
					return(&ffi_type_sint32);
				case TYPE_U64:
					return(&ffi_type_uint64);
				case TYPE_I64:
					return(&ffi_type_sint64);
				case TYPE_F32:
					return(&ffi_type_float);
				case TYPE_F64:
					return(&ffi_type_double);
			}
		} break;

		case TYPE_POINTER:
			return(&ffi_type_pointer);

		case TYPE_ANY:
		{
			ffi_type** elts = mem_arena_alloc_array(arena, ffi_type*, 3);
			elts[0] = &ffi_type_uint64;
			elts[1] = &ffi_type_uint64;
			elts[2] = 0;

			ffi_type* ffiType = mem_arena_alloc_type(arena, ffi_type);
			memset(ffiType, 0, sizeof(ffi_type));

			ffiType->type = FFI_TYPE_STRUCT;
			ffiType->elements = elts;

			return(ffiType);
		}break;

		/*
		case TYPE_ARRAY:
		case TYPE_SLICE:
		{
			ffi_type** elts = mem_arena_alloc_array(arena, ffi_type*, 3);
			elts[0] = &ffi_type_pointer;
			elts[1] = &ffi_type_uint64;
			elts[2] = 0;

			ffi_type* ffiType = mem_arena_alloc_type(arena, ffi_type);

			//NOTE(martin): size and alignment are computed by ffi_prep_cif(), and must be
			//              initialized to zero
			ffiType->size = 0;
			ffiType->alignment = 0;
			ffiType->type = FFI_TYPE_STRUCT;
			ffiType->elements = elts;

			return(ffiType);
		}

		case TYPE_STRUCT:
		{
			ffi_type** elts = mem_arena_alloc_array(arena, ffi_type*, type->eltCount+1);

			u64 fieldIndex = 0;
			for_each_in_list(&type->params, field, ir_type, listElt)
			{
				ffi_type* ffiFieldType = ffi_type_from_ir_type(arena, field->type);
				if(!ffiFieldType)
				{
					return(0);
				}
				elts[fieldIndex] = ffiFieldType;
				fieldIndex++;
			}
			//NOTE(martin): elements buffer must be zero-terminated
			elts[type->structType.fieldCount] = 0;

			ffi_type* ffiType = mem_arena_alloc_type(arena, ffi_type);

			//NOTE(martin): size and alignment are computed by ffi_prep_cif(), and must be
			//              initialized to zero
			ffiType->size = 0;
			ffiType->alignment = 0;
			ffiType->type = FFI_TYPE_STRUCT;
			ffiType->elements = elts;

			return(ffiType);
		}
		//*/

		default:
			ASSERT(0, "not implemented yet...");
			return(0);
	}
}

void vm_init(vm_info* vm, q_program* program, q_ringbuffer* commandBuffer, q_ringbuffer* feedbackBuffer, q_ringbuffer* outputBuffer, vm_options options)
{
	//NOTE(martin): reset vm struct
	memset(vm, 0, sizeof(vm_info));

	vm->options = options;

	mem_base_allocator* baseAllocator = mem_base_allocator_default();

	//NOTE(martin): allocate and copy code section
	vm->codeSize = program->codeSize;
	vm->bytecode = (u8*)mem_base_reserve(baseAllocator, program->codeSize);
	mem_base_commit(baseAllocator, vm->bytecode, program->codeSize);
	memcpy(vm->bytecode, program->contents + program->codeOffset, program->codeSize);

	//NOTE(martin): allocate vm memory
	vm->memory = (u8*)mem_base_reserve(baseAllocator, VM_MEM_SIZE);

	//NOTE(martin): commit rodata and bss memory
	u64 initialCommitSize = AlignUpOnPow2(program->rodataSize + program->bssSize, 4<<10);
	mem_base_commit(baseAllocator, vm->memory, initialCommitSize);

	//NOTE(martin): copy rodata section at the begining of memory
	memcpy(vm->memory, program->contents + program->rodataOffset, program->rodataSize);

	//NOTE(martin): zero-fill bss
	memset(vm->memory + program->rodataSize, 0, program->bssSize);

	//NOTE(martin): init vm struct
	vm->allocOffset = initialCommitSize;
	vm->freeList = 0;
	ListInit(&vm->tasks);
	ListInit(&vm->taskFreeList);

	vm->heapBaseOffset = vm->allocOffset + VM_STACK_ZONE_SIZE ;
	ASSERT(vm->heapBaseOffset < VM_MEM_SIZE);

	u64 heapSize = VM_MEM_SIZE - vm->heapBaseOffset;
	vm->mspaceHandle = create_mspace_with_base(vm->memory + vm->heapBaseOffset, heapSize, 0);
	mspace_track_large_chunks(vm->mspaceHandle, 1);

	vm->commandBuffer = commandBuffer;
	vm->feedbackBuffer = feedbackBuffer;
	vm->outputBuffer = outputBuffer;

	//NOTE: - read ffi dependencies/symbols from program rodata?
	//      - load dlls and symbols
	//      - compute/store cif structure in bss from program ffi type information (how to we know where to put it in bss??)
	//			-> or, have a permanent arena where we put cif struct, set slot in bss to point to that struct
	//          (ie we have a fixed native struct in bss, that points to cif in permanent arena)
	//			-> or, just append these in memory, after stack but before heap...
	//		-> so, what do we need to compute cif structure? -> type signature of function, and location of our qed ffi struct
	//      - put function pointers at appropriate location in qed ffi struct (alongside with cif pointer)

	mem_arena_init(&vm->foreign);
	vm->ffiTable = mem_arena_alloc_array(&vm->foreign, vm_ffi_info*, program->foreignSymbolsCount);
	u64 foreignSymbolIndex = 0;
	for_each_in_list(&program->foreignDeps, dep, qc_foreign_dep, listElt)
	{
		void* lib = 0;
		char* libPath = mp_string_to_cstring(mem_scratch(), dep->path);;

		if(!mp_string_cmp(dep->path, mp_string_lit("_runtime_")))
		{
			lib = dlopen(0, RTLD_NOW | RTLD_LOCAL);
		}
		else
		{
			lib = dlopen(libPath, RTLD_NOW | RTLD_LOCAL);
		}

		if(!lib)
		{
			LOG_ERROR("Can't load library '%s'\n", libPath);
			ASSERT(0);
			//TODO: return error so as not to try running the vm
		}
		else
		{
			for_each_in_list(&dep->functions, sym, qc_foreign_sym, listElt)
			{
				vm_ffi_info* ffiInfo = mem_arena_alloc_type(&vm->foreign, vm_ffi_info);

				//NOTE(martin): load function pointer
				char* name = mp_string_to_cstring(mem_scratch(), sym->name);
				ffiInfo->funPtr = dlsym(lib, name);
				if(!ffiInfo->funPtr)
				{
					LOG_ERROR("Can't load symbol '%s' from library '%s'\n", name, libPath);
					//TODO: return error so as not to try running the vm
				}

				//NOTE(martin): prepare cif structure
				//TODO: count params in checker!
				u64 paramCount = 0;
				for_each_in_list(&sym->signature->params, param, ir_param, listElt)
				{
					paramCount++;
				}

				ffiInfo->argCount = paramCount;
				ffiInfo->argOffsets = mem_arena_alloc_array(&vm->foreign, u64, paramCount);
				ffi_type** atypes = mem_arena_alloc_array(&vm->foreign, ffi_type*, paramCount);

				u64 paramIndex = 0;
				for_each_in_list(&sym->signature->params, param, ir_param, listElt)
				{
					ffi_type* atype = ffi_type_from_ir_type(&vm->foreign, param->type);
					ffiInfo->argOffsets[paramIndex] = param->offset;
					atypes[paramIndex] = atype;
					paramIndex++;
				}
				ffi_type* rtype = ffi_type_from_ir_type(&vm->foreign, sym->signature->retType);

				ffi_prep_cif(&ffiInfo->cif, FFI_DEFAULT_ABI, paramCount, rtype, atypes);

				//NOTE(martin): add entry to the ffi table
				vm->ffiTable[foreignSymbolIndex] = ffiInfo;
				foreignSymbolIndex++;
			}
		}
	}

	//NOTE: load editor widgets data
	u8* widgetSection = (u8*)program->contents + program->widgetsOffset;
	u8* ptr = widgetSection;
	while(ptr < widgetSection + program->widgetsSize)
	{
		bc_widget* widget = (bc_widget*)ptr;

		//NOTE: allocate memory and load widget initial data
		//TODO: we need the kind of widget here. For now assume tempo curve
		u8* data = ptr + sizeof(bc_widget);
		u8* dst = (u8*)mspace_malloc(vm->mspaceHandle, widget->size);
		memcpy(dst, data, widget->size);

		//NOTE: set global
		*(u64*)(&vm->memory[widget->globalOffset]) = (dst - vm->memory);

		//NOTE: register widget in a (modIndex,cellID)->global ptr map
		u64 key = hash_aesdec_u64_x2(widget->moduleIndex, widget->cellID);
		q_hashmap_insert(&vm->widgetMap, key, widget->globalOffset);

		ptr += sizeof(bc_widget) + widget->size;
	}

	__vmInfo = vm;
}

void vm_cleanup(vm_info* vm)
{
	//TODO: cleanup loaded dlls

	q_hashmap_cleanup(&vm->widgetMap);

	#if DEBUG
		if(vm->totalAllocations != 0)
		{
			LOG_ERROR("alloc/free mismatch, totalAllocations = %lli\n", vm->totalAllocations);
		}
	#endif

	destroy_mspace(vm->mspaceHandle);

	mem_base_allocator* baseAllocator = mem_base_allocator_default();
	mem_base_release(baseAllocator, vm->memory, VM_MEM_SIZE);
	mem_base_release(baseAllocator, vm->bytecode, vm->codeSize);
	vm->memory = 0;
	vm->bytecode = 0;

	mem_arena_release(&vm->foreign);
}

u8* vm_stack_alloc(vm_info* vm)
{
	u8* stack = 0;
	if(vm->freeList)
	{
		stack = vm->freeList;
		vm->freeList = *(u8**)(vm->freeList);
	}
	else
	{
		DEBUG_ASSERT(vm->allocOffset + VM_TASK_STACK_SIZE <= vm->heapBaseOffset);
		stack = vm->memory + vm->allocOffset;
		vm->allocOffset += VM_TASK_STACK_SIZE;

		mem_base_allocator* baseAllocator = mem_base_allocator_default();
		mem_base_commit(baseAllocator, stack, VM_TASK_STACK_SIZE);
	}
	return(stack);
}

void vm_stack_recycle(vm_info* vm, u8* stack)
{
	*(u8**)(stack) = vm->freeList;
	vm->freeList = stack;
}

//------------------------------------------------------------------------------------
// task handles and reference counting
//------------------------------------------------------------------------------------
/*
	Task structures are allocated from a fixed-size pool and associated with a task handles,
	which packs the task's index in the pool, and a generation index. When a task is recycled,
	its generation index is incremented to invalidate previous handles.

	Task stacks are allocated from the vm's memory.

	Tasks maintain two reference counts:
		- activeCaptures counts the number of other active tasks that capture the task's stack
		- openHandles counts the number of handles to that task being retained

	When activeCaptures reaches zero, we can recycle the task's stack.
	When openHandles reaches zero, if there are no captures, we can recycle the task structure.

	A task maintains a handle on itself while running, and captures its own stack. When a task ends, we release
	the handle and the capture on itself, so that the task's stack and struture are automatically recycled if needed.

	When launching a task, we capture the stack of its parent. When a task's stack is recycled, we release the capture
	on the task's parent. Thus a stack lives as long as there is any descendant task still running.
	This is sometimes wasteful, since a task could capture an ancestor and not need to capture the intermediate
	ancestors' stacks, but it avoids the need to keep track of which ancestors a tasks effectively captures.
*/

u64 vm_task_handle_from_ptr(vm_info* vm, vm_task* task)
{
	u64 slot = (u32)(task - vm->taskPool);
	u64 generation = task->generation;
	return(generation<<32 | slot);
}

vm_task* vm_task_ptr_from_handle(vm_info* vm, u64 handle)
{
	u32 slot = (u32)(handle & 0xffffffff);
	u32 generation = (u32)(handle>>32);
	ASSERT(slot < vm->nextTaskSlot);
	vm_task* task = &(vm->taskPool[slot]);
	ASSERT(task->generation == generation);
	return(task);
}

vm_task* vm_task_alloc(vm_info* vm)
{
	vm_task* task = ListPopEntry(&vm->taskFreeList, vm_task, listElt);
	if(!task)
	{
		ASSERT(vm->nextTaskSlot < VM_MAX_TASK_COUNT);
		task = &(vm->taskPool[vm->nextTaskSlot]);
		vm->nextTaskSlot++;
		task->generation = 1;
	}

	task->opStack = 0;
	task->ctlStack = 0;
	task->osp = 0;
	task->csp = 0;
	task->bp = 0;
	task->ip = 0;
	task->sr = 0;

	ListPush(&vm->tasks, &task->listElt);
	return(task);
}

void vm_task_release_parent_capture(vm_info* vm, vm_task* task);

void vm_task_recycle(vm_info* vm, vm_task* task)
{
	vm_task_release_parent_capture(vm, task);

	if(task->ownReturnPtr)
	{
		DEBUG_ASSERT(task->retValue >= vm->heapBaseOffset
		             && task->retValue < VM_MEM_SIZE);

		mspace_free(vm->mspaceHandle, vm->memory + task->retValue);
		vm->totalAllocations--;
	}
	ListRemove(&vm->tasks, &task->listElt);

	task->generation++;
	ListPush(&vm->taskFreeList, &task->listElt);
}

void vm_task_capture_stack(vm_info* vm, vm_task* task)
{
	task->activeCaptures++;
}

void vm_task_release_stack(vm_info* vm, vm_task* task)
{
	LOG_DEBUG("releasing stack of task %p\n", task);
	DEBUG_ASSERT(task->activeCaptures > 0);

	task->activeCaptures--;
	if(!task->activeCaptures && task->status == VM_TASK_DONE)
	{
		//NOTE(martin): recycle the task's stack
		vm_stack_recycle(vm, task->opStack);
		task->opStack = 0;
		task->ctlStack = 0;

		//NOTE(martin): if the task has no open handles, we can also dispose of its task structure
		if(!task->openHandles)
		{
			vm_task_recycle(vm, task);
		}
	}
}

void vm_task_handle_retain(vm_info* vm, u64 handle)
{
	vm_task* task = vm_task_ptr_from_handle(vm, handle);
	task->openHandles++;
}

void vm_task_handle_release(vm_info* vm, u64 handle)
{
	vm_task* task = vm_task_ptr_from_handle(vm, handle);
	DEBUG_ASSERT(task->openHandles > 0);

	task->openHandles--;
	if(!task->openHandles && !task->activeCaptures && task->status == VM_TASK_DONE)
	{
		//NOTE(martin): recycle the task's structure. The stack should have been recycled earlier since
		//              activeCapture is zero.
		vm_task_recycle(vm, task);
	}
}

void vm_task_release_parent_capture(vm_info* vm, vm_task* task)
{
	vm_task* parent = task->parent;
	if(parent)
	{
		ListRemove(&parent->children, &task->parentListElt);
		vm_task_release_stack(vm, parent);
	}
}

//------------------------------------------------------------------------------------
// Push/Pop generic functions
//------------------------------------------------------------------------------------

//NOTE(martin): push/pop on operand stack is always 8-byte aligned.
//              operands are zero-extended by default.
//TODO(martin): zeroing the operand is very likely free, but we should verify this!

#define VM_DEF_PUSHOP(type) \
void _cat2_(vm_pushop_, type)(vm_task* task, type value) \
{                                                        \
	DEBUG_ASSERT(task->osp < VM_OPSTACK_SIZE);           \
	*((u64*)(task->opStack + task->osp)) = 0;            \
	*((type*)(task->opStack + task->osp)) = (type)value; \
	task->osp += 8;                                      \
}

#define VM_DEF_POPOP(type) \
type _cat2_(vm_popop_, type)(vm_task* task)        \
{                                                  \
	DEBUG_ASSERT(task->osp > 0);                   \
    task->osp -= 8;                                \
	return(*((type*)(task->opStack + task->osp))); \
}

#define VM_DEF_TOPOP(type) \
type _cat2_(vm_topop_, type)(vm_task* task)            \
{                                                      \
	DEBUG_ASSERT(task->osp < VM_OPSTACK_SIZE);         \
	return(*((type*)(task->opStack + task->osp - 8))); \
}


VM_DEF_PUSHOP(u8)
VM_DEF_PUSHOP(i8)
VM_DEF_PUSHOP(u16)
VM_DEF_PUSHOP(i16)
VM_DEF_PUSHOP(u32)
VM_DEF_PUSHOP(i32)
VM_DEF_PUSHOP(u64)
VM_DEF_PUSHOP(i64)
VM_DEF_PUSHOP(f32)
VM_DEF_PUSHOP(f64)

VM_DEF_POPOP(u8)
VM_DEF_POPOP(i8)
VM_DEF_POPOP(u16)
VM_DEF_POPOP(i16)
VM_DEF_POPOP(u32)
VM_DEF_POPOP(i32)
VM_DEF_POPOP(u64)
VM_DEF_POPOP(i64)
VM_DEF_POPOP(f32)
VM_DEF_POPOP(f64)

VM_DEF_TOPOP(u8)
VM_DEF_TOPOP(i8)
VM_DEF_TOPOP(u16)
VM_DEF_TOPOP(i16)
VM_DEF_TOPOP(u32)
VM_DEF_TOPOP(i32)
VM_DEF_TOPOP(u64)
VM_DEF_TOPOP(i64)
VM_DEF_TOPOP(f32)
VM_DEF_TOPOP(f64)

//NOTE(martin): push/pop on callstack can be unaligned
#define VM_DEF_PUSHLOCAL(type) \
void _cat2_(vm_pushlocal_, type)(vm_task* task, type value)      \
{                                                                \
	DEBUG_ASSERT(task->csp + sizeof(type) <= VM_CALLSTACK_SIZE); \
	*((type*)(task->csp + task->ctlStack)) = (type)value;        \
	task->csp += sizeof(type);                                   \
}

#define VM_DEF_POPLOCAL(type) \
type _cat2_(vm_poplocal_, type)(vm_task* task)      \
{                                                   \
    DEBUG_ASSERT(task->csp >= sizeof(type));        \
    task->csp -= sizeof(type);                      \
	return(*((type*)(task->csp + task->ctlStack))); \
}

VM_DEF_PUSHLOCAL(u8)
VM_DEF_PUSHLOCAL(u16)
VM_DEF_PUSHLOCAL(u32)
VM_DEF_PUSHLOCAL(u64)
VM_DEF_PUSHLOCAL(f32)
VM_DEF_PUSHLOCAL(f64)

VM_DEF_POPLOCAL(u8)
VM_DEF_POPLOCAL(u16)
VM_DEF_POPLOCAL(u32)
VM_DEF_POPLOCAL(u64)
VM_DEF_POPLOCAL(f32)
VM_DEF_POPLOCAL(f64)

#define VM_DEF_GETIMM(type) \
type _cat2_(vm_getimm_,type)(u8* bytecode, vm_task* task) \
{                                                         \
	type value = *(type*)(bytecode + task->ip);           \
	task->ip += sizeof(type);                             \
	return(value);                                        \
}

VM_DEF_GETIMM(u8)
VM_DEF_GETIMM(i8)
VM_DEF_GETIMM(u16)
VM_DEF_GETIMM(i16)
VM_DEF_GETIMM(u32)
VM_DEF_GETIMM(i32)
VM_DEF_GETIMM(u64)
VM_DEF_GETIMM(i64)
VM_DEF_GETIMM(f32)
VM_DEF_GETIMM(f64)


//------------------------------------------------------------------------------------
// VM evaluation
//------------------------------------------------------------------------------------

i64 vm_run_task(void* userPointer);


vm_task* vm_task_create(vm_info* vm, vm_task* parent, u64 startIP)
{
	//NOTE(martin): init new vm with new call stack
	vm_task* task = vm_task_alloc(vm);

	task->vm = vm;

	task->opStack = vm_stack_alloc(vm);
	task->ctlStack = task->opStack + VM_OPSTACK_SIZE;

	task->osp = 0;
	task->csp = 0;
	task->bp = 0;
	task->ip = startIP;
	task->sr = 0;

	ListInit(&task->children);
	task->parent = parent;
	if(parent)
	{
		ListAppend(&parent->children, &task->parentListElt);
		vm_task_capture_stack(vm, parent);
	}

	task->openHandles = 1;
	task->activeCaptures = 1;

	ASSERT(task->parent != task);
	return(task);
}

void vm_send_target(vm_info* vm, u64 targetOffset)
{
	if(!vm->options.noFeedback)
	{
		q_msg msg = {.kind = Q_MSG_JMP_TARGET,
	               	.point = {.offset = targetOffset}};
		q_ringbuffer_push_msg(vm->feedbackBuffer, &msg);
	}
}

u64* vm_collect_call_stack(vm_info* vm, mem_arena* arena, vm_task* task, u64* callStackDepth)
{
	//NOTE: collect call stack
	u64* callStack = (u64*)(arena->ptr + arena->offset);
	*callStackDepth = 0;
	u64 frameBase = task->bp;

	while(frameBase)
	{
		u64 returnAddress = *(u64*)(task->ctlStack + frameBase - 16);

		u64* slot = mem_arena_alloc_type(arena, u64);
		*slot = returnAddress - OP_SIZE[OP_CALL];
		(*callStackDepth)++;

		frameBase = *(u64*)(task->ctlStack + frameBase - 8);
	}
	if(task->capturedFrame)
	{
		u64* slot = mem_arena_alloc_type(arena, u64);
		*slot = task->callSite;
		(*callStackDepth)++;

		u64 parentStackDepth = 0;
		vm_collect_call_stack(vm, arena, task->parent, &parentStackDepth);
		(*callStackDepth) += parentStackDepth;
	}
	return(callStack);
}

void vm_send_resumed(vm_info* vm, vm_task* task, u64 offset)
{
	if(!vm->options.noFeedback)
	{
		//NOTE: collect call stack and send resumed message
		u64 callStackDepth = 0;

		mem_arena* scratch = mem_scratch();
		u64* callStack = vm_collect_call_stack(vm, scratch, task, &callStackDepth);

		q_msg* resumedMsg = (q_msg*)mem_arena_alloc(scratch, sizeof(q_msg) + callStackDepth*sizeof(u64));
		resumedMsg->kind = Q_MSG_RESUMED;
		resumedMsg->point = (qed_exec_point){.offset = offset};
		resumedMsg->payloadSize = callStackDepth*sizeof(u64);
		memcpy(resumedMsg->payload, callStack, resumedMsg->payloadSize);
		q_ringbuffer_push_msg(vm->feedbackBuffer, resumedMsg);

		mem_scratch_clear();
	}
}


void vm_printf(vm_info* vm, const char* fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	mp_string string = mp_string_pushfv(mem_scratch(), fmt, ap);
	va_end(ap);

	if(vm->options.printStdout)
	{
		printf("%.*s", (int)string.len, string.ptr);
	}
	else
	{
		q_ringbuffer_write(vm->outputBuffer, string.len, (u8*)string.ptr);

		printf("%.*s", (int)string.len, string.ptr);
	}

	mem_scratch_clear();
}

void vm_eval_task(vm_info* vm, vm_task* task)
{
	u8* memory = vm->memory;
	u64 codeSize = vm->codeSize;
	u8* bytecode = vm->bytecode;

	bool run = true;
	while(run)
	{
		vm->currentTask = task;

		if(task->ip >= codeSize)
		{
			return;
		}

		u8 opcode = bytecode[task->ip];
		task->ip++;

		switch(opcode)
		{
			//NOTE(martin): constants
			case OP_CONST8:
				vm_pushop_u8(task, vm_getimm_u8(bytecode, task));
				break;

			case OP_CONST16:
				vm_pushop_u16(task, vm_getimm_u16(bytecode, task));
				break;

			case OP_CONST32:
				vm_pushop_u32(task, vm_getimm_u32(bytecode, task));
				break;

			case OP_CONST64:
				vm_pushop_u64(task, vm_getimm_u64(bytecode, task));
				break;

			//NOTE(martin): local variable
			case OP_LOCAL:
			{
				u64 offset = vm_getimm_u64(bytecode, task);
				u64 address = (task->ctlStack - memory) + task->bp + offset;
				vm_pushop_u64(task, address);
			} break;

			//NOTE(martin): argument marshalling
			case OP_ARG:
			{
				u64 offset = vm_getimm_u64(bytecode, task);
				u64 address = (u64)(task->ctlStack - memory) + task->csp + 16 + offset;
				vm_pushop_u64(task, address);
			}	break;

			//NOTE(martin): loads
			case OP_LOAD8:
				vm_pushop_u8(task, (*(u8*)&memory[vm_popop_u64(task)]));
				break;

			case OP_LOAD16:
				vm_pushop_u16(task, (*(u16*)&memory[vm_popop_u64(task)]));
				break;

			case OP_LOAD32:
				vm_pushop_u32(task, (*(u32*)&memory[vm_popop_u64(task)]));
				break;

			case OP_LOAD64:
				vm_pushop_u64(task, (*(u64*)&memory[vm_popop_u64(task)]));
				break;

			//NOTE(martin): stores
			#define OP_STORE_DEF(size) \
			case _cat2_(OP_STORE, size): \
			{ \
				_cat2_(u,size) value = _cat2_(vm_popop_u, size)(task); \
				u64 address = vm_popop_u64(task);                      \
				DEBUG_ASSERT(address + size < VM_MEM_SIZE);            \
				*(_cat2_(u, size)*)&memory[address] = value;           \
			} break;

			OP_STORE_DEF(8)
			OP_STORE_DEF(16)
			OP_STORE_DEF(32)
			OP_STORE_DEF(64)
			#undef OP_STORE_DEF

			case OP_COPY:
			{
				u64 src = vm_popop_u64(task);
				u64 dst = vm_popop_u64(task);
				u64 size = vm_getimm_u64(bytecode, task);

				DEBUG_ASSERT(dst + size < VM_MEM_SIZE);
				DEBUG_ASSERT(src + size < VM_MEM_SIZE);
				memcpy(memory+dst, memory+src, size);
			} break;

			//NOTE(martin): stack
			case OP_DUP:
			{
			    u64 value = vm_popop_u64(task);
			    vm_pushop_u64(task, value);
			    vm_pushop_u64(task, value);
			} break;

			case OP_POP:
			    vm_popop_u64(task);
				break;

			//NOTE(martin): integer arithmetic
			#define OP_BINOP_DEF(name, type, op) \
			case name : \
			{ \
				type rhs = _cat2_(vm_popop_, type)(task); \
				type lhs = _cat2_(vm_popop_, type)(task); \
				type res = lhs op rhs;                    \
				_cat2_(vm_pushop_, type)(task, res);      \
			} break;

			OP_BINOP_DEF(OP_ADD, u64, +)
			OP_BINOP_DEF(OP_SUB, u64, -)
			OP_BINOP_DEF(OP_MUL, u64, *)
			OP_BINOP_DEF(OP_DIV, u64, /)
			OP_BINOP_DEF(OP_SDIV, i64, /)
			OP_BINOP_DEF(OP_MOD, u64, %)

			case OP_NEG:
				vm_pushop_i64(task, -vm_popop_i64(task));
				break;

			//NOTE(martin): floating point single precision arithmetic
			OP_BINOP_DEF(OP_FADDS, f32, +)
			OP_BINOP_DEF(OP_FSUBS, f32, -)
			OP_BINOP_DEF(OP_FMULS, f32, *)
			OP_BINOP_DEF(OP_FDIVS, f32, /)

			//NOTE(martin): floating point double precision arithmetic
			OP_BINOP_DEF(OP_FADDD, f64, +)
			OP_BINOP_DEF(OP_FSUBD, f64, -)
			OP_BINOP_DEF(OP_FMULD, f64, *)
			OP_BINOP_DEF(OP_FDIVD, f64, /)

			//NOTE(martin): bitwise operators
			OP_BINOP_DEF(OP_AND, u64, &)
			OP_BINOP_DEF(OP_OR, u64, |)
			OP_BINOP_DEF(OP_XOR, u64, ^)

			case OP_NOT:
				vm_pushop_u64(task, ~vm_popop_u64(task));
				break;

			OP_BINOP_DEF(OP_SHL, u64, <<)
			OP_BINOP_DEF(OP_SHR, u64, >>)
			OP_BINOP_DEF(OP_SAL, i64, <<)
			OP_BINOP_DEF(OP_SAR, i64, >>)

			//NOTE(martin): comparisons and set
			#define OP_CMP_DEF(name, type) \
			case name : \
			{ \
				type rhs = _cat2_(vm_popop_, type)(task);         \
				type lhs = _cat2_(vm_popop_, type)(task);         \
				task->sr = (lhs==rhs) ? 0 : (lhs < rhs ? -1 : 1); \
			} break;

			OP_CMP_DEF(OP_CMPU, u64)
			OP_CMP_DEF(OP_CMPS, i64)
			OP_CMP_DEF(OP_CMPF, f32)
			OP_CMP_DEF(OP_CMPD, f64)

			#define OP_SET_DEF(name, op) \
			case name: \
				vm_pushop_u64(task, (task->sr op 0)? 1 : 0); \
				break;

			OP_SET_DEF(OP_SETE, ==)
			OP_SET_DEF(OP_SETN, !=)
			OP_SET_DEF(OP_SETL, <)
			OP_SET_DEF(OP_SETG, >)
			OP_SET_DEF(OP_SETLE, <=)
			OP_SET_DEF(OP_SETGE, >=)

			//NOTE(martin): zero/sign-extend
			case OP_ZX8:
				vm_pushop_u64(task, (u64)vm_popop_u8(task));
				break;

			case OP_ZX16:
				vm_pushop_u64(task, (u64)vm_popop_u16(task));
				break;

			case OP_ZX32:
				vm_pushop_u64(task, (u64)vm_popop_u32(task));
				break;

			case OP_SX8:
				vm_pushop_i64(task, (i64)vm_popop_i8(task));
				break;

			case OP_SX16:
				vm_pushop_i64(task, (i64)vm_popop_i16(task));
				break;

			case OP_SX32:
				vm_pushop_i64(task, (i64)vm_popop_i32(task));
				break;

			//NOTE(martin): conversions
			#define OP_CONV_DEF(name, from, to) \
			case name : \
			{ \
				from opd = _cat2_(vm_popop_, from)(task); \
				_cat2_(vm_pushop_, to)(task, (to)opd);     \
			} break;

			OP_CONV_DEF(OP_CVI2F, i64, f32)
			OP_CONV_DEF(OP_CVF2I, f32, i64)
			OP_CONV_DEF(OP_CVI2D, i64, f64)
			OP_CONV_DEF(OP_CVD2I, f64, i64)
			OP_CONV_DEF(OP_CVU2F, u64, f32)
			OP_CONV_DEF(OP_CVF2U, f32, u64)
			OP_CONV_DEF(OP_CVU2D, u64, f64)
			OP_CONV_DEF(OP_CVD2U, f64, u64)
			OP_CONV_DEF(OP_CVF2D, f32, f64)
			OP_CONV_DEF(OP_CVD2F, f64, f32)

			case OP_CVPTRQ2N:
			{
				u64 qptr = vm_popop_u64(task);
				u8* nptr = vm->memory + qptr;
				vm_pushop_u64(task, (uintptr_t)nptr);
			} break;

			case OP_CVPTRN2Q:
			{
				u8* nptr = (u8*)vm_popop_u64(task);
				DEBUG_ASSERT(nptr >= vm->memory && nptr < vm->memory + VM_MEM_SIZE);
				u64 qptr = nptr - vm->memory;
				vm_pushop_u64(task, qptr);
			} break;

			//NOTE(martin): jumps
			case OP_JMP:
				task->ip += vm_getimm_i32(bytecode, task);
				break;

			#define OP_CJMP_DEF(name, op) \
			case name: \
			{ \
				i32 offset = vm_getimm_i32(bytecode, task);  \
				if(task->sr op 0)                  \
				{                                \
					task->ip += offset;            \
				}                                \
			} break;

			OP_CJMP_DEF(OP_JE, ==)
			OP_CJMP_DEF(OP_JN, !=)
			OP_CJMP_DEF(OP_JLT, <)
			OP_CJMP_DEF(OP_JGT, >)
			OP_CJMP_DEF(OP_JLE, <=)
			OP_CJMP_DEF(OP_JGE, >=)

			//NOTE(martin): functions call/ret
			case OP_CALL:
			{
				u64 address = vm_getimm_u64(bytecode, task);
				vm_pushlocal_u64(task, task->ip);
				vm_pushlocal_u64(task, task->bp);
				task->bp = task->csp;
				task->ip = address;
			} break;

			case OP_ENTER:
				task->csp += vm_getimm_u64(bytecode, task);
				break;

			case OP_LEAVE:
			{
				task->retValue = vm_topop_u64(task);

				task->csp = task->bp;
				if(task->csp == 0)
				{
					//NOTE(martin): we are at the root of the call stack, so we end the current task.
					run = false;
					break;
				}
				task->bp = vm_poplocal_u64(task);
				task->ip = vm_poplocal_u64(task);

			} break;

			case OP_FFI_CALL:
			{
				u64 index = vm_getimm_u64(bytecode, task);
				vm_ffi_info* ffiInfo = vm->ffiTable[index];

				void** argPointers = mem_arena_alloc_array(mem_scratch(), void*, ffiInfo->argCount);
				for(int i=0; i<ffiInfo->argCount; i++)
				{
					argPointers[i] = (void*)(task->ctlStack + task->csp + 16 + ffiInfo->argOffsets[i]);
				}

				//TODO: support larger return types
				ffi_arg result = 0;

				ffi_call(&ffiInfo->cif, FFI_FN(ffiInfo->funPtr), &result, argPointers);

				vm_pushop_u64(task, result);

				mem_scratch_clear();
			} break;

			//NOTE(martin): slice
			case OP_SLICE_ARRAY:
			{
				u64 arrayAddress = vm_popop_u64(task);
				u64 end = vm_popop_u64(task);
				u64 start = vm_popop_u64(task);
				u64 dst = vm_popop_u64(task);

				u64 eltSize = vm_getimm_u64(bytecode, task);

				//TODO: check array bounds
				ASSERT(start <= end);

				*(u64*)&memory[dst] = end - start;
				*(u64*)&memory[dst + 8] = arrayAddress + start*eltSize;
			} break;

			case OP_RESLICE:
			{
				u64 sliceAddress = vm_popop_u64(task);
				u64 end = vm_popop_u64(task);
				u64 start = vm_popop_u64(task);
				u64 dst = vm_popop_u64(task);

				u64 eltSize = vm_getimm_u64(bytecode, task);

				u64 sliceLen = *(u64*)&memory[sliceAddress];
				u64 slicePtr = *(u64*)&memory[sliceAddress + 8];

				//TODO: replace this by a vm error
				ASSERT(start <= end);
				ASSERT(start < sliceLen);
				ASSERT(end <= sliceLen);

				*(u64*)&memory[dst] = end - start;
				*(u64*)&memory[dst + 8] = slicePtr + start*eltSize;
			} break;

			case OP_SLICE_INDEX:
			{
				u64 index = vm_popop_u64(task);
				u64 sliceAddress = vm_popop_u64(task);
				u64 eltSize = vm_getimm_u64(bytecode, task);

				u64 sliceLen = *(u64*)&memory[sliceAddress];
				u64 slicePtr = *(u64*)&memory[sliceAddress + 8];

				ASSERT(index < sliceLen);
				vm_pushop_u64(task, slicePtr + index*eltSize);
			} break;

			//NOTE(martin): halt
			case OP_HALT:
				run = false;
				break;

			//NOTE(martin): temporary builtins
			case OP_PUTU:
				vm_printf(vm, "%llu", vm_popop_u64(task));
				break;

			case OP_PUTI:
				vm_printf(vm, "%lli", vm_popop_i64(task));
				break;

			case OP_PUTF:
				vm_printf(vm, "%f", vm_popop_f32(task));
				break;

			case OP_PUTD:
				vm_printf(vm, "%f", vm_popop_f64(task));
				break;

			case OP_PUTS:
			{
				u64 sliceAddress = vm_popop_u64(task);
				u64 sliceLen = *(u64*)&memory[sliceAddress];
				u64 slicePtr = *(u64*)&memory[sliceAddress + 8];

				char* bytes = (char*)(memory + slicePtr);

				vm_printf(vm, "%.*s", (int)sliceLen, bytes);
			} break;

			case OP_PUTNL:
				vm_printf(vm, "\n");
				break;

			case OP_ALLOC:
			{
				u64 size = vm_popop_u64(task);
				u8* ptr = (u8*)mspace_malloc(vm->mspaceHandle, size);
				if(!ptr)
				{
					vm_pushop_u64(task, 0);
				}
				else
				{
					DEBUG_ASSERT((ptr > memory + vm->heapBaseOffset) && (ptr - memory < VM_MEM_SIZE));
					u64 offset = ptr - memory;
					vm_pushop_u64(task, offset);
					vm->totalAllocations++;
				}
			} break;

			case OP_FREE:
			{
				u64 offset = vm_popop_u64(task);
				DEBUG_ASSERT(offset < VM_MEM_SIZE && offset >= vm->heapBaseOffset);
				u8* ptr = memory + offset;
				mspace_free(vm->mspaceHandle, ptr);
				vm->totalAllocations--;
			} break;

			case OP_SYNC_BEAT:
			{
				f64 tempo = vm_popop_f64(task);
				f64 beat = vm_popop_f64(task);
				u64 handle = vm_popop_u64(task);

				qvm_sync_beat(handle, beat, tempo);
			} break;

			case OP_TASK:
			{
				u64 address = vm_getimm_u64(bytecode, task);
				u32 argSize = vm_getimm_u32(bytecode, task);
				u32 flags = vm_getimm_u32(bytecode, task);

				//NOTE(martin): create a child task
				vm_task* child = vm_task_create(vm, task, address);

				//NOTE: copy arguments from parent's frame to new call stack
				memcpy(child->ctlStack, task->ctlStack + task->csp + 16, argSize);

				if(flags & OP_TASK_FLAG_OWN_RETURN_PTR)
				{
					child->retValue = *(u64*)child->ctlStack;
					child->ownReturnPtr = true;
				}
				if(flags & OP_TASK_FLAG_CAPTURED_FRAME)
				{
					child->capturedFrame = true;
					child->callSite = task->ip -17;
				}

				//NOTE(martin): launch a scheduler task to run the vm task
				child->schedHandle = sched_task_create(vm_run_task, child);

				//NOTE(martin): create a handle for the task and put it on the operand stack
				u64 taskHandle = vm_task_handle_from_ptr(vm, child);
				child->openHandles++;
				vm_pushop_u64(task, taskHandle);

			} break;

			case OP_TEMPO:
			{
				u64 descHandle = vm_popop_u64(task);
				u64 handle = vm_popop_u64(task);

				u64 descAddress = *(u64*)&vm->memory[descHandle];

				bc_tempo_curve_desc* bcDesc = (bc_tempo_curve_desc*)(vm->memory + descAddress);
				sched_curve_descriptor_elt* elements = (sched_curve_descriptor_elt*)(vm->memory + descAddress + sizeof(bc_tempo_curve_desc));

				sched_curve_descriptor desc;
				desc.axes = (sched_curve_axes)bcDesc->axes;
				desc.startPoint = 0;
				desc.transformedStartPoint = 0;
				desc.eltCount = bcDesc->eltCount;
				desc.elements = elements;

				vm_task* taskOpd = vm_task_ptr_from_handle(vm, handle);
				sched_task_timescale_set_tempo_curve(taskOpd->schedHandle, &desc);
			} break;

			case OP_PAUSE:
			{
				task->status = VM_TASK_PAUSED;
				sched_wait(vm_popop_f64(task));
				task->status = VM_TASK_RUNNING;
				vm_send_resumed(vm, task, task->ip-1);
			} break;

			case OP_WAIT:
			{
				u64 handle = vm_popop_u64(task);
				u8 recursive = vm_getimm_u8(bytecode, task);

				vm_task* waitedTask = vm_task_ptr_from_handle(vm, handle);
				if(waitedTask->status != VM_TASK_DONE)
				{
					task->status = VM_TASK_WAITING;
					sched_task_signal signal = recursive ? SCHED_SIG_COMPLETED : SCHED_SIG_RETIRED;
					sched_wait_for_task(waitedTask->schedHandle, signal, -1);
					task->status = VM_TASK_RUNNING;
				}
				vm_send_resumed(vm, task, task->ip-2);

				vm_pushop_u64(task, waitedTask->retValue);
				vm_task_handle_release(vm, handle);
			} break;

			case OP_TIMEOUT:
			{
				f64 timeout = vm_popop_f64(task);
				u64 handle = vm_popop_u64(task);
				u8 recursive = vm_getimm_u8(bytecode, task);

				vm_task* taskOpd = vm_task_ptr_from_handle(vm, handle);
				taskOpd->status = VM_TASK_PAUSED;
				sched_task_signal signal = recursive ? SCHED_SIG_COMPLETED : SCHED_SIG_RETIRED;
				sched_wakeup_code wakeup = sched_wait_for_task(taskOpd->schedHandle, signal, timeout);
				taskOpd->status = VM_TASK_RUNNING;

				if(wakeup == SCHED_WAKEUP_SIGNALED)
				{
					vm_pushop_u64(task, 1);
				}
				else
				{
					vm_pushop_u64(task, 0);
				}
				vm_send_resumed(vm, task, task->ip-2);
			} break;

			case OP_STANDBY:
			{
				task->status = VM_TASK_STANDBY;
				sched_suspend();
				task->status = VM_TASK_RUNNING;
				vm_send_resumed(vm, task, task->ip-1);
			} break;

			case OP_BACKGROUND:
				task->backgroundIP = task->ip-1;
				task->status = VM_TASK_BACKGROUND;
				sched_background();
				break;

			case OP_FOREGROUND:
				sched_foreground();
				task->status = VM_TASK_RUNNING;
				break;

			case OP_FDUP:
			{
				u64 handle = vm_topop_u64(task);
				vm_task_handle_retain(vm, handle);
			} break;

			case OP_FDROP:
			{
				u64 handle = vm_popop_u64(task);
				vm_task_handle_release(vm, handle);
			} break;

			case OP_CAPTURE:
			{
				u64 captureLevel = vm_getimm_u64(bytecode, task);
				u64 frameOffset = vm_getimm_u64(bytecode, task);

				vm_task* capturedTask = task;
				for(int i=0; i<captureLevel; i++)
				{
					capturedTask = capturedTask->parent;
				}

				u64 address = (capturedTask->ctlStack - memory) + frameOffset;
				vm_pushop_u64(task, address);

			} break;

			case OP_TRACE:
				vm_send_target(vm, task->ip - 1);
				break;

			default:
				ASSERT(0, "invalid opcode");
		}
	}
}

i64 vm_progress_report_task(void* userPointer)
{
	vm_info* vm = (vm_info*)userPointer;

	while(1)
	{
		//NOTE(martin): report progresses
		for_each_in_list(&vm->tasks, task, vm_task, listElt)
		{
			sched_task schedTaskHandle = task->schedHandle;
			if(schedTaskHandle.h != 0)
			{
				//NOTE: collect call stack
				u64 callStackDepth = 0;
				u64* callStack = vm_collect_call_stack(vm, mem_scratch(), task, &callStackDepth);

				//NOTE(martin): send progress report and call stack
				q_msg* msg = (q_msg*)mem_arena_alloc(mem_scratch(), sizeof(q_msg) + callStackDepth*sizeof(u64));

				if(task->status == VM_TASK_PAUSED)
				{
					msg->kind = Q_MSG_PROGRESS;
					msg->progress.point.offset = task->ip - 1; //WARN: is it always the case that a task's ip is 1 after the wait instruction while waiting?

					//TODO: see how we can atomically send progress report and callstack

					//TODO: maybe report timings with only one function call, as a return struct
					msg->progress.initialDelay = sched_task_initial_delay(schedTaskHandle); //TODO rename this function...
					msg->progress.localRemaining = sched_task_local_delay(schedTaskHandle);
				}
				else if(task->status == VM_TASK_WAITING)
				{
					msg->kind = Q_MSG_WAITING;
					msg->point.offset = task->ip - 2;
				}
				else if(task->status == VM_TASK_BACKGROUND)
				{
					msg->kind = Q_MSG_WAITING;
					msg->point.offset = task->backgroundIP;
				}
				else if(task->status == VM_TASK_STANDBY)
				{
					msg->kind = Q_MSG_STANDBY;
					msg->point.offset = task->ip - 1;
				}

				msg->payloadSize = callStackDepth*sizeof(u64);
				memcpy(msg->payload, callStack, msg->payloadSize);

				if(!vm->options.noFeedback)
				{
					q_ringbuffer_push_msg(vm->feedbackBuffer, msg);
				}

				mem_scratch_clear();
			}
		}

		//NOTE(martin): read and dispatch command messages
		q_msg* msg = q_ringbuffer_poll_msg(vm->commandBuffer, mem_scratch());
		while(msg->kind != Q_MSG_NONE)
		{
			switch(msg->kind)
			{
				case Q_MSG_TRIGGER:
				{
					u64 offset = msg->point.offset;
					for_each_in_list(&vm->tasks, task, vm_task, listElt)
					{
						if(  task->status == VM_TASK_STANDBY
						  && (task->ip-1) == offset)
						{
							sched_task schedTaskHandle = task->schedHandle;
							if(schedTaskHandle.h != 0)
							{
								sched_task_resume(schedTaskHandle);
							}
						}
					}
				} break;

				case Q_MSG_UPDATE_TEMPO_CURVE:
				{
					bc_widget* widget = (bc_widget*)msg->payload;
					DEBUG_ASSERT(msg->payloadSize == widget->size + sizeof(bc_widget));

					//NOTE: find global pointer
					u64 key = hash_aesdec_u64_x2(widget->moduleIndex, widget->cellID);
					u64 globalAddr = 0;
					if(q_hashmap_find(&vm->widgetMap, key, &globalAddr))
					{
						//NOTE: free existing curve data
						u64 dataAddr = *(u64*)&vm->memory[globalAddr];
						mspace_free(vm->mspaceHandle, vm->memory + dataAddr);

						//NOTE: load new curve data and update global pointer
						u8* data = (u8*)msg->payload + sizeof(bc_widget);
						u8* dst = (u8*)mspace_malloc(vm->mspaceHandle, widget->size);
						memcpy(dst, data, widget->size);

						*(u64*)(vm->memory + globalAddr) = dst - vm->memory;

						//TODO: lookup running task to change their tempo curve
					}

				} break;

				default:
					break;
			}
			mem_scratch_clear();
			msg = q_ringbuffer_poll_msg(vm->commandBuffer, mem_scratch());
		}

		mem_scratch_clear();
		sched_wait(1./60.);
	}
	return(0);
}

void vm_eval(vm_info* vm, u64 startIP)
{
	f32 start = ClockGetTime(SYS_CLOCK_MONOTONIC);
	vm_task* task = vm_task_create(vm, 0, startIP);
	task->opStack = vm_stack_alloc(vm);
	task->ctlStack = task->opStack + VM_OPSTACK_SIZE;
	task->ip = startIP;

	task->schedHandle = sched_task_self();

	sched_task_create(vm_progress_report_task, vm);

	vm_eval_task(vm, task);

	//TODO: check if task needs recycling
	//vm_task_cleanup(vm, task);

	f32 end = ClockGetTime(SYS_CLOCK_MONOTONIC);
	LOG_DEBUG("VM evaluation ended in %fs\n", end-start);
}

i64 vm_run_task(void* userPointer)
{
	vm_task* task = (vm_task*)userPointer;
	vm_info* vm = task->vm;

	//NOTE(martin): run the task
	vm_eval_task(vm, task);

	//NOTE(martin): release the scheduler task's handle
	sched_task_release(task->schedHandle);

	//NOTE(martin): task ended
	task->status = VM_TASK_DONE;

	//NOTE(martin): release the task's self handle and the task's self capture. This will
	//              recycle the task structure and/or stack as needed.
	DEBUG_ASSERT(task->openHandles > 0);
	task->openHandles--;
	vm_task_release_stack(vm, task);

	return(0);
}


//----------------------------------------------------------------------------
// foreign vm API
//----------------------------------------------------------------------------

extern "C" u8* qvm_get_memory()
{
	vm_info* vm = __vmInfo;
	return(vm->memory);
}

extern "C" u8* qvm_alloc(u64 size)
{
	vm_info* vm = __vmInfo;
	u8* ptr = (u8*)mspace_malloc(vm->mspaceHandle, size);
	if(ptr)
	{
		DEBUG_ASSERT((ptr > vm->memory + vm->heapBaseOffset) && (ptr - vm->memory < VM_MEM_SIZE));
		vm->totalAllocations++;
	}
	printf("returning native pointer %p (= Q pointer %llu)\n", ptr, (u64)(ptr - vm->memory));
	return(ptr);
}

extern "C" void qvm_free(u8* ptr)
{
	vm_info* vm = __vmInfo;
	if(ptr)
	{
		printf("got native pointer %p (= Q pointer %llu)\n", ptr, (u64)(ptr - vm->memory));

		DEBUG_ASSERT((ptr > vm->memory + vm->heapBaseOffset) && (ptr - vm->memory < VM_MEM_SIZE));
		mspace_free(vm->mspaceHandle, ptr);
		vm->totalAllocations--;
	}
}

extern "C" void qvm_puts(mp_string string)
{
	vm_info* vm = __vmInfo;
	q_ringbuffer_write(vm->outputBuffer, string.len, (u8*)string.ptr);
}


//WARN: completely arbitrary values for now
const f32 SYNC_DEFAULT_CATCHUP_RATIO = 1./3.,
          SYNC_OFFSET_EPSILON = 0.001;

extern "C" void qvm_sync_beat(u64 handle, f64 srcPos, f64 srcTempo)
{
	vm_info* vm = __vmInfo;
	//TODO: use a version that doesn't assert when handle is invalid?
	vm_task* task = vm_task_ptr_from_handle(vm, handle);

	f64 logicalPos = sched_task_logical_pos(task->schedHandle);
	f64 logicalTime = sched_task_transformed_logical_pos(task->schedHandle);
	f64 selfTempo = sched_task_tempo(task->schedHandle);

	//NOTE(martin): offset is positive if task is ahead of sync source
	f64 offset = logicalPos - srcPos;
	f64 minCatchUpTime = 0;

	if(fabs(offset) < SYNC_OFFSET_EPSILON)
	{
		return;
	}

	f64 offsetSign = (offset > 0) ? 1 : -1;
	f64 catchUpTime = fabs(offset) * (1./SYNC_DEFAULT_CATCHUP_RATIO + offsetSign)/srcTempo;
	f64 target = logicalPos + catchUpTime * srcTempo - offset;


	float controlNorm = 0.5*minimum(catchUpTime, fabs(target - logicalPos));
	float dx1 = controlNorm/sqrt(1 + selfTempo*selfTempo);
	float dx2 = controlNorm/sqrt(1 + srcTempo*srcTempo);

	float c1x = dx1/catchUpTime; //WARN: control point's x are normalized to (0,1)
	float c1y = logicalPos + selfTempo*dx1;
	float c2x = 1 - dx2/catchUpTime;
	float c2y = target - srcTempo*dx2;


	////////////////////////////////////////////////////////////
	//TODO: Investigate why sometimes first correction is ok, but then tempo
	//      stops...
	////////////////////////////////////////////////////////////

	sched_curve_descriptor_elt elements[2] = {
		{.type = SCHED_CURVE_BEZIER,
		 .startValue = logicalPos,
		 .endValue = target,
		 .length = catchUpTime,
		 .p1x = c1x,
		 .p1y = c1y,
		 .p2x = c2x,
		 .p2y = c2y },
		{.type = SCHED_CURVE_LINEAR,
		 .startValue = target,
		 .endValue = target + 1*srcTempo,
		 .length = 1}};

	sched_curve_descriptor desc = {
		.axes = SCHED_CURVE_TIME_POS,
		.eltCount = 2,
		.elements = elements,
		.startPoint = logicalTime,
		.transformedStartPoint = logicalPos};

	LOG_DEBUG("[Pos %f, src %f] Set catch up curve: pos(%f -> %f), time(%f -> %f), tempo %f, stopTempo = %f\n",
	       logicalPos,
	       srcPos,
	       elements[0].startValue,
	       elements[0].endValue,
	       desc.startPoint,
	       desc.startPoint + elements[0].length,
	       (elements[0].endValue - elements[0].startValue)/elements[0].length,
	       (elements[1].endValue - elements[1].startValue)/elements[1].length);

	sched_task_timescale_set_tempo_curve(task->schedHandle, &desc);

	ASSERT(sched_task_logical_pos(task->schedHandle) == logicalPos);

	//NOTE: the next time we yield, the task will be rescheduled appropriately, so
	//      there is no need to reschedule anything here,
	//		BUT, we might want to recompute task's srcLoc from localLoc?

}

extern "C" f64 qvm_sync_get_tempo(u64 handle)
{
	vm_info* vm = __vmInfo;
	vm_task* task = vm_task_ptr_from_handle(vm, handle);

	return(sched_task_tempo(task->schedHandle));
}

extern "C" f64 qvm_sync_get_beat(u64 handle)
{
	vm_info* vm = __vmInfo;
	vm_task* task = vm_task_ptr_from_handle(vm, handle);

	return(sched_task_logical_pos(task->schedHandle));
}

extern "C" f64 qvm_sync_get_clock()
{
	return(sched_get_clock());

}

#undef LOG_SUBSYSTEM
