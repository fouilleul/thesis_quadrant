/************************************************************//**
*
*	@file: ringbuffer.cpp
*	@author: Martin Fouilleul
*	@date: 03/01/2022
*	@revision:
*
*****************************************************************/
#include"ringbuffer.h"

#define LOG_SUBSYSTEM "ringbuffer"

void q_ringbuffer_init(q_ringbuffer* ring, u8 capExp)
{
	u32 cap = 1<<capExp;
	ring->mask = cap - 1;
	ring->readIndex = 0;
	ring->writeIndex = 0;
	ring->buffer = (u8*)malloc(cap);
}

void q_ringbuffer_cleanup(q_ringbuffer* ring)
{
	free(ring->buffer);
}

u32 q_ringbuffer_read_available(q_ringbuffer* ring)
{
	return((ring->writeIndex - ring->readIndex) & ring->mask);
}

u32 q_ringbuffer_write_available(q_ringbuffer* ring)
{
	//NOTE(martin): we keep one sentinel byte between write index and read index,
	//              when the buffer is full, to avoid overrunning read index.
	//              Hence, available write space is size - 1 - available read space.
	return(ring->mask - q_ringbuffer_read_available(ring));
}

u32 q_ringbuffer_write(q_ringbuffer* ring, u32 size, u8* data)
{
	u32 read = ring->readIndex;
	u32 write = ring->writeIndex;

	u32 writeAvailable = q_ringbuffer_write_available(ring);
	if(size > writeAvailable)
	{
		DEBUG_ASSERT("not enough space available");
		size = writeAvailable;
	}

	if(read <= write)
	{
		u32 copyCount = minimum(size, ring->mask + 1 - write);
		memcpy(ring->buffer + write, data, copyCount);

		data += copyCount;
		copyCount = size - copyCount;
		memcpy(ring->buffer, data, copyCount);
	}
	else
	{
		memcpy(ring->buffer + write, data, size);
	}
	ring->writeIndex = (write + size) & ring->mask;
	return(size);
}

u32 q_ringbuffer_read(q_ringbuffer* ring, u32 size, u8* data)
{
	u32 read = ring->readIndex;
	u32 write = ring->writeIndex;

	u32 readAvailable = q_ringbuffer_read_available(ring);
	if(size > readAvailable)
	{
		size = readAvailable;
	}

	if(read <= write)
	{
		memcpy(data, ring->buffer + read, size);
	}
	else
	{
		u32 copyCount = minimum(size, ring->mask + 1 - read);
		memcpy(data, ring->buffer + read, copyCount);

		data += copyCount;
		copyCount = size - copyCount;
		memcpy(data, ring->buffer, copyCount);
	}
	ring->readIndex = (read + size) & ring->mask;
	return(size);
}

void q_ringbuffer_push_msg(q_ringbuffer* ring, q_msg* msg)
{
	u32 sizeToWrite = sizeof(q_msg) + msg->payloadSize;

	if(q_ringbuffer_write_available(ring) >= sizeToWrite)
	{
		int size = q_ringbuffer_write(ring, sizeToWrite, (u8*)msg);
		ASSERT(size == sizeToWrite);
	}
	else
	{
		LOG_ERROR("ring buffer full, dropped message\n");
	}
}

q_msg* q_ringbuffer_poll_msg(q_ringbuffer* ring, mem_arena* arena)
{
	if(q_ringbuffer_read_available(ring) >= sizeof(q_msg))
	{
		q_msg hdr = {};

		int size = q_ringbuffer_read(ring, sizeof(q_msg), (u8*)&hdr);
		ASSERT(size == sizeof(q_msg));

		u32 totalSize = sizeof(q_msg) + hdr.payloadSize;
		q_msg* msg = (q_msg*)mem_arena_alloc_array(arena, char, totalSize);
		memcpy(msg, &hdr, sizeof(q_msg));

		if(q_ringbuffer_read_available(ring) >= msg->payloadSize)
		{
			q_ringbuffer_read(ring, msg->payloadSize, (u8*)msg+sizeof(q_msg));
		}
		else
		{
			LOG_ERROR("message payload size mismatch\n");
		}
		return(msg);
	}
	else
	{
		q_msg* msg = mem_arena_alloc_type(arena, q_msg);
		memset(msg, 0, sizeof(q_msg));
		msg->kind = Q_MSG_NONE;
		return(msg);
	}
}


#undef LOG_SUBSYSTEM
