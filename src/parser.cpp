/************************************************************//**
*
*	@file: parser.cpp
*	@author: Martin Fouilleul
*	@date: 13/05/2022
*	@revision:
*
*****************************************************************/
#include"parser.h"
#include"workspace.h"
#include"ast.h"

typedef struct parse_context
{
	q_cell* parent;
	q_cell* currentChild;

	q_module* module;

} parse_context;

/////////////////////////////////////////////////////////////////
// Draft
/////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------
//NOTE: parse context / token management
//---------------------------------------------------------------------------------
void parse_context_init(parse_context* context, q_module* module, q_cell* cell)
{
	context->module = module;
	context->parent = cell;
	context->currentChild = cell_first_child(cell);
}

void parser_shift(parse_context* context)
{
	if(context->currentChild)
	{
		context->currentChild = cell_next_sibling(context->currentChild);
	}
}

q_cell* parser_look(parse_context* context)
{
	while( context->currentChild
	     &&(context->currentChild->kind == CELL_COMMENT))
	{
		parser_shift(context);
	}
	if(context->currentChild && context->currentChild->kind == CELL_HOLE)
	{
		cell_text_replace(context->currentChild, (mp_string){0, 0});
	}
	return(context->currentChild);
}

q_cell* parser_parent(parse_context* context)
{
	return(context->parent);
}

void parser_enter_cell(parse_context* context)
{
	q_cell* cell = context->currentChild;
	context->parent = cell;
	context->currentChild = cell_first_child(cell);
}

void parser_leave_cell(parse_context* context)
{
	context->currentChild = context->parent;
	context->parent = context->parent->parent;
}

bool parser_is_at_end(parse_context* context)
{
	return(context->currentChild == 0);
}

//---------------------------------------------------------------------------------
//NOTE: error generation
//---------------------------------------------------------------------------------

void parser_emit_error(parse_context* context, q_error_kind kind, q_ast* ast, mp_string msg, mp_string cellHint)
{
	q_module* module = context->module;
	q_error* error = ListPopEntry(&module->parsingErrorFreeList, q_error, moduleElt);
	if(!error)
	{
		error = mem_arena_alloc_type(&module->astArena, q_error);
	}
	memset(error, 0, sizeof(q_error));

	//WARN: parser error strings are allocated on the heap
	error->msg.ptr = malloc_array(char, msg.len);
	error->msg.len = msg.len;
	strncpy(error->msg.ptr, msg.ptr, msg.len);

	if(cellHint.len)
	{
		error->cellHint.ptr = malloc_array(char, cellHint.len);
		error->cellHint.len = cellHint.len;
		strncpy(error->cellHint.ptr, cellHint.ptr, cellHint.len);
	}

	error->kind = kind;
	error->ast = ast;
	error->astGen = ast->generation;
	ListAppend(&context->module->parsingErrors, &error->moduleElt);
}

//TODO: variants that infers range / parentNode?

//---------------------------------------------------------------------------------
//NOTE: ast building
//---------------------------------------------------------------------------------
void ast_insert(q_ast* after, q_ast* ast)
{
	ast->parent = after->parent;
	ListInsert(&after->parent->children, &after->listElt, &ast->listElt);
	after->parent->childCount++;
}

void ast_push(q_ast* parent, q_ast* ast)
{
	ast->parent = parent;
	ListAppend(&parent->children, &ast->listElt);
	parent->childCount++;
}

void ast_detach(q_ast* ast)
{
	if(ast->parent)
	{
		ListRemove(&ast->parent->children, &ast->listElt);
		ast->parent->childCount--;
		ast->parent = 0;
	}
}

q_ast* ast_alloc(q_module* module, ast_kind kind)
{
	q_ast* ast = ListPopEntry(&module->astFreeList, q_ast, listElt);

	if(!ast)
	{
		ast = mem_arena_alloc_type(&module->astArena, q_ast);
		ast->generation = 0;
	}
	u64 generation = ast->generation;
	memset(ast, 0, sizeof(q_ast));

	ast->generation = generation;
	ast->kind = kind;
	return(ast);
}

void ast_recycle(parse_context* context, q_ast* ast)
{
	for_each_in_list_safe(&ast->children, child, q_ast, listElt)
	{
		ast_recycle(context, child);
	}
	ast_detach(ast);
	ast->generation++;
	ListPush(&context->module->astFreeList, &ast->listElt);
}

void parser_clear_holes(q_cell* cell)
{
	for_each_in_list(&cell->children, child, q_cell, listElt)
	{
		if(child->kind == CELL_HOLE)
		{
			cell_text_replace(child, (mp_string){0, 0});
		}
		else
		{
			parser_clear_holes(child);
		}
	}
}

q_ast* ast_from_cell(parse_context* context, ast_kind kind, q_cell* cell)
{
	q_range range = {.start = {.parent = cell->parent, .leftFrom = cell, 0},
	                 .end = {.parent = cell->parent, .leftFrom = cell_next_sibling(cell), 0}};

	q_ast* ast = ast_alloc(context->module, kind);
	ast->range = range;

	q_ast* oldAst = cell_ast_lookup(cell);
	cell_ast_cache(cell, ast);

	return(ast);
}

q_ast* ast_with_range(parse_context* context, ast_kind kind, q_range range)
{
	q_ast* ast = ast_alloc(context->module, kind);
	ast->range = range;
	return(ast);
}

q_ast* parse_error(parse_context* context, const char* expected)
{
	q_ast* ast = 0;
	q_error_kind kind = Q_ERROR_INVALID;
	q_cell* cell = parser_look(context);

	mp_string msg = {0};
	mp_string cellHint = {0};
	if(!cell)
	{
		kind = Q_ERROR_MISSING;
		q_point point = {parser_parent(context), 0, 0};
		q_range range = {point, point};
		ast = ast_with_range(context, AST_ERROR_MISSING, range);
		msg = mp_string_pushf(mem_scratch(), "missing %s", expected);
		cellHint = mp_string_from_cstring((char*)expected);
	}
	else if(cell->kind == CELL_HOLE)
	{
		kind = Q_ERROR_MISSING;
		cell_text_replace(cell, mp_string_from_cstring((char*)expected));

		ast = ast_from_cell(context, AST_ERROR_MISSING, cell);
		msg = mp_string_pushf(mem_scratch(), "missing %s", expected);
	}
	else
	{
		kind = Q_ERROR_INVALID;
		ast = ast_from_cell(context, AST_ERROR_INVALID, cell);
		msg = mp_string_pushf(mem_scratch(), "invalid token, expected %s", expected);
	}
	parser_emit_error(context, kind, ast, msg, cellHint);
	parser_shift(context);
	return(ast);
}

//---------------------------------------------------------------------------------
//NOTE: parsing
//---------------------------------------------------------------------------------

q_ast* parse_expression(parse_context* context);
q_ast* parse_expression_or_type(parse_context* context);
q_ast* parse_initializer(parse_context* context);
q_ast* parse_type_specification(parse_context* context);
q_ast* parse_statement(parse_context* context);
q_ast* parse_body(parse_context* context);

void parse_expect_end(parse_context* context, q_ast* parent)
{
	for(q_cell* cell = parser_look(context);
	    cell != 0;
	    parser_shift(context), cell = parser_look(context))
	{
		//TODO: cell_range(cell) ?
		q_range range = {.start = {.parent = cell->parent, .leftFrom = cell, 0},
	                     .end = {.parent = cell->parent, .leftFrom = cell_next_sibling(cell), 0}};

	    q_ast* ast = ast_from_cell(context, AST_ERROR_INVALID, cell);
	    ast_push(parent, ast);
	    parser_emit_error(context, Q_ERROR_INVALID, ast, mp_string_lit("invalid token, expected end of cell"), (mp_string){0});
		parser_clear_holes(cell);
	}
}

q_ast* parse_nary_expression(parse_context* context, ast_kind kind, u32 arity)
{
	q_ast* ast = ast_from_cell(context, kind, parser_parent(context));

	parser_shift(context); //WARN: this must happen _after_ ast_from_cell, since ast_from_cell might remove the following cell

	for(u32 i=0; i<arity; i++)
	{
		q_ast* opd = parse_expression(context);
		if(!opd)
		{
			opd = parse_error(context, "expression");
		}
		opd->parseRule = PARSE_RULE_EXPRESSION;
		ast_push(ast, opd);
	}

	parse_expect_end(context, ast);
	return(ast);
}

#define parse_unary_expression(context, kind) parse_nary_expression(context, kind, 1)
#define parse_binary_expression(context, kind) parse_nary_expression(context, kind, 2)
#define parse_ternary_expression(context, kind) parse_nary_expression(context, kind, 3)


q_ast* parse_call(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_CALL, parser_parent(context));

	q_cell* cell = parser_look(context);
	q_ast* nameNode = ast_from_cell(context, AST_ID, cell);
	parser_shift(context);

	q_cell* parent = parser_parent(context);
	q_cell* firstArgCell = parser_is_at_end(context) ? 0 : parser_look(context);
	q_range range = {.start = {parent, firstArgCell, 0},
	                 .end = {parent, 0, 0}};

	q_ast* args = ast_with_range(context, AST_ARGS, range);

	for(q_cell* cell = parser_look(context);
	    cell_token(cell) != TOKEN_END;
	    cell = parser_look(context))
	{
		q_ast* child = parse_initializer(context);

		//NOTE: here we're special casing errors to let them be handled by the checker,
		//      were we can look up expected arguments.
		if(!child)
		{
			child = ast_from_cell(context, AST_ERROR_INVALID, cell);
			parser_shift(context);
		}
		child->parseRule = PARSE_RULE_INITIALIZER;
		ast_push(args, child);
	}

	ast_push(ast, nameNode);
	ast_push(ast, args);
	return(ast);
}

q_ast* parse_return(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_RETURN, parser_parent(context));
	parser_shift(context);

	q_ast* opd = parse_initializer(context);
	if(!opd)
	{
		opd = parse_error(context, "return value");
	}
	opd->parseRule = PARSE_RULE_INITIALIZER;
	ast_push(ast, opd);

	parse_expect_end(context, ast);
	return(ast);
}

q_ast* parse_if(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_IF, parser_parent(context));
	parser_shift(context);

	q_ast* cond = parse_expression(context);
	if(!cond)
	{
		cond = parse_error(context, "condition expression");
	}
	cond->parseRule = PARSE_RULE_EXPRESSION;
	ast_push(ast, cond);

	q_ast* ifBranch = parse_expression(context);
	if(!ifBranch)
	{
		ifBranch = parse_error(context, "if branch");
	}
	ifBranch->parseRule = PARSE_RULE_EXPRESSION;
	ast_push(ast, ifBranch);

	if(!parser_is_at_end(context))
	{
		q_ast* elseBranch = parse_expression(context);
		if(!elseBranch)
		{
			elseBranch = parse_error(context, "else branch");
		}
		elseBranch->parseRule = PARSE_RULE_EXPRESSION;
		ast_push(ast, elseBranch);
	}

	parse_expect_end(context, ast);

	return(ast);
}

q_ast* parse_for(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_FOR, parser_parent(context));
	parser_shift(context);

	q_ast* init = parse_statement(context);
	if(!init)
	{
		init = parse_error(context, "loop initialization");
	}
	init->parseRule = PARSE_RULE_STATEMENT;
	ast_push(ast, init);

	q_ast* cond = parse_expression(context);
	if(!cond)
	{
		cond = parse_error(context, "loop condition");
	}
	cond->parseRule = PARSE_RULE_EXPRESSION;
	ast_push(ast, cond);

	q_ast* post = parse_expression(context);
	if(!post)
	{
		post = parse_error(context, "loop post expression");
	}
	post->parseRule = PARSE_RULE_EXPRESSION;
	ast_push(ast, post);

	q_ast* body = parse_body(context);
	ast_push(ast, body);

	parse_expect_end(context, ast);

	return(ast);
}

q_ast* parse_while(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_WHILE, parser_parent(context));
	parser_shift(context);

	q_ast* cond = parse_expression(context);
	if(!cond)
	{
		cond = parse_error(context, "loop condition");
	}
	cond->parseRule = PARSE_RULE_EXPRESSION;
	ast_push(ast, cond);


	q_ast* body = parse_body(context);
	ast_push(ast, body);

	parse_expect_end(context, ast);

	return(ast);
}

void parse_statement_list(parse_context* context, q_ast* parent)
{
	for(q_cell* cell = parser_look(context);
	    cell != 0;
	    cell = parser_look(context))
	{
		q_ast* stmt = parse_statement(context);

		if(!stmt)
		{
			stmt = parse_error(context, "statement");
		}
		stmt->parseRule = PARSE_RULE_STATEMENT;
		ast_push(parent, stmt);
	}
}

q_ast* parse_do(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_DO, parser_parent(context));
	parser_shift(context);
	parse_statement_list(context, ast);
	return(ast);
}

q_ast* parse_background(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_BACKGROUND, parser_parent(context));
	parser_shift(context);
	parse_statement_list(context, ast);
	return(ast);
}

q_ast* parse_tempo_spec(parse_context* context)
{
	q_ast* ast = 0;

	q_cell* cell = parser_look(context);
	if(  cell_token(cell) == TOKEN_UI
	  && cell->ui.kind == CELL_UI_TEMPO_CURVE)
	{
		ast = ast_from_cell(context, AST_TEMPO_EDITOR, cell);
	}
	return(ast);
}

q_ast* parse_tempo_attribute(parse_context* context)
{
	q_ast* tempoSpec = 0;

	q_cell* cell = parser_look(context);
	if(cell_token(cell) == TOKEN_ATTRIBUTE)
	{
		parser_enter_cell(context);
			q_cell* head = parser_look(context);
			if(  cell_token(head) == TOKEN_IDENTIFIER
			  && !mp_string_cmp(head->text.string, mp_string_lit("tempo")))
			{
				parser_shift(context);
				tempoSpec = parse_tempo_spec(context);
				if(!tempoSpec)
				{
					tempoSpec = parse_error(context, "tempo specification");
				}
			}
		parser_leave_cell(context);
	}
	if(tempoSpec)
	{
		parser_shift(context);
	}
	return(tempoSpec);
}

q_ast* parse_flow(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_FLOW, parser_parent(context));
	parser_shift(context);

	q_ast* tempo = parse_tempo_attribute(context);
	if(!tempo)
	{
		q_point point = {parser_parent(context), parser_look(context), 0};
		q_range range = {point, point};
		tempo = ast_with_range(context, AST_OPT_EMPTY, range);
	}

	q_ast* body = parse_body(context);

	ast_push(ast, tempo);
	ast_push(ast, body);

	return(ast);
}

q_ast* parse_standby(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_STANDBY, parser_parent(context));
	parser_shift(context);

	return(ast);
}

q_ast* parse_wait(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_WAIT, parser_parent(context));
	parser_shift(context);

	q_ast* recursive = 0;
	q_cell* cell = parser_look(context);
	if(cell_token(cell) == TOKEN_ATTRIBUTE)
	{
		parser_enter_cell(context);
			q_cell* head = parser_look(context);
			if(cell_token(head) == TOKEN_IDENTIFIER
			  && !mp_string_cmp(head->text.string, mp_string_lit("recursive")))
			{
				recursive = ast_from_cell(context, AST_ATTR_RECURSIVE, parser_parent(context));
				parse_expect_end(context, recursive);
			}
			else
			{
				//TODO: error
			}
		parser_leave_cell(context);
		parser_shift(context);
	}
	if(!recursive)
	{
		q_point point = {parser_parent(context), parser_look(context), 0};
		q_range range = {point, point};
		recursive = ast_with_range(context, AST_OPT_EMPTY, range);
	}
	ast_push(ast, recursive);

	q_ast* opd = parse_expression(context);
	if(!opd)
	{
		opd = parse_error(context, "future expression");
	}
	ast_push(ast, opd);
	return(ast);
}

q_ast* parse_timeout(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_WAIT, parser_parent(context));
	parser_shift(context);

	q_ast* recursive = 0;
	q_cell* cell = parser_look(context);
	if(cell_token(cell) == TOKEN_ATTRIBUTE)
	{
		parser_enter_cell(context);
			q_cell* head = parser_look(context);
			if(cell_token(head) == TOKEN_IDENTIFIER
			  && !mp_string_cmp(head->text.string, mp_string_lit("recursive")))
			{
				recursive = ast_from_cell(context, AST_ATTR_RECURSIVE, parser_parent(context));
				parse_expect_end(context, recursive);
			}
			else
			{
				//TODO: error
			}
		parser_leave_cell(context);
		parser_shift(context);
	}
	if(!recursive)
	{
		q_point point = {parser_parent(context), parser_look(context), 0};
		q_range range = {point, point};
		recursive = ast_with_range(context, AST_OPT_EMPTY, range);
	}
	ast_push(ast, recursive);

	q_ast* opd = parse_expression(context);
	if(!opd)
	{
		opd = parse_error(context, "future expression");
	}
	ast_push(ast, opd);


	q_ast* expr = parse_expression(context);
	if(!expr)
	{
		expr = parse_error(context, "timeout expression");
	}
	ast_push(ast, expr);
	return(ast);

}


q_ast* parse_field(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_FIELD, parser_parent(context));
	parser_shift(context);

	q_cell* cell = parser_look(context);
	q_ast* name = 0;
	if(cell_token(cell) == TOKEN_IDENTIFIER)
	{
		name = ast_from_cell(context, AST_ID, cell);
		parser_shift(context);
	}
	else
	{
		name = parse_error(context, "field name");
	}

	q_ast* object = parse_expression(context);
	if(!object)
	{
		object = parse_error(context, "object");
	}
	object->parseRule = PARSE_RULE_EXPRESSION;

	ast_push(ast, name);
	ast_push(ast, object);

	return(ast);
}

q_ast* parse_cast(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_CAST, parser_parent(context));
	parser_shift(context);

	q_ast* type = parse_type_specification(context);
	if(!type)
	{
		type = parse_error(context, "type specification");
	}
	type->parseRule = PARSE_RULE_TYPESPEC;

	q_ast* expr = parse_expression(context);
	if(!expr)
	{
		expr = parse_error(context, "expression");
	}
	expr->parseRule = PARSE_RULE_EXPRESSION;

	ast_push(ast, type);
	ast_push(ast, expr);

	return(ast);
}


q_ast* parse_put(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_PUT, parser_parent(context));
	parser_shift(context);

	for(q_cell* cell = parser_look(context);
	    cell != 0;
	    parser_shift(context), cell = parser_look(context))
	{
		q_ast* arg = parse_initializer(context);
		if(!arg)
		{
			arg = parse_error(context, "initializer");
		}
		ast_push(ast, arg);
	}
	return(ast);
}

q_ast* parse_sizeof(parse_context* context)
{
	parser_shift(context);
	q_ast* ast = ast_from_cell(context, AST_SIZEOF, parser_parent(context));

	q_cell* opdCell = parser_look(context);
	q_ast* opd = 0;
	if(cell_token(opdCell) == TOKEN_POLY)
	{
		opd = ast_from_cell(context, AST_POLY_ID, opdCell);
	}
	else
	{
		opd = parse_expression_or_type(context);
	}
	if(!opd)
	{
		opd = parse_error(context, "expression or type specification");
	}
	ast_push(ast, opd);
	return(ast);
}

q_ast* parse_compound_expression(parse_context* context)
{
	q_cell* cell = parser_look(context);
	q_ast* ast = 0;
	switch(cell_token(cell))
	{
		case TOKEN_OP_SUB:
			if(parser_parent(context)->childCount <=2)
			{
				ast = parse_unary_expression(context, AST_NEG);
			}
			else
			{
				ast = parse_binary_expression(context, AST_SUB);
			}
			break;
		case TOKEN_OP_ADD:
			ast = parse_binary_expression(context, AST_ADD);
			break;
		case TOKEN_OP_MUL:
			ast = parse_binary_expression(context, AST_MUL);
			break;
		case TOKEN_OP_DIV:
			ast = parse_binary_expression(context, AST_DIV);
			break;
		case TOKEN_OP_MOD:
			ast = parse_binary_expression(context, AST_MOD);
			break;
		case TOKEN_OP_EQ:
			ast = parse_binary_expression(context, AST_EQ);
			break;
		case TOKEN_OP_NEQ:
			ast = parse_binary_expression(context, AST_NEQ);
			break;
		case TOKEN_OP_LT:
			ast = parse_binary_expression(context, AST_LT);
			break;
		case TOKEN_OP_GT:
			ast = parse_binary_expression(context, AST_GT);
			break;
		case TOKEN_OP_LE:
			ast = parse_binary_expression(context, AST_LE);
			break;
		case TOKEN_OP_GE:
			ast = parse_binary_expression(context, AST_GE);
			break;

		case TOKEN_KW_LAND:
			ast = parse_binary_expression(context, AST_LAND);
			break;
		case TOKEN_KW_LOR:
			ast = parse_binary_expression(context, AST_LOR);
			break;
		case TOKEN_KW_LNOT:
			ast = parse_unary_expression(context, AST_LNOT);
			break;

		case TOKEN_IDENTIFIER:
			ast = parse_call(context);
			break;

		case TOKEN_KW_AT:
			ast = parse_binary_expression(context, AST_AT);
			break;

		case TOKEN_KW_SET:
			ast = parse_binary_expression(context, AST_SET);
			break;

		case TOKEN_KW_REF:
			ast = parse_unary_expression(context, AST_REF);
			break;

		case TOKEN_KW_DEREF:
			ast = parse_unary_expression(context, AST_DEREF);
			break;

		case TOKEN_KW_LEN:
			ast = parse_unary_expression(context, AST_LEN);
			break;

		case TOKEN_KW_SLICE:
			ast = parse_ternary_expression(context, AST_SLICE);
			break;

		case TOKEN_KW_SIZEOF:
			ast = parse_sizeof(context);
			break;

		case TOKEN_KW_FIELD:
			ast = parse_field(context);
			break;

		case TOKEN_KW_CAST:
			return(parse_cast(context));

		case TOKEN_KW_IF:
			ast = parse_if(context);
			break;

		case TOKEN_KW_FOR:
			ast = parse_for(context);
			break;

		case TOKEN_KW_WHILE:
			ast = parse_while(context);
			break;

		case TOKEN_KW_DO:
			ast = parse_do(context);
			break;

		case TOKEN_KW_RETURN:
			ast = parse_return(context);
			break;

		case TOKEN_KW_FLOW:
			ast = parse_flow(context);
			break;

		case TOKEN_KW_BACKGROUND:
			ast = parse_background(context);
			break;

		case TOKEN_KW_PAUSE:
			ast = parse_unary_expression(context, AST_PAUSE);
			break;

		case TOKEN_KW_STANDBY:
			ast = parse_standby(context);
			break;

		case TOKEN_KW_WAIT:
			ast = parse_wait(context);
			break;

		case TOKEN_KW_TIMEOUT:
			ast = parse_timeout(context);
			break;

		case TOKEN_KW_FDUP:
			ast = parse_unary_expression(context, AST_FDUP);
			break;

		case TOKEN_KW_FDROP:
			ast = parse_unary_expression(context, AST_FDROP);
			break;

		case TOKEN_KW_PUT:
			ast = parse_put(context);
			break;

		default:
			break;
	}
	return(ast);
}

q_ast* parse_expression(parse_context* context)
{
	q_cell* cell = parser_look(context);
	q_ast* ast = 0;

	switch(cell_token(cell))
	{
		//NOTE(martin): literals and variable
		case TOKEN_CHAR:
			ast = ast_from_cell(context, AST_CHAR, cell);
			break;

		case TOKEN_INT:
			ast = ast_from_cell(context, AST_INT, cell);
			break;

		case TOKEN_FLOAT:
			ast = ast_from_cell(context, AST_FLOAT, cell);
			break;

		case TOKEN_IDENTIFIER:
			ast = ast_from_cell(context, AST_ID, cell);
			break;

		case TOKEN_KW_NIL:
			ast = ast_from_cell(context, AST_NIL, cell);
			break;

		case TOKEN_LIST:
			parser_enter_cell(context);
			ast = parse_compound_expression(context);
			parser_leave_cell(context);
			break;

		default:
			break;
	}
	if(ast)
	{
		parser_shift(context);
	}
	return(ast);
}

q_ast* parse_array(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_ARRAY, parser_parent(context));
	for(q_cell* cell = parser_look(context);
	    cell != 0;
	    cell = parser_look(context))
	{
		q_ast* elt = parse_initializer(context);
		if(!elt)
		{
			elt = parse_error(context, "array element initializer");
		}
		ast_push(ast, elt);
	}
	return(ast);
}

q_ast* parse_initializer(parse_context* context)
{
	q_cell* cell = parser_look(context);
	q_ast* ast = 0;

	q_token token = cell_token(cell);
	if(token == TOKEN_STRING)
	{
		ast = ast_from_cell(context, AST_STRING, cell);
		parser_shift(context);
	}
	else if(token == TOKEN_ARRAY)
	{
		parser_enter_cell(context);
		ast = parse_array(context);
		parser_leave_cell(context);
		parser_shift(context);
	}
	else
	{
		ast = parse_expression(context);
	}
	return(ast);
}

q_ast* parse_type_array(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_TYPE_ARRAY, parser_parent(context));

	parser_shift(context);
	q_cell* cell = parser_look(context);
	q_ast* count = 0;
	if(cell_token(cell) == CELL_INT)
	{
		count = ast_from_cell(context, AST_INT, cell);
		parser_shift(context);
	}
	else
	{
		count = parse_error(context, "array length");
	}

	cell = parser_look(context);
	q_ast* typespec = parse_type_specification(context);
	if(!typespec)
	{
		typespec = parse_error(context, "type specification");
	}
	typespec->parseRule = PARSE_RULE_TYPESPEC;

	parse_expect_end(context, ast);

	ast_push(ast, count);
	ast_push(ast, typespec);

	return(ast);
}

q_ast* parse_type_indirect(parse_context* context, ast_kind kind)
{
	q_ast* ast = ast_from_cell(context, kind, parser_parent(context));

	parser_shift(context);
	q_cell* cell = parser_look(context);
	q_ast* typespec = parse_type_specification(context);
	if(!typespec)
	{
		typespec = parse_error(context, "type specification");
	}
	typespec->parseRule = PARSE_RULE_TYPESPEC;

	parse_expect_end(context, ast);
	ast_push(ast, typespec);
	return(ast);
}

q_ast* parse_field_spec(parse_context* context)
{
	q_ast* name = 0;
	q_cell* cell = parser_look(context);
	if(cell_token(cell) == TOKEN_IDENTIFIER)
	{
		name = ast_from_cell(context, AST_ID, cell);
		parser_shift(context);
	}
	else
	{
		name = parse_error(context, "field name");
	}

	q_ast* typespec = parse_type_specification(context);
	if(!typespec)
	{
		typespec = parse_error(context, "type specification");
	}
	typespec->parseRule = PARSE_RULE_TYPESPEC;

	q_range range = {name->range.start, typespec->range.end};
	q_ast* ast = ast_with_range(context, AST_FIELD_SPEC, range);
	ast_push(ast, name);
	ast_push(ast, typespec);
	return(ast);
}

q_ast* parse_type_struct(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_TYPE_STRUCT, parser_parent(context));

	parser_shift(context);

	q_cell* cell = parser_look(context);
	do
	{
		q_ast* field = parse_field_spec(context);
		DEBUG_ASSERT(field, "errors are handled in callee");
		ast_push(ast, field);
		cell = parser_look(context);

	} while(cell != 0);

	return(ast);
}

q_ast* parse_compound_type_specification(parse_context* context)
{
	q_cell* cell = parser_look(context);
	q_ast* ast = 0;
	switch(cell_token(cell))
	{
		case TOKEN_KW_ARRAY:
			ast = parse_type_array(context);
			break;

		case TOKEN_KW_SLICE:
			ast = parse_type_indirect(context, AST_TYPE_SLICE);
			break;

		case TOKEN_KW_POINTER:
			ast = parse_type_indirect(context, AST_TYPE_POINTER);
			break;

		case TOKEN_KW_FUTURE:
			ast = parse_type_indirect(context, AST_TYPE_FUTURE);
			break;

		case TOKEN_KW_STRUCT:
			ast = parse_type_struct(context);
			break;

		default:
			break;
	}

	return(ast);
}

q_ast* parse_type_specification(parse_context* context)
{
	q_cell* cell = parser_look(context);

	q_ast* ast = 0;
	q_token token = cell_token(cell);

	switch(token)
	{
		case TOKEN_IDENTIFIER:
			ast = ast_from_cell(context, AST_ID, cell);
			break;

		case TOKEN_POLY:
			ast = ast_from_cell(context, AST_POLY_ID, cell);
			break;

		case TOKEN_LIST:
		{
			parser_enter_cell(context);
			ast = parse_compound_type_specification(context);
			parser_leave_cell(context);
		} break;

		default:
			break;
	}
	if(ast)
	{
		parser_shift(context);
	}

	return(ast);
}

q_ast* parse_compound_expression_or_type(parse_context* context)
{
	q_cell* head = parser_look(context);
	switch(cell_token(head))
	{
		case TOKEN_KW_ARRAY:
		case TOKEN_KW_STRUCT:
		case TOKEN_KW_FUTURE:
		case TOKEN_KW_POINTER:
			return(parse_compound_type_specification(context));

		case TOKEN_KW_SLICE:
		{
			q_cell* parent = parser_parent(context);
			if(parent->childCount <= 2)
			{
				return(parse_compound_type_specification(context));
			}
			else
			{
				return(parse_expression(context));
			}
		} break;

		default:
			return(parse_compound_expression(context));
	}
}

q_ast* parse_expression_or_type(parse_context* context)
{
	q_cell* cell = parser_look(context);
	q_ast* res = 0;
	switch(cell_token(cell))
	{
		case TOKEN_LIST:
			parser_enter_cell(context);
				res = parse_compound_expression_or_type(context);
			parser_leave_cell(context);

			if(res)
			{
				parser_shift(context);
			}
			break;

		case TOKEN_IDENTIFIER:
			parser_shift(context);
			res = ast_from_cell(context, AST_ID, cell);
			break;

		default:
			res = parse_expression(context);
	}
	return(res);
}

q_ast* parse_type_declaration(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_TYPE_DECL, parser_parent(context));
	parser_shift(context);

	q_ast* name = 0;
	q_cell* cell = parser_look(context);
	if(cell_token(cell) != TOKEN_IDENTIFIER)
	{
		name = parse_error(context, "type name");
	}
	else
	{
		name = ast_from_cell(context, AST_ID, cell);
		parser_shift(context);
	}

	cell = parser_look(context);
	q_ast* typespec = parse_type_specification(context);
	if(!typespec)
	{
		typespec = parse_error(context, "type specification");
	}
	typespec->parseRule = PARSE_RULE_TYPESPEC;

	parse_expect_end(context, ast);

	ast_push(ast, name);
	ast_push(ast, typespec);
	return(ast);
}

q_ast* parse_variable_declaration(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_VAR_DECL, parser_parent(context));
	parser_shift(context);

	q_ast* name = 0;
	q_cell* cell = parser_look(context);
	if(cell_token(cell) != TOKEN_IDENTIFIER)
	{
		name = parse_error(context, "variable name");
	}
	else
	{
		name = ast_from_cell(context, AST_ID, cell);
		parser_shift(context);
	}

	cell = parser_look(context);
	q_ast* typespec = parse_type_specification(context);
	if(!typespec)
	{
		typespec = parse_error(context, "type specification");
	}
	typespec->parseRule = PARSE_RULE_TYPESPEC;

	ast_push(ast, name);
	ast_push(ast, typespec);

	cell = parser_look(context);
	if(cell)
	{
		q_ast* expr = parse_initializer(context);
		if(!expr)
		{
			expr = parse_error(context, "initializer");
		}
		expr->parseRule = PARSE_RULE_INITIALIZER;
		ast_push(ast, expr);
	}

	parse_expect_end(context, ast);

	return(ast);
}

q_ast* parse_varg(parse_context* context)
{
	q_ast* varg = 0;
	q_cell* cell = parser_look(context);
	if(cell_token(cell) == TOKEN_LIST)
	{
		parser_enter_cell(context);
		{
			q_cell* head = parser_look(context);
			if(cell_token(head) == TOKEN_KW_VARG)
			{
				varg = ast_from_cell(context, AST_VARG, cell);

				parser_shift(context);
				cell = parser_look(context);
				q_ast* typespec = 0;

				if(cell_token(cell) == TOKEN_POLY)
				{
					typespec = ast_from_cell(context, AST_POLY_ID, cell);
					parser_shift(context);
				}
				else
				{
					typespec = parse_type_specification(context);
				}

				if(!typespec)
				{
					typespec = parse_error(context, "variadic parameter type specification");
				}
				typespec->parseRule = PARSE_RULE_TYPESPEC;

				ast_push(varg, typespec);
			}
		}
		parser_leave_cell(context);
	}

	if(varg)
	{
		parser_shift(context);
	}
	return(varg);
}

q_ast* parse_proc_signature(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_PROC_SIG, parser_parent(context));

	q_cell* firstCell = parser_look(context);
	q_cell* arrowCell = 0;

	//NOTE: create params with an empty range, we fix this after parsing parameters
	q_range range = {{parser_parent(context), firstCell, 0}, {parser_parent(context), firstCell, 0}};
	q_ast* params = ast_with_range(context, AST_PARAM_LIST, range);
	q_ast* retType = 0;

	for(q_cell* cell = parser_look(context);
	    cell != 0;
	    cell = parser_look(context))
	{
		q_token token = cell_token(cell);
		if(token == TOKEN_OP_ARROW)
		{
			//NOTE(martin): return type declaration
			arrowCell = cell;
			parser_shift(context);
			retType = parse_type_specification(context);
			if(!retType)
			{
				retType = parse_error(context, "return type specification");
			}
			retType->parseRule = PARSE_RULE_TYPESPEC;
			break;
		}
		else
		{
			//NOTE(martin): parameter declaration
			q_ast* name = 0;
			if(token == TOKEN_IDENTIFIER)
			{
				name = ast_from_cell(context, AST_ID, cell);
				parser_shift(context);
			}
			else if(token == TOKEN_POLY)
			{
				name = ast_from_cell(context, AST_POLY_ID, cell);
				parser_shift(context);
			}
			else
			{
				name = parse_error(context, "parameter name");
			}

			cell = parser_look(context);
			token = cell_token(cell);

			q_ast* type = 0;
			if(token == TOKEN_POLY)
			{
				type = ast_from_cell(context, AST_POLY_ID, cell);
				parser_shift(context);
			}
			else if(token == TOKEN_KW_TYPEID)
			{
				type = ast_from_cell(context, AST_TYPEID, cell);
				parser_shift(context);
			}
			else
			{
				type = parse_type_specification(context);
			}

			if(!type)
			{
				type = parse_varg(context);
			}

			if(!type)
			{
				//TODO: precise name of parameter?
				type = parse_error(context, "parameter type specification");
			}
			type->parseRule = PARSE_RULE_TYPESPEC;

			q_range range = {name->range.start, type->range.end};
			q_ast* param = ast_with_range(context, AST_PARAM, range);
			ast_push(param, name);
			ast_push(param, type);

			ast_push(params, param);
		}
	}
	params->range = (q_range){{.parent = parser_parent(context),
	                           .leftFrom = firstCell,
	                           .textIndex = 0},
	                          {.parent = parser_parent(context),
	                           .leftFrom = arrowCell,
	                           .textIndex = 0}};
	parse_expect_end(context, ast);

	ast_push(ast, params);
	if(retType)
	{
		ast_push(ast, retType);
	}
	return(ast);
}

q_ast* parse_compound_statement(parse_context* context)
{
	q_cell* cell = parser_look(context);
	q_ast* ast = 0;
	switch(cell_token(cell))
	{
		case TOKEN_KW_VAR:
			ast = parse_variable_declaration(context);
			break;

		//...
		default:
			break;
	}
	return(ast);
}

q_ast* parse_statement(parse_context* context)
{
	q_ast* stmt = parse_expression(context);

	if(!stmt)
	{
		q_cell* cell = parser_look(context);

		if(cell_token(cell) == TOKEN_LIST)
		{
			parser_enter_cell(context);
			stmt = parse_compound_statement(context);
			parser_leave_cell(context);
			if(stmt)
			{
				parser_shift(context);
			}
		}
	}
	return(stmt);
}

q_ast* parse_body(parse_context* context)
{
	q_range range = {.start = {parser_parent(context), parser_look(context), 0},
	                 .end = {parser_parent(context), 0, 0}};
	q_ast* body = ast_with_range(context, AST_BODY, range);

	parse_statement_list(context, body);
	return(body);
}

q_ast* parse_proc_declaration(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_PROC_DECL, parser_parent(context));
	parser_shift(context);

	q_ast* name = 0;
	q_cell* cell = parser_look(context);
	if(cell_token(cell) != TOKEN_IDENTIFIER)
	{
		name = parse_error(context, "procedure name");
	}
	else
	{
		name = ast_from_cell(context, AST_ID, cell);
		parser_shift(context);
	}

	q_ast* sig = 0;
	cell = parser_look(context);
	if(cell_token(cell) == TOKEN_LIST)
	{
		parser_enter_cell(context);
			sig = parse_proc_signature(context);
		parser_leave_cell(context);
	}
	if(!sig)
	{
		sig = parse_error(context, "procedure signature");
	}
	else
	{
		parser_shift(context);
	}

	q_ast* body = parse_body(context);

	ast_push(ast, name);
	ast_push(ast, sig);
	ast_push(ast, body);

	return(ast);
}

q_ast* parse_import(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_IMPORT, parser_parent(context));
	parser_shift(context);

	q_cell* cell = parser_look(context);
	q_ast* name = 0;
	if(cell_token(cell) == TOKEN_IDENTIFIER)
	{
		name = ast_from_cell(context, AST_ID, cell);
		parser_shift(context);
	}
	else
	{
		name = parse_error(context, "module name");
	}
	ast_push(ast, name);

	q_ast* alias = 0;
	cell = parser_look(context);
	if(cell_token(cell) == TOKEN_ATTRIBUTE)
	{
		parser_enter_cell(context);
			q_cell* head = parser_look(context);
			if(  cell_token(head) == TOKEN_IDENTIFIER
			  && !mp_string_cmp(head->text.string, mp_string_lit("as")))
			{
				alias = ast_from_cell(context, AST_ATTR_AS, parser_parent(context));
				q_ast* aliasName = 0;

				parser_shift(context);
				cell = parser_look(context);
				if(cell_token(cell) == TOKEN_IDENTIFIER)
				{
					aliasName = ast_from_cell(context, AST_ID, cell);
				}
				else
				{
					aliasName = parse_error(context, "namespace alias");
				}
				ast_push(alias, aliasName);
			}
		parser_leave_cell(context);
		if(alias)
		{
			parser_shift(context);
		}
		else
		{
			alias = parse_error(context, "'as' attribute");
		}
	}
	else
	{
		q_point point = {parser_parent(context), parser_look(context), 0};
		q_range range = {point, point};
		alias = ast_with_range(context, AST_OPT_EMPTY, range);
	}
	ast_push(ast, alias);

	parse_expect_end(context, ast);
	return(ast);
}

q_ast* parse_foreign_proc(parse_context* context)
{
	q_ast* ast = ast_from_cell(context, AST_FOREIGN_DEF, parser_parent(context));
	parser_shift(context);

	q_ast* name = 0;
	q_cell* cell = parser_look(context);
	if(cell_token(cell) != TOKEN_IDENTIFIER)
	{
		name = parse_error(context, "procedure name");
	}
	else
	{
		name = ast_from_cell(context, AST_ID, cell);
		parser_shift(context);
	}

	q_ast* sig = 0;
	cell = parser_look(context);
	if(cell_token(cell) == TOKEN_LIST)
	{
		parser_enter_cell(context);
			sig = parse_proc_signature(context);
		parser_leave_cell(context);
	}
	if(!sig)
	{
		sig = parse_error(context, "procedure signature");
	}
	else
	{
		parser_shift(context);
	}

	//TODO: parse linkName attribute

	ast_push(ast, name);
	ast_push(ast, sig);

	return(ast);
}

q_ast* parse_foreign_block(parse_context* context)
{
	parser_shift(context);
	q_ast* ast = ast_from_cell(context, AST_FOREIGN_BLOCK, parser_parent(context));

	q_cell* nameCell = parser_look(context);
	q_ast* nameAst = 0;

	if(cell_token(nameCell) != TOKEN_STRING)
	{
		nameAst = parse_error(context, "library name (string)");
	}
	else
	{
		nameAst = ast_from_cell(context, AST_ID, nameCell);
		parser_shift(context);
	}
	ast_push(ast, nameAst);

	q_cell* parent = parser_parent(context);
	q_point defStart = {parent, parser_look(context), 0};
	q_point defEnd = {parent, ListLastEntry(&parent->children, q_cell, listElt), 0};
	q_range defRange = {defStart, defEnd};
	q_ast* defs = ast_with_range(context, AST_MULTI, defRange);

	for(q_cell* cell = parser_look(context);
	    cell != 0;
	    cell = parser_look(context))
	{
		q_ast* def = 0;
		if(cell_token(cell) == TOKEN_LIST)
		{
			parser_enter_cell(context);
			q_cell* head = parser_look(context);
			if(cell_token(head) == TOKEN_KW_DEF)
			{
				def = parse_foreign_proc(context);
			}
			parser_leave_cell(context);
		}

		if(def)
		{
			parser_shift(context);
		}
		else
		{
			def = parse_error(context, "foreign procedure");
		}
		ast_push(defs, def);
	}
	ast_push(ast, defs);
	return(ast);
}

q_ast* parse_top_level(parse_context* context)
{
	q_ast* ast = 0;

	q_cell* cell = parser_look(context);
	if(cell_token(cell) == TOKEN_LIST)
	{
		parser_enter_cell(context);
		q_cell* head = parser_look(context);
		switch(cell_token(head))
		{
			case TOKEN_KW_IMPORT:
				ast = parse_import(context);
				break;

			case TOKEN_KW_FOREIGN:
				ast = parse_foreign_block(context);
				break;

			case TOKEN_KW_VAR:
				ast = parse_variable_declaration(context);
				break;
			case TOKEN_KW_TYPE:
				ast = parse_type_declaration(context);
				break;
			case TOKEN_KW_DEF:
				ast = parse_proc_declaration(context);
				break;

			default:
				break;
		}
		parser_leave_cell(context);
	}
	if(ast)
	{
		parser_shift(context);
	}

	return(ast);
}

q_ast* parse_root(parse_context* context)
{
	q_ast* ast = ast_alloc(context->module, AST_ROOT);
	q_cell* cell = parser_look(context);
	while(cell)
	{
		q_ast* top = parse_top_level(context);
		if(!top)
		{
			top = parse_error(context, "top-level expression");
		}
		top->parseRule = PARSE_RULE_TOP_LEVEL;
		ast_push(ast, top);

		cell = parser_look(context);
	}
	return(ast);
}

void parse_module(q_module* module)
{
	q_cell* rootCell = module->tree.rootCell;

	parse_context context;
	parse_context_init(&context, module, rootCell);

	q_ast* ast = parse_root(&context);

	if(module->ast)
	{
		ast_recycle(&context, module->ast);
	}
	module->ast = ast;
	cell_ast_cache(rootCell, ast);

}
