/************************************************************//**
*
*	@file: ringbuffer.h
*	@author: Martin Fouilleul
*	@date: 03/01/2022
*	@revision:
*
*****************************************************************/
#ifndef __RINGBUFFER_H_
#define __RINGBUFFER_H_

#include<stdatomic.h>
#include"typedefs.h"

typedef struct q_ringbuffer
{
	u32 mask;
	_Atomic(u32) readIndex;
	_Atomic(u32) writeIndex;

	u8* buffer;

} q_ringbuffer;

void q_ringbuffer_init(q_ringbuffer* ring, u8 capExp);
void q_ringbuffer_cleanup(q_ringbuffer* ring);
u32 q_ringbuffer_read_available(q_ringbuffer* ring);
u32 q_ringbuffer_write_available(q_ringbuffer* ring);
u32 q_ringbuffer_write(q_ringbuffer* ring, u32 size, u8* data);
u32 q_ringbuffer_read(q_ringbuffer* ring, u32 size, u8* data);


typedef enum { Q_MSG_NONE,
               Q_MSG_PAUSED,
               Q_MSG_RESUMED,
               Q_MSG_PROGRESS,
               Q_MSG_WAITING,
               Q_MSG_STANDBY,
               Q_MSG_JMP_TARGET,
               Q_MSG_TRIGGER,
               Q_MSG_UPDATE_TEMPO_CURVE,
               /* ... */ } q_msg_kind;

typedef struct qed_exec_point
{
	u64 offset;
} qed_exec_point;

typedef struct q_msg_progress
{
	qed_exec_point point;
	f64 initialDelay;
	f64 localRemaining;
} q_msg_progress;

typedef struct q_msg
{
	q_msg_kind kind;

	union
	{
		qed_exec_point point;
		q_msg_progress progress;
	};

	u32 payloadSize;
	char payload[];
} q_msg;

void q_ringbuffer_push_msg(q_ringbuffer* ring, q_msg* msg);
q_msg* q_ringbuffer_poll_msg(q_ringbuffer* ring, mem_arena* arena);

#endif //__RINGBUFFER_H_
