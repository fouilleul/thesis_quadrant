/************************************************************//**
*
*	@file: completions.h
*	@author: Martin Fouilleul
*	@date: 15/06/2022
*	@revision:
*
*****************************************************************/
#ifndef __COMPLETIONS_H_
#define __COMPLETIONS_H_

#include"typedefs.h"
#include"tokens.h"
#include"ir.h"

typedef struct q_completion
{
	cell_kind cellKind;
	token_class tokenClass;
	mp_string text;
	mp_string help;

	ir_type_pattern typePattern;

} q_completion;

q_completion Q_TOP_LEVEL_HEAD_COMPLETIONS[] = {
	{CELL_LIST, TOKEN_CLASS_KEYWORD, mp_string_lit("def"), mp_string_lit("procedure definition")},
	{CELL_LIST, TOKEN_CLASS_KEYWORD, mp_string_lit("var"), mp_string_lit("variable declaration")},
	{CELL_LIST, TOKEN_CLASS_KEYWORD, mp_string_lit("type"), mp_string_lit("type definition")},
};
const u32 Q_TOP_LEVEL_HEAD_COMPLETIONS_COUNT = sizeof(Q_TOP_LEVEL_HEAD_COMPLETIONS)/sizeof(q_completion);

q_completion Q_STATEMENT_HEAD_COMPLETIONS[] = {
	{CELL_LIST, TOKEN_CLASS_KEYWORD, mp_string_lit("var"), mp_string_lit("variable declaration")},
//	{CELL_LIST, TOKEN_CLASS_KEYWORD, mp_string_lit("return"), mp_string_lit("return statement")},

	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("+"), mp_string_lit("arithmetic add")},
	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("-"), mp_string_lit("arithmetic subtract")},
	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("*"), mp_string_lit("arithmetic multiply")},
	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("/"), mp_string_lit("arithmetic divide")},
	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("%"), mp_string_lit("arithmetic modulo")},
	//etc...
	{CELL_LIST, TOKEN_CLASS_CALL, {0, 0}, mp_string_lit("proc call")},
	{CELL_WORD, TOKEN_CLASS_VAR, {0, 0}, mp_string_lit("variable")},
};
const u32 Q_STATEMENT_HEAD_COMPLETIONS_COUNT = sizeof(Q_STATEMENT_HEAD_COMPLETIONS)/sizeof(q_completion);

q_completion Q_EXPR_HEAD_COMPLETIONS[] = {
	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("+"), mp_string_lit("arithmetic add"), {TYPE_PATTERN_NUMERIC}},
	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("-"), mp_string_lit("arithmetic subtract"), {TYPE_PATTERN_NUMERIC}},
	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("*"), mp_string_lit("arithmetic multiply"), {TYPE_PATTERN_NUMERIC}},
	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("/"), mp_string_lit("arithmetic divide"), {TYPE_PATTERN_NUMERIC}},
	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("%"), mp_string_lit("arithmetic modulo"), {TYPE_PATTERN_NUMERIC}},

	{CELL_LIST, TOKEN_CLASS_CALL, {0, 0}, mp_string_lit("proc call")},
	{CELL_WORD, TOKEN_CLASS_VAR, {0, 0}, mp_string_lit("variable")},
};
const u32 Q_EXPR_HEAD_COMPLETIONS_COUNT = sizeof(Q_EXPR_HEAD_COMPLETIONS)/sizeof(q_completion);

q_completion Q_INIT_HEAD_COMPLETIONS[] = {
	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("+"), mp_string_lit("arithmetic add"), {TYPE_PATTERN_NUMERIC}},
	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("-"), mp_string_lit("arithmetic subtract"), {TYPE_PATTERN_NUMERIC}},
	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("*"), mp_string_lit("arithmetic multiply"), {TYPE_PATTERN_NUMERIC}},
	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("/"), mp_string_lit("arithmetic divide"), {TYPE_PATTERN_NUMERIC}},
	{CELL_LIST, TOKEN_CLASS_OPERATOR, mp_string_lit("%"), mp_string_lit("arithmetic modulo"), {TYPE_PATTERN_NUMERIC}},
	//etc...
	{CELL_ARRAY, TOKEN_CLASS_ARRAY, {0, 0}, mp_string_lit("array literal"), {TYPE_PATTERN_ARRAY_OR_SLICE}},
	{CELL_STRING, TOKEN_CLASS_STRING, {0, 0}, mp_string_lit("string literal"), {TYPE_PATTERN_STRING}},

	{CELL_LIST, TOKEN_CLASS_CALL, {0, 0}, mp_string_lit("proc call")},
	{CELL_WORD, TOKEN_CLASS_VAR, {0, 0}, mp_string_lit("variable")},
};
const u32 Q_INIT_HEAD_COMPLETIONS_COUNT = sizeof(Q_INIT_HEAD_COMPLETIONS)/sizeof(q_completion);


q_completion Q_TYPESPEC_HEAD_COMPLETIONS[] = {
	{CELL_LIST, TOKEN_CLASS_KEYWORD, mp_string_lit("array"), mp_string_lit("array type specification")},
	{CELL_LIST, TOKEN_CLASS_KEYWORD, mp_string_lit("future"), mp_string_lit("future type specification")},
	{CELL_LIST, TOKEN_CLASS_KEYWORD, mp_string_lit("ptr"), mp_string_lit("pointer type specification")},
	{CELL_LIST, TOKEN_CLASS_KEYWORD, mp_string_lit("slice"), mp_string_lit("slice type specification")},
	{CELL_LIST, TOKEN_CLASS_KEYWORD, mp_string_lit("struct"), mp_string_lit("structure type specification")},
	//etc...
};
const u32 Q_TYPESPEC_HEAD_COMPLETIONS_COUNT = sizeof(Q_TYPESPEC_HEAD_COMPLETIONS)/sizeof(q_completion);


typedef struct q_completion_array
{
	u32 count;
	q_completion* data;
} q_completion_array;

q_completion_array Q_HEAD_COMPLETIONS[] =
{
	{0, 0},
	{Q_EXPR_HEAD_COMPLETIONS_COUNT, Q_EXPR_HEAD_COMPLETIONS},
	{Q_INIT_HEAD_COMPLETIONS_COUNT, Q_INIT_HEAD_COMPLETIONS},
	{Q_STATEMENT_HEAD_COMPLETIONS_COUNT, Q_STATEMENT_HEAD_COMPLETIONS},
	{Q_TOP_LEVEL_HEAD_COMPLETIONS_COUNT, Q_TOP_LEVEL_HEAD_COMPLETIONS},
	{Q_TYPESPEC_HEAD_COMPLETIONS_COUNT, Q_TYPESPEC_HEAD_COMPLETIONS},
};




#endif //__COMPLETIONS_H_
