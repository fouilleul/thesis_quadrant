/************************************************************//**
*
*	@file: generator.cpp
*	@author: Martin Fouilleul
*	@date: 07/07/2022
*	@revision:
*
*****************************************************************/

#include"workspace.h"
#include"ir.h"
#include"bytecode.h"

typedef struct qc_foreign_sym
{
	list_elt listElt;
	mp_string name;
	ir_type* signature;

} qc_foreign_sym;

typedef struct qc_foreign_dep
{
	list_elt listElt;
	mp_string path;
	list_info functions;

} qc_foreign_dep;

typedef struct qc_loc
{
	u64 parentID;
	u64 leftFromID;
} qc_loc;

typedef struct qc_loc_entry
{
	u64 moduleID;
	u64 id;
	u64 offset;
} qc_loc_entry;

typedef struct gen_context
{
	mem_arena* arena;
	q_program* program;
	list_info lrefs;
	list_info locs;

	u64 targetBlockCount;
	list_info targetBlocks;
	list_info pendingTargetBlocks;

	u64 moduleIndex;

} gen_context;

typedef struct gen_lref
{
	list_elt listElt;
	u64 offset;
	ir_symbol* symbol;
} gen_lref;

typedef struct gen_loc_elt
{
	list_elt listElt;
	qc_loc_entry entry;

} gen_loc_elt;

///////////////////////////////////////////////////////

typedef struct qc_target_block
{
	u64 targetOffset;
	u64 moduleID;
	qc_loc start;
	qc_loc end;
} qc_target_block;

typedef struct gen_target_block_elt
{
	list_elt listElt;
	qc_target_block block;

} gen_target_block_elt;

///////////////////////////////////////////////////////

//----------------------------------------------------------------------------------
// Bytecode emitting functions
//----------------------------------------------------------------------------------
#define DEF_GEN_PUSH(type) \
void _cat2_(gen_push_, type) (gen_context* context, type value) \
{                                                               \
	type* ptr = mem_arena_alloc_type(context->arena, type); \
	*ptr = value;                                               \
}

DEF_GEN_PUSH(u8)
DEF_GEN_PUSH(u16)
DEF_GEN_PUSH(u32)
DEF_GEN_PUSH(u64)
DEF_GEN_PUSH(f32)
DEF_GEN_PUSH(f64)

//----------------------------------------------------------------------------------
// Source location generation
//----------------------------------------------------------------------------------
i64 gen_get_code_offset(gen_context* context)
{
	return(context->arena->offset - context->program->codeOffset);
}

void gen_loc(gen_context* context, ir_node* node)
{
	gen_loc_elt* elt = mem_arena_alloc_type(mem_scratch(), gen_loc_elt);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//TODO: handle locations that are at the end of a block, and not pointing to any cell, eg. implicit returns
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	elt->entry.moduleID = context->moduleIndex;
	elt->entry.id = node->ast->range.start.leftFrom ? node->ast->range.start.leftFrom->id : 0;
	elt->entry.offset = gen_get_code_offset(context);
	context->program->locCount++;
	ListAppend(&context->locs, &elt->listElt);
}

void gen_begin_target_block(gen_context* context, qed_point point)
{
	gen_target_block_elt* elt = mem_arena_alloc_type(mem_scratch(), gen_target_block_elt);
	elt->block.targetOffset = gen_get_code_offset(context);
	elt->block.moduleID = context->moduleIndex;
	elt->block.start.parentID = point.parent ? point.parent->id : 0;
	elt->block.start.leftFromID = point.leftFrom ? point.leftFrom->id : 0;

	ListAppend(&context->pendingTargetBlocks, &elt->listElt);

	gen_push_u8(context, OP_TRACE);
}

void gen_end_pending_target_blocks(gen_context* context, qed_point point)
{
	//NOTE(martin): set end of each pending blocks and append them to the target blocks list
	//              we also prune empty blocks here.
	for_each_in_list_safe(&context->pendingTargetBlocks, elt, gen_target_block_elt, listElt)
	{
		elt->block.end.parentID = point.parent ? point.parent->id : 0;
		elt->block.end.leftFromID = point.leftFrom ? point.leftFrom->id : 0;
		ListRemove(&context->pendingTargetBlocks, &elt->listElt);

		if(  elt->block.start.parentID != elt->block.end.parentID
		  || elt->block.start.leftFromID != elt->block.end.leftFromID )
		{
			ListAppend(&context->targetBlocks, &elt->listElt);
			context->targetBlockCount++;
		}
	}
}

//----------------------------------------------------------------------------------
// Serializing types
//----------------------------------------------------------------------------------

void gen_type_info(gen_context* context, bc_type* bcType, ir_type* irType)
{
	irType->offset = (u64)(((char*)bcType - (char*)context->arena->ptr) - context->program->rodataOffset);

	printf("generating type %.*s at offset %llu\n", (int)irType->name.len, irType->name.ptr, irType->offset);

	irType = ir_type_unwrap(irType);
	bcType->kind = (bc_type_kind)irType->kind;
	bcType->size = irType->size;
	bcType->align = irType->align;

	switch(irType->kind)
	{
		case TYPE_VOID:
		case TYPE_ANY:
			break;

      	case TYPE_PRIMITIVE:
			bcType->primitive = (bc_type_primitive)irType->primitive;
			break;

		case TYPE_ARRAY:
			bcType->eltCount = irType->eltCount;
			//NOTE(martin): deliberate fall-through

		case TYPE_POINTER:
		case TYPE_FUTURE:
		case TYPE_SLICE:
		{
			bc_type* eltType = mem_arena_alloc_type(context->arena, bc_type);
			gen_type_info(context, eltType, irType->eltType);
			bc_set_relptr(bcType->eltType, eltType);
		} break;

		case TYPE_STRUCT:
		{
			bc_type_field* fields = mem_arena_alloc_array(context->arena, bc_type_field, irType->eltCount);
			bc_set_relptr(bcType->fields.ptr, fields);
			bcType->fields.len = irType->eltCount;

			u32 fieldIndex = 0;
			for_each_in_list(&irType->params, param, ir_param, listElt)
			{
				bc_type_field* bcField = &(fields[fieldIndex]);

				bc_type* bcFieldType = mem_arena_alloc_type(context->arena, bc_type);
				gen_type_info(context, bcFieldType, param->type);

				bc_set_relptr(bcField->type, bcFieldType);

				mp_string name = mp_string_push_copy(context->arena, param->name);
				bc_set_relptr(bcField->name.ptr, name.ptr);
				bcField->name.len = name.len;

				bcField->offset = param->offset;

				fieldIndex++;
			}
		} break;

		default:
			ASSERT(0, "unreachable");
			break;
	}
}

//----------------------------------------------------------------------------------
// Bytecode generation function
//----------------------------------------------------------------------------------

void gen_value_expr(gen_context* context, ir_node* ir);
void gen_node(gen_context* context, ir_node* ir);
void gen_call(gen_context* context, ir_node* ir);

void gen_bool_conversion_status(gen_context* context, ir_type* from)
{
	from = ir_type_unwrap(from);

	gen_push_u8(context, OP_CONST8);
	gen_push_u8(context, 0);

	if(from->kind == TYPE_PRIMITIVE && from->primitive == TYPE_B8)
	{
		gen_push_u8(context, OP_CMPU);
	}
	else if(ir_type_is_integer(from))
	{
		if(ir_type_is_signed(from))
		{
			gen_push_u8(context, OP_CMPS);
		}
		else
		{
			gen_push_u8(context, OP_CMPU);
		}
	}
	else if(from->primitive == TYPE_F32)
	{
		gen_push_u8(context, OP_CMPF);
	}
	else if(from->primitive == TYPE_F64)
	{
		gen_push_u8(context, OP_CMPD);
	}
	else
	{
		DEBUG_ASSERT(0, "should be unreachable");
	}
}

void gen_type_cast(gen_context* context, ir_type* from, ir_type* to)
{
	to = ir_type_unwrap(to);
	from = ir_type_unwrap(from);

	if(from->kind == TYPE_POINTER && to->kind == TYPE_POINTER)
	{
		return;
	}

	if(from->primitive == to->primitive)
	{
		return;
	}

	if(to->kind == TYPE_PRIMITIVE && to->primitive == TYPE_B8)
	{
		//NOTE(martin): conversion to bool is equivalent to a comparison with 0 and a setn
		gen_bool_conversion_status(context, from);
		gen_push_u8(context, OP_SETN);
		return;
	}

	if(ir_type_is_integer(from) && ir_type_is_integer(to))
	{
		if(from->size < to->size && ir_type_is_signed(from))
		{
			//Sign-extend
			switch(from->size)
			{
				case 1:
					gen_push_u8(context, OP_SX8);
					break;
				case 2:
					gen_push_u8(context, OP_SX16);
					break;
				case 4:
					gen_push_u8(context, OP_SX32);
					break;
			}
		}
		else
		{
			//Zero-extend (or discard high bits)
			switch(from->size)
			{
				case 1:
					gen_push_u8(context, OP_ZX8);
					break;
				case 2:
					gen_push_u8(context, OP_ZX16);
					break;
				case 4:
					gen_push_u8(context, OP_ZX32);
					break;
			}
		}
	}
	else if(ir_type_is_integer(from))
	{
		// First convert to u64 or i64, then convert to float
		if(ir_type_is_signed(from))
		{
			gen_type_cast(context, from, ir_type_i64());
			if(to->primitive == TYPE_F32)
			{
				gen_push_u8(context, OP_CVI2F);
			}
			else
			{
				gen_push_u8(context, OP_CVI2D);
			}
		}
		else
		{
			gen_type_cast(context, from, ir_type_u64());
			if(to->primitive == TYPE_F32)
			{
				gen_push_u8(context, OP_CVU2F);
			}
			else
			{
				gen_push_u8(context, OP_CVU2D);
			}
		}
	}
	else if(ir_type_is_integer(to))
	{
		// First convert to i64, then convert to target int type
		if(from->primitive == TYPE_F32)
		{
			gen_push_u8(context, OP_CVF2I);
		}
		else
		{
			gen_push_u8(context, OP_CVD2I);
		}
		gen_type_cast(context, ir_type_i64(), to);
	}
	else
	{
		// Convert between floats
		if(from->primitive == TYPE_F32)
		{
			gen_push_u8(context, OP_CVF2D);
		}
		else
		{
			gen_push_u8(context, OP_CVD2F);
		}
	}
}

void gen_cast(gen_context* context, ir_node* ir)
{
	ir_node* expr = ir_first_child(ir);
	gen_value_expr(context, expr);
	gen_loc(context, ir);
	gen_type_cast(context, expr->type, ir->type);
}

void gen_pointer_nil(gen_context* context, ir_node* node)
{
	gen_push_u8(context, OP_CONST8);
	gen_push_u8(context, 0);
}

void gen_const(gen_context* context, ir_node* ir)
{
	gen_loc(context, ir);

	ir_type* baseType = ir_type_unwrap(ir->type);
	switch(baseType->primitive)
	{
		case TYPE_U8:
		case TYPE_I8:
			gen_push_u8(context, OP_CONST8);
			gen_push_u8(context, ir->valueU64);
			break;

		case TYPE_U16:
		case TYPE_I16:
			gen_push_u8(context, OP_CONST16);
			gen_push_u16(context, ir->valueU64);
			break;

		case TYPE_U32:
		case TYPE_I32:
			gen_push_u8(context, OP_CONST32);
			gen_push_u32(context, ir->valueU64);
			break;

		case TYPE_U64:
		case TYPE_I64:
			gen_push_u8(context, OP_CONST64);
			gen_push_u64(context, ir->valueU64);
			break;

		case TYPE_F32:
			gen_push_u8(context, OP_CONST32);
			gen_push_f32(context, (f32)ir->valueF64);
			break;

		case TYPE_F64:
			gen_push_u8(context, OP_CONST64);
			gen_push_f64(context, ir->valueF64);
			break;

		default:
			DEBUG_ASSERT(0, "unreachable");
			break;
	}

	if(ir_type_is_integer(baseType) && ir_type_is_signed(baseType))
	{
		switch(baseType->size)
		{
			case 1:
				gen_push_u8(context, OP_SX8);
				break;
			case 2:
				gen_push_u8(context, OP_SX16);
				break;
			case 4:
				gen_push_u8(context, OP_SX32);
				break;
		}
	}
}

void gen_neg(gen_context* context, ir_node* ir)
{
	ir_node* opd = ir_first_child(ir);
	gen_value_expr(context, opd);

	gen_loc(context, ir);

	ir_type* baseType = ir_type_unwrap(ir->type);
	if(ir_type_is_integer(baseType))
	{
		gen_push_u8(context, OP_NEG);
	}
	else if(baseType->primitive == TYPE_F32)
	{
		gen_push_u8(context, OP_FNEGS);
	}
	else
	{
		gen_push_u8(context, OP_FNEGD);
	}
}

void gen_arithmetic_binop(gen_context* context, ir_node* ir)
{
	ir_node* lhs = ir_first_child(ir);
	ir_node* rhs = ir_next_sibling(lhs);

	gen_value_expr(context, lhs);
	gen_value_expr(context, rhs);

	DEBUG_ASSERT(ir->kind >= IR_ADD && ir->kind <= IR_MOD);

	gen_loc(context, ir);

	ir_type* baseType = ir_type_unwrap(ir->type);
	vm_opcode base = OP_NOP;
	if(ir_type_is_integer(baseType))
	{
		base = OP_ADD;
	}
	else if(baseType->primitive == TYPE_F32)
	{
		base = OP_FADDS;
	}
	else
	{
		base = OP_FADDD;
	}

	vm_opcode opcode = (vm_opcode)(base + ((int)ir->kind - (int)IR_ADD));
	if(opcode == OP_DIV && ir_type_is_signed(baseType))
	{
		opcode = OP_SDIV;
	}

	gen_push_u8(context, opcode);
}

void gen_cmp_status(gen_context* context, ir_node* ir)
{
	ir_node* lhs = ir_first_child(ir);
	ir_node* rhs = ir_next_sibling(lhs);

	gen_value_expr(context, lhs);
	gen_value_expr(context, rhs);

	gen_loc(context, ir);
	ir_type* baseType = ir_type_unwrap(lhs->type);

	if(ir_type_is_integer(baseType))
	{
		if(ir_type_is_signed(baseType))
		{
			gen_push_u8(context, OP_CMPS);
		}
		else
		{
			gen_push_u8(context, OP_CMPU);
		}
	}
	else if(baseType->primitive == TYPE_F32)
	{
		gen_push_u8(context, OP_CMPF);
	}
	else if(baseType->primitive == TYPE_F64)
	{
		gen_push_u8(context, OP_CMPD);
	}
	else
	{
		DEBUG_ASSERT(0, "should be unreachable");
	}
}

void gen_comparison(gen_context* context, ir_node* ir)
{
	/*NOTE(martin):
		This is called for naked comparisons or comparisons that are used in a store operation
	    We use the pattern cmp, set{e,n,l,g,le,ge} to leave the result of the comparison on the
	    stack.
	*/
	gen_cmp_status(context, ir);

	vm_opcode opcode = (vm_opcode)(OP_SETE + (ir->kind - IR_EQ));
	gen_push_u8(context, opcode);
}


void gen_patch_jump_operand(gen_context* context, u64 label, u64 patchLoc)
{
	char* code = context->arena->ptr + context->program->codeOffset;
	*(u32*)(code + patchLoc) = label - (patchLoc + 4);
}

void gen_patch_jump_operand_list(gen_context* context, u64 label, list_info* patchList)
{
	for_each_in_list(patchList, lref, gen_lref, listElt)
	{
		gen_patch_jump_operand(context, label, lref->offset);
	}
}

void gen_short_circuit(gen_context* context, ir_node* ir, bool fallCondition, list_info* patchList)
{
	/*NOTE(martin):
		Generate short-circuit logic so that 'fallCondition' falls to the next instruction and '!fallCondition' jumps to another location.
		Jumps operands offsets are added to patchList to be patched to the desired location upstream
	*/

	//NOTE: locs are inserted in gen_cmp_status()

	switch(ir->kind)
	{
		//NOTE(martin): for comparison operation, negate the condition on the jump
		case IR_EQ:
			gen_cmp_status(context, ir);
			gen_push_u8(context, fallCondition ? OP_JN : OP_JE);
			break;
		case IR_NEQ:
			gen_cmp_status(context, ir);
			gen_push_u8(context, fallCondition ? OP_JE : OP_JN);
			break;
		case IR_LT:
			gen_cmp_status(context, ir);
			gen_push_u8(context, fallCondition ? OP_JGE : OP_JLT);
			break;
		case IR_GT:
			gen_cmp_status(context, ir);
			gen_push_u8(context, fallCondition ? OP_JLE : OP_JGT);
			break;
		case IR_LE:
			gen_cmp_status(context, ir);
			gen_push_u8(context, fallCondition ? OP_JGT : OP_JLE);
			break;
		case IR_GE:
			gen_cmp_status(context, ir);
			gen_push_u8(context, fallCondition ? OP_JLT : OP_JGE);
			break;

		//NOTE(martin): for conversion operation, generate a cmp with 0, and jump if non zero (when fallCondition == true),
		//              or if zero (when fallCondition == false).
		case IR_CAST:
			gen_value_expr(context, ir_first_child(ir));
			gen_loc(context, ir);
			gen_bool_conversion_status(context, ir->type);
			gen_push_u8(context, fallCondition ? OP_JE : OP_JN);
			break;

		//NOTE(martin): logical connectives, construct short circuit from two smaller short-circuits
		case IR_LAND:
		{
			ir_node* lhs = ir_first_child(ir);
			ir_node* rhs = ir_next_sibling(lhs);

			if(fallCondition)
			{
				gen_short_circuit(context, lhs, true, patchList);
				gen_short_circuit(context, rhs, true, patchList);
			}
			else
			{
				list_info patchEnd;
				ListInit(&patchEnd);
				gen_short_circuit(context, lhs, true, &patchEnd);
				gen_short_circuit(context, rhs, false, patchList);
				gen_patch_jump_operand_list(context, gen_get_code_offset(context), &patchEnd);
			}
			return;
		}

		case IR_LOR:
		{
			ir_node* lhs = ir_first_child(ir);
			ir_node* rhs = ir_next_sibling(lhs);

			if(fallCondition)
			{
				list_info patchEnd;
				ListInit(&patchEnd);
				gen_short_circuit(context, lhs, false, &patchEnd);
				gen_short_circuit(context, rhs, true, patchList);
				gen_patch_jump_operand_list(context, gen_get_code_offset(context), &patchEnd);
			}
			else
			{
				gen_short_circuit(context, lhs, false, patchList);
				gen_short_circuit(context, rhs, false, patchList);
			}
			return;
		}

		case IR_LNOT:
			gen_short_circuit(context, ir_first_child(ir), !fallCondition, patchList);
			return;

		default:
			ASSERT(0, "should be unreachable");
	}

	//NOTE(martin): add patch location and push dummy target
	gen_lref* lref = mem_arena_alloc_type(mem_scratch(), gen_lref);
	lref->offset = gen_get_code_offset(context);
	ListAppend(patchList, &lref->listElt);

	gen_push_u32(context, 0);
}

void gen_eval_short_circuit(gen_context* context, ir_node* ir)
{
	/*NOTE(martin):
		- generate short-circuit logic so that 'true' falls to the next instruction
		- push 1 on opstack in true branch and jump to end
		- push 0 on opstack in false branch
		- patch jump operands
	*/
	gen_loc(context, ir);

	list_info patchList;
	ListInit(&patchList);

	gen_short_circuit(context, ir, true, &patchList);
	gen_push_u8(context, OP_CONST8);
	gen_push_u8(context, 1);
	gen_push_u8(context, OP_JMP);
	u64 patchJmp = gen_get_code_offset(context);
	gen_push_u32(context, 0);

	u64 elseLbl = gen_get_code_offset(context);
	gen_push_u8(context, OP_CONST8);
	gen_push_u8(context, 0);
	u64 endLbl = gen_get_code_offset(context);

	//NOTE(martin): patch jumps in the short-circuit logic and unconditional jump to end
	gen_patch_jump_operand_list(context, elseLbl, &patchList);
	gen_patch_jump_operand(context, endLbl, patchJmp);
}

void gen_var_from_symbol(gen_context* context, ir_symbol* symbol)
{
	if(symbol->global)
	{
		//NOTE(martin): globals are put at a fixed offset after the rodata section,
		//              so we add this section's size to the variable offset
		gen_push_u8(context, OP_CONST64);
		gen_push_u64(context, context->program->rodataSize + symbol->offset);
	}
	else if(symbol->capture.level)
	{
		//NOTE(martin): variable is a capture, so we need to load the content of the pointer
		//              on the operand stack
		gen_push_u8(context, OP_LOCAL);
		gen_push_u64(context, symbol->offset);
		gen_push_u8(context, OP_LOAD64);
	}
	else
	{
		//NOTE(martin): local variables offsets are relative to the current frame pointer
		gen_push_u8(context, OP_LOCAL);
		gen_push_u64(context, symbol->offset);
	}
}

void gen_var(gen_context* context, ir_node* ir)
{
	gen_loc(context, ir);
	gen_var_from_symbol(context, ir->symbol);
}

void gen_array_index(gen_context* context, ir_node* ir)
{
	ir_node* index = ir_first_child(ir);
	ir_node* array = ir_next_sibling(index);

	//TODO: generate bounds check in debug mode?
	gen_node(context, array);
	gen_value_expr(context, index);

	gen_loc(context, ir);

	ir_type* baseArrayType = ir_type_unwrap(array->type);
	if(baseArrayType->kind == TYPE_ARRAY)
	{
		ir_type* baseEltType = ir_type_unwrap(baseArrayType->eltType);

		gen_push_u8(context, OP_CONST64);
		gen_push_u64(context, baseEltType->size);
		gen_push_u8(context, OP_MUL);
		gen_push_u8(context, OP_ADD);
	}
	else if(baseArrayType->kind == TYPE_SLICE)
	{
		ir_type* baseEltType = ir_type_unwrap(baseArrayType->eltType);

		gen_push_u8(context, OP_SLICE_INDEX);
		gen_push_u64(context, baseEltType->size);
	}
	else
	{
		ASSERT(0, "unreachable");
	}
}

void gen_array_len(gen_context* context, ir_node* ir)
{
	ir_node* object = ir_first_child(ir);
	ir_type* objectBaseType = ir_type_unwrap(object->type);

	if(objectBaseType->kind == TYPE_ARRAY)
	{
		gen_loc(context, ir);
		gen_push_u8(context, OP_CONST64);
		gen_push_u64(context, objectBaseType->eltCount);
	}
	else if(objectBaseType->kind == TYPE_SLICE)
	{
		gen_node(context, object);
		gen_loc(context, ir);
		gen_push_u8(context, OP_LOAD64);
	}
	else
	{
		ASSERT(0, "unreachable");
	}
}

void gen_struct_field(gen_context* context, ir_node* ir)
{
	ir_node* object = ir_first_child(ir);
	ir_param* param = ir->param;

	gen_node(context, object);

	gen_loc(context, ir);

	gen_push_u8(context, OP_CONST64);
	gen_push_u64(context, param->offset);
	gen_push_u8(context, OP_ADD);
}

void gen_slice(gen_context* context, ir_node* ir)
{
	//NOTE: we assume here that destination has been generated by upstream nodes

	ir_node* start = ir_first_child(ir);
	ir_node* end = ir_next_sibling(start);
	ir_node* object = ir_next_sibling(end);

	gen_value_expr(context, start);
	gen_value_expr(context, end);
	gen_node(context, object);

	gen_loc(context, ir);

	ir_type* baseType = ir_type_unwrap(object->type);
	if(baseType->kind == TYPE_ARRAY)
	{
		gen_push_u8(context, OP_SLICE_ARRAY);
	}
	else if(baseType->kind == TYPE_SLICE)
	{
		gen_push_u8(context, OP_RESLICE);
	}
	else
	{
		ASSERT(0, "unreachable");
	}
	ir_type* baseEltType = ir_type_unwrap(baseType->eltType);
	gen_push_u64(context, baseEltType->size);
}

void gen_ref(gen_context* context, ir_node* ir)
{
	gen_node(context, ir_first_child(ir));
}

void gen_deref(gen_context* context, ir_node* ir)
{
	gen_value_expr(context, ir_first_child(ir));
}

void gen_store_from_type(gen_context* context, ir_type* type)
{
	ir_type* baseType = ir_type_unwrap(type);
	if(baseType->kind == TYPE_PRIMITIVE)
	{
		switch(baseType->size)
		{
			case 1:
				gen_push_u8(context, OP_STORE8);
				break;
			case 2:
				gen_push_u8(context, OP_STORE16);
				break;
			case 4:
				gen_push_u8(context, OP_STORE32);
				break;
			case 8:
				gen_push_u8(context, OP_STORE64);
				break;

			default:
				ASSERT(0, "unreachable");
		}
	}
	else if(baseType->kind == TYPE_ARRAY)
	{
		gen_push_u8(context, OP_COPY);
		gen_push_u64(context, baseType->size);
	}
	else if(baseType->kind == TYPE_SLICE)
	{
		gen_push_u8(context, OP_COPY);
		gen_push_u64(context, 16);
	}
	else if(baseType->kind == TYPE_ANY)
	{
		gen_push_u8(context, OP_COPY);
		gen_push_u64(context, 16);
	}
	else if(baseType->kind == TYPE_STRUCT)
	{
		gen_push_u8(context, OP_COPY);
		gen_push_u64(context, baseType->size);
	}
	else if(  baseType->kind == TYPE_FUTURE
	       || baseType->kind == TYPE_POINTER)
	{
		gen_push_u8(context, OP_STORE64);
	}
	else
	{
		ASSERT(0, "unreachable");
	}
}

void gen_array_init(gen_context* context, ir_node* ir);
void gen_box(gen_context* context, ir_node* ir);

void gen_store_rhs_and_op(gen_context* context, ir_node* ir, ir_node* rhs)
{
	if(rhs->kind == IR_ARRAY_INIT)
	{
		gen_loc(context, ir);		//TODO: is that the right place?
		gen_array_init(context, rhs);
	}
	else if(rhs->kind == IR_SLICE)
	{
		gen_loc(context, ir);
		gen_slice(context, rhs);
	}
	else if(rhs->kind == IR_BOX)
	{
		gen_loc(context, ir);
		gen_box(context, rhs);
	}
	else
	{
		gen_value_expr(context, rhs);
		gen_loc(context, ir);
		gen_store_from_type(context, rhs->type);
	}
}

void gen_array_init(gen_context* context, ir_node* ir)
{
	//WARN: we assume the target base address has already been computed here
	gen_loc(context, ir);
	ir_type* baseEltType = ir_type_unwrap(ir->type->eltType);

	u64 eltIndex = 0;
	for_each_in_list(&ir->children, child, ir_node, listElt)
	{
		if(child != ir_last_child(ir))
		{
			gen_push_u8(context, OP_DUP);
		}

		gen_push_u8(context, OP_CONST64);
		gen_push_u64(context, eltIndex);
		gen_push_u8(context, OP_CONST64);
		gen_push_u64(context, baseEltType->size);
		gen_push_u8(context, OP_MUL);
		gen_push_u8(context, OP_ADD);

		gen_store_rhs_and_op(context, ir, child);

		eltIndex++;
	}
}



void gen_store(gen_context* context, ir_node* ir)
{
	ir_node* lhs = ir_first_child(ir);
	ir_node* rhs = ir_next_sibling(lhs);

	if(rhs->kind == IR_ARRAY_INIT
	  && rhs->childCount == 0)
	{
		return;
	}
	else if(rhs->kind == IR_CALL)
	{
		ir_type* baseRetType = ir_type_unwrap(rhs->type);
		if(  baseRetType->kind == TYPE_STRUCT
		  || baseRetType->kind == TYPE_SLICE
		  || baseRetType->kind == TYPE_ARRAY)
		{
			gen_push_u8(context, OP_ARG);
			gen_push_u64(context, 0);
			gen_node(context, lhs);
			gen_call(context, rhs);

			//NOTE: proc that return a struct leave the implicit return pointer on the stack.
			//      here since we're not reusing the pointer, we must add a pop.
			gen_push_u8(context, OP_POP);
		}
		else
		{
			gen_node(context, lhs);
			gen_store_rhs_and_op(context, ir, rhs);
		}
	}
	else
	{
		gen_node(context, lhs);
		gen_store_rhs_and_op(context, ir, rhs);
	}
}

void gen_tstore(gen_context* context, ir_node* ir)
{
	ir_node* lhs = ir_first_child(ir);
	ir_node* rhs = ir_next_sibling(lhs);

	if(rhs->kind == IR_ARRAY_INIT
	  && rhs->childCount == 0)
	{
		gen_node(context, lhs);
		return;
	}
	else if(rhs->kind == IR_CALL)
	{
		ir_type* baseRetType = ir_type_unwrap(rhs->type);
		if(  baseRetType->kind == TYPE_STRUCT
		  || baseRetType->kind == TYPE_SLICE
		  || baseRetType->kind == TYPE_ARRAY)
		{
			gen_push_u8(context, OP_ARG);
			gen_push_u64(context, 0);
			gen_node(context, lhs);

			//WARN: we generate a store for the return pointer in gen_call_arguments()
			gen_call(context, rhs);

			// pop if value is not used?
		}
		else
		{
			gen_node(context, lhs);
			gen_push_u8(context, OP_DUP);
			gen_store_rhs_and_op(context, ir, rhs);
		}
	}
	else
	{
		gen_node(context, lhs);
		gen_push_u8(context, OP_DUP);
		gen_store_rhs_and_op(context, ir, rhs);
	}
}

void gen_box(gen_context* context, ir_node* ir)
{
	//NOTE: we assume destination has already been generated by upstream node
	ir_node* src = ir_first_child(ir);

	gen_push_u8(context, OP_DUP);

	//NOTE(martin): put address of src in dst first field
	gen_node(context, src);
	gen_push_u8(context, OP_STORE64);

	//NOTE(martin): put address of type in dst second field
	gen_push_u8(context, OP_CONST8);
	gen_push_u8(context, 8);
	gen_push_u8(context, OP_ADD);
	gen_push_u8(context, OP_CONST64);
	gen_push_u64(context, src->type->offset);
	gen_push_u8(context, OP_STORE64);
}

void gen_rodata_ref(gen_context* context, ir_node* ir)
{
	gen_loc(context, ir);

	u64 offset = ir->symbol->offset;
	gen_push_u8(context, OP_CONST64);
	gen_push_u64(context, offset);
}

void gen_rodata_symbol(gen_context* context, ir_symbol* symbol)
{
	mem_arena* arena = context->arena;
	u8* rodataStart = (u8*)arena->ptr + context->program->rodataOffset;
	memcpy(rodataStart + symbol->offset, symbol->data.ptr, symbol->data.len);
}

void gen_put(gen_context* context, ir_node* ir)
{
	for_each_in_list(&ir->children, expr, ir_node, listElt)
	{
		gen_value_expr(context, expr);

		//TODO: loc?

		ir_type* baseType = ir_type_unwrap(expr->type);
		if(baseType->kind == TYPE_PRIMITIVE)
		{
			if(ir_type_is_integer(baseType))
			{
				if(ir_type_is_signed(baseType))
				{
					gen_push_u8(context, OP_PUTI);
				}
				else
				{
					gen_push_u8(context, OP_PUTU);
				}
			}
			else if(baseType->primitive == TYPE_F32)
			{
				gen_push_u8(context, OP_PUTF);
			}
			else
			{
				gen_push_u8(context, OP_PUTD);
			}
		}
		else if(baseType->kind == TYPE_SLICE)
		{
			gen_push_u8(context, OP_PUTS);
		}
		else
		{
			ASSERT(0, "unreachable");
		}
	}
	gen_push_u8(context, OP_PUTNL);
}

void gen_value_expr(gen_context* context, ir_node* ir)
{
	gen_node(context, ir);

	ir_type* baseType = ir_type_unwrap(ir->type);

	if( ( ir->kind == IR_VAR
	    ||ir->kind == IR_TSTORE
	    ||ir->kind == IR_AT
	    ||ir->kind == IR_FIELD
	    ||ir->kind == IR_DEREF)
	  &&( baseType->kind == TYPE_PRIMITIVE
	    ||baseType->kind == TYPE_POINTER
	    ||baseType->kind == TYPE_FUTURE))
	{
		switch(baseType->size)
		{
			case 1:
				gen_push_u8(context, OP_LOAD8);
				if(ir_type_is_integer(baseType) && ir_type_is_signed(baseType))
				{
					gen_push_u8(context, OP_SX8);
				}
				break;
			case 2:
				gen_push_u8(context, OP_LOAD16);
				if(ir_type_is_integer(baseType) && ir_type_is_signed(baseType))
				{
					gen_push_u8(context, OP_SX16);
				}
				break;
			case 4:
				gen_push_u8(context, OP_LOAD32);
				if(ir_type_is_integer(baseType) && ir_type_is_signed(baseType))
				{
					gen_push_u8(context, OP_SX32);
				}
				break;
			case 8:
				gen_push_u8(context, OP_LOAD64);
				break;
			default:
				ASSERT(0, "unreachable");
		}
	}
}

void gen_cond(gen_context* context, ir_node* ir)
{
	//TODO: split if/if_else ?
	ir_node* condition = ir_first_child(ir);
	ir_node* branchIf = ir_next_sibling(condition);
	ir_node* branchElse = ir_next_sibling(branchIf);

	/*NOTE(martin): generate this bytecode template:
		<test expr>
		jz targetElse
		<branchIf>
		jmp targetEnd
		targetElse:
		<branchElse>
		targetEnd:
	*/

	gen_loc(context, ir);

	list_info patchList;
	ListInit(&patchList);

	gen_end_pending_target_blocks(context, condition->ast->range.end);

	switch(condition->kind)
	{
		//NOTE(martin): if the condition expression is a comparison, we emit a cmp,
		//              and use the conditional jmp that _negates_ the condition, in
		//              order to skip the ifBranch if the condition is false.
		case IR_EQ:
			gen_cmp_status(context, condition);
			gen_push_u8(context, OP_JN);
			gen_push_u32(context, 0);
			break;
		case IR_NEQ:
			gen_cmp_status(context, condition);
			gen_push_u8(context, OP_JE);
			gen_push_u32(context, 0);
			break;
		case IR_LT:
			gen_cmp_status(context, condition);
			gen_push_u8(context, OP_JGE);
			gen_push_u32(context, 0);
			break;
		case IR_GT:
			gen_cmp_status(context, condition);
			gen_push_u8(context, OP_JLE);
			gen_push_u32(context, 0);
			break;
		case IR_LE:
			gen_cmp_status(context, condition);
			gen_push_u8(context, OP_JGT);
			gen_push_u32(context, 0);
			break;
		case IR_GE:
			gen_cmp_status(context, condition);
			gen_push_u8(context, OP_JLT);
			gen_push_u32(context, 0);
			break;

		case IR_CAST:
			gen_value_expr(context, ir_first_child(condition));
			gen_loc(context, condition);
			gen_bool_conversion_status(context, condition->type);
			gen_push_u8(context, OP_JE);
			gen_push_u32(context, 0);
			break;

		//NOTE: if node is a logic operator, generate short circuit in 'fall if true' configuration
		case IR_LAND:
		case IR_LOR:
		case IR_LNOT:
			gen_short_circuit(context, condition, true, &patchList);
			break;

		//NOTE(martin): by default, we evaluate the expression, compare to zero and jump if result is equal

		//TODO: only comparison, logic op, load and conversion can produce a bool value, so we should cover these cases
		//      and assert that the other are unreachable
		default:
			gen_value_expr(context, condition);
			gen_push_u8(context, OP_CONST8);
			gen_push_u8(context, 0);
			gen_push_u8(context, OP_CMPU);
			gen_push_u8(context, OP_JE);
			gen_push_u32(context, 0);
			break;
	}

	u64 patch1 = gen_get_code_offset(context) - 4;

	gen_begin_target_block(context, branchIf->ast->range.start);

	gen_node(context, branchIf);

	if(branchElse)
	{
		gen_end_pending_target_blocks(context, branchIf->ast->range.end);

		//NOTE(martin): emit unconditional jmp to end, then emit else branch
		gen_push_u8(context, OP_JMP);
		u64 patch2 = gen_get_code_offset(context);
		gen_push_u32(context, 0);

		u64 target1 = gen_get_code_offset(context);
		gen_begin_target_block(context, branchElse->ast->range.start);
		gen_node(context, branchElse);
		u64 target2 = gen_get_code_offset(context);

		//NOTE(martin): patch
		if(condition->kind == IR_LAND || condition->kind == IR_LOR || condition->kind == IR_LNOT)
		{
			gen_patch_jump_operand_list(context, target1, &patchList);
		}
		else
		{
			gen_patch_jump_operand(context, target1, patch1);
		}
		gen_patch_jump_operand(context, target2, patch2);
		gen_end_pending_target_blocks(context, branchElse->ast->range.end);
	}
	else
	{
		u64 target1 = gen_get_code_offset(context);
		if(condition->kind == IR_LAND || condition->kind == IR_LOR)
		{
			gen_patch_jump_operand_list(context, target1, &patchList);
		}
		else
		{
			gen_patch_jump_operand(context, target1, patch1);
		}
	}
	gen_begin_target_block(context, ir->ast->range.end);
}

void gen_loop(gen_context* context, ir_node* ir, ir_node* init, ir_node* condition, ir_node* increment, ir_node* body)
{
	/*NOTE(martin): generate this bytecode template:

			<init>
			jmp targetCond
		targetStart:
			<body>
			<increment>
		targetCond:
			<test cond>
			jnz targetStart
	*/
	gen_loc(context, ir);

	////////////////////////////////////////////////////
	//TODO blocks
	////////////////////////////////////////////////////
	if(init)
	{
		gen_node(context, init);
	}

	gen_push_u8(context, OP_JMP);
	i64 patchJmp = gen_get_code_offset(context);
	gen_push_u32(context, 0);

	i64 targetStart = gen_get_code_offset(context);
	gen_begin_target_block(context, body->ast->range.start);
	gen_node(context, body);
	gen_end_pending_target_blocks(context, body->ast->range.end);

	if(increment)
	{
		gen_node(context, increment);
	}

	gen_patch_jump_operand(context, gen_get_code_offset(context), patchJmp);

	gen_begin_target_block(context, condition->ast->range.start);

	switch(condition->kind)
	{
		//NOTE(martin): if the condition expression is a comparison, we emit a cmp,
		//              and conditionally jump to the start of the body if the condition is true.
		case IR_EQ:
			gen_cmp_status(context, condition);
			gen_push_u8(context, OP_JE);
			gen_push_u32(context, targetStart - (gen_get_code_offset(context)+4));
			break;
		case IR_NEQ:
			gen_cmp_status(context, condition);
			gen_push_u8(context, OP_JN);
			gen_push_u32(context, targetStart - (gen_get_code_offset(context)+4));
			break;
		case IR_LT:
			gen_cmp_status(context, condition);
			gen_push_u8(context, OP_JLT);
			gen_push_u32(context, targetStart - (gen_get_code_offset(context)+4));
			break;
		case IR_GT:
			gen_cmp_status(context, condition);
			gen_push_u8(context, OP_JGT);
			gen_push_u32(context, targetStart - (gen_get_code_offset(context)+4));
			break;
		case IR_LE:
			gen_cmp_status(context, condition);
			gen_push_u8(context, OP_JLE);
			gen_push_u32(context, targetStart - (gen_get_code_offset(context)+4));
			break;
		case IR_GE:
			gen_cmp_status(context, condition);
			gen_push_u8(context, OP_JGE);
			gen_push_u32(context, targetStart - (gen_get_code_offset(context)+4));
			break;

		case IR_CAST:
		{
			ir_node* opd = ir_first_child(condition);
			gen_value_expr(context, opd);
			gen_loc(context, condition);
			gen_bool_conversion_status(context, opd->type);
			gen_push_u8(context, OP_JN);
			gen_push_u32(context, targetStart - (gen_get_code_offset(context)+4));
		} break;

		//NOTE: if node is a logic operator, generate short circuit in 'fall if false' configuration
		case IR_LAND:
		case IR_LOR:
		case IR_LNOT:
		{
			list_info patchList = {};
			gen_short_circuit(context, condition, false, &patchList);
			gen_patch_jump_operand_list(context, targetStart, &patchList);
		} break;

		//NOTE(martin): by default, we evaluate the expression, compare to zero and jump if result is not equal

		//TODO: only comparison, logic op, load and conversion can produce a bool value, so we should cover these cases
		//      and assert that the other are unreachable
		default:
			gen_value_expr(context, condition);
			gen_push_u8(context, OP_CONST8);
			gen_push_u8(context, 0);
			gen_push_u8(context, OP_CMPU);
			gen_push_u8(context, OP_JN);
			gen_push_u32(context, targetStart - (gen_get_code_offset(context)+4));
			break;
	}
	gen_end_pending_target_blocks(context, condition->ast->range.end);
	gen_begin_target_block(context, ir->ast->range.end);
}

void gen_while(gen_context* context, ir_node* ir)
{
	ir_node* cond = ir_first_child(ir);
	ir_node* body = ir_next_sibling(cond);

	gen_loop(context, ir, 0, cond, 0, body);
}

void gen_for(gen_context* context, ir_node* ir)
{
	ir_node* init = ir_first_child(ir);
	ir_node* cond = ir_next_sibling(init);
	ir_node* increment = ir_next_sibling(cond);
	ir_node* body = ir_next_sibling(increment);
	gen_loop(context, ir, init, cond, increment, body);
}

u64 gen_call_arguments(gen_context* context, ir_node* ir, bool foreignCall)
{
	u64 argOffset = 0;
	ir_type* baseRetType = ir_type_unwrap(ir->type);
	if(  baseRetType->kind == TYPE_STRUCT
	  || baseRetType->kind == TYPE_SLICE
	  || baseRetType->kind == TYPE_ARRAY)
	{
		argOffset = 8;
	}

	for_each_in_list(&ir->children, arg, ir_node, listElt)
	{
		ir_type* argBaseType = ir_type_unwrap(arg->type);
		argOffset = AlignUpOnPow2(argOffset, argBaseType->align);

		gen_push_u8(context, OP_ARG);
		gen_push_u64(context, argOffset); //TODO: ensure we have done the param layout
		gen_value_expr(context, arg);

		argOffset += argBaseType->size;
	}

	for_each_in_list_reverse(&ir->children, arg, ir_node, listElt)
	{
	//////////////////////////////////////////////////////////////////
	//TODO: coalesce with gen_store_from_type() ?
	//////////////////////////////////////////////////////////////////
		ir_type* baseType = ir_type_unwrap(arg->type);
		if(baseType->kind == TYPE_PRIMITIVE)
		{
			switch(baseType->size)
			{
				case 1:
					gen_push_u8(context, OP_STORE8);
					break;
				case 2:
					gen_push_u8(context, OP_STORE16);
					break;
				case 4:
					gen_push_u8(context, OP_STORE32);
					break;
				case 8:
					gen_push_u8(context, OP_STORE64);
					break;
				default:
					ASSERT(0, "unreachable");
			}
		}
		else if(baseType->kind == TYPE_SLICE)
		{
			gen_push_u8(context, OP_COPY);
			gen_push_u64(context, 16);
		}
		else if(baseType->kind == TYPE_STRUCT)
		{
			gen_push_u8(context, OP_COPY);
			gen_push_u64(context, baseType->size);
		}
		else if(baseType->kind == TYPE_ANY)
		{
			gen_push_u8(context, OP_COPY);
			gen_push_u64(context, 16);
		}
		else if(baseType->kind == TYPE_FUTURE)
		{
			gen_push_u8(context, OP_STORE64);
		}
		else if(baseType->kind == TYPE_POINTER)
		{
			if(foreignCall)
			{
				gen_push_u8(context, OP_CVPTRQ2N);
			}
			gen_push_u8(context, OP_STORE64);
		}
		else
		{
			ASSERT(0, "unreachable");
		}
	}

	if(  baseRetType->kind == TYPE_STRUCT
	  || baseRetType->kind == TYPE_SLICE
	  || baseRetType->kind == TYPE_ARRAY)
	{
		//NOTE: pass implicit return pointer, that has be generated upstream
		gen_push_u8(context, OP_STORE64);
	}

	return(argOffset);
}

void gen_call(gen_context* context, ir_node* ir)
{
	ir_symbol* procSymbol = ir->symbol;

	u64 argSize = gen_call_arguments(context, ir, (procSymbol->proc.foreign != 0));

	gen_end_pending_target_blocks(context, ir->ast->range.end);

	if(procSymbol->proc.capturedFrame)
	{
		gen_loc(context, ir);
		gen_push_u8(context, OP_TASK);

		//NOTE(martin): reserve start address immediate value
		gen_push_u64(context, 0xdeadbeef);

		gen_lref* lref = mem_arena_alloc_type(mem_scratch(), gen_lref);
		lref->symbol = procSymbol;
		lref->offset = gen_get_code_offset(context) - 8;
		ListAppend(&context->lrefs, &lref->listElt);

		////////////////////////////////////////////////////////
		//TODO review this
		//NOTE(martin): set arg size immediate value
		gen_push_u32(context, argSize);

		//NOTE(martin): set flags
		gen_push_u32(context, OP_TASK_FLAG_CAPTURED_FRAME);

		//NOTE(martin): wait for task to retire and get the result
		gen_push_u8(context, OP_WAIT);
		gen_push_u8(context, 0);

		if(procSymbol->type->retType->kind == TYPE_VOID)
		{
			//NOTE(martin): discard return value
			gen_push_u8(context, OP_POP);
		}
	}
	else if(procSymbol->proc.foreign)
	{
		gen_loc(context, ir);
		gen_push_u8(context, OP_FFI_CALL);
		gen_push_u64(context, procSymbol->proc.foreignIndex);
		if(procSymbol->type->retType->kind == TYPE_POINTER)
		{
			gen_push_u8(context, OP_CVPTRN2Q);
		}
		else if(procSymbol->type->retType->kind == TYPE_VOID)
		{
			gen_push_u8(context, OP_POP);
		}
	}
	else
	{
		//NOTE(martin): this case handles both normal calls and stubs to foreign calls
		gen_loc(context, ir);
		gen_push_u8(context, OP_CALL);
		gen_push_u64(context, 0xdeadbeef);

		gen_lref* lref = mem_arena_alloc_type(mem_scratch(), gen_lref);
		lref->symbol = procSymbol;
		lref->offset = gen_get_code_offset(context) - 8;
		ListAppend(&context->lrefs, &lref->listElt);
	}
	gen_begin_target_block(context, ir->ast->range.end);
}

void gen_return(gen_context* context, ir_node* ir)
{
	ir_node* rhs = ir_first_child(ir);
	if(rhs)
	{
		ir_type* baseType = ir_type_unwrap(rhs->type);
		if(  baseType->kind == TYPE_STRUCT
		  || baseType->kind == TYPE_SLICE
		  || baseType->kind == TYPE_ARRAY)
		{
			if(rhs->kind == IR_ARRAY_INIT
	  		&& rhs->childCount == 0)
			{
				gen_push_u8(context, OP_LOCAL);
				gen_push_u64(context, 0);
				gen_push_u8(context, OP_LOAD64);
			}
			else if(rhs->kind == IR_CALL)
			{
				ir_type* baseRetType = ir_type_unwrap(rhs->type);
				if(  baseRetType->kind == TYPE_STRUCT
		  		|| baseRetType->kind == TYPE_SLICE
		  		|| baseRetType->kind == TYPE_ARRAY)
				{
					gen_push_u8(context, OP_ARG);
					gen_push_u64(context, 0);

					gen_push_u8(context, OP_LOCAL);
					gen_push_u64(context, 0);
					gen_push_u8(context, OP_LOAD64);

					gen_call(context, rhs);

					gen_push_u8(context, OP_LOCAL);
					gen_push_u64(context, 0);
					gen_push_u8(context, OP_LOAD64);
				}
				else
				{
					gen_push_u8(context, OP_LOCAL);
					gen_push_u64(context, 0);
					gen_push_u8(context, OP_LOAD64);
					gen_push_u8(context, OP_DUP);

					gen_store_rhs_and_op(context, ir, rhs);
				}
			}
			else
			{
				gen_push_u8(context, OP_LOCAL);
				gen_push_u64(context, 0);
				gen_push_u8(context, OP_LOAD64);
				gen_push_u8(context, OP_DUP);

				gen_store_rhs_and_op(context, ir, rhs);
			}
		}
		else
		{
			gen_value_expr(context, rhs);
		}
	}
	gen_loc(context, ir);
	gen_push_u8(context, OP_LEAVE);
}

void gen_multi(gen_context* context, ir_node* ir)
{
	for_each_in_list(&ir->children, child, ir_node, listElt)
	{
		gen_node(context, child);

		ir_type* baseType = ir_type_unwrap(child->type);
		if(baseType->kind == TYPE_FUTURE)
		{
			gen_push_u8(context, OP_FDROP);
		}
		else if(baseType->kind != TYPE_VOID)
		{
			gen_push_u8(context, OP_POP);
		}
	}
}

void gen_tempo(gen_context* context, ir_node* ir)
{
	//TODO: differentiate between flat expression (or bpm?) and curve...
	gen_push_u8(context, OP_DUP);

	gen_push_u8(context, OP_CONST64);
	gen_push_u64(context, ir->symbol->offset);

	gen_loc(context, ir);
	gen_push_u8(context, OP_TEMPO);
}

void gen_flow(gen_context* context, ir_node* ir)
{
	ir_type* returnBaseType = ir_type_unwrap(ir->symbol->type->retType);
	if(returnBaseType->kind == TYPE_STRUCT)
	{
		//NOTE(martin): if the async block returns a struct, we allocate space for that struct
		//              and pass it as a first argument
		gen_push_u8(context, OP_ARG);
		gen_push_u64(context, 0);
		gen_push_u8(context, OP_CONST64);
		gen_push_u64(context, returnBaseType->size);
		gen_push_u8(context, OP_ALLOC);

		gen_push_u8(context, OP_STORE64);
	}

	gen_loc(context, ir);
	gen_push_u8(context, OP_TASK);
	gen_push_u64(context, 0xdeadbeef);

	gen_lref* lref = mem_arena_alloc_type(mem_scratch(), gen_lref);
	lref->symbol = ir->symbol;
	lref->offset = gen_get_code_offset(context) - 8;
	ListAppend(&context->lrefs, &lref->listElt);

	u32 argSize = 0;
	u32 flags = OP_TASK_FLAG_NONE;

	if(returnBaseType->kind == TYPE_STRUCT)
	{
		argSize += 8;
		flags |= OP_TASK_FLAG_OWN_RETURN_PTR;
	}
	gen_push_u32(context, argSize);
	gen_push_u32(context, flags);

	ir_node* tempoNode = ir_first_child(ir);
	if(tempoNode)
	{
		gen_tempo(context, tempoNode);
	}

	gen_push_u8(context, OP_CONST64);
	gen_push_f64(context, 0);
	gen_push_u8(context, OP_PAUSE);
}

void gen_standby(gen_context* context, ir_node* ir)
{
	gen_loc(context, ir);
	gen_push_u8(context, OP_STANDBY);
	gen_end_pending_target_blocks(context, ir->ast->range.end);
	gen_begin_target_block(context, ir->ast->range.end);
}

void gen_pause(gen_context* context, ir_node* ir)
{
	ir_node* opd = ir_first_child(ir);
	gen_value_expr(context, opd);
	gen_loc(context, ir);
	gen_push_u8(context, OP_PAUSE);
	gen_end_pending_target_blocks(context, ir->ast->range.end);
	gen_begin_target_block(context, ir->ast->range.end);
}

void gen_wait(gen_context* context, ir_node* ir, bool recursive)
{
	ir_node* expr = ir_first_child(ir);

	ir_type* baseType = ir_type_unwrap(ir->type);
	if(baseType->kind == TYPE_STRUCT)
	{
		if(ir->symbol->global)
		{
			gen_push_u8(context, OP_CONST64);
		}
		else
		{
			gen_push_u8(context, OP_LOCAL);
		}
		gen_push_u64(context, ir->symbol->offset);
		gen_push_u8(context, OP_DUP);
	}

	gen_value_expr(context, expr);
	gen_loc(context, ir);
	gen_push_u8(context, OP_WAIT);

	gen_push_u8(context, recursive ? 1 : 0);
	if(baseType->kind == TYPE_VOID)
	{
		gen_push_u8(context, OP_POP);
	}
	else if(baseType->kind == TYPE_STRUCT)
	{
		gen_push_u8(context, OP_COPY);
		gen_push_u64(context, baseType->size);
	}

	gen_end_pending_target_blocks(context, ir->ast->range.end);
	gen_begin_target_block(context, ir->ast->range.end);
}

void gen_timeout(gen_context* context, ir_node* ir, bool recursive)
{
	ir_node* expr = ir_first_child(ir);
	ir_node* timeout = ir_next_sibling(expr);
	gen_value_expr(context, expr);
	gen_value_expr(context, timeout);
	gen_loc(context, ir);
	gen_push_u8(context, OP_TIMEOUT);
	gen_push_u8(context, recursive ? 1 : 0);

	gen_end_pending_target_blocks(context, ir->ast->range.end);
	gen_begin_target_block(context, ir->ast->range.end);
}

void gen_background(gen_context* context, ir_node* ir)
{
	////////////////////////////////////////////////////////////
	//TODO: we should disallow nesting background blocks
	//      we should disallow launching new tasks when in background
	//		we should bring task back to foreground when exiting
	//      from the block with a return statement...
	////////////////////////////////////////////////////////////
	gen_loc(context, ir);
	gen_push_u8(context, OP_BACKGROUND);
	gen_multi(context, ir);
	gen_push_u8(context, OP_FOREGROUND);
}

void gen_fdup(gen_context* context, ir_node* ir)
{
	ir_node* opd = ir_first_child(ir);
	gen_value_expr(context, opd);
	gen_loc(context, ir);
	gen_push_u8(context, OP_FDUP);
}

void gen_fdrop(gen_context* context, ir_node* ir)
{
	ir_node* opd = ir_first_child(ir);
	gen_value_expr(context, opd);
	gen_loc(context, ir);
	gen_push_u8(context, OP_FDROP);
}

void gen_node(gen_context* context, ir_node* ir)
{
	switch(ir->kind)
	{
		case IR_NOP:
			break;

		//TODO: reduce to IR_BINOP / IR_COMP ?
		case IR_ADD:
		case IR_SUB:
		case IR_MUL:
		case IR_DIV:
		case IR_MOD:
			gen_arithmetic_binop(context, ir);
			break;

		case IR_NEG:
			gen_neg(context, ir);
			break;

		case IR_LT:
		case IR_GT:
		case IR_LE:
		case IR_GE:
		case IR_EQ:
		case IR_NEQ:
			gen_comparison(context, ir);
			break;

		case IR_LAND:
		case IR_LOR:
		case IR_LNOT:
			gen_eval_short_circuit(context, ir);
			break;

		case IR_CAST:
			gen_cast(context, ir);
			break;

		case IR_POINTER_NIL:
			gen_pointer_nil(context, ir);
			break;

		case IR_INT:
		case IR_FLOAT:
			gen_const(context, ir);
			break;

		case IR_VAR:
			gen_var(context, ir);
			break;

		case IR_RODATA:
			gen_rodata_ref(context, ir);
			break;

		case IR_ARRAY_INIT:
			gen_array_init(context, ir);
			break;

		case IR_STORE:
			gen_store(context, ir);
			break;

		case IR_TSTORE:
			gen_tstore(context, ir);
			break;

		case IR_CALL:
			gen_call(context, ir);
			break;

		case IR_RETURN:
			gen_return(context, ir);
			break;

		case IR_BODY:
			gen_multi(context, ir);
			break;

		case IR_IF:
			gen_cond(context, ir);
			break;

		case IR_WHILE:
			gen_while(context, ir);
			break;

		case IR_FOR:
			gen_for(context, ir);
			break;

		case IR_AT:
			gen_array_index(context, ir);
			break;

		case IR_LEN:
			gen_array_len(context, ir);
			break;

		case IR_FIELD:
			gen_struct_field(context, ir);
			break;

		case IR_SLICE:
			gen_slice(context, ir);
			break;

		case IR_REF:
			gen_ref(context, ir);
			break;

		case IR_DEREF:
			gen_deref(context, ir);
			break;

		case IR_BOX:
			gen_box(context, ir);
			break;

		case IR_PUT:
			gen_put(context, ir);
			break;
/*
		case IR_ALLOC:
			gen_alloc(context, ir);
			break;

		case IR_ALLOC_SLICE:
			gen_alloc_slice(context, ir);
			break;

		case IR_FREE:
			gen_free(context, ir);
			break;

		case IR_SYNC_BEAT:
			gen_sync_beat(context, ir);
			break;
*/
		case IR_FLOW:
			gen_flow(context, ir);
			break;

		case IR_PAUSE:
			gen_pause(context, ir);
			break;

		case IR_WAIT:
			gen_wait(context, ir, false);
			break;
		case IR_WAIT_RECURSIVE:
			gen_wait(context, ir, true);
			break;

		case IR_TIMEOUT:
			gen_timeout(context, ir, false);
			break;
		case IR_TIMEOUT_RECURSIVE:
			gen_timeout(context, ir, true);
			break;

		case IR_STANDBY:
			gen_standby(context, ir);
			break;

		case IR_BACKGROUND:
			gen_background(context, ir);
			break;

		case IR_FDUP:
			gen_fdup(context, ir);
			break;

		case IR_FDROP:
			gen_fdrop(context, ir);
			break;

		default:
			ASSERT(0, "not implemented yet");
			break;
	}
}

u64 gen_layout_variables(ir_scope* scope, u64 startOffset)
{
	u64 offset = startOffset;
	for_each_in_list(&scope->variables, varSym, ir_symbol, auxElt)
	{
		ir_type* baseType = ir_type_unwrap(varSym->type);
		offset = AlignUpOnPow2(offset, baseType->align);
		varSym->offset = offset;
		offset += baseType->size;
	}

	u64 maxFrameSize = offset;
	for_each_in_list(&scope->children, child, ir_scope, listElt)
	{
		u64 childFrameSize = gen_layout_variables(child, offset);
		maxFrameSize = maximum(maxFrameSize, childFrameSize);
	}
	return(maxFrameSize);
}

void gen_proc(gen_context* context, ir_symbol* procSym)
{
	//NOTE(martin): layout variables in proc
	procSym->proc.frameSize = gen_layout_variables(procSym->proc.body->scope, 0);

	procSym->offset = gen_get_code_offset(context);
	gen_push_u8(context, OP_ENTER);
	gen_push_u64(context, procSym->proc.frameSize);

	//NOTE(martin): copy captured variables in stack
	for_each_in_list(&procSym->proc.captures, capture, ir_symbol, capture.listElt)
	{
		//NOTE(martin): get the captured variable and count the number of ancestors between that variable's
		//              task and the current task.
		u32 totalCaptureLevel = capture->capture.level;
		ir_symbol* variable = capture->capture.symbol;
		while(variable->capture.level)
		{
			totalCaptureLevel += variable->capture.level;
			variable = variable->capture.symbol;
		}

		//NOTE(martin): put the address of the capture pointer on the stack
		gen_push_u8(context, OP_LOCAL);
		gen_push_u64(context, capture->offset);

		//NOTE(martin): put the address of the captured variable on the stack
		gen_push_u8(context, OP_CAPTURE);
		gen_push_u64(context, totalCaptureLevel);
		gen_push_u64(context, variable->offset);

		//NOTE(martin): store the address of the captured variable in the capture pointer
		gen_push_u8(context, OP_STORE64);
	}

	gen_begin_target_block(context, procSym->proc.body->ast->range.start);

	gen_multi(context, procSym->proc.body);

	if(procSym->type->retType->kind == TYPE_VOID)
	{
		gen_push_u8(context, OP_LEAVE);
	}
	else
	{
		//TODO: avoid having to do an unnecessary leave at the end - ensure there's a return with
		//      a value on all exit paths
		gen_push_u8(context, OP_CONST8);
		gen_push_u8(context, 0);
		gen_push_u8(context, OP_LEAVE);
	}

	gen_end_pending_target_blocks(context, procSym->proc.body->ast->range.end);
}

ir_type* ir_type_copy(mem_arena* arena, ir_type* src)
{
	ir_type* type = mem_arena_alloc_type(arena, ir_type);
	type->kind = src->kind;
	type->size = src->size;
	type->align = src->align;
	type->name = mp_string_push_copy(arena, src->name);
	type->primitive = src->primitive;
	type->symbol = src->symbol;

	ListInit(&type->params);
	for_each_in_list(&src->params, srcParam, ir_param, listElt)
	{
		ir_param* param = mem_arena_alloc_type(arena, ir_param);
		param->name = mp_string_push_copy(arena, srcParam->name);
		param->type = ir_type_copy(arena, ir_type_unwrap(srcParam->type));
		param->offset = srcParam->offset;
		ListAppend(&type->params, &param->listElt);
	}

	if(src->eltType)
	{
		type->eltType = ir_type_copy(arena, src->eltType);
	}
	type->eltCount = src->eltCount;

	return(type);
}

void gen_foreign_block(gen_context* context, ir_symbol* block)
{
	mem_arena* arena = context->arena;

	qc_foreign_dep* dep = mem_arena_alloc_type(arena, qc_foreign_dep);
	memset(dep, 0, sizeof(qc_foreign_dep));

	dep->path = mp_string_push_copy(arena, block->name);

	for_each_in_list(&block->foreignBlock.procDefs, def, ir_symbol, proc.foreignElt)
	{
		//NOTE: layout proc parameters
		u64 offset = 0;
		for_each_in_list(&def->type->params, param, ir_param, listElt)
		{
			ir_type* baseType = ir_type_unwrap(param->type);
			offset = AlignUpOnPow2(offset, baseType->align);
			param->offset = offset;
			offset += baseType->size;
		}

		//NOTE: copy symbol and type info into program image
		qc_foreign_sym* pgmSym = mem_arena_alloc_type(arena, qc_foreign_sym);
		pgmSym->name = mp_string_push_copy(arena, def->name);
		pgmSym->signature = ir_type_copy(arena, def->type);
		ListAppend(&dep->functions, &pgmSym->listElt);

		def->proc.foreignIndex = context->program->foreignSymbolsCount;
		context->program->foreignSymbolsCount++;
	}
	ListAppend(&context->program->foreignDeps, &dep->listElt);
}

void gen_widget_initial_data(gen_context* context, q_cell* cell)
{
	//WARN: for now we assume widget is a tempo curve editor
	DEBUG_ASSERT(cell->kind == CELL_UI && cell->ui.kind == CELL_UI_TEMPO_CURVE);

	qed_curve_editor* editor = (qed_curve_editor*)cell->ui.data.ptr;

	bc_tempo_curve_desc* desc = mem_arena_alloc_type(context->arena, bc_tempo_curve_desc);
	desc->axes = editor->axes;
	desc->eltCount = editor->eltCount;

	sched_curve_descriptor_elt* elements = mem_arena_alloc_array(context->arena, sched_curve_descriptor_elt, desc->eltCount);

	u32 i = 0;
	for_each_in_list(&editor->elements, editorElt, qed_curve_editor_elt, listElt)
	{
		elements[i].type = editorElt->type;
		elements[i].length = (editorElt->p[3].x - editorElt->p[0].x);
		elements[i].startValue = editorElt->p[0].y;
		elements[i].endValue = editorElt->p[3].y;

		//NOTE(martin): descriptor control points X's are normalized to (0, 1)
		f32 eltLengthScale = 1/(editorElt->p[3].x - editorElt->p[0].x);

		//NOTE(martin): control points are normalized to 0,1 between startValue/endValue end 0/length
		elements[i].p1x = (editorElt->p[1].x - editorElt->p[0].x) * eltLengthScale;
		elements[i].p1y = editorElt->p[1].y;
		elements[i].p2x = (editorElt->p[2].x - editorElt->p[0].x) * eltLengthScale;
		elements[i].p2y = editorElt->p[2].y;

		i++;
	}
}

void gen_widget_info(gen_context* context, bc_widget* bcWidget, ir_symbol* sym, u64 moduleIndex)
{
	mem_arena* arena = context->arena;

	q_cell* cell = sym->widget.cell;

	bcWidget->moduleIndex = moduleIndex;
	bcWidget->cellID = cell->id;
	bcWidget->globalOffset = sym->widget.global->offset;

	u64 start = context->arena->offset;
	gen_widget_initial_data(context, cell);

	sym->offset = start;
	bcWidget->size = context->arena->offset - start;
}


void q_program_init(q_program* program)
{
	memset(program, 0, sizeof(q_program));
}

void gen_program_global_init(gen_context* context, list_info* buildList)
{
	gen_push_u8(context, OP_ENTER);
	gen_push_u64(context, 0);
	for_each_in_list(buildList, module, q_module, buildListElt)
	{
		ir_symbol* procSymbol = checker_find_symbol_in_module(module, mp_string_lit("$_module_init"));
		DEBUG_ASSERT(procSymbol, "module init proc not found");

		gen_push_u8(context, OP_CALL);
		gen_push_u64(context, 0xdeadbeef);

		gen_lref* lref = mem_arena_alloc_type(mem_scratch(), gen_lref);
		lref->symbol = procSymbol;
		lref->offset = gen_get_code_offset(context) - 8;
		ListAppend(&context->lrefs, &lref->listElt);
	}
	gen_push_u8(context, OP_LEAVE);
}

void gen_program(mem_arena* arena, q_program* program, list_info* buildList)
{
	q_program_init(program);

	gen_context context = {0};
	context.arena = arena;
	context.program = program;

	program->contents = context.arena->ptr + context.arena->offset;

	//NOTE(martin): generate FFI data
	for_each_in_list(buildList, module, q_module, buildListElt)
	{
		for_each_in_list(&module->foreignBlocks, block, ir_symbol, auxElt)
		{
			gen_foreign_block(&context, block);
		}
	}

	//NOTE(martin): generate rodata symbols
	program->rodataOffset = context.arena->offset;

	for_each_in_list(buildList, module, q_module, buildListElt)
	{
		for_each_in_list(&module->rodata, rodata, ir_symbol, auxElt)
		{
			ir_type* type = ir_type_unwrap(rodata->type);
			program->rodataSize = AlignUpOnPow2(program->rodataSize, type->align);
			rodata->offset = program->rodataSize;
			program->rodataSize += type->size;
		}
	}
	mem_arena_alloc(context.arena, program->rodataSize);

	//NOTE(martin): generate rodata blobs
	for_each_in_list(buildList, module, q_module, buildListElt)
	{
		for_each_in_list(&module->rodata, symbol, ir_symbol, auxElt)
		{
			gen_rodata_symbol(&context, symbol);
		}
	}

	//NOTE: generate type info
	for_each_in_list(buildList, module, q_module, buildListElt)
	{
		for_each_in_list(&module->types, typeRef, ir_typeref, listElt)
		{
			bc_type* bcType = mem_arena_alloc_type(context.arena, bc_type);
			gen_type_info(&context, bcType, typeRef->type);
		}
	}
	program->rodataSize = context.arena->offset - program->rodataOffset;

	//NOTE: layout global variables, after rodata
	for_each_in_list(buildList, module, q_module, buildListElt)
	{
		for_each_in_list(&module->globalScope->variables, variable, ir_symbol, auxElt)
		{
			ir_type* type = ir_type_unwrap(variable->type);
			program->bssSize = AlignUpOnPow2(program->bssSize, type->align);
			variable->offset = program->rodataSize + program->bssSize;
			program->bssSize += type->size;
		}
	}

	//NOTE: generate editor widgets data
	{
		program->widgetsOffset = context.arena->offset;
		u64 moduleIndex = 0;
		for_each_in_list(buildList, module, q_module, buildListElt)
		{
			for_each_in_list(&module->editorWidgets, widget, ir_symbol, auxElt)
			{
				//put editor widget data here, i.e. moduleID + widgetID + global ptr + initial data
				bc_widget* bcWidget = mem_arena_alloc_type(context.arena, bc_widget);
				gen_widget_info(&context, bcWidget, widget, moduleIndex);
			}
			module->moduleIndex = moduleIndex;
			moduleIndex++;
		}
		program->widgetsSize = context.arena->offset - program->widgetsOffset;
	}

	//NOTE(martin): start of code section, generate global scope initializers
	program->codeOffset = context.arena->offset;

	gen_program_global_init(&context, buildList);

	//NOTE(martin): generate procedures
	context.moduleIndex = 0;
	for_each_in_list(buildList, module, q_module, buildListElt)
	{
		module->codeOffset = gen_get_code_offset(&context);
		for_each_in_list(&module->procedures, procSym, ir_symbol, auxElt)
		{
			if(!procSym->proc.foreign && !procSym->type->polymorphic)
			{
				gen_proc(&context, procSym);
				//TODO: end pending target blocks in each function??
			}
		}
		module->codeLen = gen_get_code_offset(&context) - module->codeOffset;
		context.moduleIndex++;
	}

	program->codeSize = context.arena->offset - program->codeOffset;

	//NOTE(martin): patch calls
	char* code = context.arena->ptr + program->codeOffset;
	for_each_in_list(&context.lrefs, lref, gen_lref, listElt)
	{
		*(u64*)(code + lref->offset) = lref->symbol->offset;
	}

	//NOTE(martin): collate locations
	program->locOffset = context.arena->offset;
	program->locSize = program->locCount * sizeof(qc_loc_entry);
	qc_loc_entry* entries = mem_arena_alloc_array(context.arena, qc_loc_entry, program->locSize);

	u64 entryIndex = 0;
	for_each_in_list(&context.locs, loc, gen_loc_elt, listElt)
	{
		entries[entryIndex] = loc->entry;
		entryIndex++;
	}

	//NOTE(martin): generate module table
	program->modulesOffset = context.arena->offset;

	for_each_in_list(buildList, module, q_module, buildListElt)
	{
		u64* lenPtr = mem_arena_alloc_type(context.arena, u64);
		*lenPtr = module->path.len;

		char* strPtr = mem_arena_alloc_array(context.arena, char, module->path.len);
		memcpy(strPtr, module->path.ptr, module->path.len);

		program->modulesCount++;
	}
	program->modulesSize = context.arena->offset - program->modulesOffset;


	//NOTE(martin): collate target blocks
	program->targetBlocksOffset = context.arena->offset;
	program->targetBlocksCount = context.targetBlockCount;
	qc_target_block* blocks = mem_arena_alloc_array(context.arena, qc_target_block, program->targetBlocksCount*sizeof(qc_target_block));
	u64 blockIndex = 0;
	for_each_in_list(&context.targetBlocks, elt, gen_target_block_elt, listElt)
	{
		blocks[blockIndex] = elt->block;
		blockIndex++;
	}
}
