/************************************************************//**
*
*	@file: bytecode.h
*	@author: Martin Fouilleul
*	@date: 11/08/2021
*	@revision:
*
*****************************************************************/

#include"macro_helpers.h"

#ifndef __BYTECODE_H_
#define __BYTECODE_H_

#define OPCODE_LIST() \
OP_X(NOP, "nop", 0, 0, 0, 0) \
\
/* constants */ \
OP_X(CONST8, "const8", 1, 1, 0, 0)   \
OP_X(CONST16, "const16", 1, 2, 0, 0) \
OP_X(CONST32, "const32", 1, 4, 0, 0) \
OP_X(CONST64, "const64", 1, 8, 0, 0) \
\
/* address of local variable */ \
OP_X(LOCAL, "local", 1, 8, 0, 0)     \
\
/* address of call argument */ \
OP_X(ARG, "arg", 1, 8, 0, 0)     \
\
/* loads */ \
OP_X(LOAD8, "load8", 0, 0, 0, 0)     \
OP_X(LOAD16, "load16", 0, 0, 0, 0)   \
OP_X(LOAD32, "load32", 0, 0, 0, 0)   \
OP_X(LOAD64, "load64", 0, 0, 0, 0)   \
\
/* stores */ \
OP_X(STORE8, "store8", 0, 0, 0, 0)   \
OP_X(STORE16, "store16", 0, 0, 0, 0) \
OP_X(STORE32, "store32", 0, 0, 0, 0) \
OP_X(STORE64, "store64", 0, 0, 0, 0) \
\
/* copy */ \
OP_X(COPY, "copy", 1, 8, 0, 0) \
\
/* stack */ \
OP_X(DUP, "dup", 0, 0, 0, 0) \
OP_X(POP, "pop", 0, 0, 0, 0) \
\
/* integer arithmetics */ \
OP_X(ADD, "add", 0, 0, 0, 0) \
OP_X(SUB, "sub", 0, 0, 0, 0) \
OP_X(MUL, "mul", 0, 0, 0, 0) \
OP_X(DIV, "div", 0, 0, 0, 0) \
OP_X(MOD, "mod", 0, 0, 0, 0) \
OP_X(SDIV, "sdiv", 0, 0, 0, 0) \
OP_X(NEG, "neg", 0, 0, 0, 0) \
\
/* float arithmetics, single precision */ \
OP_X(FADDS, "fadds", 0, 0, 0, 0) \
OP_X(FSUBS, "fsubs", 0, 0, 0, 0) \
OP_X(FMULS, "fmuls", 0, 0, 0, 0) \
OP_X(FDIVS, "fdivs", 0, 0, 0, 0) \
OP_X(FNEGS, "fnegs", 0, 0, 0, 0) \
\
/* float arithmetics, double precision */ \
OP_X(FADDD, "faddd", 0, 0, 0, 0) \
OP_X(FSUBD, "fsubd", 0, 0, 0, 0) \
OP_X(FMULD, "fmuld", 0, 0, 0, 0) \
OP_X(FDIVD, "fdivd", 0, 0, 0, 0) \
OP_X(FNEGD, "fnegd", 0, 0, 0, 0) \
\
/* bitwise operators */ \
OP_X(AND, "and", 0, 0, 0, 0) \
OP_X(OR, "or", 0, 0, 0, 0) \
OP_X(XOR, "xor", 0, 0, 0, 0) \
OP_X(NOT, "not", 0, 0, 0, 0) \
OP_X(SHL, "shl", 0, 0, 0, 0) \
OP_X(SHR, "shr", 0, 0, 0, 0) \
OP_X(SAL, "sal", 0, 0, 0, 0) \
OP_X(SAR, "sar", 0, 0, 0, 0) \
\
/* comparison */ \
OP_X(CMPU, "cmpu", 0, 0, 0, 0) \
OP_X(CMPS, "cmps", 0, 0, 0, 0) \
OP_X(CMPF, "cmpf", 0, 0, 0, 0) \
OP_X(CMPD, "cmpd", 0, 0, 0, 0) \
OP_X(SETE, "sete", 0, 0, 0, 0) \
OP_X(SETN, "setn", 0, 0, 0, 0) \
OP_X(SETL, "setl", 0, 0, 0, 0) \
OP_X(SETG, "setg", 0, 0, 0, 0) \
OP_X(SETLE, "setle", 0, 0, 0, 0) \
OP_X(SETGE, "setge", 0, 0, 0, 0) \
\
/* zero/sign extend */ \
OP_X(ZX8, "zx8", 0, 0, 0, 0) \
OP_X(ZX16, "zx16", 0, 0, 0, 0) \
OP_X(ZX32, "zx32", 0, 0, 0, 0) \
OP_X(SX8, "sx8", 0, 0, 0, 0) \
OP_X(SX16, "sx16", 0, 0, 0, 0) \
OP_X(SX32, "sx32", 0, 0, 0, 0) \
\
/* conversions */ \
OP_X(CVI2F, "cvi2f", 0, 0, 0, 0) \
OP_X(CVF2I, "cvf2i", 0, 0, 0, 0) \
OP_X(CVI2D, "cvi2d", 0, 0, 0, 0) \
OP_X(CVD2I, "cvd2i", 0, 0, 0, 0) \
OP_X(CVU2F, "cvu2f", 0, 0, 0, 0) \
OP_X(CVF2U, "cvf2u", 0, 0, 0, 0) \
OP_X(CVU2D, "cvu2d", 0, 0, 0, 0) \
OP_X(CVD2U, "cvd2u", 0, 0, 0, 0) \
OP_X(CVF2D, "cvf2d", 0, 0, 0, 0) \
OP_X(CVD2F, "cvd2f", 0, 0, 0, 0) \
OP_X(CVPTRQ2N, "cvptrq2n", 0, 0, 0, 0) \
OP_X(CVPTRN2Q, "cvptrn2q", 0, 0, 0, 0) \
\
/* jumps */ \
OP_X(JMP, "jmp", 1, 4, 0, 0) \
OP_X(JE, "je", 1, 4, 0, 0) \
OP_X(JN, "jn", 1, 4, 0, 0) \
OP_X(JLT, "jlt", 1, 4, 0, 0) \
OP_X(JGT, "jgt", 1, 4, 0, 0) \
OP_X(JLE, "jle", 1, 4, 0, 0) \
OP_X(JGE, "jge", 1, 4, 0, 0) \
\
/* call/ret */ \
OP_X(CALL, "call", 1, 8, 0, 0) \
OP_X(ENTER, "enter", 1, 8, 0, 0) \
OP_X(LEAVE, "leave", 0, 0, 0, 0) \
OP_X(FFI_CALL, "ffi_call", 1, 8, 0, 0) \
\
/* slice */ \
OP_X(SLICE_ARRAY, "slice", 1, 8, 0, 0) \
OP_X(RESLICE, "reslice", 1, 8, 0, 0) \
OP_X(SLICE_INDEX, "sindex", 1, 8, 0, 0) \
\
/* halt */ \
OP_X(HALT, "halt", 0, 0, 0, 0) \
\
/* temporary builtins */ \
OP_X(PUTU, "putu", 0, 0, 0, 0) \
OP_X(PUTI, "puti", 0, 0, 0, 0) \
OP_X(PUTF, "putf", 0, 0, 0, 0) \
OP_X(PUTD, "putd", 0, 0, 0, 0) \
OP_X(PUTS, "puts", 0, 0, 0, 0) \
OP_X(PUTNL, "putnl", 0, 0, 0, 0) \
OP_X(ALLOC, "alloc", 0, 0, 0, 0) \
OP_X(FREE, "free", 0, 0, 0, 0) \
OP_X(SYNC_BEAT, "sync_beat", 0, 0, 0, 0) \
\
/* timing */\
OP_X(TASK, "task", 3, 8, 4, 4) \
OP_X(TEMPO, "tempo", 0, 0, 0, 0) \
OP_X(CAPTURE, "capture", 2, 8, 8, 0) \
OP_X(PAUSE, "pause", 0, 0, 0, 0) \
OP_X(WAIT, "wait", 1, 1, 0, 0) \
OP_X(TIMEOUT, "timeout", 1, 1, 0, 0) \
OP_X(STANDBY, "standby", 0, 0, 0, 0) \
OP_X(BACKGROUND, "background", 0, 0, 0, 0) \
OP_X(FOREGROUND, "foreground", 0, 0, 0, 0) \
OP_X(FDUP, "fdup", 0, 0, 0, 0) \
OP_X(FDROP, "fdrop", 0, 0, 0, 0) \
/* tracing/debugging */\
OP_X(TRACE, "trace", 0, 0, 0, 0) \
\
OP_X(INVALID, "_invalid_", 0, 0, 0, 0)

typedef enum {
#define OP_X(op, str, count, sz1, sz2, sz3) _cat2_(OP_, op),
OPCODE_LIST()
#undef OP_X
OP_MAX_COUNT
} vm_opcode;

static const char* OP_STRINGS[OP_MAX_COUNT] =
{
#define OP_X(op, str, count, sz1, sz2, sz3) str,
OPCODE_LIST()
#undef OP_X
};

static const int OP_OPERAND_COUNT[OP_MAX_COUNT] =
{
#define OP_X(op, str, count, sz1, sz2, sz3) count,
OPCODE_LIST()
#undef OP_X
};

static const int OP_OPERAND_SIZE[OP_MAX_COUNT][3] =
{
#define OP_X(op, str, count, sz1, sz2, sz3) {sz1, sz2, sz3},
OPCODE_LIST()
#undef OP_X
};

static const int OP_SIZE[OP_MAX_COUNT] =
{
#define OP_X(op, str, count, sz1, sz2, sz3) (1+sz1+sz2+sz3),
OPCODE_LIST()
#undef OP_X
};

const u32 OP_TASK_FLAG_NONE = 0,
          OP_TASK_FLAG_OWN_RETURN_PTR = 1,
          OP_TASK_FLAG_CAPTURED_FRAME = 1<<1;


typedef struct bc_relptr
{
	i64 offset;
} bc_relptr;

#define bc_set_relptr(var, addr) (var.offset = (i64)((char*)(addr) - (char*)(&(var))))
#define bc_get_relptr(var) (((char*)&(var)) + (var.offset))

typedef struct bc_array
{
	u64 len;
	bc_relptr ptr;
} bc_array;

typedef struct bc_type_field
{
	bc_array name;
	bc_relptr type;
	u32 offset;

} bc_type_field;


//WARN(martin): this must stay in sync with type_kind
typedef enum {
	BC_TYPE_ERROR,
	BC_TYPE_VOID,
	BC_TYPE_PRIMITIVE,
	BC_TYPE_NAMED,
	BC_TYPE_POINTER,
	BC_TYPE_FUTURE,
	BC_TYPE_ARRAY,
	BC_TYPE_SLICE,
	BC_TYPE_STRUCT,
	BC_TYPE_PROC,
	BC_TYPE_ANY,
	} bc_type_kind;

typedef enum { BC_TYPE_B8,
               BC_TYPE_U8,
               BC_TYPE_I8,
               BC_TYPE_U16,
               BC_TYPE_I16,
               BC_TYPE_U32,
               BC_TYPE_I32,
               BC_TYPE_U64,
               BC_TYPE_I64,
               BC_TYPE_F32,
               BC_TYPE_F64 } bc_type_primitive;

typedef struct bc_type
{
	bc_type_kind kind;
	u32 size;
	u8 align;

	bc_type_primitive primitive;
	bc_relptr eltType;
	u64 eltCount;

	bc_array fields;

} bc_type;

typedef struct bc_widget
{
	u64 moduleIndex;
	u64 cellID;
	u64 globalOffset;
	u32 size;

} bc_widget;

typedef struct bc_tempo_curve_desc
{
	u32 axes;
	u32 eltCount;
} bc_tempo_curve_desc;


#endif //__BYTECODE_H_
