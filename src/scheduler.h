/************************************************************//**
*
*	@file: scheduler.h
*	@author: Martin Fouilleul
*	@date: 09/07/2020
*	@revision:
*
*****************************************************************/
#ifndef __SCHEDULER_H_
#define __SCHEDULER_H_

#include"typedefs.h"
#include"lists.h"
#include"sched_curves.h"

//---------------------------------------------------------------
// Scheduler types
//---------------------------------------------------------------
typedef f64 sched_steps;

typedef i64(*sched_task_proc)(void* userPointer);

typedef struct sched_task { u64 h; } sched_task;

typedef u64 sched_task_signal;
const sched_task_signal SCHED_SIG_RETIRED   = 0x01,
                        SCHED_SIG_COMPLETED = 0x01<<1;

typedef enum { SCHED_WAKEUP_INVALID_HANDLE,
               SCHED_WAKEUP_TIMEOUT,
               SCHED_WAKEUP_SIGNALED,
               SCHED_WAKEUP_CANCELLED } sched_wakeup_code;

//---------------------------------------------------------------
// Scheduler API
//---------------------------------------------------------------

//NOTE: start / end the scheduler. This will create a first task for the calling function
void sched_init();
void sched_end();

//NOTE: tasks
sched_task sched_task_create(sched_task_proc proc, void* userPointer);
sched_task sched_task_create_detached(sched_task_proc proc, void* userPointer);
sched_task sched_task_create_for_parent(sched_task parent, sched_task_proc proc, void* userPointer);

sched_task sched_task_self();
void sched_task_cancel(sched_task task);
void sched_task_suspend(sched_task task);
void sched_task_resume(sched_task task);

//NOTE: task queries
//TODO: status?
sched_steps sched_task_initial_delay(sched_task task);
sched_steps sched_task_local_delay(sched_task task);
f64 sched_task_global_delay(sched_task task);

//NOTE: task's timescales
void sched_task_timescale_set_tempo_curve(sched_task task, sched_curve_descriptor* descriptor);

//NOTE(martin): task self scheduling
void sched_cancel();
void sched_suspend();
void sched_wait(sched_steps steps);

sched_wakeup_code sched_wait_for_task(sched_task task, sched_task_signal signal, sched_steps timeout);

#define sched_wait_retirement(task) sched_wait_for_task(task, SCHED_SIG_RETIRED, -1)
#define sched_wait_completion(task) sched_wait_for_task(task, SCHED_SIG_COMPLETED, -1)

//NOTE(martin): handles management functions
int sched_task_get_exit_code(sched_task task, u64* exitCode);
void sched_task_release(sched_task task);
sched_task sched_task_duplicate(sched_task task);

//NOTE(martin): background jobs
void sched_background();
void sched_foreground();

#endif //__SCHEDULER_H_
