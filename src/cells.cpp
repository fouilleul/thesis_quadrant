/************************************************************//**
*
*	@file: cells.cpp
*	@author: Martin Fouilleul
*	@date: 11/05/2022
*	@revision:
*
*****************************************************************/
#include"cells.h"
#include"tempo_editor.h"
#include"ast.h"

//WARN: this should be kept in sync with cell_kind enum.
bool cell_has_children(q_cell* cell)
{
	return(cell->kind >= CELL_ROOT && cell->kind <= CELL_ARRAY);
}

bool cell_has_text(q_cell* cell)
{
	return(cell->kind >= CELL_HOLE && cell->kind <= CELL_WORD);
}

bool cell_can_edit(q_cell* cell)
{
	return(cell_has_text(cell) && cell->kind != CELL_HOLE);
}

bool cell_has_ancestor(q_cell* cell, q_cell* ancestor)
{
	while(cell)
	{
		if(cell == ancestor)
		{
			return(true);
		}
		cell = cell->parent;
	}
	return(false);
}

q_cell* cell_alloc_with_id(cell_tree* tree, cell_kind kind, u64 id)
{
	q_cell* cell = mem_pool_alloc_type(&tree->cellPool, q_cell);
	memset(cell, 0, sizeof(q_cell));

	cell->kind = kind;
	cell->id = id;

	u64 key = hash_aesdec_u64(cell->id);
	q_hashmap_insert(&tree->cellMap, key, (u64)cell);

	return(cell);
}

q_cell* cell_alloc(cell_tree* tree, cell_kind kind)
{
	u64 id = tree->nextCellID;
	tree->nextCellID++;
	return(cell_alloc_with_id(tree, kind, id));
}

void cell_tree_init(cell_tree* tree)
{
	memset(tree, 0, sizeof(cell_tree));
	mem_pool_init(&tree->cellPool, sizeof(q_cell));

	tree->nextCellID = 1;
	tree->rootCell = cell_alloc(tree, CELL_ROOT);
}

void cell_recycle(cell_tree* tree, q_cell* cell)
{
	u64 key = hash_aesdec_u64(cell->id);
	q_hashmap_remove(&tree->cellMap, key);

	if(cell->parent)
	{
		ListRemove(&cell->parent->children, &cell->listElt);
		cell->parent->childCount--;
	}

	for_each_in_list_safe(&cell->children, child, q_cell, listElt)
	{
		cell_recycle(tree, child);
	}

	if(cell->text.string.ptr)
	{
		free(cell->text.string.ptr);
	}

	if(cell->kind == CELL_UI)
	{
		switch(cell->ui.kind)
		{
			case CELL_UI_NONE:
				break;
			case CELL_UI_TEMPO_CURVE:
				qed_tempo_editor_recycle(tree, cell);
				break;
		}
	}

	mem_pool_recycle(&tree->cellPool, cell);
}

void cell_push(q_cell* parent, q_cell* cell)
{
	DEBUG_ASSERT(cell != parent);
	cell->parent = parent;
	ListAppend(&parent->children, &cell->listElt);
	cell->parent->childCount++;
}

void cell_insert(q_cell* afterSibling, q_cell* cell)
{
	DEBUG_ASSERT(cell != afterSibling->parent);
	cell->parent = afterSibling->parent;
	ListInsert(&cell->parent->children, &afterSibling->listElt, &cell->listElt);
	cell->parent->childCount++;
}

void cell_insert_before(q_cell* beforeSibling, q_cell* cell)
{
	DEBUG_ASSERT(cell != beforeSibling->parent);
	cell->parent = beforeSibling->parent;
	ListInsertBefore(&cell->parent->children, &beforeSibling->listElt, &cell->listElt);
	cell->parent->childCount++;
}

void cell_text_replace(q_cell* cell, mp_string string)
{
	if(string.len > cell->text.cap)
	{
		free(cell->text.string.ptr);
		cell->text.cap = string.len*1.5;
		cell->text.string.ptr = malloc_array(char, cell->text.cap);
	}
	cell->text.string.len = string.len;
	memcpy(cell->text.string.ptr, string.ptr, string.len);
}

void cell_ast_cache(q_cell* cell, q_ast* ast)
{
	cell->ast = ast;
	cell->astGen = ast->generation;
}

q_ast* cell_ast_lookup(q_cell* cell)
{
	if(cell->ast && cell->astGen == cell->ast->generation)
	{
		return(cell->ast);
	}
	else
	{
		return(0);
	}
}

q_cell* cell_find(cell_tree* tree, u64 id)
{
	u64 val = 0;
	u64 key = hash_aesdec_u64(id);
	if(q_hashmap_find(&tree->cellMap, key, &val))
	{
		return((q_cell*)val);
	}
	else
	{
		return(0);
	}
}

void cell_flash(q_cell* cell)
{
	cell->flashValue = CELL_FLASH_LENGTH;
}
