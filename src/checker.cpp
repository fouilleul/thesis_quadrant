/************************************************************//**
*
*	@file: checker.cpp
*	@author: Martin Fouilleul
*	@date: 23/06/2022
*	@revision:
*
*****************************************************************/

#include"ir.h"
#include"checker.h"

#define LOG_SUBSYSTEM "checker"

void checker_context_init(checker_context* context, q_workspace* workspace, q_module* module)
{
	memset(context, 0, sizeof(checker_context));
	context->workspace = workspace;
	context->module = module;
	context->arena = &module->symbolArena;
}

mp_string ast_copy_string(mem_arena* arena, q_ast* ast)
{
	mp_string string = {0};
	if(ast->range.start.leftFrom)
	{
		string = mp_string_push_copy(arena, ast->range.start.leftFrom->text.string);
	}
	return(string);
}

q_ast* ast_get_operand(q_ast* ast, u32 index)
{
	q_ast* opd = ast_first_child(ast);
	for(u32 i = 0; i<index && opd; i++)
	{
		opd = ast_next_sibling(opd);
	}
	return(opd);
}

//------------------------------------------------------------------------------------
// Errors
//------------------------------------------------------------------------------------

void check_error_va(checker_context* context, q_ast* ast, const char* fmt, va_list ap)
{
	q_error* error = mem_arena_alloc_type(context->arena, q_error);
	memset(error, 0, sizeof(q_error));
	error->kind = Q_ERROR_CHECKING;
	error->ast = ast;
	//WARN: build error string is allocated on the symbol arena
	error->msg = mp_string_pushfv(context->arena, fmt, ap);
	ListAppend(&context->module->checkingErrors, &error->moduleElt);
}

void check_error(checker_context* context, q_ast* ast, const char* fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	check_error_va(context, ast, fmt, ap);
	va_end(ap);
}

void check_error_missing(checker_context* context, q_ast* parent, mp_string msg, mp_string cellHint)
{
	q_range range = {parent->range.end, parent->range.end};
	q_ast* ast = ast_alloc(context->module, AST_ERROR_MISSING);
	ast->range = range;
	ast_push(parent, ast);

	q_error* error = mem_arena_alloc_type(context->arena, q_error);
	memset(error, 0, sizeof(q_error));
	error->kind = Q_ERROR_MISSING;
	error->ast = ast;
	error->msg = msg;
	error->cellHint = cellHint;

	ListAppend(&context->module->checkingErrors, &error->moduleElt);
}

//------------------------------------------------------------------------------------
// Symbols map functions
//------------------------------------------------------------------------------------

ir_symbol* ir_symbol_alloc(checker_context* context)
{
	ir_symbol* symbol = mem_arena_alloc_type(context->arena, ir_symbol);
	memset(symbol, 0, sizeof(ir_symbol));
	return(symbol);
}

ir_scope* checker_scope_top(checker_context* context)
{
	return(context->scopeStackTop);
}

ir_scope* checker_scope_push(checker_context* context, scope_kind kind)
{
	ir_scope* scope = mem_arena_alloc_type(context->arena, ir_scope);
	memset(scope, 0, sizeof(ir_scope));
	scope->kind = kind;

	for(int i=0; i<IR_SCOPE_BUCKET_COUNT; i++)
	{
		ListInit(&scope->buckets[i]);
	}
	ListInit(&scope->variables);

	if(context->scopeStackTop)
	{
		ListAppend(&context->scopeStackTop->children, &scope->listElt);
	}

	scope->parent = context->scopeStackTop;
	context->scopeStackTop = scope;

	return(scope);
}

void checker_scope_pop(checker_context* context)
{
	ir_scope* scope = context->scopeStackTop;
	if(scope)
	{
		context->scopeStackTop = scope->parent;
	}
}

#define checker_scope_block(context, kind, ir) \
	ir->scope = checker_scope_push(context, kind); \
	for(int __i__=0; __i__==0; ++__i__, checker_scope_pop(context))


ir_symbol* checker_insert_symbol(checker_context* context, ir_symbol* symbol)
{
	//WARN(martin): checker_insert_symbol() inserts symbol if the name is not already used.
	//              Otherwise it returns previously defined symbol

	ir_scope* scope = checker_scope_top(context);
	DEBUG_ASSERT(scope, "empty scope stack");

	u64 hash = q_hash_string(symbol->name);
	u64 index = hash & IR_SCOPE_INDEX_MASK;

	//NOTE(martin):	first do a search, so that we don't insert the same symbol twice in the same scope...
	for_each_in_list(&scope->buckets[index], prevDef, ir_symbol, bucketElt)
	{
		if(prevDef->hash == hash && !mp_string_cmp(symbol->name, prevDef->name))
		{
			return(prevDef);
		}
	}

	//NOTE(martin): now we can insert the symbol
	//TODO: here we consider all symbols of the global 'frame' as globals, not only those in the top-level scope.
	//      we should however make a control frame for scoped symbols in the global frame, e.g. treat the module level
	//      as a 'main' proc.
	symbol->global = (context->proc == 0);

	ListPush(&scope->buckets[index], &symbol->bucketElt);
	symbol->hash = hash;

	//NOTE(martin): if we inserted a variable, reserve space in the scope frame
	if(symbol->kind == SYM_VAR)
	{
		/*
		ir_type* baseType = ir_type_unwrap(symbol->type);
		//TODO(martin): alignment !!!
		symbol->offset = scope->frameSize;
		scope->frameSize += baseType->size;
		DEBUG_ASSERT(scope->frameSize);
		*/
		ListAppend(&scope->variables, &symbol->auxElt);
	}
	return(0);
}

ir_symbol* checker_add_anonymous_variable(checker_context* context, ir_type* type)
{
	//NOTE(martin): create an unnamed variable and returns the corresponding symbol.
	ir_scope* scope = checker_scope_top(context);
	DEBUG_ASSERT(scope, "empty scope stack");

	ir_symbol* symbol = ir_symbol_alloc(context);
	symbol->kind = SYM_VAR;
	symbol->type = type;
	symbol->name = mp_string_lit("<anonymous_var>");
	symbol->anonymous = true;

	symbol->global = (context->proc == 0);

	ListAppend(&scope->variables, &symbol->auxElt);

	return(symbol);
}

ir_symbol* checker_add_anonymous_global(checker_context* context, ir_type* type)
{
	//NOTE(martin): create an unnamed variable and returns the corresponding symbol.
	ir_scope* scope = context->module->globalScope;
	DEBUG_ASSERT(scope, "no global scope");

	ir_symbol* symbol = ir_symbol_alloc(context);
	symbol->kind = SYM_VAR;
	symbol->type = type;
	symbol->name = mp_string_lit("<anonymous_global>");
	symbol->anonymous = true;

	symbol->global = true;

	ListAppend(&scope->variables, &symbol->auxElt);

	return(symbol);
}

ir_symbol* checker_add_editor_widget(checker_context* context, q_cell* cell, ir_symbol* widgetPtr)
{
	ir_symbol* symbol = ir_symbol_alloc(context);
	symbol->kind = SYM_EDITOR_WIDGET;
	symbol->widget.cell = cell;
	symbol->widget.global = widgetPtr;

	ListAppend(&context->module->editorWidgets, &symbol->auxElt);
	return(symbol);
}

ir_symbol* checker_add_rodata(checker_context* context, ir_type* type, mp_string data)
{
	//NOTE(martin): create an unnamed read-only data blob and returns the corresponding symbol.

	ir_scope* globalScope = context->module->globalScope;
	DEBUG_ASSERT(globalScope, "no global scope");

	ir_symbol* symbol = ir_symbol_alloc(context);
	symbol->kind = SYM_RODATA;
	symbol->type = type;
	symbol->name = mp_string_lit("<rodata>");
	symbol->data = data;

	ListAppend(&context->module->rodata, &symbol->auxElt);

	return(symbol);
}

void checker_add_global_type(checker_context* context, mp_string name, ir_type* type)
{
	//NOTE(martin): create a named global type and inserts the corresponding symbol in the map.
	//              global symbols are only added once, so we just assert that they are previously
	//              undefined.

	ir_symbol* symbol = ir_symbol_alloc(context);
	symbol->name = name;
	symbol->kind = SYM_TYPE;
	symbol->status = SYM_COMPLETE;
	symbol->type = type;
	ir_symbol* prev = checker_insert_symbol(context, symbol);
	DEBUG_ASSERT(!prev);
}

ir_symbol* checker_find_symbol_in_module(q_module* module, mp_string name)
{
	ir_scope* scope = module->globalScope;
	u64 hash = q_hash_string(name);
	u64 index = hash & IR_SCOPE_INDEX_MASK;

	ir_symbol* result = 0;

	for_each_in_list(&scope->buckets[index], symbol, ir_symbol, bucketElt)
	{
		if(symbol->hash == hash && !mp_string_cmp(symbol->name, name))
		{
			result = symbol;
			break;
		}
	}
	return(result);
}

ir_symbol* checker_find_symbol_in_context(checker_context* context, mp_string name)
{
	u64 capturedLevel = 0;
	ir_scope* firstTaskScope = 0;

	u64 hash = q_hash_string(name);
	u64 index = hash & IR_SCOPE_INDEX_MASK;

	ir_scope* scope = context->scopeStackTop;

	while(scope)
	{
		for_each_in_list(&scope->buckets[index], symbol, ir_symbol, bucketElt)
		{
			if(symbol->hash == hash && !mp_string_cmp(symbol->name, name))
			{
				if(capturedLevel && !symbol->global)
				{
					//NOTE(martin): a local symbol was found after traversing one or more task scopes.
					//              We create a capture for this symbol in the first traversed task scope,
					//              and return that capture.
					ir_symbol* capture = ir_symbol_alloc(context);
					capture->kind = SYM_VAR;
					capture->name = symbol->name;
					capture->hash = hash;
					capture->type = symbol->type;
					capture->capture.offset = symbol->offset;
					capture->capture.level = capturedLevel;
					capture->capture.symbol = symbol;

					/*
					capture->offset = scope->frameSize;
					firstTaskScope->frameSize += 8;
					*/
					ListPush(&firstTaskScope->buckets[index], &capture->bucketElt);
					ListAppend(&context->proc->proc.captures, &capture->capture.listElt);

					return(capture);
				}
				else
				{
					return(symbol);
				}
			}
		}

		if(scope->kind == SCOPE_TASK)
		{
			if(!firstTaskScope)
			{
				firstTaskScope = scope;
			}
			capturedLevel++;
		}

		scope = scope->parent;
	}
	return(0);
}

typedef struct checker_find_result
{
	ir_symbol* symbol;
	q_module* module;
} checker_find_result;

checker_find_result checker_find_symbol_and_module(checker_context* context, mp_string name)
{
	checker_find_result result = {0};

	mem_arena* scratch = mem_scratch();
	mp_string_list separators = {};
	mp_string_list_push(scratch, &separators, mp_string_lit(":"));
	mp_string_list stringList = mp_string_split(scratch, name, separators);

	mp_string_elt* baseNameElt = ListLastEntry(&stringList.list, mp_string_elt, listElt);
	mp_string baseName = baseNameElt->string;
	ListRemove(&stringList.list, &baseNameElt->listElt);
	stringList.eltCount--;
	stringList.len -= baseName.len;

	mp_string moduleName = mp_string_list_join(scratch, stringList);

	if(moduleName.len)
	{
		ir_symbol* moduleSymbol = checker_find_symbol_in_module(context->module, moduleName);
		if(moduleSymbol && moduleSymbol->kind == SYM_MODULE)
		{
			result.module = moduleSymbol->module;
			result.symbol = checker_find_symbol_in_module(result.module, baseName);
		}
		else
		{
			//TODO: generate error. We need range here
			LOG_ERROR("couldn't find module symbol %.*s\n", (int)moduleName.len, moduleName.ptr);
		}
	}
	else
	{
		ir_symbol* symbol = checker_find_symbol_in_context(context, baseName);
		result.module = context->module;
		result.symbol = symbol;
	}
	return(result);
}

ir_symbol* checker_find_symbol(checker_context* context, mp_string name)
{
	checker_find_result result = checker_find_symbol_and_module(context, name);
	return(result.symbol);
}

//------------------------------------------------------------------------------------
// types
//------------------------------------------------------------------------------------

ir_type* ir_type_unwrap(ir_type* t)
{
	while(t->kind == TYPE_NAMED)
	{
		ir_symbol* symbol = t->symbol;
		if(symbol && symbol->kind == SYM_TYPE)
		{
			t = symbol->type;
		}
		else
		{
			break;
		}
	}
	return(t);
}

bool ir_type_is_signed(ir_type* type)
{
	if(type->kind != TYPE_PRIMITIVE || type->primitive == TYPE_B8)
	{
		return(false);
	}
	if(type->primitive == TYPE_F32 || type->primitive == TYPE_F64)
	{
		return(true);
	}
	else
	{
		return(((int)type->primitive - 1) & 0x01);
	}
}

bool ir_type_is_float(ir_type* type)
{
	if(type->kind != TYPE_PRIMITIVE)
	{
		return(false);
	}
	return(type->primitive == TYPE_F32 || type->primitive == TYPE_F64);
}

bool ir_type_is_integer(ir_type* type)
{
	if(type->kind != TYPE_PRIMITIVE)
	{
		return(false);
	}
	return(type->primitive != TYPE_F32 && type->primitive != TYPE_F64 && type->primitive != TYPE_B8);
}

bool ir_type_is_numeric(ir_type* type)
{
	return(type->kind == TYPE_PRIMITIVE && type->primitive != TYPE_B8);
}


ir_param* ir_param_alloc(checker_context* context)
{
	ir_param* p = mem_arena_alloc_type(context->arena, ir_param);
	memset(p, 0, sizeof(ir_param));
	return(p);
}

ir_type* ir_type_alloc(checker_context* context)
{
	ir_type* t = mem_arena_alloc_type(context->arena, ir_type);
	memset(t, 0, sizeof(ir_type));
	return(t);
}

ir_type* ir_type_void()
{
	static ir_type t = {.kind = TYPE_VOID, .size = 0, .align = 1, .name = mp_string_lit("void") };
	return(&t);
}

ir_type* ir_type_any()
{
	static ir_type t = { .kind = TYPE_ANY, .size = 16, .align = 8, .name = mp_string_lit("any")};
	return(&t);
}

#define Q_PRIMITIVE_TYPE_DEF(label, prim, sz) \
ir_type* _cat2_(ir_type_, label)() \
{ \
	static ir_type t = { .kind = TYPE_PRIMITIVE, .primitive = prim, .size = sz, .align = sz, .name = mp_string_lit(#label)}; \
	return(&t); \
}

Q_PRIMITIVE_TYPE_DEF(b8, TYPE_B8, 1)
Q_PRIMITIVE_TYPE_DEF(u8, TYPE_U8, 1)
Q_PRIMITIVE_TYPE_DEF(i8, TYPE_I8, 1)
Q_PRIMITIVE_TYPE_DEF(u16, TYPE_U16, 2)
Q_PRIMITIVE_TYPE_DEF(i16, TYPE_I16, 2)
Q_PRIMITIVE_TYPE_DEF(u32, TYPE_U32, 4)
Q_PRIMITIVE_TYPE_DEF(i32, TYPE_I32, 4)
Q_PRIMITIVE_TYPE_DEF(u64, TYPE_U64, 8)
Q_PRIMITIVE_TYPE_DEF(i64, TYPE_I64, 8)
Q_PRIMITIVE_TYPE_DEF(f32, TYPE_F32, 4)
Q_PRIMITIVE_TYPE_DEF(f64, TYPE_F64, 8)

ir_type* ir_type_signed_variant(ir_type* type)
{
	if(type->kind == TYPE_PRIMITIVE)
	{
		switch(type->primitive)
		{
			case TYPE_U8:
				return(ir_type_i8());
			case TYPE_U16:
				return(ir_type_i16());
			case TYPE_U32:
				return(ir_type_i32());
			case TYPE_U64:
				return(ir_type_i64());
			default:
				break;
		}
	}
	return(type);
}


ir_type* ir_type_error()
{
	static ir_type t = {.kind = TYPE_ERROR, .size = 1, .align = 1 };
	return(&t);
}

ir_type* ir_type_named(checker_context* context, ir_symbol* symbol)
{
	ir_type* t = ir_type_alloc(context);
	t->kind = TYPE_NAMED;
	t->symbol = symbol;
	t->polymorphic = false;
	return(t);
}

ir_type* ir_type_poly(checker_context* context, mp_string name)
{
	ir_type* t = ir_type_alloc(context);
	t->kind = TYPE_POLY;
	t->name = name;
	t->polymorphic = true;
	return(t);
}

ir_type* ir_type_poly_typeid(checker_context* context)
{
	static ir_type t = {.kind = TYPE_POLY_TYPEID, .name = mp_string_lit("typeid"), .polymorphic = true };
	return(&t);
}

ir_type* ir_type_future(checker_context* context, ir_type* retType)
{
	ir_type* t = ir_type_alloc(context);
	t->kind = TYPE_FUTURE;
	t->retType = retType;
	t->size = 8;
	t->align = 8;
	t->polymorphic = retType->polymorphic;
	return(t);
}

ir_type* ir_type_pointer(checker_context* context, ir_type* objType)
{
	ir_type* t = ir_type_alloc(context);
	t->kind = TYPE_POINTER;
	t->eltType = objType;
	t->size = 8;
	t->align = 8;
	t->polymorphic = objType->polymorphic;
	return(t);
}

ir_type* ir_type_array(checker_context* context, u64 count, ir_type* eltType)
{
	ir_type* t = ir_type_alloc(context);
	t->kind = TYPE_ARRAY;
	t->eltCount = count;
	t->eltType = eltType;

	ir_type* baseEltType = ir_type_unwrap(eltType);
	t->size = baseEltType->size * count;
	t->align = baseEltType->align;
	t->polymorphic = eltType->polymorphic;

	return(t);
}

ir_type* ir_type_slice(checker_context* context, ir_type* eltType)
{
	ir_type* t = ir_type_alloc(context);
	t->kind = TYPE_SLICE;
	t->eltType = eltType;
	t->size = 16;
	t->align = 8;
	t->polymorphic = eltType->polymorphic;
	return(t);
}

void ir_type_push_param(checker_context* context, ir_type* type, mp_string paramName, ir_type* paramType)
{
	ir_param* param = ir_param_alloc(context);
	param->name = paramName;
	param->type = paramType;
	ListAppend(&type->params, &param->listElt);
	type->eltCount++;
	type->polymorphic = type->polymorphic || paramType->polymorphic;
}

void check_type_struct_layout(checker_context* context, ir_type* type);

ir_type* ir_type_tempo_curve_elt(checker_context* context)
{
	static ir_type* t = 0;
	if(!t)
	{
		t = ir_type_alloc(context);

		t->kind = TYPE_STRUCT;
		t->size = 64;
		t->align = 8;
		t->name = mp_string_lit("tempo_curve_elt");
		ListInit(&t->params);

		ir_type_push_param(context, t, mp_string_lit("kind"), ir_type_u8());
		ir_type_push_param(context, t, mp_string_lit("length"), ir_type_f64());
		ir_type_push_param(context, t, mp_string_lit("starTempo"), ir_type_f64());
		ir_type_push_param(context, t, mp_string_lit("endTempo"), ir_type_f64());
		ir_type_push_param(context, t, mp_string_lit("p1x"), ir_type_f64());
		ir_type_push_param(context, t, mp_string_lit("p1y"), ir_type_f64());
		ir_type_push_param(context, t, mp_string_lit("p2x"), ir_type_f64());
		ir_type_push_param(context, t, mp_string_lit("p2y"), ir_type_f64());

		check_type_struct_layout(context, t);
	}
	return(t);
}

ir_type* ir_type_tempo_curve(checker_context* context)
{
	static ir_type* t = 0;
	if(!t)
	{
		t = ir_type_alloc(context);

		t->kind = TYPE_STRUCT;
		t->size = 24;
		t->align = 8;
		t->name = mp_string_lit("tempo_curve");
		ListInit(&t->params);

		ir_type_push_param(context, t, mp_string_lit("axes"), ir_type_u8());

		ir_type* eltSliceType = ir_type_slice(context, ir_type_tempo_curve_elt(context));
		ir_type_push_param(context, t, mp_string_lit("elements"), eltSliceType);

		check_type_struct_layout(context, t);
	}
	return(t);
}

//------------------------------------------------------------------------------------
// ir helpers
//------------------------------------------------------------------------------------

bool ir_is_addressable(ir_node* ir)
{
	switch(ir->kind)
	{
		case IR_VAR:
		case IR_AT:
		case IR_FIELD:
		case IR_DEREF:
			return(true);

		case IR_CALL:
		case IR_WAIT:
		{
			//////////////////////////////////////////////////////////////////
			//TODO: review this
			//////////////////////////////////////////////////////////////////
			ir_type* baseRetType = ir_type_unwrap(ir->type);
			if( baseRetType->kind == TYPE_ARRAY
			  ||baseRetType->kind == TYPE_SLICE
			  ||baseRetType->kind == TYPE_STRUCT)
			{
				return(true);
			}
		} break;

		default:
			break;
	}
	return(false);
}

void ir_push(ir_node* parent, ir_node* child)
{
	ListAppend(&parent->children, &child->listElt);
	child->parent = parent;
	parent->childCount++;
}

ir_node* ir_alloc(checker_context* context, q_ast* ast, ir_kind kind, ir_type* type)
{
	ir_node* ir = mem_arena_alloc_type(context->arena, ir_node);
	memset(ir, 0, sizeof(ir_node));
	ir->ast = ast;
	ir->kind = kind;
	ir->type = type;
	return(ir);
}

ir_node* ir_error(checker_context* context, q_ast* ast)
{
	ir_node* ir = ir_alloc(context, ast, IR_ERROR, ir_type_error());
	return(ir);
}

ir_node* ir_int(checker_context* context, q_ast* ast, u64 valueU64)
{
	ir_node* ir = ir_alloc(context, ast, IR_INT, ir_type_u64());
	ir->implicitlyConvertible = true;
	ir->mode = IR_CONSTANT;
	ir->valueU64 = valueU64;
	return(ir);
}

ir_node* ir_float(checker_context* context, q_ast* ast, f64 valueF64)
{
	ir_node* ir = ir_alloc(context, ast, IR_FLOAT, ir_type_f64());
	ir->implicitlyConvertible = true;
	ir->mode = IR_CONSTANT;
	ir->valueF64 = valueF64;
	return(ir);
}

ir_node* ir_rodata(checker_context* context, q_ast* ast, ir_symbol* symbol)
{
	ir_node* ir = ir_alloc(context, ast, IR_RODATA, symbol->type);
	ir->symbol = symbol;
	ir->mode = IR_CONSTANT;
	return(ir);
}

ir_node* ir_unop(checker_context* context, q_ast* ast, ir_kind kind, ir_type* type, ir_node* opd)
{
	ir_node* ir = ir_alloc(context, ast, kind, type);
	ir_push(ir, opd);

	ir->implicitlyConvertible = opd->implicitlyConvertible;
	return(ir);
}

ir_node* ir_binop(checker_context* context, q_ast* ast, ir_kind kind, ir_type* type, ir_node* lhs, ir_node* rhs)
{
	ir_node* ir = ir_alloc(context, ast, kind, type);
	ir_push(ir, lhs);
	ir_push(ir, rhs);

	//TODO: compileTimeValue and implicitlyConvertible
	ir->implicitlyConvertible = lhs->implicitlyConvertible && rhs->implicitlyConvertible;
	return(ir);
}

ir_node* ir_variable(checker_context* context, q_ast* ast, ir_symbol* symbol)
{
	ir_node* ir = ir_alloc(context, ast, IR_VAR, symbol->type);
	ir->symbol = symbol;
	return(ir);
}

ir_node* ir_at(checker_context* context, q_ast* ast, ir_node* index, ir_node* object)
{
	ir_type* objectBaseType = ir_type_unwrap(object->type);
	DEBUG_ASSERT(objectBaseType->kind == TYPE_ARRAY || objectBaseType->kind == TYPE_SLICE);

	ir_node* ir = ir_alloc(context, ast, IR_AT, objectBaseType->eltType);
	ir_push(ir, index);
	ir_push(ir, object);
	return(ir);
}

ir_node* ir_len(checker_context* context, q_ast* ast, ir_node* object)
{
	ir_node* ir = ir_alloc(context, ast, IR_LEN, ir_type_u64());
	ir_push(ir, object);

	ir->implicitlyConvertible = true;
	return(ir);
}

ir_node* ir_field(checker_context* context, q_ast* ast, ir_node* object, ir_param* field)
{
	ir_node* ir = ir_alloc(context, ast, IR_FIELD, field->type);
	ir->param = field;
	ir_push(ir, object);
	return(ir);
}

ir_node* ir_slice(checker_context* context, q_ast* ast, ir_node* start, ir_node* end, ir_node* object, ir_type* dstType)
{
	ir_node* ir = ir_alloc(context, ast, IR_SLICE, dstType);
	ir_push(ir, start);
	ir_push(ir, end);
	ir_push(ir, object);
	return(ir);
}

ir_node* ir_ref(checker_context* context, q_ast* ast, ir_node* opd)
{
	ir_node* ir = ir_alloc(context, ast, IR_REF, ir_type_pointer(context, opd->type));
	ir_push(ir, opd);
	return(ir);
}

ir_node* ir_deref(checker_context* context, q_ast* ast, ir_node* opd, ir_type* eltType)
{
	ir_node* ir = ir_alloc(context, ast, IR_DEREF, eltType);
	ir_push(ir, opd);
	return(ir);
}

ir_node* ir_call(checker_context* context, q_ast* ast, ir_symbol* symbol)
{
	ir_node* ir = ir_alloc(context, ast, IR_CALL, symbol->type->retType);
	ir->symbol = symbol;
	return(ir);
}

ir_node* ir_return(checker_context* context, q_ast* ast, ir_node* expr)
{
	ir_node* ir = ir_alloc(context, ast, IR_RETURN, ir_type_void());
	if(expr)
	{
		ir_push(ir, expr);
	}
	return(ir);
}

ir_node* ir_cast(checker_context* context, q_ast* ast, ir_node* opd, ir_type* type)
{
	ir_node* ir = ir_alloc(context, ast, IR_CAST, type);
	ir->mode = opd->mode;
	ir_push(ir, opd);
	return(ir);
}

ir_node* ir_implicit_cast(checker_context* context, ir_node* opd, ir_type* type)
{
	ir_node* ir = ir_alloc(context, opd->ast, IR_CAST, type);
	ir->implicitlyConvertible = opd->implicitlyConvertible;
	ir->mode = opd->mode;
	ir_push(ir, opd);
	return(ir);
}

ir_node* ir_store(checker_context* context, q_ast* ast, ir_node* var, ir_node* opd)
{
	ir_node* ir = ir_alloc(context, ast, IR_STORE, ir_type_void());
	ir_push(ir, var);
	ir_push(ir, opd);
	return(ir);
}

ir_node* ir_tstore(checker_context* context, q_ast* ast, ir_node* var, ir_node* opd)
{
	ir_node* ir = ir_alloc(context, ast, IR_TSTORE, var->type);
	ir_push(ir, var);
	ir_push(ir, opd);
	return(ir);
}

ir_node* ir_box(checker_context* context, q_ast* ast, ir_node* src)
{
	ir_node* ir = ir_alloc(context, ast, IR_BOX, ir_type_any());
	ir_push(ir, src);
	return(ir);
}

ir_node* ir_body(checker_context* context, q_ast* ast)
{
	ir_node* ir = ir_alloc(context, ast, IR_BODY, ir_type_void());
	return(ir);
}

ir_node* ir_if(checker_context* context, q_ast* ast, ir_node* condition, ir_node* ifBranch, ir_node* elseBranch)
{
	ir_node* ir = ir_alloc(context, ast, IR_IF, ifBranch->type);
	ir_push(ir, condition);
	ir_push(ir, ifBranch);
	if(elseBranch)
	{
		ir_push(ir, elseBranch);
	}
	return(ir);
}

ir_node* ir_while(checker_context* context, q_ast* ast, ir_node* condition, ir_node* body)
{
	ir_node* ir = ir_alloc(context, ast, IR_WHILE, ir_type_void());
	ir_push(ir, condition);
	ir_push(ir, body);
	return(ir);
}

ir_node* ir_tempo_curve(checker_context* context, q_ast* ast, ir_symbol* curvePtr)
{
	ir_node* ir = ir_alloc(context, ast, IR_TEMPO_CURVE, curvePtr->type);
	ir->symbol = curvePtr;
	return(ir);
}

ir_node* ir_flow(checker_context* context, q_ast* ast, ir_symbol* symbol, ir_node* tempo)
{
	ir_type* type = ir_type_future(context, symbol->type->retType);
	ir_node* ir = ir_alloc(context, ast, IR_FLOW, type);
	ir->symbol = symbol;
	if(tempo)
	{
		ir_push(ir, tempo);
	}
	return(ir);
}

ir_node* ir_standby(checker_context* context, q_ast* ast)
{
	ir_node* ir = ir_alloc(context, ast, IR_STANDBY, ir_type_void());
	return(ir);
}

ir_node* ir_pause(checker_context* context, q_ast* ast, ir_node* expr)
{
	ir_node* ir = ir_alloc(context, ast, IR_PAUSE, ir_type_void());
	ir_push(ir, expr);
	return(ir);
}

ir_node* ir_wait(checker_context* context, q_ast* ast, ir_node* expr, ir_type* retType, bool recursive)
{
	//NOTE(martin): retType is passed by caller, which avoid to unwrap expr->type twice
	ir_kind kind = recursive ? IR_WAIT_RECURSIVE : IR_WAIT;
	ir_node* ir = ir_alloc(context, ast, kind, retType);
	ir_push(ir, expr);
	return(ir);
}

ir_node* ir_timeout(checker_context* context, q_ast* ast, ir_node* handle, ir_node* expr, ir_type* retType, bool recursive)
{
	ir_kind kind = recursive ? IR_TIMEOUT_RECURSIVE : IR_TIMEOUT;
	ir_node* ir = ir_alloc(context, ast, kind, retType);
	ir_push(ir, handle);
	ir_push(ir, expr);
	return(ir);
}

ir_node* ir_fdup(checker_context* context, q_ast* ast, ir_node* expr)
{
	ir_node* ir = ir_alloc(context, ast, IR_FDUP, expr->type);
	ir_push(ir, expr);
	return(ir);
}

ir_node* ir_fdrop(checker_context* context, q_ast* ast, ir_node* expr)
{
	ir_node* ir = ir_alloc(context, ast, IR_FDROP, ir_type_void());
	ir_push(ir, expr);
	return(ir);
}

//------------------------------------------------------------------------------------
// checking types
//------------------------------------------------------------------------------------

void check_type_symbol(checker_context* context, ir_symbol* symbol);
ir_type* check_typespec_relaxed(checker_context* context, q_ast* ast);
ir_type* check_typespec(checker_context* context, q_ast* ast);
ir_node* check_initializer(checker_context* context, q_ast* ast, ir_type* dstType);
ir_node* check_flow(checker_context* context, q_ast* ast, ir_type* expectedType);

ir_type* check_type_id(checker_context* context, q_ast* ast)
{
	mp_string name = ast_copy_string(mem_scratch(), ast);
	ir_symbol* symbol = checker_find_symbol(context, name);
	ir_type* t = 0;
	if(!symbol)
	{
		check_error(context, ast, "unknown type identifier '%.*s'", (int)name.len, name.ptr);
		t = ir_type_error();
	}
	else if(symbol->kind != SYM_TYPE)
	{
		check_error(context, ast, "symbol '%.*s' is not a type identifier", (int)name.len, name.ptr);
		t = ir_type_error();
	}
	else
	{
		t = ir_type_named(context, symbol);
	}
	return(t);
}

ir_type* check_type_poly_id(checker_context* context, q_ast* ast)
{
	mp_string name = ast_copy_string(context->arena, ast);
	return(ir_type_poly	(context, name));
}

ir_type* check_type_array(checker_context* context, q_ast* ast)
{
	//TODO: allow compile-time expressions
	q_ast* len = ast_first_child(ast);

	if(len->kind != AST_INT)
	{
		check_error(context, len, "array length must be an integer literal");
		return(ir_type_error());
	}
	u64 count = len->range.start.leftFrom->valueU64;

	ir_type* eltType = check_typespec(context, ast_next_sibling(len));
	if(eltType->kind == TYPE_ERROR)
	{
		return(eltType);
	}

	ir_type* type = ir_type_array(context, count, eltType);
	return(type);
}

ir_type* check_type_slice(checker_context* context, q_ast* ast)
{
	ir_type* eltType = check_typespec_relaxed(context, ast_first_child(ast));
	ir_type* type = ir_type_slice(context, eltType);
	return(type);
}

ir_type* check_type_future(checker_context* context, q_ast* ast)
{
	ir_type* retType = check_typespec_relaxed(context, ast_first_child(ast));
	return(ir_type_future(context, retType));
}

ir_type* check_type_pointer(checker_context* context, q_ast* ast)
{
	ir_type* retType = check_typespec_relaxed(context, ast_first_child(ast));
	return(ir_type_pointer(context, retType));
}

void check_type_struct_layout(checker_context* context, ir_type* type)
{
	u32 fieldOffset = 0;
	for_each_in_list(&type->params, field, ir_param, listElt)
	{
		ir_type* baseFieldType = ir_type_unwrap(field->type);
		fieldOffset = AlignUpOnPow2(fieldOffset, baseFieldType->align);
		field->offset = fieldOffset;
		fieldOffset += baseFieldType->size;
	}
	type->size = fieldOffset;
	type->align = 8;
}


ir_type* check_type_struct(checker_context* context, q_ast* ast)
{
	ir_type* type = ir_type_alloc(context);
	type->kind = TYPE_STRUCT;

	for_each_in_list(&ast->children, astParam, q_ast, listElt)
	{
		q_ast* nameNode = ast_first_child(astParam);
		mp_string name = ast_copy_string(context->arena, nameNode);

		//TODO: could coalesce code with param checking in fundef?
		q_ast* typeNode = ast_next_sibling(nameNode);
		ir_type* paramType = check_typespec(context, typeNode);
		if(paramType->kind == TYPE_VOID)
		{
			check_error(context, typeNode, "struct field can't be of type void");
		}

		ir_param* param = ir_param_alloc(context);
		param->name = name;
		param->type = paramType;

		ListAppend(&type->params, &param->listElt);
		type->eltCount++;

		type->polymorphic |= param->type->polymorphic;
	}
	check_type_struct_layout(context, type);
	return(type);
}


ir_type* check_typespec_relaxed(checker_context* context, q_ast* ast)
{
	switch(ast->kind)
	{
		case AST_ID:
			return(check_type_id(context, ast));
		case AST_POLY_ID:
			return(check_type_poly_id(context, ast));
		case AST_TYPE_ARRAY:
			return(check_type_array(context, ast));
		case AST_TYPE_SLICE:
			return(check_type_slice(context, ast));
		case AST_TYPE_FUTURE:
			return(check_type_future(context, ast));
		case AST_TYPE_POINTER:
			return(check_type_pointer(context, ast));
		case AST_TYPE_STRUCT:
			return(check_type_struct(context, ast));
		default:
			//NOTE: there is already a parsing error generated so no need to do anything
			return(ir_type_error());
	}
}

ir_type* check_typespec_poly(checker_context* context, q_ast* ast)
{
	ir_type* type = check_typespec_relaxed(context, ast);
	if(type->kind == TYPE_NAMED)
	{
		ir_symbol* symbol = type->symbol;
		switch(symbol->status)
		{
			case SYM_UNDEF:
				check_type_symbol(context, type->symbol);
				type->size = symbol->type->size;
				type->align = symbol->type->align;
				break;
			case SYM_COMPLETE:
				type->size = symbol->type->size;
				type->align = symbol->type->align;
				break;
			case SYM_PENDING:
				check_error(context, ast, "cyclic type definition");
				type = ir_type_error();
				break;
		}
	}
	return(type);
}

ir_type* check_instantiate_polymorphic_type(checker_context* context, ir_type* poly, list_info* bindings)
{
	if(!poly->polymorphic)
	{
		return(poly);
	}

	ir_type* instancedType = 0;

	switch(poly->kind)
	{
		case TYPE_POLY:
		{
			for_each_in_list(bindings, binding, ir_poly_binding, listElt)
			{
				if(!mp_string_cmp(poly->name, binding->name))
				{
					instancedType = binding->type;
					break;
				}
			}
			if(!instancedType)
			{
				//TODO: should return an error, but we have no range here...
				instancedType = ir_type_error();
			}
		} break;

		case TYPE_ARRAY:
		{
			ir_type* eltType = check_instantiate_polymorphic_type(context, poly->eltType, bindings);
			instancedType = ir_type_array(context, poly->eltCount, eltType);
		} break;

		case TYPE_SLICE:
		{
			ir_type* eltType = check_instantiate_polymorphic_type(context, poly->eltType, bindings);
			instancedType = ir_type_slice(context, eltType);
		} break;

		case TYPE_STRUCT:
		{
			instancedType = ir_type_alloc(context);
			instancedType->kind = TYPE_STRUCT;

			for_each_in_list(&poly->params, polyField, ir_param, listElt)
			{
				ir_param* instanceField = ir_param_alloc(context);
				instanceField->name = polyField->name;
				instanceField->type = check_instantiate_polymorphic_type(context, polyField->type, bindings);

				//TODO: check for void field

				ListAppend(&instancedType->params, &instanceField->listElt);
				instancedType->eltCount++;
			}
			check_type_struct_layout(context, instancedType);
		} break;

		case TYPE_FUTURE:
		{
			ir_type* retType = check_instantiate_polymorphic_type(context, poly->retType, bindings);
			instancedType = ir_type_future(context, retType);
		} break;

		case TYPE_POINTER:
		{
			ir_type* eltType = check_instantiate_polymorphic_type(context, poly->eltType, bindings);
			instancedType = ir_type_pointer(context, eltType);
		} break;

		default:
			DEBUG_ASSERT("unreachable");
			break;
	}
	return(instancedType);
}

ir_type* check_typespec(checker_context* context, q_ast* ast)
{
	ir_type* t = check_typespec_poly(context, ast);
	t = check_instantiate_polymorphic_type(context, t, context->polyBindings);

	//TODO: errors?
	return(t);
}

void check_type_symbol(checker_context* context, ir_symbol* symbol)
{
	if(symbol->status != SYM_COMPLETE)
	{
		symbol->status = SYM_PENDING;
		q_ast* spec = ast_next_sibling(ast_first_child(symbol->ast));
		symbol->type = check_typespec(context, spec);
		symbol->status = SYM_COMPLETE;
	}
}

void check_type_declarations(checker_context* context, q_ast* scopeNode)
{
	list_info typeSymbols = {};

	for_each_in_list(&scopeNode->children, child, q_ast, listElt)
	{
		if(child->kind == AST_TYPE_DECL)
		{
			q_ast* nameNode = ast_first_child(child);
			mp_string name = ast_copy_string(context->arena, nameNode);

			if(nameNode->kind == AST_ID)
			{
				ir_symbol* symbol = ir_symbol_alloc(context);
				symbol->kind = SYM_TYPE;
				symbol->status = SYM_UNDEF;
				symbol->name = name;
				symbol->ast = child;

				ir_symbol* prevDecl = checker_insert_symbol(context, symbol);
				if(prevDecl)
				{
					check_error(context, nameNode, "redefinition of type %.*s", (int)name.len, name.ptr);
				}
				ListAppend(&typeSymbols, &symbol->auxElt);
			}
		}
	}

	for_each_in_list(&typeSymbols, symbol, ir_symbol, auxElt)
	{
		if(symbol->status == SYM_UNDEF)
		{
			check_type_symbol(context, symbol);
		}
	}
}

void checker_register_boxed_type(checker_context* context, ir_type* type)
{
	for_each_in_list(&context->module->types, typeRef, ir_typeref, listElt)
	{
		if(typeRef->type == type)
		{
			return;
		}
	}

	ir_typeref* typeRef = mem_arena_alloc_type(context->arena, ir_typeref);
	typeRef->type = type;
	ListAppend(&context->module->types, &typeRef->listElt);
}

void check_variable_declaration(checker_context* context, q_ast* ast)
{
	q_ast* idNode = ast_first_child(ast);
	if(idNode->kind != AST_ID)
	{
		return;
	}
	mp_string name = ast_copy_string(context->arena, idNode);

	q_ast* typeNode = ast_next_sibling(idNode);
	ir_type* type = check_typespec(context, typeNode);
	if(type->kind == TYPE_VOID)
	{
		check_error(context, typeNode, "variable can't be of type void");
		return;
	}
	if(type->kind == TYPE_ERROR)
	{
		return;
	}

	//NOTE: add variable in scope
	ir_symbol* symbol = ir_symbol_alloc(context);
	symbol->kind = SYM_VAR;
	symbol->name = name;
	symbol->type = type;
	symbol->ast = ast;
	symbol->status = SYM_PENDING;

	ir_symbol* prevDefined = checker_insert_symbol(context, symbol);
	if(prevDefined)
	{
		check_error(context, idNode, "redefinition of variable %.*s", (int)symbol->name.len, symbol->name.ptr);
		//TODO: error note pointing to previously defined symbol
	}
}

void check_variable_declarations_in_scope(checker_context* context, q_ast* scopeNode)
{
	for_each_in_list(&scopeNode->children, node, q_ast, listElt)
	{
		if(node->kind == AST_VAR_DECL)
		{
			check_variable_declaration(context, node);
		}
	}
}

ir_type* check_varg(checker_context* context, q_ast* ast)
{
	q_ast* typeAst = ast_first_child(ast);
	ir_type* result = 0;

	if(!typeAst)
	{
		result = ir_type_error();
	}
	else
	{
		ir_type* vaType = check_typespec_poly(context, typeAst);
		if(vaType->kind == TYPE_VOID)
		{
			check_error(context, typeAst, "variadic parameter can't be of type void");
		}
		result = ir_type_slice(context, vaType);
	}
	return(result);
}

ir_type* check_type_proc(checker_context* context, q_ast* ast)
{
	ir_type* type = ir_type_alloc(context);
	type->kind = TYPE_PROC;
	type->retType = ir_type_void();

	u32 paramOffset = 0;

	q_ast* paramsAst = ast_first_child(ast);
	q_ast* retParamAst = ast_next_sibling(paramsAst);

	for_each_in_list(&paramsAst->children, astParam, q_ast, listElt)
	{
		q_ast* nameNode = ast_first_child(astParam);
		q_ast* typeNode = ast_next_sibling(nameNode);

		if(nameNode->kind != AST_ID && nameNode->kind != AST_POLY_ID)
		{
			//NOTE: error for this are generated in parser
			continue;
		}
		mp_string name = ast_copy_string(context->arena, nameNode);

		ir_type* paramType = ir_type_void();
		switch(typeNode->kind)
		{
			case AST_TYPEID:
			{
				paramType = ir_type_poly_typeid(context);
				if(nameNode->kind != AST_POLY_ID)
				{
					check_error(context, nameNode, "explicit type parameter should be a polymorphic identifier");
				}
			} break;

			case AST_VARG:
			{
				paramType = check_varg(context, typeNode);

				if(paramType->kind != TYPE_ERROR)
				{
					DEBUG_ASSERT(paramType->kind == TYPE_SLICE);

					ir_type* eltType = paramType->eltType;
					if(eltType->kind ==TYPE_VOID)
					{
						check_error(context, typeNode, "parameter can't be of type void");
					}
				}
			} break;

			default:
				paramType = check_typespec_poly(context, typeNode);
				if(paramType->kind == TYPE_VOID)
				{
					check_error(context, typeNode, "parameter can't be of type void");
				}
				break;
		}

		ir_param* param = ir_param_alloc(context);
		param->name = name;
		param->type = paramType;
		param->variadic = (typeNode->kind == AST_VARG);
		ListAppend(&type->params, &param->listElt);

		if(paramType->polymorphic)
		{
			type->polymorphic = true;
		}

		type->eltCount++;
	}

	if(retParamAst)
	{
		type->retType = check_typespec_poly(context, retParamAst);
		ir_type* baseRetType = ir_type_unwrap(type->retType);

		if(baseRetType->kind == TYPE_ANY)
		{
			check_error(context, retParamAst, "type 'any' can't be returned from a procedure");
		}

		if(  baseRetType->kind == TYPE_STRUCT
		  || baseRetType->kind == TYPE_SLICE
		  || baseRetType->kind == TYPE_ARRAY)
		{
			ir_param* param = ir_param_alloc(context);
			param->name = mp_string_lit("<hidden return pointer>");
			param->type = ir_type_pointer(context, type->retType);
			param->offset = 0;

			type->retParam  = param;
		}
	}
	return(type);
}

ir_type* check_type_proc(checker_context* context, q_ast* ast);
void check_proc_body(checker_context* context, q_ast* ast, ir_symbol* procSymbol, list_info* polyBindings);
ir_node* check_value_expr(checker_context* context, q_ast* ast);
ir_node* check_address_expr(checker_context* context, q_ast* ast);
ir_node* check_ast(checker_context* context, q_ast* ast);
ir_node* check_call_initializer(checker_context* context, q_ast* ast);

ir_node* check_char(checker_context* context, q_ast* ast)
{
	ir_node* ir = ir_int(context, ast, ast->range.start.leftFrom->valueU64);
	return(ir);
}

ir_node* check_int(checker_context* context, q_ast* ast)
{
	ir_node* ir = ir_int(context, ast, ast->range.start.leftFrom->valueU64);
	return(ir);
}

ir_node* check_float(checker_context* context, q_ast* ast)
{
	ir_node* ir = ir_float(context, ast, ast->range.start.leftFrom->valueF64);
	return(ir);
}

ir_node* check_arithmetic_unop(checker_context* context, q_ast* ast)
{
	q_ast* opdAst = ast_first_child(ast);
	ir_node* opd = check_value_expr(context, opdAst);

	ir_type* baseType = ir_type_unwrap(opd->type);
	ir_type* resultType = baseType;

	if(!ir_type_is_numeric(baseType))
	{
		check_error(context, opdAst, "invalid operand type for arithmetic operation");
		resultType = ir_type_error();
	}
	else if(!ir_type_is_signed(baseType))
	{
		if(opd->implicitlyConvertible)
		{
			resultType = ir_type_signed_variant(resultType);
			opd = ir_implicit_cast(context, opd, resultType);
		}
		else
		{
			check_error(context, opdAst, "unsigned type can't be negated");
			resultType = ir_type_error();
		}
	}

	ir_node* ir = ir_unop(context, ast, IR_NEG, resultType, opd);
	return(ir);
}

ir_type* check_arithmetic_result_type(checker_context* context, ir_node* lhs, ir_node* rhs)
{
	ir_type* resultType = 0;
	ir_type* lhsBaseType = ir_type_unwrap(lhs->type);
	ir_type* rhsBaseType = ir_type_unwrap(rhs->type);

	if(!ir_type_is_numeric(lhsBaseType))
	{
		check_error(context, lhs->ast, "invalid operand type for arithmetic operation");
		resultType = ir_type_error();
	}
	else if(!ir_type_is_numeric(rhsBaseType))
	{
		check_error(context, rhs->ast, "invalid operand type for arithmetic operation");
		resultType = ir_type_error();
	}
	else if(!lhs->implicitlyConvertible && !rhs->implicitlyConvertible)
	{
		//NOTE(martin): if both operands are non literals, they must be of the same type
		if(lhsBaseType->primitive != rhsBaseType->primitive)
		{
			check_error(context, lhs->ast->parent, "operands are of different types");
			resultType = ir_type_error();
		}
		else
		{
			resultType = lhsBaseType;
		}
	}
	else if(lhs->implicitlyConvertible && rhs->implicitlyConvertible)
	{
		//NOTE(martin): both operand are literals, we implicitly convert towards a float, or towards a signed operand
		//              (literal operands are either f64, u64 or i64).
		if(ir_type_is_float(lhsBaseType) != ir_type_is_float(rhsBaseType))
		{
			// If one is float and the other int, take the float
			if(ir_type_is_float(lhsBaseType))
			{
				resultType = lhsBaseType;
			}
			else
			{
				resultType = rhsBaseType;
			}
		}
		else if(ir_type_is_float(lhsBaseType))
		{
			resultType = lhsBaseType;
		}
		else
		{
			// If both are integers, and if one is signed, take signed variant
			if(ir_type_is_signed(lhsBaseType))
			{
				resultType = lhsBaseType;
			}
			else
			{
				resultType = rhsBaseType;
			}
		}
	}
	else
	{
		//NOTE(martin): if one of the operand is a literal expr, we may convert it to the type of the other operand.
		//              if the converter operand is a literal _token_, we convert it in place.
		//TODO: issue a warning if the converted operand doesn't fit in its destination type.

		if(lhs->implicitlyConvertible)
		{
			if(ir_type_is_float(lhsBaseType) && !ir_type_is_float(rhsBaseType))
			{
				check_error(context, lhs->ast, "can't implicitly convert floating point literal to integer type");
				resultType = ir_type_error();
			}
			else
			{
				resultType = rhsBaseType;
			}
		}
		else
		{
			if(ir_type_is_float(rhsBaseType) && !ir_type_is_float(lhsBaseType))
			{
				check_error(context, rhs->ast, "can't implicitly convert floating point literal to integer type");
				resultType = ir_type_error();
			}
			else
			{
				resultType = lhsBaseType;
			}
		}
	}

	DEBUG_ASSERT(resultType);
	return(resultType);
}

bool ir_type_is_equivalent(ir_type* t1, ir_type* t2)
{
	t1 = ir_type_unwrap(t1);
	t2 = ir_type_unwrap(t2);

	if(t1 == t2)
	{
		return(true);
	}

	if(t1->kind != t2->kind)
	{
		return(false);
	}
	switch(t1->kind)
	{
		case TYPE_PRIMITIVE:
			return(t1->primitive == t2->primitive);

		case TYPE_SLICE:
			return(ir_type_is_equivalent(t1->eltType, t2->eltType));

		case TYPE_ARRAY:
			return(  ir_type_is_equivalent(t1->eltType, t2->eltType)
			      && t1->eltCount == t2->eltCount);

		case TYPE_FUTURE:
			return(ir_type_is_equivalent(t1->retType, t2->retType));

		case TYPE_POINTER:
			return(ir_type_is_equivalent(t1->eltType, t2->eltType));

		case TYPE_STRUCT:
		{
			//NOTE(martin): named structs use nominative typing, whereas anonymous structs use structural typing.
			if(t1->name.len || t2->name.len)
			{
				return(!mp_string_cmp(t1->name, t2->name));
			}
			else
			{
				ir_param* param1 = ListFirstEntry(&t1->params, ir_param, listElt);
				ir_param* param2 = ListFirstEntry(&t2->params, ir_param, listElt);
				while(param1 && param2)
				{
					if(mp_string_cmp(param1->name, param2->name) || !ir_type_is_equivalent(param1->type, param2->type))
					{
						return(false);
					}
					param1 = ListNextEntry(&t1->params, param1, ir_param, listElt);
					param2 = ListNextEntry(&t2->params, param2, ir_param, listElt);
				}
				if(param1 || param2)
				{
					return(false);
				}
				else
				{
					return(true);
				}
			}
		}

		default:
			ASSERT(0, "unreachable");
	}
}

ir_node* check_implicit_bool_conversion(checker_context* context, ir_node* ir)
{
	ir_type* baseType = ir_type_unwrap(ir->type);

	if(baseType->kind != TYPE_PRIMITIVE)
	{
		//TODO: some non-primitive types could be converted to bool, ie pointers?
		//      for now we only convert primitive types
		check_error(context, ir->ast, "can't convert expression to boolean type");
		return(ir); //TODO: should return a node with type error?
	}
	else if(baseType->primitive == TYPE_B8)
	{
		return(ir);
	}
	else
	{
		return(ir_implicit_cast(context, ir, ir_type_b8()));
	}
}

ir_node* check_implicit_conversion(checker_context* context, q_ast* ast, ir_node* ir, ir_type* expectedType)
{
	ir_type* baseType = ir_type_unwrap(ir->type);
	expectedType = ir_type_unwrap(expectedType);

	if(ir_type_is_equivalent(baseType, expectedType))
	{
		return(ir);
	}
	else if(expectedType->kind == TYPE_ANY)
	{
		//NOTE: handle boxing conversion. If src is a non-addressable value, we copy it to an anonymous variable first.
		checker_register_boxed_type(context, ir->type);

		if(!ir_is_addressable(ir))
		{
			ir_symbol* tmp = checker_add_anonymous_variable(context, ir->type);
			ir_node* var = ir_variable(context, ast, tmp);
			ir = ir_tstore(context, ast, var, ir);
		}
		return(ir_box(context, ast, ir));
	}
	else if(  baseType->kind != TYPE_PRIMITIVE
	       || expectedType->kind != TYPE_PRIMITIVE
	       || !ir->implicitlyConvertible)
	{
		check_error(context, ast, "non-convertible operand of incompatible type");
		return(ir); //TODO: should return a node with type error?
	}
	else if(ir_type_is_integer(expectedType) && !ir_type_is_integer(baseType))
	{
		check_error(context, ast, "implicit conversion from floating-point to integer type is forbidden");
		return(ir);
	}
	else
	{
		return(ir_implicit_cast(context, ir, expectedType));
	}
}

ir_node* check_arithmetic_binop(checker_context* context, q_ast* ast, ir_kind kind)
{
	q_ast* lhsAst = ast_first_child(ast);
	q_ast* rhsAst = ast_next_sibling(lhsAst);

	ir_node* lhs = check_value_expr(context, lhsAst);
	ir_node* rhs = check_value_expr(context, rhsAst);

	ir_type* resultType = check_arithmetic_result_type(context, lhs, rhs);

	if(!ir_type_is_equivalent(lhs->type, resultType))
	{
		lhs = ir_implicit_cast(context, lhs, resultType);
	}
	if(!ir_type_is_equivalent(rhs->type, resultType))
	{
		rhs = ir_implicit_cast(context, rhs, resultType);
	}
	ir_node* ir = ir_binop(context, ast, kind, resultType, lhs, rhs);
	return(ir);
}

ir_node* check_comparison(checker_context* context, q_ast* ast, ir_kind kind)
{
	ir_node* node = check_arithmetic_binop(context, ast, kind);
	node->type = ir_type_b8();
	return(node);
}

ir_node* check_logic_unop(checker_context* context, q_ast* ast)
{
	ir_node* opd = check_value_expr(context, ast_first_child(ast));
	opd = ir_implicit_cast(context, opd, ir_type_b8());

	ir_node* ir = ir_unop(context, ast, IR_LNOT, ir_type_b8(), opd);
	return(ir);
}

ir_node* check_logic_binop(checker_context* context, q_ast* ast, ir_kind kind)
{
	q_ast* lhsAst = ast_first_child(ast);
	q_ast* rhsAst = ast_next_sibling(lhsAst);

	ir_node* lhs = check_value_expr(context, lhsAst);
	ir_node* rhs = check_value_expr(context, rhsAst);

	ir_type* lhsBaseType = ir_type_unwrap(lhs->type);
	ir_type* rhsBaseType = ir_type_unwrap(rhs->type);

	//TODO: use convert to bool?
	if(lhsBaseType->kind != TYPE_PRIMITIVE)
	{
		check_error(context, lhsAst, "invalid operand type for logic operation");
	}
	else if(lhsBaseType->primitive != TYPE_B8)
	{
		lhs = ir_implicit_cast(context, lhs, ir_type_b8());
	}

	if(rhsBaseType->kind != TYPE_PRIMITIVE)
	{
		check_error(context, rhsAst, "invalid operand type for logic operation");
	}
	else if(rhsBaseType->primitive != TYPE_B8)
	{
		rhs = ir_implicit_cast(context, rhs, ir_type_b8());
	}

	ir_node* ir = ir_binop(context, ast, kind, ir_type_b8(), lhs, rhs);
	return(ir);
}


ir_node* check_var(checker_context* context, q_ast* ast)
{
	mp_string name = ast_copy_string(mem_scratch(), ast);
	ir_symbol* symbol = checker_find_symbol(context, name);
	if(!symbol)
	{
		check_error(context, ast, "unknown variable '%.*s'", (int)name.len, name.ptr);
		return(ir_error(context, ast));
	}
	if(symbol->kind != SYM_VAR)
	{
		check_error(context, ast, "symbol '%.*s' is not a variable", (int)name.len, name.ptr);
		//TODO: add a note to definition
		return(ir_error(context, ast));
	}

	ir_symbol* baseSymbol = symbol->capture.level ? symbol->capture.symbol : symbol;
	if(  baseSymbol->status != SYM_COMPLETE
	  && !(baseSymbol->global && context->proc))
	{
		//WARN(martin): we currently don't check global variables used-before-init in functions
		check_error(context, ast, "variable '%.*s' used before declaration", (int)name.len, name.ptr);
	}
	ir_node* ir = ir_variable(context, ast, symbol);
	return(ir);
}

ir_node* check_set(checker_context* context, q_ast* ast)
{
	q_ast* lhsAst = ast_first_child(ast);
	q_ast* rhsAst = ast_next_sibling(lhsAst);

	ir_node* lhs = check_address_expr(context, lhsAst);
	ir_node* rhs = check_initializer(context, rhsAst, lhs->type);

	ir_node* ir = 0;
	if(rhs->type->kind != TYPE_ERROR)
	{
		ir = ir_store(context, ast, lhs, rhs);
	}
	else
	{
		ir = ir_error(context, ast);
	}
	return(ir);
}

ir_node* check_at(checker_context* context, q_ast* ast)
{
	q_ast* indexAst = ast_first_child(ast);
	q_ast* objAst = ast_next_sibling(indexAst);

	ir_node* index = check_value_expr(context, indexAst);
	ir_node* object = check_address_expr(context, objAst);

	ir_type* objectBaseType = ir_type_unwrap(object->type);

	ir_type* indexBaseType = ir_type_unwrap(index->type);
	if(!ir_type_is_integer(indexBaseType))
	{
		check_error(context, indexAst, "index expression should be of integer type");
		return(ir_error(context, ast));
	}

	if(objectBaseType->kind != TYPE_ARRAY && objectBaseType->kind != TYPE_SLICE)
	{
		check_error(context, objAst, "indexed expression is not an array or slice");
		return(ir_error(context, ast));
	}

	index = ir_implicit_cast(context, index, ir_type_u64());

	ir_node* ir = ir_at(context, ast, index, object);
	return(ir);
}

ir_node* check_len(checker_context* context, q_ast* ast)
{
	q_ast* objAst = ast_first_child(ast);

	ir_node* object = check_address_expr(context, objAst);
	ir_type* objectBaseType = ir_type_unwrap(object->type);

	if(objectBaseType->kind != TYPE_ARRAY && objectBaseType->kind != TYPE_SLICE)
	{
		check_error(context, objAst, "len operator must be applied to an array or slice");
		return(ir_error(context, objAst));
	}
	ir_node* ir = ir_len(context, ast, object);
	return(ir);
}


ir_node* check_field(checker_context* context, q_ast* ast)
{
	q_ast* nameAst = ast_first_child(ast);
	q_ast* objAst = ast_next_sibling(nameAst);

	mp_string name = ast_copy_string(mem_scratch(), nameAst);

	ir_node* object = check_address_expr(context, objAst);
	ir_type* objectBaseType = ir_type_unwrap(object->type);

	if(objectBaseType->kind == TYPE_POINTER)
	{
		object = ir_deref(context, objAst, object, objectBaseType->eltType);
		objectBaseType = ir_type_unwrap(object->type);
	}

	if(objectBaseType->kind != TYPE_STRUCT)
	{
		check_error(context, objAst, "accessed expression is not a struct");
		return(ir_error(context, objAst));
	}

	for_each_in_list(&objectBaseType->params, param, ir_param, listElt)
	{
		if(!mp_string_cmp(param->name, name))
		{
			ir_node* ir = ir_field(context, ast, object, param);
			return(ir);
		}
	}
	check_error(context, nameAst, "struct does not contain a '%.*s' field", (int)name.len, name.ptr);
	return(ir_error(context, ast));
}

ir_node* check_slice_transient(checker_context* context, q_ast* ast)
{
	q_ast* startAst = ast_first_child(ast);
	q_ast* endAst = ast_next_sibling(startAst);
	q_ast* objAst = ast_next_sibling(endAst);

	ir_node* start = check_initializer(context, startAst, ir_type_u64());
	ir_node* end = check_initializer(context, endAst, ir_type_u64());
	ir_node* object = check_address_expr(context, objAst);

	ir_type* objectBaseType = ir_type_unwrap(object->type);

	ir_type* dstType = 0;
	switch(objectBaseType->kind)
	{
		case TYPE_ARRAY:
			dstType = ir_type_slice(context, object->type->eltType);
			break;

		case TYPE_SLICE:
			dstType = object->type;
			break;

		default:
			check_error(context, objAst, "slice operand must be of type array or slice");
			dstType = ir_type_error();
			break;
	}
	ir_node* ir = ir_slice(context, ast, start, end, object, dstType);
	return(ir);
}

ir_node* check_slice(checker_context* context, q_ast* ast)
{
	//NOTE: slices an object (array or slice) into an anonymous slice variable.
	// This is used when the slice operator is explictly used inside expressions.

	ir_node* slice = check_slice_transient(context, ast);
	ir_symbol* tmp = checker_add_anonymous_variable(context, slice->type);
	ir_node* var = ir_variable(context, ast, tmp);
	ir_node* ir = ir_tstore(context, ast, var, slice);

	return(ir);
}

ir_node* check_sizeof(checker_context* context, q_ast* ast)
{
	/////////////////////////////////////////
	//TODO: check_expr_or_typespec
	/////////////////////////////////////////
	q_ast* typeAst = ast_first_child(ast);
	ir_type* type = check_typespec(context, typeAst);
	ir_type* baseType = ir_type_unwrap(type);
	if(type->kind == TYPE_VOID)
	{
		check_error(context, typeAst, "Cannot take size of void");
	}
	ir_node* ir = ir_int(context, ast, baseType->size);
	return(ir);
}

ir_node* check_string_initializer(checker_context* context, q_ast* ast, ir_type* dstType)
{
	mp_string data = ast_copy_string(context->arena, ast);
	ir_type* type = ir_type_array(context, data.len, ir_type_u8());
	ir_symbol* rodataSymbol = checker_add_rodata(context, type, data);

	ir_node* ir = 0;

	ir_type* dstBaseType = ir_type_unwrap(dstType);
	if(dstBaseType->kind == TYPE_ARRAY)
	{
		ir_type* eltBaseType = ir_type_unwrap(dstBaseType->eltType);
		if(eltBaseType->kind == TYPE_PRIMITIVE
		  && eltBaseType->primitive == TYPE_U8)
		{
			if(dstBaseType->eltCount > data.len)
			{
				check_error(context, ast, "string initializer is too short for destination");
			}
			else if(dstBaseType->eltCount < data.len)
			{
				check_error(context, ast, "string initializer is too long for destination");
			}

			ir = ir_rodata(context, ast, rodataSymbol);
		}
	}
	else if(dstBaseType->kind == TYPE_SLICE)
	{
		ir_type* eltBaseType = ir_type_unwrap(dstBaseType->eltType);
		if(eltBaseType->kind == TYPE_PRIMITIVE
		  && eltBaseType->primitive == TYPE_U8)
		{
			ir_node* rodata = ir_rodata(context, ast, rodataSymbol);
			ir_node* start = ir_int(context, ast, 0);
			ir_node* end = ir_int(context, ast, data.len);
			ir = ir_slice(context, ast, start, end, rodata, dstType);
		}
	}
	else if(dstBaseType->kind == TYPE_ANY)
	{
		ir_type* sliceType = ir_type_slice(context, ir_type_u8());
		checker_register_boxed_type(context, sliceType);

		ir_node* rodata = ir_rodata(context, ast, rodataSymbol);
		ir_node* start = ir_int(context, ast, 0);
		ir_node* end = ir_int(context, ast, data.len);
		ir_node* slice = ir_slice(context, ast, start, end, rodata, sliceType);
		ir_symbol* tmp = checker_add_anonymous_variable(context, sliceType);
		ir_node* var = ir_variable(context, ast, tmp);
		ir_node* store = ir_tstore(context, ast, var, slice);

		ir = ir_box(context, ast, store);
	}

	if(!ir)
	{
		check_error(context, ast, "string initializer can only initialize array or slice of u8 type");
		ir = ir_error(context, ast); //TODO: shouldn't the range be the entire affectation node?
	}

	return(ir);
}

ir_node* check_array_initializer(checker_context* context, q_ast* ast, ir_type* dstType)
{
	ir_node* ir = 0;
	ir_type* dstBaseType = ir_type_unwrap(dstType);

	if(dstBaseType->kind == TYPE_ARRAY)
	{
		ir = ir_alloc(context, ast, IR_ARRAY_INIT, dstType);

		u64 eltIndex = 0;
		q_ast* child = ast_first_child(ast);
		while(child && eltIndex < dstBaseType->eltCount)
		{
			ir_node* elt = check_initializer(context, child, dstBaseType->eltType);
			ir_push(ir, elt);

			child = ast_next_sibling(child);
			eltIndex++;
		}
		//NOTE: here we allow non-initialized elements at end of array

		while(child)
		{
			check_error(context, child, "excess element in array initializer");

			ir_node* elt = check_initializer(context, child, dstBaseType->eltType);
			ir_push(ir, elt);
			child = ast_next_sibling(child);
		}
	}
	else if(dstBaseType->kind == TYPE_SLICE)
	{
		ir_type* backingType = ir_type_array(context, ast->childCount, dstBaseType->eltType);

		ir_symbol* backingSymbol = checker_add_anonymous_variable(context, backingType);
		ir_node* backing = ir_variable(context, ast, backingSymbol);

		ir_node* arrayInit = ir_alloc(context, ast, IR_ARRAY_INIT, backingType);
		for_each_in_list(&ast->children, child, q_ast, listElt)
		{
			ir_node* elt = check_initializer(context, child, dstBaseType->eltType);
			ir_push(arrayInit, elt);
		}

		ir_node* array = ir_tstore(context, ast, backing, arrayInit);
		ir_node* start = ir_int(context, ast, 0);
		ir_node* end = ir_int(context, ast, ast->childCount);
		ir = ir_slice(context, ast, start, end, array, dstType);
	}
	else
	{
		check_error(context, ast, "array initializer can only initialize an array or slice type");
		ir = ir_error(context, ast); //TODO: shouldn't the range be the entire affectation node?
	}

	return(ir);
}

ir_node* check_nil_initializer(checker_context* context, q_ast* srcAst, ir_type* expectedType)
{
	ir_node* ir = 0;
	ir_type* baseType = ir_type_unwrap(expectedType);
	if(baseType->kind == TYPE_POINTER)
	{
		ir = ir_alloc(context, srcAst, IR_POINTER_NIL, expectedType);
	}
/*TODO
	else if(baseType->kind == IR_TYPE_SLICE)
	{
		node = ir_node_alloc(context, srcNode->range, IR_SLICE_NIL, expectedType);
	}
*/	else
	{
		check_error(context, srcAst, "nil initializer can only be used with slice or pointer types");
		ir = ir_error(context, srcAst);
	}
	return(ir);
}

ir_node* check_initializer(checker_context* context, q_ast* ast, ir_type* dstType)
{
	//NOTE: Check initializer. The result may represent a temporary value that needs to be stored.
	ir_node* ir = 0;
	switch(ast->kind)
	{
		case AST_STRING:
			ir = check_string_initializer(context, ast, dstType);
			break;

		case AST_ARRAY:
			ir = check_array_initializer(context, ast, dstType);
			break;

		case AST_SLICE:
		{
			ir = check_slice_transient(context, ast);
			if(!ir_type_is_equivalent(ir->type, dstType))
			{
				check_error(context, ast, "invalid type for initializer");
			}

		} break;

		case AST_CALL:
		{
			ir = check_call_initializer(context, ast);
			if(ir->type->kind != TYPE_ERROR)
			{
				ir = check_implicit_conversion(context, ast, ir, dstType);
			}
		} break;

		case AST_FLOW:
			ir = check_flow(context, ast, dstType);
			break;

		case AST_NIL:
			ir = check_nil_initializer(context, ast, dstType);
			break;

		default:
		{
			ir = check_value_expr(context, ast);
			if(ir->type->kind != TYPE_ERROR)
			{
				ir = check_implicit_conversion(context, ast, ir, dstType);
			}
		} break;
	}

	return(ir);
}

ir_node* check_transient_initializer(checker_context* context, q_ast* ast, ir_type* dstType)
{
	//NOTE: Check initializer and store it in a temporary variable if it doesn't fit on operand stack
	// This is used when the parent doesn't explicitly stores the value but it needs to persist or be addressed.
	// (e.g. a slicing operation that is then indexed)

	ir_node* ir = check_initializer(context, ast, dstType);
	if(  ir->kind == IR_SLICE
	  || ir->kind == IR_BOX)
	{
		ir_symbol* tmp = checker_add_anonymous_variable(context, dstType);
		ir_node* var = ir_variable(context, ast, tmp);
		ir = ir_tstore(context, ast, var, ir);
	}
	//TODO handle other compound values
	return(ir);
}

ir_node* check_ref(checker_context* context, q_ast* ast)
{
	q_ast* opd = ast_first_child(ast);
	ir_node* expr = check_address_expr(context, opd);
	ir_node* ir = ir_ref(context, ast, expr);
	return(ir);
}

ir_node* check_deref(checker_context* context, q_ast* ast)
{
	q_ast* opd = ast_first_child(ast);
	ir_node* ptr = check_value_expr(context, opd);
	ir_type* basePtrType = ir_type_unwrap(ptr->type);
	ir_type* eltType = basePtrType->eltType;
	if(basePtrType->kind != TYPE_POINTER)
	{
		check_error(context, ast, "deref only applies to pointer types");
		eltType = ir_type_error();
	}
	ir_node* ir = ir_deref(context, ast, ptr, eltType);
	return(ir);
}

ir_node* check_cast(checker_context* context, q_ast* ast)
{
	q_ast* typeAst = ast_first_child(ast);
	q_ast* opd = ast_next_sibling(typeAst);

	ir_type* dstType = check_typespec(context, typeAst);
	ir_node* expr = check_value_expr(context, opd);

	ir_type* baseDstType = ir_type_unwrap(dstType);
	ir_type* baseSrcType = ir_type_unwrap(expr->type);

	if( (baseDstType->kind != TYPE_POINTER
	    || baseSrcType->kind != TYPE_POINTER)
	  &&(baseDstType->kind != TYPE_PRIMITIVE
	    || baseSrcType->kind != TYPE_PRIMITIVE))
	{
		check_error(context, ast, "cast only applies to pointer or primitive types");
	}

	ir_node* ir = ir_cast(context, ast, expr, dstType);
	return(ir);
}

int check_infer_binding_from_concrete_type(checker_context* context, list_info* bindings, ir_type* poly, ir_type* concrete)
{
	ir_type* polyBase = ir_type_unwrap(poly);
	ir_type* concreteBase = ir_type_unwrap(concrete);

	if(polyBase->kind == TYPE_POLY)
	{
		// check consistency
		for_each_in_list(bindings, binding, ir_poly_binding, listElt)
		{
			if(!mp_string_cmp(poly->name, binding->name))
			{
				if(!ir_type_is_equivalent(concrete, binding->type))
				{
					//////////////////////////////////////////////////////////////////////////////
					//TODO should attach the error to def but we don't have a range here?
					//////////////////////////////////////////////////////////////////////////////
				}
				return(-1);
			}
		}

		//NOTE: create binding
		ir_poly_binding* binding = mem_arena_alloc_type(context->arena, ir_poly_binding);
		binding->name = poly->name;
		binding->type = concrete;
		ListAppend(bindings, &binding->listElt);

		return(0);
	}
	else if(polyBase->kind != concreteBase->kind)
	{
		//TODO: ERROR
		return(-1);
	}
	else
	{
		switch(polyBase->kind)
		{
			case TYPE_ARRAY:
			{
				if(polyBase->eltCount != concreteBase->eltCount)
				{
					//TODO: ERROR
					return(-1);
				}
				check_infer_binding_from_concrete_type(context, bindings, polyBase->eltType, concreteBase->eltType);
			} break;

			case TYPE_SLICE:
			case TYPE_FUTURE:
			case TYPE_POINTER:
				check_infer_binding_from_concrete_type(context, bindings, polyBase->eltType, concreteBase->eltType);
				break;

			case TYPE_STRUCT:
			{
				ASSERT("not implemented yet");
			} break;

			default:
				break;
		}
		return(0);
	}
}


ir_symbol* check_instantiate_polymorphic_procedure(checker_context* context, ir_symbol* polyProc, list_info* polyBindings)
{
	//NOTE(martin): create the proc instance signature
	ir_type* instanceType = ir_type_alloc(context);
	instanceType->kind = TYPE_PROC;
	instanceType->retType = polyProc->type->retType;

	u64 paramOffset = 0;
	for_each_in_list(&polyProc->type->params, polyParam, ir_param, listElt)
	{
		if(polyParam->type->kind != TYPE_POLY_TYPEID)
		{
			ir_param* instanceParam = ir_param_alloc(context);
			instanceParam->name = polyParam->name;
			instanceParam->type = check_instantiate_polymorphic_type(context, polyParam->type, polyBindings);

			ListAppend(&instanceType->params, &instanceParam->listElt);
			instanceType->eltCount++;
		}
	}

	//TODO: coalesce with check_type_proc
	//NOTE: handle return type
	instanceType->retType = check_instantiate_polymorphic_type(context, polyProc->type->retType, polyBindings);
	ir_type* baseRetType = ir_type_unwrap(instanceType->retType);

	if(baseRetType->kind == TYPE_ANY)
	{
		q_ast* typeAst = ast_get_operand(polyProc->ast, 1);
		q_ast* retParamAst = ast_get_operand(typeAst, 1);
		check_error(context, retParamAst, "type 'any' can't be returned from a procedure");
	}

	if(  baseRetType->kind == TYPE_STRUCT
	  || baseRetType->kind == TYPE_SLICE
	  || baseRetType->kind == TYPE_ARRAY)
	{
		ir_param* param = ir_param_alloc(context);
		param->name = mp_string_lit("<hidden return pointer>");
		param->type = ir_type_pointer(context, instanceType->retType);
		param->offset = 0;

		instanceType->retParam  = param;
	}

	//NOTE(martin): create the proc instance symbol
	ir_symbol* instance = ir_symbol_alloc(context);
	instance->kind = SYM_PROC;
	instance->name = polyProc->name;
	instance->type = instanceType;
	instance->ast = polyProc->ast;

	//NOTE(martin): check body and add proc to global function list
	q_ast* bodyAst = ast_get_operand(instance->ast, 2);
	check_proc_body(context, bodyAst, instance, polyBindings);
	ListAppend(&context->module->procedures, &instance->auxElt);

	//WARN: polymorphic instances are not put in the symbol table, so we use bucketElt to chain them into
	//      module->polyProcInstances list
	ListAppend(&context->module->polyProcInstances, &instance->bucketElt);

	return(instance);
}

ir_symbol* check_find_or_instantiate_polymorphic_procedure(q_workspace* workspace, q_module* module, ir_symbol* polyProc, list_info* polyBindings)
{
	ir_symbol* res = 0;
	//TODO: find instance

	if(!res)
	{
		checker_context context;
		checker_context_init(&context, workspace, module);
		context.scopeStackTop = module->globalScope;

		res = check_instantiate_polymorphic_procedure(&context, polyProc, polyBindings);
	}
	return(res);
}

ir_node* check_call_initializer(checker_context* context, q_ast* ast)
{
	q_ast* nameAst = ast_first_child(ast);
	q_ast* argListAst = ast_next_sibling(nameAst);

	mp_string name = ast_copy_string(mem_scratch(), nameAst);

	checker_find_result findResult = checker_find_symbol_and_module(context, name);
	q_module* procModule = findResult.module;
	ir_symbol* procSymbol = findResult.symbol;

	if(!procSymbol)
	{
		check_error(context, ast, "unknown procedure '%.*s'", (int)name.len, name.ptr);
		return(ir_error(context, ast));
	}
	if(procSymbol->kind != SYM_PROC)
	{
		check_error(context, ast, "symbol '%.*s' is not a function", (int)name.len, name.ptr);
		return(ir_error(context, ast));
	}
	DEBUG_ASSERT(procSymbol->type->kind == TYPE_PROC);

	//NOTE: create call node
	ir_node* call = ir_call(context, ast, procSymbol);

	//NOTE(martin): check arguments
	ir_param* param = ListFirstEntry(&procSymbol->type->params, ir_param, listElt);

	list_info* argList = &argListAst->children;
	q_ast* argAst = ListFirstEntry(argList, q_ast, listElt);

	ir_node* variadicInitializer = 0;
	ir_param* variadicParam = 0;
	list_info polyBindings = {0};

	u32 argIndex = 0;
	while(param != 0)
	{
		if(param->variadic)
		{
			//NOTE: count arguments
			u32 vargCount = argListAst->childCount - argIndex;

			//TODO: if there's no arg, create a dummy ast node for the empty variadic slice
			q_ast* firstArgAst = argAst;
			if(!argAst)
			{
				q_range range = {argListAst->range.end, argListAst->range.end};
				argAst = ast_alloc(context->module, AST_OPT_EMPTY);
				argAst->range = range;
				ast_push(argListAst, argAst);

				firstArgAst = argAst;
			}

			DEBUG_ASSERT(param->type->kind == TYPE_SLICE);

			//NOTE: get type of element
			ir_node* firstArg = 0;
			ir_type* vaType = 0;

			if(param->type->polymorphic)
			{
				if(argAst->kind != AST_OPT_EMPTY)
				{
					firstArg = check_value_expr(context, argAst);
					vaType = firstArg->type;
					check_infer_binding_from_concrete_type(context, &polyBindings, param->type->eltType, firstArg->type);
				}
				else
				{
					mp_string msg = mp_string_pushf(context->arena, "polymorphic variadic parameter is ambiguous without argument");
					mp_string cellHint = mp_string_pushf(context->arena, "%.*s", (int)param->name.len, param->name.ptr);
					check_error_missing(context, argListAst, msg, cellHint);
					vaType = ir_type_error();
				}
			}
			else
			{
				vaType = param->type->eltType;
			}

			//NOTE: create a variadic initializer
			ir_type* initArrayType = ir_type_array(context, vargCount, vaType);
			ir_type* initSliceType = ir_type_slice(context, vaType);

			//WARN: variadic initializer is a special case, because it is produced by the combination
			//      of several AST nodes, that can't be grouped during parsing (since it requires knowing
			//      the procedure signature). Rather than complicating the tracing of all IR nodes, we affect
			//      a "wrong" range here. However, this is not too damageable, since the variadic initializer
			//      itself should be transparent to the source map, where only the individual arguments should
			//      appear.

			//NOTE: build array initializer and store it to anonymous array variable
			ir_node* initArray = ir_alloc(context, firstArgAst, IR_ARRAY_INIT, initArrayType);

			if(firstArg)
			{
				ir_push(initArray, firstArg);
				argAst = ListNextEntry(argList, firstArgAst, q_ast, listElt);
			}
			while(argAst)
			{
				if(argAst->kind != AST_OPT_EMPTY)
				{
					ir_node* irArg = check_transient_initializer(context, argAst, vaType);

					if(irArg->type->kind == TYPE_ERROR)
					{
						q_cell* cell = argAst->range.start.leftFrom;
						if(cell && cell->kind == CELL_HOLE)
						{
							mp_string msg = mp_string_pushf(mem_scratch(), "varg %.*s", (int)param->name.len, param->name.ptr);
							cell_text_replace(cell, msg);
						}
						check_error(context, argAst, "wrong type for variadic argument %.*s", (int)param->name.len, param->name.ptr);
					}

					ir_push(initArray, irArg);
				}
				argAst = ListNextEntry(argList, argAst, q_ast, listElt);
			}
			ir_symbol* initArraySymbol = checker_add_anonymous_variable(context, initArrayType);
			ir_node* initArrayVar = ir_variable(context, firstArgAst, initArraySymbol);
			initArray = ir_tstore(context, firstArgAst, initArrayVar, initArray);

			//NOTE: slice array variable into anonymous slice variable
			ir_node* start = ir_int(context, firstArgAst, 0);
			ir_node* end = ir_int(context, firstArgAst, vargCount);
			ir_node* initSlice = ir_slice(context, firstArgAst, start, end, initArray, initSliceType);

			ir_symbol* initSliceSymbol = checker_add_anonymous_variable(context, initSliceType);
			ir_node* initSliceVar = ir_variable(context, firstArgAst, initSliceSymbol);

			ir_node* variadicInitializer = ir_tstore(context, firstArgAst, initSliceVar, initSlice);

			variadicParam = param;

			ir_push(call, variadicInitializer);

			param = ListNextEntry(&procSymbol->type->params, param, ir_param, listElt);
			break;
		}
		else if(argAst)
		{
			if(param->type->kind == TYPE_POLY_TYPEID)
			{
				ir_type* type = check_typespec(context, argAst);
				ir_poly_binding* binding = mem_arena_alloc_type(context->arena, ir_poly_binding);
				binding->name = param->name;
				binding->type = type;
				ListAppend(&polyBindings, &binding->listElt);
			}
			else
			{
				//NOTE(martin): check initializer, or infer binding for polymorphic parameters. Type mismatches
				//              are checked in these callees.
				ir_node* arg = 0;
				if(param->type->polymorphic)
				{
					arg = check_value_expr(context, argAst);
					check_infer_binding_from_concrete_type(context, &polyBindings, param->type, arg->type);
				}
				else
				{
					arg = check_transient_initializer(context, argAst, param->type);
				}

				if(arg->type->kind == TYPE_ERROR)
				{
					q_cell* cell = argAst->range.start.leftFrom;
					if(cell && cell->kind == CELL_HOLE)
					{
						mp_string msg = mp_string_pushf(mem_scratch(), "argument %.*s", (int)param->name.len, param->name.ptr);
						cell_text_replace(cell, msg);
					}
					check_error(context, argAst, "wrong type for argument %.*s", (int)param->name.len, param->name.ptr);
				}

				//NOTE: if param type is an array, convert the argument to a slice of same type
				/*
				ir_type* paramBaseType = ir_type_unwrap(param->type);
				if(paramBaseType->kind == IR_TYPE_ARRAY)
				{
					ir_type* passedType = ir_type_slice(context, paramBaseType->eltType);
					arg = check_implicit_conversion(context, arg, passedType);
				}
				*/
				ir_push(call, arg);
			}

			param = ListNextEntry(&procSymbol->type->params, param, ir_param, listElt);
			argAst = ListNextEntry(argList, argAst, q_ast, listElt);

			argIndex++;
		}
		else
		{
			break;
		}
	}

	//NOTE: check missing or excess arguments
	while(param != 0)
	{
		mp_string msg = mp_string_pushf(context->arena, "missing argument %.*s", (int)param->name.len, param->name.ptr);
		mp_string cellHint = mp_string_pushf(context->arena, "%.*s", (int)param->name.len, param->name.ptr);
		check_error_missing(context, argListAst, msg, cellHint);
		param = ListNextEntry(&procSymbol->type->params, param, ir_param, listElt);
	}
	while(argAst != 0)
	{
		q_cell* cell = argAst->range.start.leftFrom;
		if(cell && cell->kind == CELL_HOLE)
		{
			cell_text_replace(cell, mp_string_lit("excess argument"));
		}
		check_error(context, argAst, "excess argument");
		argAst = ListNextEntry(argList, argAst, q_ast, listElt);
	}

	//NOTE: find or instantiate polymorphic procedure
	if(procSymbol->type->polymorphic)
	{
		procSymbol = check_find_or_instantiate_polymorphic_procedure(context->workspace, procModule, procSymbol, &polyBindings);
		call->symbol = procSymbol;
		call->type = procSymbol->type->retType;
	}

	return(call);
}

ir_node* check_call(checker_context* context, q_ast* ast)
{
	ir_node* ir = check_call_initializer(context, ast);

	//NOTE(martin): if return type of proc is an aggregate, reserve an anonymous variable in the current frame
	//              to store the return value.
	ir_type* returnBaseType = ir_type_unwrap(ir->type);
	if(  returnBaseType->kind == TYPE_STRUCT
	  || returnBaseType->kind == TYPE_SLICE
	  || returnBaseType->kind == TYPE_ARRAY)
	{
		ir_symbol* retSlot = checker_add_anonymous_variable(context, ir->type);
		ir_node* retVar = ir_variable(context, ast, retSlot);

		ir = ir_tstore(context, ast, retVar, ir);
	}

	return(ir);
}

ir_node* check_return(checker_context* context, q_ast* ast)
{
	if(!context->proc)
	{
		check_error(context, ast, "return statement outside function body");
		return(ir_error(context, ast));
	}

	q_ast* opdAst = ast_first_child(ast);
	ir_node* expr = 0;

	if(context->proc->proc.inferReturnType)
	{
		ir_type* retType = ir_type_void();
		if(opdAst)
		{
			if(  opdAst->kind == AST_STRING
			  || opdAst->kind == AST_ARRAY)
			{
				check_error(context,
				            opdAst,
				            "can't infer concrete return type from %s literal",
				            AST_KIND_STRINGS[opdAst->kind]);
			}
			else
			{
				///////////////////////////////////////////////////////////////////////
				//TODO: this prevent returning slices/struct/arrays efficiently...
				///////////////////////////////////////////////////////////////////////
				expr = check_value_expr(context, opdAst);
				retType = expr->type;
			}
		}
		context->proc->type->retType = retType;
		context->proc->proc.inferReturnType = false;
	}
	else
	{
		//NOTE: return type is checked in check_initializer().
		//TODO: maybe the error in check_initializer() is too generic...
		if(opdAst)
		{
			expr = check_initializer(context, opdAst, context->proc->type->retType);
		}
		else
 		{
 			ir_type* baseRetType = 	ir_type_unwrap(context->proc->type->retType);
 			if(baseRetType->kind != TYPE_VOID)
 			{
				check_error(context, ast, "non-void function should return a value");
			}
		}
	}

	ir_node* ir = ir_return(context, ast, expr);
	return(ir);
}


ir_node* check_value_expr(checker_context* context, q_ast* ast)
{
	ir_node* ir = check_ast(context, ast);
	if(ir->type->kind == TYPE_VOID)
	{
		check_error(context, ast, "value-expression cannot have type 'void'");
	}

	/*
	ir_type* baseType = ir_type_unwrap(ir->type);
	if(ir->mode == IR_ADDRESS
	  && (  ir->type->kind == TYPE_PRIMITIVE
	     || ir->type->kind == TYPE_POINTER
	     || ir->type->kind == TYPE_FUTURE))
	{
		ir = ir_load(context, ir);
		ir->mode = IR_VALUE;
	}
	*/
	return(ir);
}

ir_node* check_address_expr(checker_context* context, q_ast* ast)
{
	//Coalesce with ir_is_addressable()
	switch(ast->kind)
	{
		case AST_ID:
			return(check_var(context, ast));
		case AST_AT:
			return(check_at(context, ast));
		case AST_FIELD:
			return(check_field(context, ast));
		case AST_SLICE:
			return(check_slice(context, ast));
		case AST_DEREF:
			return(check_deref(context, ast));

		case AST_CALL:
		{
			ir_node* node = check_call(context, ast);
			ir_type* baseRetType = ir_type_unwrap(node->type);
			if( baseRetType->kind == TYPE_ARRAY
			  ||baseRetType->kind == TYPE_SLICE
			  ||baseRetType->kind == TYPE_STRUCT)
			{
				return(node);
			}
		} break;
/*
		case AST_WAIT:
		case AST_RWAIT:
		{
			//TODO should check type of return is pointer or array or slice
			ir_node* node = check_wait(context, ast, (ast->kind == AST_RWAIT));
			ir_type* baseRetType = ir_type_unwrap(node->type);
			if( baseRetType->kind == TYPE_ARRAY
			  ||baseRetType->kind == TYPE_SLICE
			  ||baseRetType->kind == TYPE_STRUCT)
			{
				return(node);
			}
		} break;
*/
		default:
			break;
	}
	check_error(context, ast, "expression is not addressable");
	return(ir_error(context, ast));
}

ir_node* check_variable_initializer(checker_context* context, q_ast* ast)
{
	q_ast* nameAst = ast_first_child(ast);
	q_ast* typeAst = ast_next_sibling(nameAst);
	q_ast* initAst = ast_next_sibling(typeAst);

	ir_node* ir = 0;
	if(nameAst->kind == AST_ID)
	{
		mp_string name = ast_copy_string(context->arena, nameAst);

		//NOTE(martin):
		//  if symbol is not found or is not a variable, an error should have already been generated in check_global_variables(),
		//  so we don't need to generate it here. We also check that we only generate an init for the declaration we checked.
		ir_symbol* symbol = checker_find_symbol(context, name);
		if(symbol && symbol->kind == SYM_VAR && symbol->ast == ast)
		{
			if(initAst)
			{
				ir_node* var = ir_variable(context, nameAst, symbol);

				//NOTE: check initializer, but special-case explicit slice operator in order to avoid creating a temporary variable
				ir = check_initializer(context, initAst, symbol->type);

				ir->expectedTypePattern.kind = TYPE_PATTERN_EXACT;
				ir->expectedTypePattern.exactType = symbol->type;

				if(ir->kind != IR_ERROR)
				{
					ir = ir_store(context, ast, var, ir);
				}
			}
			symbol->status = SYM_COMPLETE;
		}
	}
	return(ir);
}

ir_node* check_builtin_put(checker_context* context, q_ast* ast)
{
	ir_node* ir = ir_alloc(context, ast, IR_PUT, ir_type_void());

	for_each_in_list(&ast->children, childAst, q_ast, listElt)
	{
		ir_node* expr = 0;
		if(childAst->kind == AST_STRING)
		{
			expr = check_transient_initializer(context, childAst, ir_type_slice(context, ir_type_u8()));
		}
		else
		{
			expr = check_value_expr(context, childAst);
			ir_type* exprBaseType = ir_type_unwrap(expr->type);

			if(exprBaseType->kind == TYPE_PRIMITIVE)
			{
				if(ir_type_is_integer(exprBaseType))
				{
					if(ir_type_is_signed(exprBaseType))
					{
						expr = ir_implicit_cast(context, expr, ir_type_i64());
					}
					else
					{
						expr = ir_implicit_cast(context, expr, ir_type_u64());
					}
				}
			}
			else if(exprBaseType->kind != TYPE_SLICE)
			{
				check_error(context, childAst, "'put' operand should be a primitive or string type");
			}
		}
		if(expr)
		{
			ir_push(ir, expr);
		}
	}
	return(ir);
}

//------------------------------------------------------------------------------------
// Check control-flow
//------------------------------------------------------------------------------------
ir_node* check_multi_and_get_scope(checker_context* context, q_ast* ast, scope_kind scopeKind, ir_scope** scope)
{
	ir_node* ir = ir_alloc(context, ast, IR_BODY, ir_type_void());

	checker_scope_block(context, scopeKind, ir)
	{
		if(scope)
		{
			*scope = checker_scope_top(context);
		}
		//NOTE(martin): first parse all top-level definitions
		check_variable_declarations_in_scope(context, ast);

		//NOTE(martin): parse statements
		for_each_in_list(&ast->children, astChild, q_ast, listElt)
		{
			ir_node* child = check_ast(context, astChild);
			if(child)
			{
				ir_push(ir, child);
			}
		}
	}
	return(ir);
}

ir_node* check_multi(checker_context* context, q_ast* ast)
{
	return(check_multi_and_get_scope(context, ast, SCOPE_NORMAL, 0));
}

ir_node* check_if(checker_context* context, q_ast* ast)
{
	q_ast* condAst = ast_first_child(ast);
	q_ast* branchIfAst = ast_next_sibling(condAst);
	q_ast* branchElseAst = ast_next_sibling(branchIfAst);

	ir_node* condition = check_value_expr(context, condAst);
	condition = check_implicit_bool_conversion(context, condition);

	ir_node* branchIf = check_ast(context, branchIfAst);
	ir_node* branchElse = 0;
	if(branchElseAst)
	{
		branchElse = check_ast(context, branchElseAst);

		//TODO: allow implicit conversion to result type if one of the branch is a literal expression
		if(  branchIf->type->kind != TYPE_ERROR
		  && branchElse->type->kind != TYPE_ERROR
		  && !ir_type_is_equivalent(branchIf->type, branchElse->type))
		{
			check_error(context, ast, "branches of if statement must have equivalent types");
		}
	}
	ir_node* ir = ir_if(context, ast, condition, branchIf, branchElse);
	return(ir);
}

ir_node* check_while(checker_context* context, q_ast* ast)
{
	q_ast* condAst = ast_first_child(ast);
	q_ast* bodyAst = ast_next_sibling(condAst);

	ir_node* condition = check_value_expr(context, condAst);
	condition = check_implicit_bool_conversion(context, condition);

	ir_node* body = check_multi(context, bodyAst);

	ir_node* ir = ir_while(context, ast, condition, body);
	return(ir);
}

ir_node* check_for(checker_context* context, q_ast* ast)
{
	ir_node* ir = ir_alloc(context, ast, IR_FOR, ir_type_void());

	checker_scope_block(context, SCOPE_NORMAL, ir)
	{
		q_ast* initAst = ast_first_child(ast);
		q_ast* condAst = ast_next_sibling(initAst);
		q_ast* incrementAst = ast_next_sibling(condAst);
		q_ast* bodyAst = ast_next_sibling(incrementAst);

		if(initAst->kind == AST_VAR_DECL)
		{
			check_variable_declaration(context, initAst);
		}
		ir_node* init = check_ast(context, initAst);
		if(!init)
		{
			init = ir_alloc(context, initAst, IR_NOP, ir_type_void());
		}

		ir_node* condition = check_value_expr(context, condAst);
		condition = check_implicit_bool_conversion(context, condition);

		ir_node* increment = check_ast(context, incrementAst);
		ir_node* body = check_multi(context, bodyAst);

		ir_push(ir, init);
		ir_push(ir, condition);
		ir_push(ir, increment);
		ir_push(ir, body);
	}
	return(ir);
}

//------------------------------------------------------------------------------------
// Check temporal constructs
//------------------------------------------------------------------------------------

ir_node* check_flow(checker_context* context, q_ast* ast, ir_type* expectedType)
{
	//NOTE(martin): flow block creates an anonymous function
	ir_symbol* procSymbol = ir_symbol_alloc(context);
	procSymbol->kind = SYM_PROC;
	procSymbol->name = mp_string_lit("<anonymous function>");
	//TODO: coalesce with check_type_proc (extract in ir_type_proc()??)
	procSymbol->type = ir_type_alloc(context);
	procSymbol->type->kind = TYPE_PROC;
	procSymbol->type->retType = ir_type_void();
	procSymbol->proc.inferReturnType = true;

	if(expectedType)
	{
		ir_type* baseExpectedType = ir_type_unwrap(expectedType);
		if(baseExpectedType->kind == TYPE_FUTURE)
		{
			procSymbol->type->retType = baseExpectedType->retType;
			procSymbol->proc.inferReturnType = false;
		}
		//NOTE(martin): if base expected type is not a future type, this will be catched in caller that provided expectedType
	}

	q_ast* tempoSpecAst = ast_first_child(ast);
	q_ast* bodyAst = ast_next_sibling(tempoSpecAst);
	ir_node* tempo = 0;

	if(tempoSpecAst->kind == AST_TEMPO_EDITOR)
	{
		//TODO: generate tempo editor IR
		ir_type* type = ir_type_pointer(context, ir_type_tempo_curve(context));
		ir_symbol* curvePtr = checker_add_anonymous_global(context, type);

		q_cell* cell = tempoSpecAst->range.start.leftFrom;
		ir_symbol* widgetSymbol = checker_add_editor_widget(context, cell, curvePtr);

		tempo = ir_tempo_curve(context, tempoSpecAst, curvePtr);
	}

	//NOTE(martin): check body statements as if it were a function's body

	ir_symbol* saveProc = context->proc;
	context->proc = procSymbol;

	ir_scope* scope = 0;
	ir_node* body = check_multi_and_get_scope(context, bodyAst, SCOPE_TASK, &scope);

	//NOTE(martin): if the block returns a struct, add a hidden return pointer at the start of the frame
	ir_type* baseReturnType = ir_type_unwrap(procSymbol->type->retType);
	if(baseReturnType->kind == TYPE_STRUCT)
	{
		//TODO: use common anonymous var constructor
		ir_symbol* var = ir_symbol_alloc(context);

		var->kind = SYM_VAR;
		var->name = mp_string_lit("<hidden return pointer>");
		var->type = ir_type_pointer(context, procSymbol->type->retType);
		var->offset = 0;

		ListPush(&scope->variables, &var->auxElt);
	}

	procSymbol->proc.body = body;
	ListAppend(&context->module->procedures, &procSymbol->auxElt);

	//NOTE(martin): restore caller symbol and capture its frame
	context->proc = saveProc;
	if(context->proc)
	{
		context->proc->proc.capturedFrame = true;
	}

	return(ir_flow(context, ast, procSymbol, tempo));
}

ir_node* check_standby(checker_context* context, q_ast* ast)
{
	return(ir_standby(context, ast));
}

ir_node* check_pause(checker_context* context, q_ast* ast)
{
	q_ast* opd = ast_first_child(ast);
	ir_node* expr = check_value_expr(context, opd);
	expr = check_implicit_conversion(context, ast, expr, ir_type_f64());
	return(ir_pause(context, ast, expr));
}

ir_node* check_wait(checker_context* context, q_ast* ast)
{
	q_ast* recursiveAttr = ast_first_child(ast);
	q_ast* opdAst = ast_next_sibling(recursiveAttr);

	ir_node* expr = check_value_expr(context, opdAst);
	ir_type* baseType = ir_type_unwrap(expr->type);
	ir_type* retType = ir_type_error();

	if(baseType->kind != TYPE_FUTURE)
	{
		check_error(context, opdAst, "wait expression must be of type 'future'");
	}
	else
	{
		retType = ir_type_unwrap(baseType->retType);
	}

	bool recursive = (recursiveAttr->kind == AST_ATTR_RECURSIVE);
	ir_node* ir = ir_wait(context, ast, expr, retType, recursive);

	//NOTE(martin): if return future type is a struct, reserve space in the current frame for the return value,
	//              and pass it as a symbol to the wait node
	if(retType->kind == TYPE_STRUCT)
	{
		ir_symbol* retSlot = checker_add_anonymous_variable(context, ir->type);
		ir->symbol = retSlot;
	}
	return(ir);
}

ir_node* check_timeout(checker_context* context, q_ast* ast)
{
	q_ast* recursiveAttr = ast_first_child(ast);
	q_ast* objAst = ast_next_sibling(recursiveAttr);
	q_ast* timeoutAst = ast_next_sibling(objAst);

	ir_node* expr = check_value_expr(context, objAst);
	ir_type* baseType = ir_type_unwrap(expr->type);
	ir_type* retType = ir_type_error();

	if(baseType->kind != TYPE_FUTURE)
	{
		check_error(context, objAst, "timeout first operand must be of type 'future'");
	}
	else
	{
		retType = ir_type_unwrap(baseType->retType);
	}

	ir_node* timeout = check_value_expr(context, timeoutAst);
	timeout = check_implicit_conversion(context, timeoutAst, timeout, ir_type_f64());

	bool recursive = (recursiveAttr->kind == AST_ATTR_RECURSIVE);
	ir_node* ir = ir_timeout(context, ast, expr, timeout, retType, recursive);

	//NOTE(martin): if return future type is a struct, reserve space in the current frame for the return value,
	//              and pass it as a symbol to the wait node
	if(retType->kind == TYPE_STRUCT)
	{
		ir_symbol* retSlot = checker_add_anonymous_variable(context, ir->type);
		ir->symbol = retSlot;
	}
	return(ir);
}

ir_node* check_background(checker_context* context, q_ast* ast)
{
	ir_node* ir = check_multi(context, ast);
	ir->kind = IR_BACKGROUND;
	return(ir);
}

ir_node* check_fdup(checker_context* context, q_ast* ast)
{
	q_ast* opdAst = ast_first_child(ast);
	ir_node* expr = check_value_expr(context, opdAst);
	ir_type* baseType = ir_type_unwrap(expr->type);
	if(baseType->kind != TYPE_FUTURE)
	{
		check_error(context, opdAst, "fdup expression must be of type 'future'");
	}
	return(ir_fdup(context, ast, expr));
}

ir_node* check_fdrop(checker_context* context, q_ast* ast)
{
	q_ast* opdAst = ast_first_child(ast);
	ir_node* expr = check_value_expr(context, opdAst);
	ir_type* baseType = ir_type_unwrap(expr->type);
	if(baseType->kind != TYPE_FUTURE)
	{
		check_error(context, opdAst, "fdrop expression must be of type 'future'");
	}
	return(ir_fdrop(context, ast, expr));
}


ir_node* check_ast(checker_context* context, q_ast* ast)
{
	ir_node* ir = 0;
	switch(ast->kind)
	{
		case AST_ERROR_MISSING:
		case AST_ERROR_INVALID:
			ir = ir_error(context, ast);
			break;

		case AST_CHAR:
			ir = check_char(context, ast);
			break;
		case AST_INT:
			ir = check_int(context, ast);
			break;
		case AST_FLOAT:
			ir = check_float(context, ast);
			break;

		//TODO: do string/array literals makes sense as statements? (eg. it could be a branch of an if statement to conditionally initialize a var?)

		// arithmetic
		case AST_NEG:
			ir = check_arithmetic_unop(context, ast);
			break;

		case AST_ADD:
			ir = check_arithmetic_binop(context, ast, IR_ADD);
			break;
		case AST_SUB:
			ir = check_arithmetic_binop(context, ast, IR_SUB);
			break;
		case AST_MUL:
			ir = check_arithmetic_binop(context, ast, IR_MUL);
			break;
		case AST_DIV:
			ir = check_arithmetic_binop(context, ast, IR_DIV);
			break;
		case AST_MOD:
			ir = check_arithmetic_binop(context, ast, IR_MOD);
			break;
		// comparison
		case AST_EQ:
			ir = check_comparison(context, ast, IR_EQ);
			break;
		case AST_NEQ:
			ir = check_comparison(context, ast, IR_NEQ);
			break;
		case AST_LT:
			ir = check_comparison(context, ast, IR_LT);
			break;
		case AST_GT:
			ir = check_comparison(context, ast, IR_GT);
			break;
		case AST_LE:
			ir = check_comparison(context, ast, IR_LE);
			break;
		case AST_GE:
			ir = check_comparison(context, ast, IR_GE);
			break;
		// logic
		case AST_LNOT:
			ir = check_logic_unop(context, ast);
			break;
		case AST_LAND:
			ir = check_logic_binop(context, ast, IR_LAND);
			break;
		case AST_LOR:
			ir = check_logic_binop(context, ast, IR_LOR);
			break;

		// temporal
		case AST_FLOW:
			ir = check_flow(context, ast, 0);
			break;

		case AST_PAUSE:
			ir = check_pause(context, ast);
			break;

		case AST_WAIT:
			ir = check_wait(context, ast);
			break;

		case AST_TIMEOUT:
			ir = check_timeout(context, ast);
			break;

		case AST_STANDBY:
			ir = check_standby(context, ast);
			break;

		case AST_BACKGROUND:
			ir = check_background(context, ast);
			break;

		case AST_FDUP:
			ir = check_fdup(context, ast);
			break;
		case AST_FDROP:
			ir = check_fdrop(context, ast);
			break;

		// put
		case AST_PUT:
			ir = check_builtin_put(context, ast);
			break;

		// sizeof
		case AST_SIZEOF:
			ir = check_sizeof(context, ast);
			break;
/*
		// alloc
		case AST_ALLOC:
			ir = check_alloc(context, ast);
			break;
		case AST_ALLOC_SLICE:
			ir = check_alloc_slice(context, ast);
			break;
		case AST_FREE:
			ir = check_free(context, ast);
			break;

		// sync
		case AST_SYNC_BEAT:
			ir = check_sync_beat(context, ast);
			break;
*/
		// data
		case AST_ID:
			ir = check_var(context, ast);
			break;
		case AST_SET:
			ir = check_set(context, ast);
			break;
		case AST_AT:
			ir = check_at(context, ast);
			break;
		case AST_LEN:
			ir = check_len(context, ast);
			break;
		case AST_FIELD:
			ir = check_field(context, ast);
			break;
		case AST_SLICE:
			ir = check_slice(context, ast);
			break;

		case AST_REF:
			ir = check_ref(context, ast);
			break;
		case AST_DEREF:
			ir = check_deref(context, ast);
			break;

		case AST_CAST:
			ir = check_cast(context, ast);
			break;

		// control
		case AST_IF:
			ir = check_if(context, ast);
			break;
		case AST_WHILE:
			ir = check_while(context, ast);
			break;
		case AST_FOR:
			ir = check_for(context, ast);
			break;

		case AST_DO:
			ir = check_multi(context, ast);
			break;

		case AST_CALL:
			ir = check_call(context, ast);
			break;
		case AST_RETURN:
			ir = check_return(context, ast);
			break;

		case AST_VAR_DECL:
			ir = check_variable_initializer(context, ast);
			break;
/*
		case AST_TEMPO_EDITOR:
			//TODO: forbid this in parser?
			check_error_loc(context, ast->range, "Naked tempo editor");
			ir = q_ast_error(context, ast->range);
			break;
*/
		default:
			DEBUG_ASSERT(0, "unreachable");
			break;
	}
	ast_ir_cache(context->module, ast, ir);
	return(ir);
}

void check_add_param_in_proc_scope(checker_context* context, ir_param* param)
{
	ir_type* paramBaseType = ir_type_unwrap(param->type);
	if(paramBaseType->kind != TYPE_ERROR)
	{
		ir_symbol* var = ir_symbol_alloc(context);
		var->kind = SYM_VAR;
		var->name = param->name;

		if(paramBaseType->kind == TYPE_ARRAY)
		{
			var->type = ir_type_slice(context, paramBaseType->eltType);
		}
		else
		{
			var->type = param->type;
		}
		//NOTE(martin): no need to check previously defined symbol here
		checker_insert_symbol(context, var);
		var->status = SYM_COMPLETE;
	}
}

void check_proc_body(checker_context* context, q_ast* ast, ir_symbol* procSymbol, list_info* polyBindings)
{
	ir_symbol* saveProc = context->proc;
	list_info* savePolyBindings = context->polyBindings;

	context->proc = procSymbol;
	context->polyBindings = polyBindings;

	procSymbol->proc.body = ir_body(context, ast);
	ast_ir_cache(context->module, ast, procSymbol->proc.body);

	checker_scope_block(context, SCOPE_PROC, procSymbol->proc.body)
	{
		//NOTE(martin): push parameters as variables in body scope
		if(procSymbol->type->retParam)
		{
			check_add_param_in_proc_scope(context, procSymbol->type->retParam);
		}
		for_each_in_list(&procSymbol->type->params, param, ir_param, listElt)
		{
			check_add_param_in_proc_scope(context, param);
		}

		//NOTE(martin): first parse all top-level definitions
		check_variable_declarations_in_scope(context, ast);

		//NOTE(martin): check body statements
		for_each_in_list(&ast->children, child, q_ast, listElt)
		{
			ir_node* stmt = check_ast(context, child);
			if(stmt)
			{
				//WARN: stmt could be null, if we're checking a variable declaration without initializer...
				ir_push(procSymbol->proc.body, stmt);
			}
		}

		//TODO: ensure that if the end of a non-void function is reachable, there is a final return statement
		//      for now we will just emit a dummy return in the bytecode generator
	}
	context->proc = saveProc;
	context->polyBindings = savePolyBindings;
}

void check_foreign_proc(checker_context* context, ir_symbol* block, q_ast* ast)
{
	q_ast* nameAst = ast_first_child(ast);
	q_ast* typeAst = ast_next_sibling(nameAst);

	if(nameAst->kind != AST_ID || typeAst->kind != AST_PROC_SIG)
	{
		return;
	}
	mp_string name = ast_copy_string(context->arena, nameAst);

	ir_symbol* symbol = ir_symbol_alloc(context);
	symbol->kind = SYM_PROC;
	symbol->name = name;
	symbol->type = check_type_proc(context, typeAst);
	symbol->proc.foreign = true;

	ir_symbol* prevDefined = checker_insert_symbol(context, symbol);
	if(prevDefined)
	{
		check_error(context, nameAst, "redefinition of function %.*s", name.len, name.ptr);
		//TODO: error note pointing to previously defined symbol
	}
	else
	{
		ListAppend(&context->module->procedures, &symbol->auxElt);
		ListAppend(&block->foreignBlock.procDefs, &symbol->proc.foreignElt);
	}
	symbol->status = SYM_COMPLETE;
}


void check_foreign_blocks(checker_context* context)
{
	for_each_in_list(&context->module->ast->children, childAst, q_ast, listElt)
	{
		if(childAst->kind == AST_FOREIGN_BLOCK)
		{
			q_ast* nameAst = ast_first_child(childAst);
			q_ast* defs = ast_next_sibling(nameAst);

			if(nameAst->kind == AST_ID)
			{
				mp_string name = ast_copy_string(mem_scratch(), nameAst);
				mp_string libPath = {};
				if(!mp_string_cmp(name, mp_string_lit("_runtime_")))
				{
					libPath = name;
				}
				else
				{
					mem_arena* scratch = mem_scratch();
					mp_string_list stringList = {};
					mp_string_list_push(scratch, &stringList, mp_string_lit("lib"));
					mp_string_list_push(scratch, &stringList, name);
					mp_string_list_push(scratch, &stringList, mp_string_lit(".dylib"));
					mp_string libName = mp_string_list_join(scratch, stringList);

					libPath = q_workspace_search_path(context->workspace, libName);

					if(!libPath.len)
					{
						check_error(context, nameAst, "Couldn't find foreign library %.s", (int)libName.len, libName.ptr);
					}
				}

				ir_symbol* block = ir_symbol_alloc(context);
				block->kind = SYM_FOREIGN_BLOCK;
				block->type = ir_type_void();
				block->name = mp_string_push_copy(context->arena, libPath);
				block->status = SYM_COMPLETE;

				for_each_in_list(&defs->children, def, q_ast, listElt)
				{
					if(def->kind == AST_FOREIGN_DEF)
					{
						check_foreign_proc(context, block, def);
					}
				}
				ListAppend(&context->module->foreignBlocks, &block->auxElt);
			}
		}
	}
}

void check_proc_declarations(checker_context* context)
{
	q_ast* root = context->module->ast;

	//NOTE(martin): first create symbols and check signature for all functions in scope,
	//              then check the body of functions. This allows having functions that
	//              call each other, as well as recusive functions.
	list_info procs = {0};
	for_each_in_list(&root->children, ast, q_ast, listElt)
	{
		if(ast->kind == AST_PROC_DECL)
		{
			q_ast* nameNode = ast_first_child(ast);
			q_ast* typeNode = ast_next_sibling(nameNode);
			if(  nameNode->kind != AST_ID
			  || typeNode->kind != AST_PROC_SIG)
			{
				continue;
			}

			mp_string name = ast_copy_string(context->arena, nameNode);

			ir_symbol* symbol = ir_symbol_alloc(context);
			symbol->kind = SYM_PROC;
			symbol->name = name;
			symbol->type = check_type_proc(context, typeNode);
			symbol->ast = ast;

			ir_symbol* prevDefined = checker_insert_symbol(context, symbol);
			if(prevDefined)
			{
				check_error(context, ast, "redefinition of function %.*s", (int)name.len, name.ptr);
				//TODO: error note pointing to previously defined symbol
			}
			else
			{
				ListAppend(&procs, &symbol->auxElt);
			}
		}
	}

	//NOTE(martin): check bodies and move function to global function list
	for_each_in_list_safe(&procs, symbol, ir_symbol, auxElt)
	{
		if(!symbol->type->polymorphic)
		{
			list_info emptyBindings = {};
			check_proc_body(context, ast_get_operand(symbol->ast, 2), symbol, &emptyBindings);
		}
		ListRemove(&procs, &symbol->auxElt);
		ListAppend(&context->module->procedures, &symbol->auxElt);
	}
}

void check_global_init(checker_context* context)
{
	q_ast* root = context->module->ast;

	ir_symbol* procSymbol = ir_symbol_alloc(context);
	procSymbol->kind = SYM_PROC;
	procSymbol->name = mp_string_lit("$_module_init");
	procSymbol->ast = root;

	procSymbol->type = ir_type_alloc(context);
	procSymbol->type->kind = TYPE_PROC;
	procSymbol->type->retType = ir_type_void();

	ir_symbol* prevDefined = checker_insert_symbol(context, procSymbol);
	DEBUG_ASSERT(!prevDefined, "redefinition of module_init procedure");

	list_info emptyBindings = {0};
	context->proc = procSymbol;
	context->polyBindings = &emptyBindings;

	procSymbol->proc.body = ir_body(context, root);
	ast_ir_cache(context->module, root, procSymbol->proc.body);

	checker_scope_block(context, SCOPE_PROC, procSymbol->proc.body)
	{
		for_each_in_list(&root->children, ast, q_ast, listElt)
		{
			if(ast->kind == AST_VAR_DECL)
			{
				ir_node* ir = check_variable_initializer(context, ast);
				if(ir)
				{
					ir_push(procSymbol->proc.body, ir);
				}
			}
		}
	}
	context->proc = 0;
	context->polyBindings = 0;

	ListAppend(&context->module->procedures, &procSymbol->auxElt);
}

void check_module(checker_context* context)
{
	checker_add_global_type(context, mp_string_lit("void"), ir_type_void());
	checker_add_global_type(context, mp_string_lit("bool"), ir_type_b8());
	checker_add_global_type(context, mp_string_lit("u8"), ir_type_u8());
	checker_add_global_type(context, mp_string_lit("i8"), ir_type_i8());
	checker_add_global_type(context, mp_string_lit("u16"), ir_type_u16());
	checker_add_global_type(context, mp_string_lit("i16"), ir_type_i16());
	checker_add_global_type(context, mp_string_lit("u32"), ir_type_u32());
	checker_add_global_type(context, mp_string_lit("i32"), ir_type_i32());
	checker_add_global_type(context, mp_string_lit("u64"), ir_type_u64());
	checker_add_global_type(context, mp_string_lit("i64"), ir_type_i64());
	checker_add_global_type(context, mp_string_lit("f32"), ir_type_f32());
	checker_add_global_type(context, mp_string_lit("f64"), ir_type_f64());
	checker_add_global_type(context, mp_string_lit("any"), ir_type_any());

	checker_add_global_type(context, mp_string_lit("tempo_curve_elt"), ir_type_tempo_curve_elt(context));
	checker_add_global_type(context, mp_string_lit("tempo_curve"), ir_type_tempo_curve(context));

	check_type_declarations(context, context->module->ast);
	check_foreign_blocks(context);
	check_variable_declarations_in_scope(context, context->module->ast);
	check_proc_declarations(context);

	check_global_init(context);
	///////////////////////////////////////////////////////////////////
	//TODO: generate global initializers...
	///////////////////////////////////////////////////////////////////
	// -> generate an init procedure
	// -> inside it, first call the init procedures of all imported modules
	// -> main module's init procedure is called once after program load, before start
	// or create an  "module init" ir node for each module. Generate all these at once
	// at start of code section, with a leave at the end. Execute this at load time,
	// before jumping to start()
}

///////////////////////////////////////////////////////////////////
//TODO move somewhere else

void ast_ir_cache(q_module* module, q_ast* ast, ir_node* ir)
{
	ast->ir = ir;
	ast->irBuildGen = module->buildCounter;
}

ir_node* ast_ir_lookup(q_module* module, q_ast* ast)
{
	return((ast->irBuildGen == module->buildCounter) ? ast->ir : 0);
}



#undef LOG_SUBSYSTEM
