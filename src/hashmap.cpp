/************************************************************//**
*
*	@file: hashmap.cpp
*	@author: Martin Fouilleul
*	@date: 25/02/2022
*	@revision:
*
*****************************************************************/
#include"hashmap.h"

//------------------------------------------------------------------------------------
// Hash to u64 hashmap
//------------------------------------------------------------------------------------
void q_hashmap_clear(q_hashmap* map)
{
	if(map->slots)
	{
		memset(map->slots, 0, (map->mask+1)*sizeof(q_hashmap_slot));
		map->filled = 0;
	}
}

void q_hashmap_cleanup(q_hashmap* map)
{
	if(!map->mask)
	{
		return;
	}

	if(map->slots)
	{
		mem_base_allocator* baseAllocator = mem_base_allocator_default();
		mem_base_release(baseAllocator, map->slots, (map->mask+1)*sizeof(q_hashmap_slot));
	}
	memset(map, 0, sizeof(q_hashmap));
}

void q_hashmap_insert(q_hashmap* map, u64 key, u64 val);

void q_hashmap_grow(q_hashmap* map)
{
	u64 oldCap = map->mask ? map->mask + 1 : 0;
	q_hashmap_slot* oldSlots = map->slots;

	u64 newCap = (map->mask+1 < Q_HASHMAP_DEFAULT_SIZE) ?
	             Q_HASHMAP_DEFAULT_SIZE :
	             next_pow2_u64((map->mask+1)*1.5);

	u64 newByteSize = newCap * sizeof(q_hashmap_slot);
	mem_base_allocator* baseAllocator = mem_base_allocator_default();
	map->slots = (q_hashmap_slot*)mem_base_reserve(baseAllocator, newByteSize);
	mem_base_commit(baseAllocator, map->slots, newByteSize);
	memset(map->slots, 0, newByteSize);

	map->mask = newCap-1;
	map->filled = 0;

	for(int i=0; i<oldCap; i++)
	{
		if(oldSlots[i].key & Q_HASHMAP_FILLED)
		{
			q_hashmap_insert(map, oldSlots[i].key, oldSlots[i].val);
		}
	}
	mem_base_release(baseAllocator, oldSlots, oldCap*sizeof(q_hashmap_slot));
}

void q_hashmap_insert(q_hashmap* map, u64 key, u64 val)
{
	//WARN(martin): we use only 63 low-bits of the key. High bit is used as filled flag.
	key |= Q_HASHMAP_FILLED;

	if(map->filled >= 0.75*map->mask)
	{
		q_hashmap_grow(map);
	}
	u64 cap = map->mask+1;
	u64 index = key & map->mask;
	while(true)
	{
		q_hashmap_slot* slot = &(map->slots[index]);
		if(!(slot->key & Q_HASHMAP_FILLED))
		{
			slot->key = key;
			slot->val = val;
			map->filled++;
			break;
		}
		index++;
		index &= map->mask;
	}
}

void q_hashmap_remove_index(q_hashmap* map, u64 index)
{
	if(!map->mask)
	{
		return;
	}

	map->slots[index].key = 0;
	u64 probeIndex = (index+1) & map->mask;

	while(true)
	{
		if(!(map->slots[probeIndex].key & Q_HASHMAP_FILLED))
		{
			return;
		}
		u64 baseProbeIndex = map->slots[probeIndex].key & map->mask;
		bool check = (index < probeIndex) ?
			            (index < baseProbeIndex && baseProbeIndex <= probeIndex) :
			            (index < baseProbeIndex || baseProbeIndex <= probeIndex);
		if(!check)
		{
			map->slots[index] = map->slots[probeIndex];
			index = probeIndex;
			map->slots[index].key = 0;
		}
		probeIndex = (probeIndex+1) & map->mask;
	}
}

void q_hashmap_remove(q_hashmap* map, u64 key)
{
	if(!map->mask)
	{
		return;
	}

	key |= Q_HASHMAP_FILLED;

	u64 index = key & map->mask;
	while(true)
	{
		q_hashmap_slot* slot = &(map->slots[index]);
		if(!(slot->key & Q_HASHMAP_FILLED))
		{
			//NOTE: not found
			break;
		}
		if(slot->key == key)
		{
			q_hashmap_remove_index(map, index);
			break;
		}
		index = (index+1) & map->mask;
	}
}

bool q_hashmap_find(q_hashmap* map, u64 key, u64* outValue)
{
	if(!map->mask)
	{
		return(false);
	}

	key |= Q_HASHMAP_FILLED;

	u64 index = key & map->mask;
	while(true)
	{
		q_hashmap_slot* slot = &(map->slots[index]);
		if(!(slot->key & Q_HASHMAP_FILLED))
		{
			//NOTE: not found
			return(false);
		}
		if(slot->key == key)
		{
			*outValue = slot->val;
			return(true);
		}
		index = (index+1) & map->mask;
	}
}

u64 q_hash_string(mp_string string)
{
	u8 seed[16] = {
    	0xaa, 0x9b, 0xbd, 0xb8,
    	0xa1, 0x98, 0xac, 0x3f,
    	0x1f, 0x94, 0x07, 0xb3,
    	0x8c, 0x27, 0x93, 0x69 };

	__m128i hash = _mm_loadu_si128((__m128i*)seed);

	u64 chunkCount = string.len / 16;
	char* at = string.ptr;

	while(chunkCount--)
	{
		__m128i in = _mm_loadu_si128((__m128i*)at);
		at += 16;

		hash = _mm_xor_si128(hash, in);
		hash = _mm_aesdec_si128(hash, _mm_setzero_si128());
		hash = _mm_aesdec_si128(hash, _mm_setzero_si128());
		hash = _mm_aesdec_si128(hash, _mm_setzero_si128());
		hash = _mm_aesdec_si128(hash, _mm_setzero_si128());
	}

	u64 restCount = string.len % 16;
	char tmp[16];
	memset(tmp, 0, 16);
	memmove(tmp, at, restCount);

	__m128i in = _mm_loadu_si128((__m128i*)tmp);
	hash = _mm_xor_si128(hash, in);
	hash = _mm_aesdec_si128(hash, _mm_setzero_si128());
	hash = _mm_aesdec_si128(hash, _mm_setzero_si128());
	hash = _mm_aesdec_si128(hash, _mm_setzero_si128());
	hash = _mm_aesdec_si128(hash, _mm_setzero_si128());

	u64 result = _mm_extract_epi64(hash, 0);
	return(result);
}
