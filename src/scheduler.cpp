/************************************************************//**
*
*	@file: scheduler.cpp
*	@author: Martin Fouilleul
*	@date: 09/07/2020
*	@revision:
*
*****************************************************************/
#include<string.h> // memset()
#include<math.h>
#include"macro_helpers.h"
#include"memory.h"
#include"platform_clock.h"
#include"platform_fibers.h"
#include"platform_thread.h"
#include"scheduler.h"
#include"sched_curves_internal.h"

#define LOG_SUBSYSTEM "Scheduler"

//----------------------------------------------------------------------------------
// Fibers and tasks structures
//----------------------------------------------------------------------------------

const u64 SCHED_FIBER_STACK_SIZE = 1<<20;

typedef enum { SCHED_SOURCE_CLOCK,
               SCHED_SOURCE_PARENT } sched_time_source;

typedef enum { SCHED_STATUS_RUNNING,
               SCHED_STATUS_SUSPENDED,
               SCHED_STATUS_BACKGROUND,
               SCHED_STATUS_RETIRED,
               SCHED_STATUS_COMPLETED,
               SCHED_STATUS_FROZEN } sched_task_status;

typedef struct sched_task_info
{
	list_elt activeListElt; //NOTE(martin): used to list all active (ie not completed )tasks
	list_elt listElt;       //NOTE(martin): used to put the task in various scheduling lists

	sched_task_info* parent;
	list_elt parentElt;
	list_elt children;

	i32 openHandles;

	sched_task_status status;
	i64 exitCode;

	list_elt jobQueueElt;
	list_elt waiting;
	list_elt waitingElt;
	sched_task_signal waitingFor;
	sched_wakeup_code wakeupCode;

	//sync / scale description
	sched_time_source timeSource;
	sched_curve* timeCurve;

	//runtime sync values
	f64 currentPos;    //NOTE: the current location in the timescale's units
	f64 transformedPos;     //NOTE: the current location in the time source's units
	f64 logicalPos; //NOTE: location of the current event or = currentPos
	f64 nextPos;

	//TODO: see if we can group that with currentPos?
	f64 lastYield;    //NOTE: last time the task was scheduled. This does not change until the task is rescheduled

	u64 ticket;

	//fiber context
	fiber_context* context;
	sched_task_proc proc;
	void* userPointer;

} sched_task_info;

//----------------------------------------------------------------------------------
// Handles structures
//----------------------------------------------------------------------------------
typedef struct sched_handle_slot
{
	u32 generation;
	union
	{
		list_elt freeListElt;
		sched_task_info* task;
	};
} sched_handle_slot;

//----------------------------------------------------------------------------------
// Background Jobs queue
//----------------------------------------------------------------------------------

const u32 SCHED_BACKGROUND_QUEUE_THREAD_COUNT = 8;

typedef struct sched_job_queue
{
	_Atomic(bool) running;
	platform_mutex* mutex;
	platform_condition* condition;
	list_elt list;

	platform_thread* threads[SCHED_BACKGROUND_QUEUE_THREAD_COUNT];

} sched_job_queue;

//----------------------------------------------------------------------------------
// Scheduler structure
//----------------------------------------------------------------------------------
const f64 SCHEDULER_FUSION_THRESHOLD = 100e-9;

typedef enum { SCHED_MESSAGE_FOREGROUND,
               SCHED_MESSAGE_WAKEUP } sched_message_kind;

typedef struct sched_message
{
	list_elt listElt;
	sched_message_kind kind;

	union
	{
		sched_task_info* task; //SCHED_MESSAGE_FOREGROUND
		sched_task taskHandle; //SCHED_MESSAGE_WAKEUP

		//...
	};

} sched_message;

const u32 SCHED_MAX_HANDLE_SLOTS = 1024;

typedef struct sched_info
{
	mem_pool messagePool;
	mem_pool taskPool;
	mem_pool stackPool;

	sched_handle_slot handleSlots[SCHED_MAX_HANDLE_SLOTS];
	u32 nextHandleSlot;
	list_elt handleFreeList;

	platform_condition* msgCondition;
	platform_mutex* msgConditionMutex;
	ticket_spin_mutex msgQueueMutex;
	ticket_spin_mutex msgPoolMutex;
	_Atomic(bool) hasMessages;
	list_elt messages;

	sched_job_queue jobQueue;

	list_elt activeTasks;
	list_elt scheduledTasks;

	sched_task_info* currentTask;

	f64 lastTimeUpdate;
	f64 timeToSleepResidue;
	f64 startTime;
	u64 nextTicket;

} sched_info;

//NOTE(martin): scheduler context
sched_info __schedInfo__;

sched_info* sched_get_context()
{
	return(&__schedInfo__);
}

//NOTE(martin): fiber entry point wrapper
i64 sched_task_start(fiber_context* context)
{
	sched_task_info* task = (sched_task_info*)context->user;
	return(task->proc(task->userPointer));
}

//-------------------------------------------------------------------------------------------------------
// Handle system
//-------------------------------------------------------------------------------------------------------

sched_handle_slot* sched_handle_slot_find(sched_info* sched, u64 h)
{
	u32 generation = (u32)(h & 0xffffffff);
	u32 index = (u32)(h>>32);

	DEBUG_ASSERT(index < sched->nextHandleSlot);
	sched_handle_slot*  slot = &(sched->handleSlots[index]);
	DEBUG_ASSERT(slot->generation == generation);
	return(slot);
}

void sched_handle_slot_recycle(sched_info* sched, sched_handle_slot* slot)
{
	slot->generation++;
	CListPush(&sched->handleFreeList, &slot->freeListElt);
}

u64 sched_handle_slot_get_packed_handle(sched_info* sched, sched_handle_slot* slot)
{
	u64 generation = (u64)slot->generation;
	u64 index = (u64)(slot - sched->handleSlots);
	u64 h = index<<32 | generation;
	return(h);
}

sched_task sched_alloc_task_handle(sched_info* sched, sched_task_info* task)
{
	sched_handle_slot* slot = 0;
	if(CListEmpty(&sched->handleFreeList))
	{
		if(sched->nextHandleSlot >= SCHED_MAX_HANDLE_SLOTS)
		{
			LOG_ERROR("Too many in-flight handles (SCHED_MAX_HANDLE_SLOTS = %u)\n", SCHED_MAX_HANDLE_SLOTS);
			return((sched_task){.h = 0});
		}
		slot = &(sched->handleSlots[sched->nextHandleSlot]);
		sched->nextHandleSlot++;
		slot->generation = 1;
	}
	else
	{
		slot = CListEntry(CListPop(&sched->handleFreeList), sched_handle_slot, freeListElt);
	}

	slot->task = task;
	return((sched_task){.h = sched_handle_slot_get_packed_handle(sched, slot)});
}

sched_task_info* sched_handle_get_task_ptr(sched_info* sched, sched_task handle)
{
	sched_handle_slot* slot = sched_handle_slot_find(sched, handle.h);
	return(slot->task);
}

//-------------------------------------------------------------------------------------------------------
// Clock / Condition wrappers
//-------------------------------------------------------------------------------------------------------

typedef f64 (*sched_clock_get_time_proc)();
typedef void (*sched_condition_timed_wait_proc)(platform_condition* cond, platform_mutex* mutex, f64 timeout);
typedef void (*sched_condition_wait_proc)(platform_condition* cond, platform_mutex* mutex);

sched_clock_get_time_proc sched_clock_get_time;
sched_condition_timed_wait_proc sched_condition_timed_wait;
sched_condition_wait_proc sched_condition_wait;

f64 sched_online_clock_get_time()
{
	return(ClockGetTime(SYS_CLOCK_MONOTONIC));
}

void sched_online_condition_timed_wait(platform_condition* cond, platform_mutex* mutex, f64 timeout)
{
	ConditionTimedWait(cond, mutex, timeout);
}

void sched_online_condition_wait(platform_condition* cond, platform_mutex* mutex)
{
	ConditionWait(cond, mutex);
}

f64 __offlineClock = 0;

f64 sched_offline_clock_get_time()
{
	return(__offlineClock);
}

void sched_offline_condition_timed_wait(platform_condition* cond, platform_mutex* mutex, f64 timeout)
{
	__offlineClock += timeout;
}

void sched_offline_condition_wait(platform_condition* cond, platform_mutex* mutex)
{
	ConditionWait(cond, mutex);
}

//-------------------------------------------------------------------------------------------------------
// Tasks position updates functions
//-------------------------------------------------------------------------------------------------------

f64 sched_task_update_pos_from_sync_curve(sched_info* sched, sched_task_info* task, sched_steps timeElapsed)
{
	DEBUG_ASSERT(task->timeCurve);
	DEBUG_ASSERT(task->timeCurve->eltCount);

	f64 newSrcLoc = task->transformedPos + timeElapsed;
	f64 newSelfLoc = sched_curve_get_position_from_time(task->timeCurve, newSrcLoc);

	//TODO: potential loss of significance, get update directly from curve?
	f64 posUpdate = newSelfLoc - task->currentPos;
	task->currentPos = newSelfLoc;
	task->transformedPos = newSrcLoc;
	task->logicalPos = task->currentPos;
/*
	if(timeElapsed)
	{
		DEBUG_ASSERT(posUpdate/timeElapsed < 100);
	}
*/
	return(posUpdate);
}

void sched_task_update_position(sched_info* sched, sched_task_info* task, sched_steps timeElapsed)
{
	//NOTE(martin): timeElapsed is expressed in parent's task. We first convert it
	//              to the local task and update the current location

	#if DEBUG
		f64 oldPos = task->currentPos;
	#endif

	f64 posUpdate = 0;
	sched_curve* curve = 0;

	if(!task->timeCurve)
	{
		posUpdate = timeElapsed;
		task->transformedPos += timeElapsed;
		task->currentPos += posUpdate;
		task->logicalPos = task->currentPos;
	}
	else
	{
		posUpdate = sched_task_update_pos_from_sync_curve(sched, task, timeElapsed);
	}

	#if DEBUG
		if(oldPos == task->currentPos && timeElapsed != 0)
		{
			LOG_WARNING("time update too small to update task's position\n");
		}
	#endif

	//NOTE(martin): now we update all children.
	for_each_in_clist(&task->children, child, sched_task_info, parentElt)
	{
		if(child->status != SCHED_STATUS_FROZEN)
		{
			sched_task_update_position(sched, child, posUpdate);
		}
	}
}

void sched_update_task_positions(sched_info* sched, sched_steps timeElapsed)
{
	for_each_in_clist(&sched->activeTasks, task, sched_task_info, activeListElt)
	{
		//NOTE(martin): we only call update on root (ie. metronome) tasks here.
		//              Each tasks updates its children after updating itself
		//              This avoid recomputing global to local delay for every task
		if(task->timeSource == SCHED_SOURCE_CLOCK && task->status != SCHED_STATUS_FROZEN)
		{
			sched_task_update_position(sched, task, (sched_steps)timeElapsed);
		}
	}
}

//-------------------------------------------------------------------------------------------------------
// Tasks conversion functions
//-------------------------------------------------------------------------------------------------------

f64 sched_local_to_source_delay_from_curve(sched_info* sched, sched_task_info* task, sched_steps steps)
{
	DEBUG_ASSERT(task->timeCurve);
	DEBUG_ASSERT(task->timeCurve->eltCount);

	f64 nextPos = task->currentPos + steps;
	f64 nextTime = sched_curve_get_time_from_position(task->timeCurve, nextPos);

	//TODO(martin): prevent potential loss of significance, get timeUpdate directly from curve?
	f64 timeUpdate = nextTime - task->transformedPos;
	return(timeUpdate);
}

f64 sched_local_to_global_delay(sched_info* sched, sched_task_info* task, sched_steps steps)
{
	//NOTE(martin): compute steps in parent's units
	if(task->timeCurve)
	{
		steps = sched_local_to_source_delay_from_curve(sched, task, steps);
	}

	//NOTE(martin): now depending on the source, return or climb up the hierarchy
	switch(task->timeSource)
	{
		case SCHED_SOURCE_CLOCK:
			return(steps);

		case SCHED_SOURCE_PARENT:
			DEBUG_ASSERT(task->parent);
			return(sched_local_to_global_delay(sched, task->parent, steps));
	}
}

//-------------------------------------------------------------------------------------------------------
// Scheduler messages handlers
//-------------------------------------------------------------------------------------------------------

void sched_task_reschedule_in_steps(sched_info* sched, sched_task_info* task, sched_steps steps);

void sched_do_foreground_cmd(sched_info* sched, sched_task_info* task)
{
	task->status = SCHED_STATUS_RUNNING;
	sched_task_reschedule_in_steps(sched, task, 0);
}

void sched_do_wakeup_cmd(sched_info* sched, sched_task taskHandle)
{
	sched_task_info* task = sched_handle_get_task_ptr(sched, taskHandle);
	if(task->status == SCHED_STATUS_SUSPENDED)
	{
		sched_task_resume(taskHandle);
	}
}

//-------------------------------------------------------------------------------------------------------
// Scheduler message queue functions
//-------------------------------------------------------------------------------------------------------

void sched_wait_for_message(sched_info* sched)
{
	MutexLock(sched->msgConditionMutex);
	while(!sched->hasMessages)
	{
		//NOTE(martin): wait on the command condition, which releases the command mutex, which
		//              allow other threads to commit command buffers to the command queue
		sched_condition_wait(sched->msgCondition, sched->msgConditionMutex);
	}
	MutexUnlock(sched->msgConditionMutex);
}

void sched_wait_for_message_or_timeout(sched_info* sched, f64 timeout)
{
	//NOTE(martin): Wait on the command condition, which releases the command mutex, which
	//              allow other threads to commit command buffers to the command queue

	MutexLock(sched->msgConditionMutex);
	while(!sched->hasMessages && (timeout > 100e-6))
	{
		//NOTE(martin): the precision of ConditionTimedWait() is somewhat proportional to the timeout.
		//              To get a better precision, we wait multiple times with geometrically decreasing timeouts
		//              until we are close enough to our desired timeout. The ratio of 0.8 gives a sub ms precision
		//              with around 4 loop iteration for a 1s delay.
		//              This also allows to gracefully handle spurious wakeups.
		//TODO(martin): do more precise measurement of timeout accuracy and choose ratio accordingly.

		f64 lastStart = sched_clock_get_time();
		sched_condition_timed_wait(sched->msgCondition, sched->msgConditionMutex, timeout * 0.8);
		timeout -= sched_clock_get_time() - lastStart;
	}
	MutexUnlock(sched->msgConditionMutex);
}

sched_message* sched_next_message(sched_info* sched, sched_message* oldMessage)
{
	if(oldMessage)
	{
		TicketSpinMutexLock(&sched->msgPoolMutex);
		{
			mem_pool_recycle(&sched->messagePool, oldMessage);
		} TicketSpinMutexUnlock(&sched->msgPoolMutex);
	}

	sched_message* message = 0;
	TicketSpinMutexLock(&sched->msgQueueMutex);
	{
		message = CListPopEntry(&sched->messages, sched_message, listElt);
		if(!message)
		{
			sched->hasMessages = false;
		}
	} TicketSpinMutexUnlock(&sched->msgQueueMutex);

	return(message);
}

void sched_dispatch_commands(sched_info* sched)
{
	sched_message* message = 0;

	while((message = sched_next_message(sched, message)) != 0)
	{
		switch(message->kind)
		{
			case SCHED_MESSAGE_FOREGROUND:
				sched_do_foreground_cmd(sched, message->task);
				break;
			case SCHED_MESSAGE_WAKEUP:
				sched_do_wakeup_cmd(sched, message->taskHandle);
				break;
			//...
		}
	}
}

sched_message* sched_message_acquire(sched_info* sched)
{
	TicketSpinMutexLock(&sched->msgPoolMutex);
		sched_message* message = mem_pool_alloc_type(&sched->messagePool, sched_message);
	TicketSpinMutexUnlock(&sched->msgPoolMutex);
	memset(message, 0, sizeof(sched_message));
	return(message);
}

void sched_message_commit(sched_info* sched, sched_message* message)
{
	MutexLock(sched->msgConditionMutex);
	{
		TicketSpinMutexLock(&sched->msgQueueMutex);
		{
			CListAppend(&sched->messages, &message->listElt);
			sched->hasMessages = true;
		} TicketSpinMutexUnlock(&sched->msgQueueMutex);

		ConditionSignal(sched->msgCondition);
	} MutexUnlock(sched->msgConditionMutex);
}

//-------------------------------------------------------------------------------------------------------
// Scheduling
//-------------------------------------------------------------------------------------------------------

void sched_task_reschedule_in_steps(sched_info* sched, sched_task_info* task, sched_steps steps)
{
	task->lastYield = task->logicalPos;
	task->nextPos = task->logicalPos + steps;
	task->ticket = sched->nextTicket++;
	task->status = SCHED_STATUS_RUNNING;

	CListAppend(&sched->scheduledTasks, &task->listElt);
}

//-------------------------------------------------------------------------------------------------------
// Tasks retirement/completion
//-------------------------------------------------------------------------------------------------------

void sched_task_recycle(sched_info* sched, sched_task_info* task)
{
	LOG_MESSAGE("recycle task %p\n", task);
	DEBUG_ASSERT(task->timeCurve == 0, "curves should have been be freed earlier as part of termination");
	mem_pool_recycle(&sched->taskPool, task);
}

void sched_task_check_if_needs_recycling(sched_info* sched, sched_task_info* task)
{
	if(task->status == SCHED_STATUS_COMPLETED
	  && !task->openHandles)
	{
		sched_task_recycle(sched, task);
	}
}

void sched_task_complete(sched_info* sched, sched_task_info* task);

void sched_task_notify_parent_of_completion(sched_info* sched, sched_task_info* task)
{
	if(task->status == SCHED_STATUS_RETIRED)
	{
		//NOTE(martin): check if all children are completed and complete this task if true
		for_each_in_clist(&task->children, child, sched_task_info, parentElt)
		{
			if(child->status != SCHED_STATUS_COMPLETED)
			{
				return;
			}
		}
		sched_task_complete(sched, task);
	}
}

void sched_signal_waiting_tasks(sched_info* sched, list_elt* waiting, sched_task_signal signal)
{
	for_each_in_clist_safe(waiting, task, sched_task_info, waitingElt)
	{
		if(task->waitingFor == signal)
		{
			task->status = SCHED_STATUS_RUNNING;
			task->wakeupCode = SCHED_WAKEUP_SIGNALED;
			CListRemove(&task->waitingElt);
			CListRemove(&task->listElt);
			sched_task_reschedule_in_steps(sched, task, 0);
		}
	}
}

void sched_signal_waiting_tasks_on_cancel(sched_info* sched, list_elt* waiting)
{
	for_each_in_clist_safe(waiting, task, sched_task_info, waitingElt)
	{
		task->status = SCHED_STATUS_RUNNING;
		task->wakeupCode = SCHED_WAKEUP_CANCELLED;
		CListRemove(&task->waitingElt);
		CListRemove(&task->listElt);
		sched_task_reschedule_in_steps(sched, task, 0);
	}
}

void sched_task_complete(sched_info* sched, sched_task_info* task)
{
	task->status = SCHED_STATUS_COMPLETED;

	//NOTE(martin): remove from active tasks
	CListRemove(&task->activeListElt);

	//NOTE(martin): notify all fibers waiting on this task's retirement
	sched_signal_waiting_tasks(sched, &task->waiting, SCHED_SIG_COMPLETED);

	//NOTE(martin): notify parent that we are completed
	if(task->parent)
	{
		sched_task_notify_parent_of_completion(sched, task->parent);
	}

	//NOTE(martin): remove from task hierarchy
	CListRemove(&task->parentElt);

	//NOTE(martin): free active resources of the task
	if(task->timeCurve)
	{
		sched_curve_destroy(task->timeCurve);
		task->timeCurve = 0;
	}

	//NOTE(martin): check if the task can be recycled
	sched_task_check_if_needs_recycling(sched, task);
}

void sched_task_retire(sched_info* sched, sched_task_info* task)
{
	task->status = SCHED_STATUS_RETIRED;

	//NOTE(martin): notify all fibers waiting on this task's retirement
	sched_signal_waiting_tasks(sched, &task->waiting, SCHED_SIG_RETIRED);

	//NOTE(martin): check if all children are completed and complete this task if true
	for_each_in_clist(&task->children, child, sched_task_info, parentElt)
	{
		if(child->status != SCHED_STATUS_COMPLETED)
		{
			return;
		}
	}
	sched_task_complete(sched, task);
}

//-------------------------------------------------------------------------------------------------------
// Scheduler background queue
//-------------------------------------------------------------------------------------------------------

_Thread_local sched_task_info* __backgroundJobCurrentTask = 0;

void* sched_background_job_main(void* userPointer)
{
	LOG_DEBUG("starting worker thread\n");

	sched_info* sched = (sched_info*)userPointer;
	sched_job_queue* queue = &sched->jobQueue;

	while(queue->running)
	{
		sched_task_info* task = 0;

		//NOTE(martin): wait for a task to be available in the queue
		MutexLock(queue->mutex);
		{
			while(CListEmpty(&queue->list) && queue->running)
			{
				ConditionWait(queue->condition, queue->mutex);
			}
			if(!queue->running)
			{
				MutexUnlock(queue->mutex);
				goto end;
			}

			task = CListPopEntry(&queue->list, sched_task_info, jobQueueElt);
		} MutexUnlock(queue->mutex);

		LOG_DEBUG("picked a background job\n");
		//NOTE(martin): yield to the task
		__backgroundJobCurrentTask = task;
		fiber_yield(task->context);

		//NOTE(martin): fiber has yielded back, post a message to the scheduler to reschedule it
		sched_message* message = sched_message_acquire(sched);
			message->kind = SCHED_MESSAGE_FOREGROUND;
			message->task = task;
		sched_message_commit(sched, message);
	}

	end:
	// cleanup

	return(0);
}

void sched_background_queue_init(sched_info* sched)
{
	sched_job_queue* queue = &sched->jobQueue;

	queue->running = true;
	queue->mutex = MutexCreate();
	queue->condition = ConditionCreate();
	CListInit(&queue->list);

	for(int i=0; i<SCHED_BACKGROUND_QUEUE_THREAD_COUNT; i++)
	{
		char name[64];
		snprintf(name, 64, "sched_worker#%i", i);
		queue->threads[i] = ThreadCreateWithName(sched_background_job_main, sched, name);
	}
}

void sched_background_queue_cleanup(sched_job_queue* queue)
{
	//NOTE(martin):
	//	We first set queue->running to false and signal all thread. After that, we know that worker threads
	//	are either soon to quit or blocked in a blocking call. In any case they won't try to take the queue
	//	lock again, so we can cancel the all and join them all.

	MutexLock(queue->mutex);
		queue->running = false;
		ConditionBroadcast(queue->condition);
	MutexUnlock(queue->mutex);

	for(int i=0; i<SCHED_BACKGROUND_QUEUE_THREAD_COUNT; i++)
	{
		ThreadCancel(queue->threads[i]);
	}
	for(int i=0; i<SCHED_BACKGROUND_QUEUE_THREAD_COUNT; i++)
	{
		ThreadJoin(queue->threads[i], 0);
	}
}

void sched_background_queue_push(sched_info* sched, sched_task_info* task)
{
	sched_job_queue* queue = &sched->jobQueue;

	//NOTE(martin): put the task in the queue and wakeup one background job
	MutexLock(queue->mutex);
	{
		CListAppend(&queue->list, &task->jobQueueElt);
		ConditionSignal(queue->condition);
	} MutexUnlock(queue->mutex);
}

//-------------------------------------------------------------------------------------------------------
// Scheduler run-loop
//-------------------------------------------------------------------------------------------------------

typedef enum { SCHED_PICKED_TASK,
               SCHED_PICKED_MESSAGE } sched_picked_event_kind;

int sched_pick_event(sched_info* sched, sched_task_info** outTask)
{
	//NOTE(martin): get the next task and its delay: check head of each task's event queue,
	//              translate dates to global task and choose the soonest
	sched_task_info* nextTask = 0;
	f64 taskDelay = DBL_MAX;

	for_each_in_clist(&sched->scheduledTasks, task, sched_task_info, listElt)
	{
		f64 localDelay = task->nextPos - task->currentPos;
		f64 delay = sched_local_to_global_delay(sched, task, localDelay);

		LOG_DEBUG("event globalDelay: %f, event logicalPos: %f, task logicalPos: %f \n",
				    delay,
				    task->nextPos,
				    task->currentPos);

		if(delay < taskDelay)
		{
			taskDelay = delay;
			nextTask = task;
		}
		else if( ((delay - taskDelay) < SCHEDULER_FUSION_THRESHOLD)
			      &&(task->ticket < nextTask->ticket))
		{
			nextTask = task;
		}
	}

	if(nextTask)
	{
		if(taskDelay > 0)
		{
			//NOTE(martin): compute real timeout and sleep
			f64 workingTime = sched_clock_get_time() - sched->lastTimeUpdate;
			f64 realTimeout = taskDelay + sched->timeToSleepResidue - workingTime; //TODO: call timeToSleepResidue realTimeoutResidue

			if(realTimeout <= 0)
			{
				sched->timeToSleepResidue = realTimeout;
			}
			else
			{
				sched_wait_for_message_or_timeout(sched, realTimeout);
			}
		}
	}
	else
	{
		//NOTE(martin): or just wait a message
		sched_wait_for_message(sched);
	}

	//NOTE(martin): wakeup from sleep
	if(!sched->hasMessages)
	{
		//NOTE(martin): wakeup after timeout. If we had a timeout, we must have a scheduled task.
		DEBUG_ASSERT(nextTask);

		f64 now = sched_clock_get_time();
		f64 timeElapsed = now - sched->lastTimeUpdate;
		sched->lastTimeUpdate = now;
		sched->timeToSleepResidue += (taskDelay - timeElapsed);

		//NOTE(martin): update delays
		f64 taskTimeUpdate = ClampLowBound(taskDelay, 0);

		//NOTE(martin): update tasks' positions
		sched_update_task_positions(sched, taskTimeUpdate);

		CListRemove(&nextTask->listElt);
		*outTask = nextTask;
		return(SCHED_PICKED_TASK);
	}
	else
	{
		//NOTE(martin): wakeup after message.
		f64 now = sched_clock_get_time();
		f64 timeElapsed = now - sched->lastTimeUpdate;
		sched->lastTimeUpdate = now;
		sched->timeToSleepResidue = 0;

		//NOTE(martin): update tasks' positions
		sched_update_task_positions(sched, timeElapsed);

		return(SCHED_PICKED_MESSAGE);
	}
}

i64 sched_run(void* userPointer)
{
	sched_info* sched = sched_get_context();

	sched->lastTimeUpdate = sched_clock_get_time();
	sched->startTime = sched->lastTimeUpdate;

	while(true)
	{
		sched_task_info* task = 0;

		switch(sched_pick_event(sched, &task))
		{
			case SCHED_PICKED_TASK:
			{
				//NOTE(martin): we picked a task, execute it
				//NOTE(martin): if the task was suspended and is awaken after its timeout, clear its status flag,
				//              set its wakeup code and remove it from the wait list its in.
				if(task->status == SCHED_STATUS_SUSPENDED)
				{
					task->status = SCHED_STATUS_RUNNING;
					task->wakeupCode = SCHED_WAKEUP_TIMEOUT;
					CListRemove(&task->waitingElt);
				}

				//NOTE(martin): set the task loc and yield to task
				task->logicalPos = task->nextPos;
				sched->currentTask = task;
				fiber_yield(task->context);

				//NOTE(martin): fiber yielded back, check if it's status
				if(!task->context->running)
				{
					task->exitCode = task->context->exitCode;
					sched_task_retire(sched, task);
				}
				else if(task->status == SCHED_STATUS_BACKGROUND)
				{
					//NOTE(martin): task requested to be put in the background queue
					sched_background_queue_push(sched, task);
				}
			} break;

			case SCHED_PICKED_MESSAGE:
			{
				//NOTE(martin): we have pending commands, dispatch them.
				sched_dispatch_commands(sched);
			} break;
		}
	}
	return(0);
}
//-------------------------------------------------------------------------------------------------------
// Misc. helpers
//-------------------------------------------------------------------------------------------------------

sched_task_info* sched_task_alloc_init(sched_info* sched, sched_task_info* parent, sched_task_proc proc, void* userPointer)
{
	sched_task_info* task = mem_pool_alloc_type(&sched->taskPool, sched_task_info);
	memset(task, 0, sizeof(sched_task_info));

	task->openHandles = 0;
	task->status = SCHED_STATUS_RUNNING;

	task->currentPos = 0;
	task->transformedPos = 0;
	task->logicalPos = 0;
	task->nextPos = 0;
	task->timeSource = parent ? SCHED_SOURCE_PARENT : SCHED_SOURCE_CLOCK;
	task->timeCurve = 0; //sched_curve_create(task->descriptor.tempo);

	//NOTE(martin): init lists
	CListInit(&task->listElt);
	CListInit(&task->waiting);
	CListInit(&task->waitingElt);

	//NOTE(martin): put the task in the task hierarchy
	task->parent = parent;
	if(parent)
	{
		CListAppend(&parent->children, &task->parentElt);
	}
	else
	{
		CListInit(&task->parentElt);
	}
	CListInit(&task->children);

	CListAppend(&sched->activeTasks, &task->activeListElt);

	//NOTE: create fiber stack and fiber info
	task->proc = proc;
	task->userPointer = userPointer;

	char* stack = (char*)mem_pool_alloc(&sched->stackPool);
	task->context = fiber_init(sched_task_start, SCHED_FIBER_STACK_SIZE, stack);
	task->context->user = task;

	return(task);
}

//*******************************************************************************************************
// Public API
//*******************************************************************************************************

//------------------------------------------------------------------------------------------------------
//NOTE: tasks
//------------------------------------------------------------------------------------------------------

sched_task sched_task_create_for_parent_ptr(sched_info* sched, sched_task_info* parent, sched_task_proc proc, void* userPointer)
{
	sched_task_info* task = sched_task_alloc_init(sched, parent, proc, userPointer);
	sched_task handle = sched_alloc_task_handle(sched, task);
	task->openHandles = 1;

	CListAppend(&sched->scheduledTasks, &task->listElt);

	return(handle);
}

sched_task sched_task_create(sched_task_proc proc, void* userPointer)
{
	sched_info* sched = sched_get_context();
	DEBUG_ASSERT(sched->currentTask);

	return(sched_task_create_for_parent_ptr(sched, sched->currentTask, proc, userPointer));
}

sched_task sched_task_create_for_parent(sched_task parent, sched_task_proc proc, void* userPointer)
{
	sched_info* sched = sched_get_context();
	sched_task_info* parentPtr = sched_handle_get_task_ptr(sched, parent);
	DEBUG_ASSERT(parentPtr);

	return(sched_task_create_for_parent_ptr(sched, parentPtr, proc, userPointer));
}

void sched_task_timescale_set_tempo_curve(sched_task task, sched_curve_descriptor* descriptor)
{
	sched_info* sched = sched_get_context();
	sched_task_info* taskPtr = sched_handle_get_task_ptr(sched, task);

	if(taskPtr->timeCurve)
	{
		sched_curve_destroy(taskPtr->timeCurve);
		taskPtr->timeCurve = 0;
	}
	if(descriptor)
	{
		taskPtr->timeCurve = sched_curve_create(descriptor);
	}

	//DEBUG
	f64 oldTransformedPos = taskPtr->transformedPos;
	if(taskPtr->timeCurve)
	{
		taskPtr->transformedPos = sched_curve_get_time_from_position(taskPtr->timeCurve, taskPtr->currentPos);
	}
	else
	{
		taskPtr->transformedPos = taskPtr->currentPos;
	}
	//DEBUG
	DEBUG_ASSERT(fabs(oldTransformedPos - taskPtr->transformedPos)<0.01);
}

f64 sched_get_clock()
{
	sched_info* sched = sched_get_context();
	return(sched_clock_get_time() - sched->startTime);
}

sched_steps sched_task_initial_delay(sched_task handle)
{
	sched_info* sched = sched_get_context();
	sched_task_info* task = sched_handle_get_task_ptr(sched, handle);
	sched_steps initialDelay = task->nextPos - task->lastYield;
	return(initialDelay);
}

sched_steps sched_task_local_delay(sched_task handle)
{
	sched_info* sched = sched_get_context();
	sched_task_info* task = sched_handle_get_task_ptr(sched, handle);
	sched_steps localDelay = task->nextPos - task->currentPos;
	return(localDelay);
}

f64 sched_task_global_delay(sched_task handle)
{
	sched_info* sched = sched_get_context();
	sched_task_info* task = sched_handle_get_task_ptr(sched, handle);
	sched_steps localDelay = task->nextPos - task->currentPos;
	f64 delay = sched_local_to_global_delay(sched, task, localDelay);
	return(delay);
}

f64 sched_task_internal_pos(sched_task handle)
{
	sched_info* sched = sched_get_context();
	sched_task_info* task = sched_handle_get_task_ptr(sched, handle);
	return(task->currentPos);
}

f64 sched_task_transformed_internal_pos(sched_task handle)
{
	sched_info* sched = sched_get_context();
	sched_task_info* task = sched_handle_get_task_ptr(sched, handle);
	return(task->transformedPos);
}

f64 sched_task_logical_pos(sched_task handle)
{
	sched_info* sched = sched_get_context();
	sched_task_info* task = sched_handle_get_task_ptr(sched, handle);
	return(task->logicalPos);
}

f64 sched_task_transformed_logical_pos(sched_task handle)
{
	sched_info* sched = sched_get_context();
	sched_task_info* task = sched_handle_get_task_ptr(sched, handle);

	f64 transformedLogicalPos = 0;
	if(!task->timeCurve)
	{
		transformedLogicalPos = task->logicalPos;
	}
	else
	{
		transformedLogicalPos = sched_curve_get_time_from_position(task->timeCurve, task->logicalPos);
	}
	return(transformedLogicalPos);
}

f64 sched_task_tempo(sched_task handle)
{
	sched_info* sched = sched_get_context();
	sched_task_info* task = sched_handle_get_task_ptr(sched, handle);

	f64 tempo = 1;
	if(task->timeCurve)
	{
		tempo = sched_curve_get_tempo_from_position(task->timeCurve, task->logicalPos);
	}
	return(tempo);
}

//------------------------------------------------------------------------------------------------------
//NOTE(martin): scheduling / waits
//------------------------------------------------------------------------------------------------------
void sched_wait(sched_steps steps)
{
	//NOTE(martin): reschedule task
	sched_info* sched = sched_get_context();
	sched_task_info* task = sched->currentTask;

	sched_task_reschedule_in_steps(sched, task, steps);

	//NOTE(martin): yield to the scheduler fiber
	fiber_yield(task->context);
}

typedef enum { SCHED_WAIT_OK = 0,
               SCHED_WAIT_INVALID_HANDLE,
               SCHED_WAIT_SIGNALED,
               SCHED_WAIT_TIMEOUT } sched_wait_code;

sched_wait_code sched_task_put_on_object_waitlist_ptr(sched_info* sched,
                                                      sched_task_info* task,
                                                      sched_task handle,
                                                      sched_task_signal signal,
                                                      sched_steps timeout)
{
	//NOTE(martin): get the handle status and waiting list
	sched_handle_slot* slot =  sched_handle_slot_find(sched, handle.h);
	sched_task_status status = slot->task->status;
	list_elt* waiting = &(slot->task->waiting);

	//NOTE(martin): return immediately if the handle is already signaled
	if(  (status == SCHED_STATUS_RETIRED && signal == SCHED_SIG_RETIRED)
	  || ((status == SCHED_STATUS_COMPLETED) && (signal == SCHED_STATUS_RETIRED || signal == SCHED_STATUS_COMPLETED )))
	{
		return(SCHED_WAIT_SIGNALED);
	}

	//NOTE(martin): schedule the task according to its timeout, mark it as suspended and put it in the waiting list.
	//              if the timeout is < 0, we just remove the task from the scheduled list
	if(timeout == 0)
	{
		return(SCHED_WAIT_TIMEOUT);
	}
	else
	{
		CListRemove(&task->waitingElt);
		CListRemove(&task->listElt);

		if(timeout < 0)
		{
			task->nextPos = 0;
		}
		else
		{
			sched_task_reschedule_in_steps(sched, task, timeout);
		}
	}
	task->status = SCHED_STATUS_SUSPENDED;
	task->waitingFor = signal;
	CListAppend(waiting, &task->waitingElt);
	return(SCHED_WAIT_OK);
}

void sched_task_put_on_object_waitlist(sched_task task, sched_task handle, sched_task_signal signal, sched_steps timeout)
{
	sched_info* sched = sched_get_context();
	sched_task_info* taskPtr = sched_handle_get_task_ptr(sched, task);
	if(taskPtr)
	{
		sched_task_put_on_object_waitlist_ptr(sched, taskPtr, handle, signal, timeout);
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		//TODO(martin): should probably check wait code and if the task has NOT been put in the waitlist,
		//              reschedule it immediately... (otherwise the the task stays scheduled as it was...)
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
	}
}

sched_wakeup_code sched_wait_for_task(sched_task handle, sched_task_signal signal, sched_steps timeout)
{
	sched_info* sched = sched_get_context();
	sched_task_info* task = sched->currentTask;

	sched_wait_code code = sched_task_put_on_object_waitlist_ptr(sched, task, handle, signal, timeout);
	switch(code)
	{
		case SCHED_WAIT_OK:
			break;
		case SCHED_WAIT_INVALID_HANDLE:
			return(SCHED_WAKEUP_INVALID_HANDLE);
		case SCHED_WAIT_SIGNALED:
			return(SCHED_WAKEUP_SIGNALED);
		case SCHED_WAIT_TIMEOUT:
			return(SCHED_WAKEUP_TIMEOUT);
	}

	//NOTE(martin): yield to the scheduler, then return wakeup code
	fiber_yield(task->context);
	return(task->wakeupCode);
}

sched_task sched_task_self()
{
	sched_info* sched = sched_get_context();
	return(sched_alloc_task_handle(sched, sched->currentTask));
}

void sched_task_suspend_ptr(sched_info* sched, sched_task_info* task)
{
	//NOTE(martin): mark the task as suspended and remove it from any scheduling list.
	//              This way the task won't be considered in sched_pick_event().
	//              Note that it will still be updated by sched_task_update_position().
	//              In case where to completely stop updating the task (and its children), we should use sched_task_freeze()
	CListRemove(&task->listElt);
	task->status = SCHED_STATUS_SUSPENDED;

	if(sched->currentTask == task)
	{
		fiber_yield(task->context);
	}
}

void sched_task_suspend(sched_task task)
{
	sched_info* sched = sched_get_context();
	sched_task_info* taskPtr = sched_handle_get_task_ptr(sched, task);
	ASSERT(taskPtr);
	sched_task_suspend_ptr(sched, taskPtr);
}

void sched_suspend()
{
	sched_info* sched = sched_get_context();
	sched_task_suspend_ptr(sched, sched->currentTask);
}

void sched_task_resume(sched_task task)
{
	sched_info* sched = sched_get_context();
	sched_task_info* taskPtr = sched_handle_get_task_ptr(sched, task);
	ASSERT(taskPtr);

	CListRemove(&taskPtr->listElt);
	taskPtr->status = SCHED_STATUS_RUNNING;
	CListAppend(&sched->scheduledTasks, &taskPtr->listElt);
}

void sched_task_cancel_ptr(sched_info* sched, sched_task_info* task)
{
	CListRemove(&task->waitingElt);
	CListRemove(&task->listElt);

	//NOTE(martin): cancel all subtasks
	for_each_in_clist_safe(&task->children, child, sched_task_info, parentElt)
	{
		sched_task_cancel_ptr(sched, child);
	}

	//NOTE(martin): notify waiting fibers of cancellation
	sched_signal_waiting_tasks_on_cancel(sched, &task->waiting);
	sched_task_retire(sched, task);
}

void sched_task_cancel(sched_task task)
{
	sched_info* sched = sched_get_context();
	sched_task_info* taskPtr = sched_handle_get_task_ptr(sched, task);
	ASSERT(taskPtr);

	sched_task_cancel_ptr(sched, taskPtr);
}

//------------------------------------------------------------------------------------------------------
//NOTE(martin): misc handles functions
//------------------------------------------------------------------------------------------------------
int sched_task_get_exit_code(sched_task task, i64* exitCode)
{
	sched_info* sched = sched_get_context();

	//TODO(martin): return error if still active...
	sched_handle_slot* slot = sched_handle_slot_find(sched, task.h);
	*exitCode = slot->task->exitCode;
	return(0);
}

void sched_task_release(sched_task handle)
{
	sched_info* sched = sched_get_context();
	sched_handle_slot* slot = sched_handle_slot_find(sched, handle.h);

	sched_task_info* task = slot->task;
	task->openHandles--;
	sched_task_check_if_needs_recycling(sched, task);

	//NOTE(martin): recycle the handle slot
	sched_handle_slot_recycle(sched, slot);
}

//------------------------------------------------------------------------------------------------------
//NOTE(martin): background jobs control
//------------------------------------------------------------------------------------------------------

void sched_background()
{
	//TODO(martin): ensure this function can't be called from background!

	sched_info* sched = sched_get_context();
	sched_task_info* task = sched->currentTask;

	DEBUG_ASSERT(CListEmpty(&task->listElt), "task should have been removed from task list by sched_pick_event()");
	DEBUG_ASSERT(CListEmpty(&task->waitingElt), "task is executing, so it should have been removed from any waiting list");

	//NOTE(martin): set the task status to background, put it at the end of the running task's list, and yield.
	//              The scheduler thread will notice the background status and put the fiber on the background jobs queue.
	task->logicalPos = 0;

	task->status = SCHED_STATUS_BACKGROUND;
	fiber_yield(task->context);
}

void sched_foreground()
{
	//NOTE(martin): yield back to the background job entry point. This will put the fiber back in the scheduler's queue
	DEBUG_ASSERT(__backgroundJobCurrentTask);
	fiber_yield(__backgroundJobCurrentTask->context);
}

//------------------------------------------------------------------------------------------------------
//NOTE(martin): sched init / end functions
//------------------------------------------------------------------------------------------------------
void sched_init_internal()
{
	sched_info* sched = sched_get_context();

	memset(sched, 0, sizeof(sched_info));

	//NOTE(martin): init memory pools
	mem_pool_init(&sched->messagePool, sizeof(sched_message));
	mem_pool_init(&sched->taskPool, sizeof(sched_task_info));
	mem_pool_init(&sched->stackPool, SCHED_FIBER_STACK_SIZE);

	//NOTE(martin): init handle map
	sched->nextHandleSlot = 0;
	CListInit(&sched->handleFreeList);

	//NOTE(martin): init command locks
	sched->msgCondition = ConditionCreate();
	sched->msgConditionMutex = MutexCreate();
	TicketSpinMutexInit(&sched->msgQueueMutex);
	TicketSpinMutexInit(&sched->msgPoolMutex);
	sched->hasMessages = false;
	CListInit(&sched->messages);

	//NOTE(martin): init scheduling variables
	sched->lastTimeUpdate = 0;
	sched->timeToSleepResidue = 0;
	sched->startTime = 0;
	sched->nextTicket = 0;

	CListInit(&sched->activeTasks);
	CListInit(&sched->scheduledTasks);
	sched->currentTask = 0;

	//NOTE(martin): init job queue
	sched_background_queue_init(sched);

	//NOTE(martin): create the main task, and the main fiber, without scheduling it. Upon return it contains the entry point to the
	//              scheduler fiber, but after the first yield, from the point of view of the scheduler, the context
	//              will contain the entry point to the main fiber.

	sched_task_info* task = sched_task_alloc_init(sched, 0, sched_run, 0);

	//NOTE(martin): set the current fiber, so that we're in the same state as if we just
	//              returned from the scheduler's fiber.
	sched->currentTask = task;

	//NOTE(martin): now wait for 0 steps, to jumpstart the sched_run() fiber.
	sched_wait(0);
}

void sched_init()
{
	sched_clock_get_time = sched_online_clock_get_time;
	sched_condition_timed_wait = sched_online_condition_timed_wait;
	sched_condition_wait = sched_online_condition_wait;

	sched_init_internal();
}

void sched_init_offline()
{
	sched_clock_get_time = sched_offline_clock_get_time;
	sched_condition_timed_wait = sched_offline_condition_timed_wait;
	sched_condition_wait = sched_offline_condition_wait;

	sched_init_internal();
}

void sched_end()
{
	sched_info* sched = sched_get_context();

	//NOTE(martin): clean background queue, join threads
	sched_background_queue_cleanup(&sched->jobQueue);

	sched_task_info* task = 0;
	//NOTE(martin): cancel all tasks
	//TODO(martin): should really be able to free all pools and be done with it ?...
	while((task = CListPopEntry(&sched->activeTasks, sched_task_info, activeListElt)) != 0)
	{
		sched_task_cancel_ptr(sched, task);
	}

	//NOTE(martin): release memory from all pools
	mem_pool_release(&sched->taskPool);
	mem_pool_release(&sched->stackPool);

	//NOTE(martin): destroy command locks
	ConditionDestroy(sched->msgCondition);
	MutexDestroy(sched->msgConditionMutex);

	//NOTE(martin): clear context
	memset(sched, 0, sizeof(sched_info));
}


//////////////////////////// WIP ////////////////////////////////////

/*
void sched_command_fiber_wakeup(sched_fiber fiber)
{
	sched_info* sched = sched_get_context();

	sched_message* message = sched_message_acquire(sched);
	{
		message->kind = SCHED_MESSAGE_WAKEUP;
		message->fiberHandle = fiber;
	} sched_message_commit(sched, message);
}
*/


#undef LOG_SUBSYSTEM
