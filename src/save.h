/************************************************************//**
*
*	@file: save.h
*	@author: Martin Fouilleul
*	@date: 25/02/2022
*	@revision:
*
*****************************************************************/
#ifndef __SAVE_H_
#define __SAVE_H_

#include"cells.h"

mp_string serialize_cell_tree(mem_arena* arena, u64 nextID, q_cell* root);
int deserialize_cell_tree(cell_tree* tree, mp_string data);

mp_string serialize_cell_span(mem_arena* arena, qed_cell_span span);
list_info deserialize_cell_span(cell_tree* tree, mp_string data);

#endif //__SAVE_H_
