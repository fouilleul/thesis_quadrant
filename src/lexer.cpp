/************************************************************//**
*
*	@file: lexer.cpp
*	@author: Martin Fouilleul
*	@date: 11/05/2022
*	@revision:
*
*****************************************************************/
#include"tokens.h"

typedef struct lex_entry
{
	q_token token;
	mp_string string;
} lex_entry;

const lex_entry LEX_OPERATORS[] = {
	#define X(tok, str) { .token = _cat2_(TOKEN_, tok), .string = mp_string_lit(str) },
		Q_TOKEN_OPERATORS(X)
	#undef X
};
const u32 LEX_OPERATOR_COUNT = sizeof(LEX_OPERATORS)/sizeof(lex_entry);

const lex_entry LEX_KEYWORDS[] = {
	#define X(tok, str) { .token = _cat2_(TOKEN_, tok), .string = mp_string_lit(str) },
		Q_TOKEN_KEYWORDS(X)
	#undef X
};
const u32 LEX_KEYWORD_COUNT = sizeof(LEX_KEYWORDS)/sizeof(lex_entry);


typedef struct lex_result
{
	mp_string string;
	cell_kind kind;
	u64 valueU64;
	f64 valueF64;
} lex_result;

lex_result lex_operator(mp_string string, u64 byteOffset)
{
	u64 startOffset = byteOffset;
	u64 endOffset = byteOffset + 1;

	while(endOffset < string.len)
	{
		char c = string.ptr[endOffset];
		if(  c == '+' || c == '-' || c == '*' || c == '/' || c == '%'
		  || c == '!' || c == '=' || c == '<' || c == '>')
		{
			endOffset += 1;
		}
		else
		{
			break;
		}
	}
	lex_result result = {.string = mp_string_slice(string, startOffset, endOffset),
	                     .kind = CELL_WORD,
	                     .valueU64 = TOKEN_UNKNOWN };

	for(int i=0; i<LEX_OPERATOR_COUNT; i++)
	{
		if(!mp_string_cmp(result.string, LEX_OPERATORS[i].string))
		{
			result.valueU64 = LEX_OPERATORS[i].token;
			break;
		}
	}

	return(result);
}

lex_result lex_identifier(mp_string string, u64 byteOffset)
{
	u64 startOffset = byteOffset;
	u64 endOffset = byteOffset + utf8_size_from_leading_char(string.ptr[startOffset]);

	while(endOffset < string.len)
	{
		char c = string.ptr[endOffset];
		if( (c >= 'a' && c <= 'z')
		  ||(c >= 'A' && c <= 'Z')
		  ||(c >= '0' && c <= '9')
		  || c == '_'
		  || c == ':')
		{
			endOffset += 1;
		}
		else
		{
			break;
		}
	}
	lex_result result = {.string = mp_string_slice(string, startOffset, endOffset),
	                     .kind = CELL_WORD,
	                     .valueU64 = TOKEN_IDENTIFIER };

	return(result);
}

lex_result lex_identifier_or_keyword(mp_string string, u64 byteOffset)
{
	lex_result result = lex_identifier(string, byteOffset);

	for(int i=0; i<LEX_KEYWORD_COUNT; i++)
	{
		if(!mp_string_cmp(result.string, LEX_KEYWORDS[i].string))
		{
			result.valueU64 = LEX_KEYWORDS[i].token;
			break;
		}
	}

	return(result);
}

lex_result lex_ui_identifier(mp_string string, u64 byteOffset)
{
	lex_result result = {0};

	if(byteOffset < string.len)
	{
		result = lex_identifier(string, byteOffset);
	}
	result.kind = CELL_UI;

	if(!mp_string_cmp(result.string, mp_string_lit("tempo_curve")))
	{
		result.valueU64 = CELL_UI_TEMPO_CURVE;
	}
	return(result);
}

lex_result lex_poly_param(mp_string string, u64 byteOffset)
{
	u64 startOffset = byteOffset;
	u64 endOffset = byteOffset + 1;

	while(endOffset < string.len)
	{
		char c = string.ptr[endOffset];
		if( (c >= 'a' && c <= 'z')
		  ||(c >= 'A' && c <= 'Z')
		  ||(c >= '0' && c <= '9')
		  || c == '_')
		{
			endOffset += 1;
		}
		else
		{
			break;
		}
	}
	lex_result result = {.string = mp_string_slice(string, startOffset, endOffset),
	                     .kind = CELL_WORD,
	                     .valueU64 = TOKEN_POLY };
	return(result);
}

lex_result lex_number(mp_string string, u64 byteOffset)
{
	u64 startOffset = byteOffset;
	u64 endOffset = byteOffset;

	u64 numberU64 = 0;
	while(endOffset < string.len)
	{
		char c = string.ptr[endOffset];
		if(c >= '0' && c <= '9')
		{
			numberU64 *= 10;
			numberU64 += c - '0';
			endOffset += 1;
		}
		else
		{
			break;
		}
	}

	lex_result result = {};

	f64 numberF64;
	if(  endOffset < string.len
	  && string.ptr[endOffset] == '.'
	  && (  endOffset + 1 >= string.len
	     || string.ptr[endOffset+1] != '.'))
	{
		endOffset += 1;

		u64 decimals = 0;
		u64 decimalCount = 0;

		while(endOffset < string.len)
		{
			char c = string.ptr[endOffset];
			if(c >= '0' && c <= '9')
			{
				decimals *= 10;
				decimals += c - '0';
				endOffset += 1;
				decimalCount += 1;
			}
			else
			{
				break;
			}
		}
		result.kind = CELL_FLOAT;
		result.string = mp_string_slice(string, startOffset, endOffset);
		result.valueF64 = (f64)numberU64 + (f64)decimals/pow(10, decimalCount);
	}
	else
	{
		result.kind = CELL_INT;
		result.string = mp_string_slice(string, startOffset, endOffset);
		result.valueU64 = numberU64;
	}
	return(result);
}

lex_result lex_error(mp_string string, u64 byteOffset)
{
	u64 startOffset = byteOffset;
	u64 endOffset = startOffset;

	while(endOffset < string.len)
	{
		utf8_dec decode = utf8_decode_at(string, endOffset);
		utf32 c = decode.codepoint;

		if( (c == '$')
		  ||(c >= 'a' && c <= 'z')
		  ||(c >= 'A' && c <= 'Z')
		  ||(c >= '0' && c <= '9')
		  ||(c == '_')
		  ||(  c == '+' || c == '-' || c == '*' || c == '/' || c == '%'
		    || c == '!' || c == '=' || c == '<' || c == '>'))
		{
			goto end;
		}
		endOffset += decode.size;
	}
	end:
	lex_result result = {.string = mp_string_slice(string, startOffset, endOffset),
	                     .kind = CELL_WORD,
	                     .valueU64 = TOKEN_UNKNOWN};
	return(result);
}

lex_result lex_next(mp_string string, u64 byteOffset, cell_kind srcKind)
{
	lex_result result = {};

	if(  srcKind == CELL_STRING
	  || srcKind == CELL_COMMENT)
	{
		//TODO for quote, set valueU64?
		result.string = mp_string_slice(string, byteOffset, string.len);
		result.kind = srcKind;
	}
	else if(srcKind == CELL_CHAR)
	{
		if(byteOffset >= string.len)
		{
			result.string = mp_string_slice(string, byteOffset, string.len);
			result.kind = srcKind;
			result.valueU64 = 0;
		}
		else
		{
			utf8_dec decode = utf8_decode_at(string, byteOffset);
			utf32 c = decode.codepoint;

			result.string = mp_string_slice(string, byteOffset, byteOffset + decode.size);
			result.kind = CELL_CHAR;
			result.valueU64 = c;
		}
	}
	else if(srcKind == CELL_UI)
	{
		result = lex_ui_identifier(string, byteOffset);
	}
	else if(byteOffset >= string.len)
	{
		//WARN: hole
		result.string = (mp_string){0};
		result.kind = CELL_HOLE;
	}
	else
	{
		utf8_dec decode = utf8_decode_at(string, byteOffset);
		utf32 c = decode.codepoint;

		/*TODO: decide if we should lex that at all...
		if(c == '#')
		{
			return(lex_gui(string, byteOffset));
		}
		else
		*/
		if(c == '$')
		{
			result = lex_poly_param(string, byteOffset);
		}
		else if( (c >= 'a' && c <= 'z')
		      	||(c >= 'A' && c <= 'Z')
		      	|| c == '_')
		{
			result = lex_identifier_or_keyword(string, byteOffset);
		}
		else if(  c == '+' || c == '-' || c == '*' || c == '/' || c == '%'
		       || c == '!' || c == '=' || c == '<' || c == '>')
		{
			result = lex_operator(string, byteOffset);
		}
		else if(c >= '0' && c <= '9')
		{
			result = lex_number(string, byteOffset);
		}
		else
		{
			result = lex_error(string, byteOffset);
		}
	}
	return(result);
}

//TODO: move that in parser
q_token cell_token(q_cell* cell)
{
	q_token token = TOKEN_END;
	if(cell)
	{
		if(cell->kind == CELL_WORD)
		{
			token = (q_token)cell->valueU64;
		}
		else
		{
			token = (q_token)cell->kind;
		}
	}
	return(token);
}
