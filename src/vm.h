/************************************************************//**
*
*	@file: vm.h
*	@author: Martin Fouilleul
*	@date: 24/03/2022
*	@revision:
*
*****************************************************************/
#ifndef __VM_H_
#define __VM_H_

#include"typedefs.h"
#include"strings.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct rt_any
{
	u64 valueOffset;
	u64 typeOffset;
} rt_any;

typedef struct rt_slice
{
	u64 len;
	u64 offset;
} rt_slice;


u8* qvm_get_memory();
u8* qvm_alloc(u64 size);
void qvm_free(u8* p);
void qvm_puts(mp_string string);

void qvm_sync_beat(u64 handle, f64 pos, f64 tempo);

f64 qvm_sync_get_tempo(u64 handle);
f64 qvm_sync_get_beat(u64 handle);
f64 qvm_sync_get_clock();

#ifdef __cplusplus
} // extern "C"
#endif


#endif //__VM_H_
