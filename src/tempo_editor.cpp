/************************************************************//**
*
*	@file: tempo_editor.cpp
*	@author: Martin Fouilleul
*	@date: 26/07/2022
*	@revision:
*
*****************************************************************/
#include"tempo_editor.h"
#include"cells.h"

typedef struct qed_curve_point
{
	f32 x;
	f32 y;
} qed_curve_point;

typedef struct qed_curve_editor_elt
{
	list_elt listElt;

	sched_curve_type type;
	qed_curve_point p[4];

	bool startDerivability;
	bool startContinuity;
	bool endDerivability;
	bool endContinuity;

} qed_curve_editor_elt;

typedef struct qed_curve_editor
{
	u32 eltCount;
	list_info elements;
	sched_curve_axes axes;

	f32 cameraX;
	f32 cameraW;
	f32 cameraH;

	qed_curve_editor_elt* focusElt;
	u32 focusPointIndex;
	bool dragging;

} qed_curve_editor;

const f32 QED_DEFAULT_CURVE_EDITOR_CAMERA_WIDTH = 10,
          QED_DEFAULT_CURVE_EDITOR_CAMERA_HEIGHT = 4;


void qed_bezier_split(qed_curve_point p[4], f32 t, qed_curve_point outLeft[4], qed_curve_point outRight[4])
{
	//NOTE(martin): split bezier curve p at parameter t, using De Casteljau's algorithm
	//              the q_n are the points along the hull's segments at parameter t
	//              the r_n are the points along the (q_n, q_n+1) segments at parameter t
	//              s is the split point.

	f32 oneMt = 1-t;

	qed_curve_point q0 = {oneMt*p[0].x + t*p[1].x,
		   oneMt*p[0].y + t*p[1].y};

	qed_curve_point q1 = {oneMt*p[1].x + t*p[2].x,
		   oneMt*p[1].y + t*p[2].y};

	qed_curve_point q2 = {oneMt*p[2].x + t*p[3].x,
		   oneMt*p[2].y + t*p[3].y};

	qed_curve_point r0 = {oneMt*q0.x + t*q1.x,
		   oneMt*q0.y + t*q1.y};

	qed_curve_point r1 = {oneMt*q1.x + t*q2.x,
		   oneMt*q1.y + t*q2.y};

	qed_curve_point s = {oneMt*r0.x + t*r1.x,
		  oneMt*r0.y + t*r1.y};;

	outLeft[0] = p[0];
	outLeft[1] = q0;
	outLeft[2] = r0;
	outLeft[3] = s;

	outRight[0] = s;
	outRight[1] = r1;
	outRight[2] = q2;
	outRight[3] = p[3];
}

void qed_curve_editor_insert_point(qed_curve_editor* editor, f32 x, f32 y)
{
	qed_curve_editor_elt* splitElt = 0;
	qed_curve_editor_elt* nextElt = 0;

	qed_curve_point lastPoint = {};
	for_each_in_list(&editor->elements, elt, qed_curve_editor_elt, listElt)
	{
		if(x > elt->p[0].x && x <= elt->p[3].x)
		{
			splitElt = elt;
			nextElt = ListNextEntry(&editor->elements, splitElt, qed_curve_editor_elt, listElt);
			break;
		}
		lastPoint = elt->p[3];
	}
	if(!splitElt)
	{
		//add an element at the end
		qed_curve_editor_elt* newElt = malloc_type(qed_curve_editor_elt);
		newElt->type = SCHED_CURVE_BEZIER;
		newElt->startContinuity = true;
		newElt->endContinuity = true;
		newElt->startDerivability = true;
		newElt->endDerivability = true;

		newElt->p[0] = lastPoint;
		newElt->p[3] = (qed_curve_point){x, y};
		newElt->p[1] = (qed_curve_point){0.75*newElt->p[0].x + 0.25*newElt->p[3].x,
		                                 0.75*newElt->p[0].y + 0.25*newElt->p[3].y};
		newElt->p[2] = (qed_curve_point){0.25*newElt->p[0].x + 0.75*newElt->p[3].x,
		                                 0.25*newElt->p[0].y + 0.75*newElt->p[3].y};

		ListAppend(&editor->elements, &newElt->listElt);
		editor->focusElt = newElt;
		editor->focusPointIndex = 3;
		editor->dragging = true;
		editor->eltCount++;
		return;
	}

	//TODO: collapse with first case

	bezier_coeffs coeffs;
	bezier_coeffs_init_with_control_points(&coeffs,
	                                       splitElt->p[0].x, splitElt->p[0].y,
	                                       splitElt->p[1].x, splitElt->p[1].y,
	                                       splitElt->p[2].x, splitElt->p[2].y,
	                                       splitElt->p[3].x, splitElt->p[3].y);

	f32 splitT = bezier_solve_x(&coeffs, x);

	qed_curve_editor_elt* newElt = malloc_type(qed_curve_editor_elt);
	newElt->type = SCHED_CURVE_BEZIER;
	newElt->startContinuity = splitElt->endContinuity;
	newElt->endContinuity = nextElt ? nextElt->startContinuity : true;
	newElt->startDerivability = splitElt->endDerivability;
	newElt->endDerivability = nextElt ? nextElt->startDerivability : true;

	qed_curve_point inSplit[4];
	memcpy(inSplit, splitElt->p, sizeof(qed_curve_point)*4);
	qed_bezier_split(inSplit, splitT, splitElt->p, newElt->p);

	ListInsert(&editor->elements, &splitElt->listElt, &newElt->listElt);

	editor->focusElt = newElt;
	editor->focusPointIndex = 0;
	editor->dragging = true;

	editor->eltCount++;
}

void qed_curve_editor_delete_point(qed_curve_editor* editor, qed_curve_editor_elt* elt, u32 pointIndex)
{
	if(pointIndex == 0)
	{
		qed_curve_editor_elt* prev = ListPrevEntry(&editor->elements, elt, qed_curve_editor_elt, listElt);
		if(prev)
		{
			qed_curve_editor_delete_point(editor, prev, 3);
		}
	}
	else if(pointIndex == 3)
	{
		qed_curve_editor_elt* next = ListNextEntry(&editor->elements, elt, qed_curve_editor_elt, listElt);
		if(next)
		{
			elt->endContinuity = next->endContinuity;
			elt->endDerivability = next->endDerivability;
			elt->p[2] = next->p[2];
			elt->p[3] = next->p[3];

			ListRemove(&editor->elements, &next->listElt);
			free(next);

			editor->focusElt = 0;
			editor->focusPointIndex = 0;
			editor->dragging = false;
			editor->eltCount--;
		}
	}
}

void qed_curve_editor_toggle_continuity(qed_curve_editor* editor, qed_curve_editor_elt* elt, u32 pointIndex)
{
	if(pointIndex == 0)
	{
		qed_curve_editor_elt* prev = ListPrevEntry(&editor->elements, elt, qed_curve_editor_elt, listElt);
		if(prev)
		{
			qed_curve_editor_toggle_continuity(editor, prev, 3);
		}
	}
	else if(pointIndex == 3)
	{
		qed_curve_editor_elt* next = ListNextEntry(&editor->elements, elt, qed_curve_editor_elt, listElt);
		if(next)
		{
			elt->endContinuity = !elt->endContinuity;
			next->startContinuity = elt->endContinuity;

			if(elt->endContinuity)
			{
				next->p[0].y = elt->p[3].y;
			}
		}
	}
}

void qed_curve_to_plot_coords(qed_curve_editor* editor, mp_aligned_rect plotFrame, f32 curveX, f32 curveY, f32* plotX, f32* plotY)
{
	*plotX = (curveX - editor->cameraX)*plotFrame.w/editor->cameraW;
	*plotY = curveY * plotFrame.h/editor->cameraH;
}

void qed_plot_to_curve_coords(qed_curve_editor* editor, mp_aligned_rect plotFrame, f32 plotX, f32 plotY, f32* curveX, f32* curveY)
{
	*curveX = plotX * editor->cameraW/plotFrame.w + editor->cameraX;
	*curveY = plotY * editor->cameraH/plotFrame.h;
}

void qed_curve_to_pixel_coords(qed_curve_editor* editor, mp_aligned_rect plotFrame, f32 curveX, f32 curveY, f32* pX, f32* pY)
{
	*pX = plotFrame.x + (curveX - editor->cameraX)*plotFrame.w/editor->cameraW;
	*pY = plotFrame.y + plotFrame.h - curveY * plotFrame.h/editor->cameraH;
}

void qed_pixel_to_curve_coords(qed_curve_editor* editor, mp_aligned_rect plotFrame, f32 pX, f32 pY, f32* curveX, f32* curveY)
{
	*curveX = (pX - plotFrame.x) * editor->cameraW/plotFrame.w + editor->cameraX;
	*curveY = (plotFrame.y + plotFrame.h - pY) * editor->cameraH/plotFrame.h;
}


//TODO: extend the acceptable camera range once we have fixed our graphics floating-point precision bugs
/*
const f32 QED_CURVE_EDITOR_MAX_CAMERA_X = 1ULL<<32,
          QED_CURVE_EDITOR_MIN_CAMERA_W = 0.001,
          QED_CURVE_EDITOR_MAX_CAMERA_W = 1ULL<<32;
*/

//TODO: move somewhere else
vec2 q_print_string(mp_gui_context* gui, mp_string string, vec2 pos, mp_graphics_color color);

const f32 QED_CURVE_EDITOR_MAX_CAMERA_X = 5000,
          QED_CURVE_EDITOR_MIN_CAMERA_W = 0.001,
          QED_CURVE_EDITOR_MAX_CAMERA_W = 5000;

const f32 QED_TEMPO_EDITOR_MIN_WIDTH = 800,
          QED_TEMPO_EDITOR_MIN_HEIGHT = 400;

bool qed_tempo_editor_gui(mp_gui_context* gui, q_cell* cell)
{
	DEBUG_ASSERT(cell->kind == CELL_UI && cell->ui.kind == CELL_UI_TEMPO_CURVE);
	qed_curve_editor* editor = (qed_curve_editor*)(cell->ui.data.ptr);

	mp_aligned_rect viewFrame = mp_gui_view_get_frame(gui);

	viewFrame.w = maximum(viewFrame.w, QED_TEMPO_EDITOR_MIN_WIDTH);
	viewFrame.h = maximum(viewFrame.h, QED_TEMPO_EDITOR_MIN_HEIGHT);

	cell->ui.width = viewFrame.w;
	cell->ui.height = viewFrame.h;

	mp_gui_view_set_frame(gui, viewFrame);

	mp_graphics_context graphics = mp_gui_get_graphics(gui);

	q_print_string(gui, mp_string_lit("Tempo Curve Editor"), (vec2){10, 10}, (mp_graphics_color){1, 1, 1, 1});

	mp_gui_style* style = mp_gui_style_top(gui);
	mp_graphics_font_extents fontExtents;
	mp_graphics_font_get_extents(graphics, style->font, &fontExtents);
	f32 fontScale = mp_graphics_font_get_scale_for_em_pixels(graphics, style->font, style->fontSize);
	f32 fontWidth = fontExtents.width * fontScale;
	f32 lineHeight = (fontExtents.ascent + fontExtents.descent) * fontScale;

	mp_aligned_rect plotFrame = {50,
	                             50 + 2*lineHeight,
	                             viewFrame.w - 100,
	                             viewFrame.h - 3*lineHeight - 100};

	mp_graphics_set_width(graphics, 1);
	mp_graphics_set_color_rgba(graphics, 0.5, 0.5, 0.5, 1);
	mp_graphics_rectangle_stroke(graphics, plotFrame.x, plotFrame.y, plotFrame.w, plotFrame.h);


	if(mp_gui_is_mouse_over(gui, plotFrame))
	{
		mp_key_mods mods = mp_gui_key_mods(gui);

		//NOTE(martin): horizontal zoom. This is adjusted with either Cmd+mouseY or wheelY, if we're not panning (with Ctrl)
		f32 zoomCtrl = 0;
		if(mods ^ MP_KEYMOD_CTRL)
		{
			zoomCtrl = mp_gui_mouse_wheel_y(gui);
		}
		//NOTE(martin):	we compute the zoom ratio and the screen space zoom displacement needed to recenter the view around
		//  the mouse cursor. We then multiply the camera width by the zoom ratio and correct camera X to keep the
		//  point under the mouse cursor fixed.
		f32 zoomRatio = 1 + zoomCtrl/plotFrame.w;
		f32 zoomOffset = (mp_gui_mouse_x(gui) - plotFrame.x)*(zoomRatio - 1);

		editor->cameraW *= zoomRatio;
		editor->cameraX -= zoomOffset * editor->cameraW/plotFrame.w;

		//NOTE(martin): horizontal pan. This is adjusted with either Ctrl+mouseX or Ctrl+wheelX
		f32 panCtrl = 0;
		if(mods & MP_KEYMOD_CTRL)
		{
			panCtrl = -mp_gui_mouse_delta_x(gui);
			if(!panCtrl)
			{
				panCtrl = -mp_gui_mouse_wheel_x(gui);
			}
		}
		f32 panValue = panCtrl * editor->cameraW/plotFrame.w;
		editor->cameraX += panValue;

		//NOTE: should clamp camera to reasonable max values (in order to avoid floating point inf/Nan at the limit)
		editor->cameraX = Clamp(editor->cameraX, 0, QED_CURVE_EDITOR_MAX_CAMERA_X);
		editor->cameraW = Clamp(editor->cameraW, QED_CURVE_EDITOR_MIN_CAMERA_W, QED_CURVE_EDITOR_MAX_CAMERA_W);
	}

	bool modified = false;
	if(mp_gui_is_mouse_button_pressed(gui, MP_MOUSE_LEFT))
	{
		for_each_in_list(&editor->elements, elt, qed_curve_editor_elt, listElt)
		{
			for(int i=0; i<4; i++)
			{
				f32 x, y;
				qed_curve_to_pixel_coords(editor, plotFrame, elt->p[i].x, elt->p[i].y, &x, &y);

				mp_aligned_rect ctrlBox = {x - 10, y - 10, 20, 20};
				if(mp_gui_is_mouse_over(gui, ctrlBox))
				{
					if(mp_gui_key_mods(gui) & MP_KEYMOD_ALT)
					{
						qed_curve_editor_delete_point(editor, elt, i);
						modified = true;
					}
					else
					{
						editor->focusElt = elt;
						editor->focusPointIndex = i;
						editor->dragging = true;
					}
					goto endFocus;
				}
			}
		}

		mp_aligned_rect frameRect = {plotFrame.x, plotFrame.y, plotFrame.w , plotFrame.h};
		if(mp_gui_is_mouse_over(gui, frameRect))
		{
			//NOTE(martin): no control point was clicked, but we clicked in plot, so we either create a point
			//              if we clicked on the curve, or deselect all points if we clicked in an empty space

			f32 mouseX = mp_gui_mouse_x(gui) - plotFrame.x;
			f32 mouseY = plotFrame.y + plotFrame.h - mp_gui_mouse_y(gui);
			f32 curveX = 0;
			f32 curveY = 0;
			qed_plot_to_curve_coords(editor, plotFrame, mouseX, mouseY, &curveX, &curveY);

			bool insert = false;
			qed_curve_point lastPoint = {};
			for_each_in_list(&editor->elements, elt, qed_curve_editor_elt, listElt)
			{
				if(curveX >= elt->p[0].x && curveX <= elt->p[3].x)
				{
					bezier_coeffs coeffs;
					bezier_coeffs_init_with_control_points(&coeffs,
					                                       elt->p[0].x, elt->p[0].y,
					                                       elt->p[1].x, elt->p[1].y,
					                                       elt->p[2].x, elt->p[2].y,
					                                       elt->p[3].x, elt->p[3].y);

					f32 s = bezier_solve_x(&coeffs, curveX);
					f32 evalY = bezier_sample_y(&coeffs, s);
					f32 screenEvalY = evalY * plotFrame.h/editor->cameraH;

					if(abs(screenEvalY - mouseY) < 10)
					{
						qed_curve_editor_insert_point(editor, curveX, evalY);
						insert = true;
						modified = true;
					}
					break;
				}
				lastPoint = elt->p[3];
			}
			if(curveX > lastPoint.x)
			{
				f32 screenLastY = lastPoint.y * plotFrame.h/editor->cameraH;
				if(abs(screenLastY - mouseY) < 10)
				{
					qed_curve_editor_insert_point(editor, curveX, lastPoint.y);
					insert = true;
					modified = true;
				}
			}

			if(!insert)
			{
				editor->focusElt = 0;
			}
		}
	}
	else if(mp_gui_is_mouse_button_released(gui, MP_MOUSE_LEFT))
	{
		modified = true;
		editor->dragging = false;
	}
	endFocus:

	if(mp_gui_is_key_pressed(gui, MP_KEY_C)
	  && (mp_gui_key_mods(gui) & MP_KEYMOD_CTRL)
	  && editor->focusElt)
	{
		qed_curve_editor_toggle_continuity(editor, editor->focusElt, editor->focusPointIndex);
		modified = true;
	}

	if(editor->dragging)
	{
		qed_curve_editor_elt* focusElt = editor->focusElt;
		qed_curve_point* p = &(focusElt->p[editor->focusPointIndex]);

		f32 oldX = p->x;
		f32 oldY = p->y;

		//TODO: haul that up (used in multiple places)?
		f32 mouseX = mp_gui_mouse_x(gui) - plotFrame.x;
		f32 mouseY = plotFrame.y + plotFrame.h - mp_gui_mouse_y(gui);
		f32 curveX = 0;
		f32 curveY = 0;
		qed_plot_to_curve_coords(editor, plotFrame, mouseX, mouseY, &curveX, &curveY);

		p->x = curveX;
		p->y = curveY;
		p->x = Clamp(p->x, editor->cameraX, editor->cameraX+editor->cameraW);
		p->y = Clamp(p->y, 0, editor->cameraH);

		//NOTE(martin): enforce first point x = 0
		//TODO: useless? should be enforced by previous clamps?
		if(  editor->focusElt == ListFirstEntry(&editor->elements, qed_curve_editor_elt, listElt)
		  && editor->focusPointIndex == 0)
		{
			p->x = 0;
		}

		f32 deltaX = p->x - oldX;
		f32 deltaY = p->y - oldY;

		//NOTE(martin): enforce x continuity at bounds
		f32 epsilon = 0.01;

		if(editor->focusPointIndex == 0)
		{
			qed_curve_editor_elt* prevElt = ListPrevEntry(&editor->elements, editor->focusElt, qed_curve_editor_elt, listElt);
			if(prevElt)
			{
				p->x = Clamp(p->x, prevElt->p[0].x + 2*epsilon, editor->focusElt->p[3].x - 2*epsilon);
				deltaX = p->x - oldX;

				prevElt->p[2].x += deltaX;
				prevElt->p[3].x = p->x;
				prevElt->p[1].x = Clamp(prevElt->p[1].x, prevElt->p[0].x + epsilon, prevElt->p[3].x - epsilon);
				prevElt->p[2].x = Clamp(prevElt->p[2].x, prevElt->p[0].x + epsilon, prevElt->p[3].x - epsilon);

				if(focusElt->startContinuity)
				{
					prevElt->p[3].y	= p->y;
					prevElt->p[2].y += deltaY;
					prevElt->p[2].y = ClampLowBound(prevElt->p[2].y, 0);
				}
			}
			focusElt->p[1].x += deltaX;
			focusElt->p[1].y += deltaY;
		}
		else if(editor->focusPointIndex == 3)
		{
			qed_curve_editor_elt* nextElt = ListNextEntry(&editor->elements, editor->focusElt, qed_curve_editor_elt, listElt);
			if(nextElt)
			{
				p->x = Clamp(p->x, editor->focusElt->p[0].x + 2*epsilon, nextElt->p[3].x - 2*epsilon);
				deltaX = p->x - oldX;

				nextElt->p[1].x += deltaX;
				nextElt->p[0].x = p->x;
				nextElt->p[1].x = Clamp(nextElt->p[1].x, nextElt->p[0].x + epsilon, nextElt->p[3].x - epsilon);
				nextElt->p[2].x = Clamp(nextElt->p[2].x, nextElt->p[0].x + epsilon, nextElt->p[3].x - epsilon);

				if(focusElt->endContinuity)
				{
					nextElt->p[0].y	= p->y;
					nextElt->p[1].y += deltaY;
					nextElt->p[1].y = ClampLowBound(nextElt->p[1].y, 0);
				}
			}
			else
			{
				//NOTE(martin): constrain last point of last element
				p->x = ClampLowBound(p->x, editor->focusElt->p[0].x);
				deltaX = p->x - oldX;
			}
			focusElt->p[2].x += deltaX;
			focusElt->p[2].y += deltaY;
		}
		//NOTE: enforce monotonicity of x (note this we use a unnecessarily restrictive condition)
		focusElt->p[1].x = Clamp(focusElt->p[1].x, focusElt->p[0].x + epsilon, focusElt->p[3].x - epsilon);
		focusElt->p[2].x = Clamp(focusElt->p[2].x, focusElt->p[0].x + epsilon, focusElt->p[3].x - epsilon);

		//TODO: we don't actually want to constrain this?
		/*
		focusElt->p[1].y = Clamp(focusElt->p[1].y, 0, 1);
		focusElt->p[2].y = Clamp(focusElt->p[2].y, 0, 1);
		*/


		/*
		//TODO: enforce derivability option
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//TODO: problem : x / y both mapped to (0,1) so distance computations are dependant on aspect ratio...
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if(focusElt->startDerivability)
		{
			qed_curve_editor_elt* prevElt = ListPrevEntry(&editor->elements, editor->focusElt, qed_curve_editor_elt, listElt);
			if(prevElt)
			{
				f32 normFocus = sqrt(Square(focusElt->p[1].x - focusElt->p[0].x) + Square(focusElt->p[1].y - focusElt->p[0].y));
				f32 normPrev = sqrt(Square(prevElt->p[2].x - prevElt->p[3].x) + Square(prevElt->p[2].y - prevElt->p[3].y));
				f32 dirX = (focusElt->p[1].x - focusElt->p[0].x)/normFocus;
				f32 dirY = (focusElt->p[1].y - focusElt->p[0].y)/normFocus;

				prevElt->p[2].x = prevElt->p[3].x - dirX * normPrev;
				prevElt->p[2].y = prevElt->p[3].y - dirY * normPrev;
			}
		}
		if(focusElt->endDerivability)
		{
			qed_curve_editor_elt* nextElt = ListNextEntry(&editor->elements, editor->focusElt, qed_curve_editor_elt, listElt);
			if(nextElt)
			{
				f32 normFocus = sqrt(Square(focusElt->p[2].x - focusElt->p[3].x) + Square(focusElt->p[2].y - focusElt->p[3].y));
				f32 normNext = sqrt(Square(nextElt->p[1].x - nextElt->p[0].x) + Square(nextElt->p[1].y - nextElt->p[0].y));
				f32 dirX = (focusElt->p[2].x - focusElt->p[3].x)/normFocus;
				f32 dirY = (focusElt->p[2].y - focusElt->p[3].y)/normFocus;

				nextElt->p[1].x = nextElt->p[0].x - dirX * normNext;
				nextElt->p[1].y = nextElt->p[0].y - dirY * normNext;
			}
		}
		//TODO: correctly clamp norm so that control points are within their elements' bounds
		*/
	}

	//TODO(martin): haul this up, and replace other uses of mouseX/Y
	bool mouseOverPlot = mp_gui_is_mouse_over(gui, plotFrame);
	f32 mouseX = mp_gui_mouse_x(gui);
	f32 mouseY = mp_gui_mouse_y(gui);

	if(mouseOverPlot)
	{
		f32 curveMouseX = 0, curveMouseY = 0;
		qed_pixel_to_curve_coords(editor, plotFrame, mouseX, mouseY, &curveMouseX, &curveMouseY);

		mem_arena* scratch = mem_scratch();
		mp_string xCoordString = mp_string_pushf(scratch, "%.3f,", curveMouseX);
		mp_string yCoordString = mp_string_pushf(scratch, "%.3f (%.0f bpm)", curveMouseY, curveMouseY*60);

		vec2 coordText = { plotFrame.x, plotFrame.y + plotFrame.h + 10};
		coordText = q_print_string(gui, xCoordString, coordText, (mp_graphics_color){1, 1, 1, 1});
		coordText.x += 20;
		q_print_string(gui, yCoordString, coordText, (mp_graphics_color){1, 1, 1, 1});
	}

	//NOTE(martin): draw grid
	{
		u64 minBarValue = (u64)ceil(editor->cameraW / 10);
		f32 barValue = (f32)next_pow2_u64(minBarValue);
		f32 barX = (floor(editor->cameraX / barValue) + 1) * barValue;

		mp_graphics_color barColor = {0.4, 0.4, 0.4, 1};

		mp_graphics_set_color(graphics, barColor);
		mp_graphics_set_width(graphics, 2);

		while(barX < editor->cameraX + editor->cameraW)
		{
			f32 screenBarX = plotFrame.x + (barX - editor->cameraX ) * plotFrame.w/editor->cameraW;

			mp_graphics_move_to(graphics, screenBarX, plotFrame.y);
			mp_graphics_line_to(graphics, screenBarX, plotFrame.y + plotFrame.h);
			mp_graphics_stroke(graphics);

			mp_string barString = mp_string_pushf(mem_scratch(), "%.0f", barX);
			vec2 barText = {screenBarX - fontWidth/2, plotFrame.y - lineHeight};
			q_print_string(gui, barString, barText, barColor);

			barX += barValue;
		}
	}

	mp_graphics_clip_push(graphics, plotFrame.x, plotFrame.y, plotFrame.w, plotFrame.h);

	//NOTE(martin): draw crosshairs
	if(mouseOverPlot)
	{
		mp_graphics_set_color_rgba(graphics, 0, 0, 0.9, 1);
		mp_graphics_set_width(graphics, 2);

		mp_graphics_move_to(graphics, mouseX, plotFrame.y);
		mp_graphics_line_to(graphics, mouseX, plotFrame.y + plotFrame.h);
		mp_graphics_stroke(graphics);

		mp_graphics_move_to(graphics, plotFrame.x, mouseY);
		mp_graphics_line_to(graphics, plotFrame.x + plotFrame.w, mouseY);
		mp_graphics_stroke(graphics);
	}

	const f32 strokeWidth = 4;
	f32 controlPointsRadius = mouseOverPlot ? 10 : strokeWidth/2;

	f32 eltStart = 0;
	for_each_in_list(&editor->elements, elt, qed_curve_editor_elt, listElt)
	{
		f32 p0x = 0, p0y = 0, p1x = 0, p1y = 0, p2x = 0, p2y = 0, p3x = 0, p3y = 0;

		if(  elt->p[3].x < editor->cameraX
		  || elt->p[0].x > (editor->cameraX + editor->cameraW))
		{
			continue;
		}

		switch(elt->type)
		{
			case SCHED_CURVE_CONST:
			case SCHED_CURVE_LINEAR:
			{
				qed_curve_to_pixel_coords(editor, plotFrame, elt->p[0].x, elt->p[0].y, &p0x, &p0y);
				qed_curve_to_pixel_coords(editor, plotFrame, elt->p[3].x, elt->p[3].y, &p3x, &p3y);

				mp_graphics_move_to(graphics, p0x, p0y);
				mp_graphics_line_to(graphics, p3x, p3y);

			} break;

			case SCHED_CURVE_BEZIER:
			{
				qed_curve_to_pixel_coords(editor, plotFrame, elt->p[0].x, elt->p[0].y, &p0x, &p0y);
				qed_curve_to_pixel_coords(editor, plotFrame, elt->p[1].x, elt->p[1].y, &p1x, &p1y);
				qed_curve_to_pixel_coords(editor, plotFrame, elt->p[2].x, elt->p[2].y, &p2x, &p2y);
				qed_curve_to_pixel_coords(editor, plotFrame, elt->p[3].x, elt->p[3].y, &p3x, &p3y);

				qed_curve_editor_elt* prevElt = ListPrevEntry(&editor->elements, elt, qed_curve_editor_elt, listElt);
				qed_curve_editor_elt* nextElt = ListNextEntry(&editor->elements, elt, qed_curve_editor_elt, listElt);

				if(  mouseOverPlot
				  && (elt == editor->focusElt
				    || ((editor->focusPointIndex == 3 || editor->focusPointIndex == 2)
				        && prevElt && prevElt == editor->focusElt)
				    || ((editor->focusPointIndex == 0 || editor->focusPointIndex == 1)
				        && nextElt && nextElt == editor->focusElt)))
				{
					mp_graphics_set_color_rgba(graphics, 0.25, 0, 1, 1);
					mp_graphics_set_width(graphics, 2);

					mp_graphics_move_to(graphics, p0x, p0y);
					mp_graphics_line_to(graphics, p1x, p1y);
					mp_graphics_stroke(graphics);

					mp_graphics_move_to(graphics, p2x, p2y);
					mp_graphics_line_to(graphics, p3x, p3y);
					mp_graphics_stroke(graphics);

					mp_graphics_circle_fill(graphics, p1x, p1y, controlPointsRadius);
					mp_graphics_circle_fill(graphics, p2x, p2y, controlPointsRadius);
				}

				mp_graphics_move_to(graphics, p0x, p0y);
				mp_graphics_cubic_to(graphics, p1x, p1y, p2x, p2y, p3x, p3y);

			} break;
		}

		mp_graphics_set_color_rgba(graphics, 1, 0.5, 0, 1);
		mp_graphics_set_width(graphics, strokeWidth);
		mp_graphics_stroke(graphics);

		//NOTE(martin): we always draw endpoints. Instead of hiding them when cursor is outside plot, we
		//              draw them with a radius equal to stroke width, to ensure rounded joints between
		//              curve elements.
		if(elt->startContinuity)
		{
			mp_graphics_set_color_rgba(graphics, 1, 0.5, 0, 1);
		}
		else
		{
			mp_graphics_set_color_rgba(graphics, 1, 0, 1, 1);
		}
		mp_graphics_circle_fill(graphics, p0x, p0y, controlPointsRadius);

		if(elt->endContinuity)
		{
			mp_graphics_set_color_rgba(graphics, 1, 0.5, 0, 1);
		}
		else
		{
			mp_graphics_set_color_rgba(graphics, 1, 0, 1, 1);
		}
		mp_graphics_circle_fill(graphics, p3x, p3y, controlPointsRadius);
	}

	f32 lastPx = 0, lastPy = 0;
	qed_curve_editor_elt* lastElt = ListLastEntry(&editor->elements, qed_curve_editor_elt, listElt);
	qed_curve_to_pixel_coords(editor, plotFrame, lastElt->p[3].x, lastElt->p[3].y, &lastPx, &lastPy);

	mp_graphics_move_to(graphics, lastPx, lastPy);
	mp_graphics_line_to(graphics, plotFrame.x + plotFrame.w, lastPy);
	mp_graphics_set_color_rgba(graphics, 1, 0.5, 0, 1);
	mp_graphics_set_width(graphics, strokeWidth);
	mp_graphics_stroke(graphics);

	if(  mouseOverPlot
	  && editor->focusElt
	  && (editor->focusPointIndex == 0 || editor->focusPointIndex == 3))
	{
		qed_curve_point p = editor->focusElt->p[editor->focusPointIndex];
		f32 px = 0, py = 0;
		qed_curve_to_pixel_coords(editor, plotFrame, p.x, p.y, &px, &py);

		mp_graphics_set_color_rgba(graphics, 1, 0, 0, 1);
		mp_graphics_circle_fill(graphics, px, py, controlPointsRadius);
	}

	mp_graphics_clip_pop(graphics);

	return(modified);
}


mp_string qed_tempo_editor_alloc(cell_tree* tree)
{
	qed_curve_editor* editor = malloc_type(qed_curve_editor);
	memset(editor, 0, sizeof(qed_curve_editor));

	editor->axes = SCHED_CURVE_POS_TEMPO;
	editor->eltCount = 1;

	editor->cameraX = 0;
	editor->cameraW = QED_DEFAULT_CURVE_EDITOR_CAMERA_WIDTH;
	editor->cameraH = QED_DEFAULT_CURVE_EDITOR_CAMERA_HEIGHT;

	ListInit(&editor->elements);

	qed_curve_editor_elt* elt = malloc_type(qed_curve_editor_elt);

	elt->type = SCHED_CURVE_BEZIER;
	elt->startContinuity = true;
	elt->endContinuity = true;
	elt->startDerivability = true;
	elt->endDerivability = true;

	elt->p[0].x = 0;
	elt->p[0].y = 1;
	elt->p[1].x = 2;
	elt->p[1].y = 1;
	elt->p[2].x = 8;
	elt->p[2].y = 1;
	elt->p[3].x = 10;
	elt->p[3].y = 1;

	ListAppend(&editor->elements, &elt->listElt);

	mp_string result = mp_string_from_buffer(sizeof(qed_curve_editor), (char*)editor);
	return(result);
}

void qed_tempo_editor_recycle(cell_tree* tree, q_cell* cell)
{
	qed_curve_editor* editor = (qed_curve_editor*)(cell->ui.data.ptr);
	cell->ui.data = (mp_string){0};

	qed_curve_editor_elt* elt = 0;
	while((elt = ListPopEntry(&editor->elements, qed_curve_editor_elt, listElt)) != 0)
	{
		free(elt);
	}
	free(editor);
}
