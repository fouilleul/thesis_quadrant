/************************************************************//**
*
*	@file: tempo_editor.h
*	@author: Martin Fouilleul
*	@date: 26/07/2022
*	@revision:
*
*****************************************************************/
#ifndef __TEMPO_EDITOR_H_
#define __TEMPO_EDITOR_H_

#include"cells.h"

mp_string qed_tempo_editor_alloc(cell_tree* tree);
void qed_tempo_editor_recycle(cell_tree* tree, q_cell* cell);
bool qed_curve_editor_gui(mp_gui_context* gui, q_cell* cell);

#endif //__TEMPO_EDITOR_H_
